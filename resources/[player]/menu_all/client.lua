local Keys = {
  ["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57, 
  ["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177, 
  ["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
  ["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
  ["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
  ["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70, 
  ["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
  ["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
  ["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

ESX = nil
local gui_menu = nil

Citizen.CreateThread(function()
    while ESX == nil do
        TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
        Citizen.Wait(0)
    end
end)

local function OpenFunctionMenu(value)

    if value == "bag" then
        TriggerEvent('esx_inventoryhud:start')
        ESX.UI.Menu.CloseAll()

    elseif value == "barbie_lyftupp" then
        TriggerEvent('esx_barbie_lyftupp:start')

    elseif value == "piggy_back" then
        TriggerEvent('piggyback:start')

    elseif value == "animation" then
        TriggerEvent('esx_animations:start')

    elseif value == "team" then
        TriggerEvent('team:start')
         ESX.UI.Menu.CloseAll()

    elseif value == "accessories" then
        TriggerEvent('esx_accessories:start')

    elseif value == "pet" then
        TriggerEvent('eden_animal:start')

    end

end

local function OpenAllMenu()

    local elements = {}

    table.insert( elements, {label = "เปิดกระเป๋า", value = "bag"} )
    table.insert( elements, {label = "ขอแลกเปลี่ยน", value = ""} )
    table.insert( elements, {label = "ขอเปิดท้ายยานพาหนะ", value = ""} )
    table.insert( elements, {label = "ขออุ้ม", value = "barbie_lyftupp"} )
    table.insert( elements, {label = "ขอให้ขี่หลัง", value = "piggy_back"} )
    table.insert( elements, {label = "ท่าทาง", value = "animation"} )
    table.insert( elements, {label = "ทีม", value = "team"} )
    table.insert( elements, {label = "เรียกรถ", value = ""} )
    table.insert( elements, {label = "อุปกรณ์สวมใส่", value = "accessories"} )
    table.insert( elements, {label = "สัตว์เลี้ยง", value = "pet"} )

    ESX.UI.Menu.Open(
        'default', GetCurrentResourceName(), 'menu_all',
        {
            title    = 'รวมเมนู',
            elements = elements
        },
        function(data, menu)
            OpenFunctionMenu(data.current.value)
        end,
        function(data, menu)
            menu.close()
        end
    )

end


-- Key Controls
Citizen.CreateThread(function()
  while true do
  Citizen.Wait(0)

      if IsControlPressed(0, Keys['F3']) then

         OpenAllMenu()

      end
  end
end)

-- template event
-- RegisterNetEvent('chat:start')
-- AddEventHandler('chat:start', function()
--   OpenAllMenu()
-- end)

function applyskin (skin)
    local model = GetHashKey(skin)
    RequestModel(model)
    while not HasModelLoaded(model) do
        RequestModel(model)
        Citizen.Wait(0)
    end
    SetPlayerModel(PlayerId(), model)
    --SetPedComponentVariation(GetPlayerPed(-1), 0, 0, 0, 2) -- fix
    SetPedRandomComponentVariation(GetPlayerPed(-1), true) -- random

    SetModelAsNoLongerNeeded(model)
    TriggerEvent('esx:restoreLoadout')
end

RegisterCommand("skin0",function(source, args)
  ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin, jobSkin)
    TriggerEvent('skinchanger:loadSkin', skin)
  end)
end, false)

RegisterCommand("skin1",function(source, args)
  applyskin("Lana")
end, false)
RegisterCommand("skin2",function(source, args)
  applyskin("Sam")
end, false)
RegisterCommand("skin3",function(source, args)
  applyskin("Deadpool4K")
end, false)
RegisterCommand("skin4",function(source, args)
  applyskin("LanaCasual")
end, false)

RegisterCommand("skin5",function(source, args)
  applyskin("lanaBikini3")
end, false)

RegisterCommand("skin6",function(source, args)
  applyskin("LanaCasual")
end, false)

RegisterCommand("natacha",function(source, args)
    applyskin("CindyB")
end, false)


local ary_skin = {
  "CaptainAmerica_civilwar",
  "CindyB",
  "Deadpool4K",
  "deathstroke",
  "DrStrange_MCU",
  "fighter",
  "Goku",
  "HarleyB",
  "HatsuneMiku1",
  "IMPatriot",
  "kokoro",
  "Lana",
  "MaiT",
  "minibabygroot",
  "MK46",
  "MK47",
  "PainDevaPath",
  "paladin",
  "playerpubg",
  "Sam",
  "shrek",
  "SSGKidGoku"
}

RegisterCommand("rand_skin",function(source, args)
  local num = math.random(1,22)
  applyskin(ary_skin[num])
end, false)
