local showPlayerBlips = false
local ignorePlayerNameDistance = false
local disPlayerNames = 15
local displayIDHeight = 1.5 --Height of ID above players head(starts at center body mass)

Citizen.CreateThread(function()
    RegisterFontFile('sarabun') -- the name of your .gfx, without .gfx
end)

function DrawText3D(x,y,z, text, r, g, b, alpha) -- some useful function, use it if you want!

    local fontId = RegisterFontId('sarabun')

    local onScreen,_x,_y=World3dToScreen2d(x,y,z)
    local px,py,pz=table.unpack(GetGameplayCamCoords())
    local dist = GetDistanceBetweenCoords(px,py,pz, x,y,z, 1)

    local scale = (1/dist)*2
    local fov = (1/GetGameplayCamFov())*100
    local scale = scale*fov
    
    if onScreen then
        SetTextScale(0.0*scale, 0.36*scale)
        SetTextFont(fontId)
        SetTextProportional(1)
        SetTextColour(r, g, b, alpha)
        SetTextDropshadow(0, 0, 0, 0, 255)
        SetTextEdge(2, 0, 0, 0, 150)
        SetTextDropShadow()
        SetTextOutline()
        SetTextEntry("STRING")
        SetTextCentre(1)
        AddTextComponentString(text)
        DrawText(_x,_y)
    end
end


Citizen.CreateThread(function()

    while true do
        -- TriggerClientEvent("passive:check", function(val)
        --     print("Callback Results: " .. val)
        -- end)

        for i=0,128 do
            N_0x31698aa80e0223f8(i)
        end
        for id = 0, 128 do

            if  ((NetworkIsPlayerActive( id )) ) then
            --if  ((NetworkIsPlayerActive( id )) and GetPlayerPed( id ) ~= GetPlayerPed( -1 )) then

                ped = GetPlayerPed( id )
                blip = GetBlipFromEntity( ped ) 
 
                x1, y1, z1 = table.unpack( GetEntityCoords( GetPlayerPed( -1 ), true ) )
                x2, y2, z2 = table.unpack( GetEntityCoords( GetPlayerPed( id ), true ) )
                distance = math.floor(GetDistanceBetweenCoords(x1,  y1,  z1,  x2,  y2,  z2,  true))
				
                local takeaway = 0.95

     --            if(ignorePlayerNameDistance) then

     --                DrawText3D(x2, y2, z2+1,  string.sub( GetPlayerName(id), 1, 15) .. " [ " .. GetPlayerServerId(id) .. " ] ", 255, 255, 255, 160 )

					-- if NetworkIsPlayerTalking(id) then
					-- 	DrawMarker(25,x2,y2,z2 - takeaway , 0, 0, 0, 0, 0, 0, 1.0, 1.0, 10.3, 0, 160, 205, 80, 0, 0, 2, 0, 0, 0, 0)
					-- else
					-- 	--DrawMarker(25, x2,y2,z2 - takeaway, 0, 0, 0, 0, 0, 0, 1.0, 1.0, 10.3, 0, 160, 0, 0, 95, 0, 2, 0, 0, 0, 0)
					-- 	DrawMarker(25, x2,y2,z2 - takeaway, 0, 0, 0, 0, 0, 0, 1.0, 1.0, 0.5, 192, 192, 192, 20, 0, 0, 2, 0, 0, 0, 0)
					-- end
     --            end


                if ((distance < disPlayerNames)) then
                    if not (ignorePlayerNameDistance) then

                        DrawMarker(0, x2, y2, z2+1.65, 0, 0, 0, 0, 0, 0, 0.3, 0.2, 0.35, 0, 102, 255, 50, true, true, 0, 0)
                        DrawText3D(x2, y2, z2+1.35,  "ประกาศจับ", 255, 0, 0, 255 )
                        DrawText3D(x2, y2, z2+1.23, "ชื่อแก๊งค์", 255, 255, 255, 160 )
                        DrawText3D(x2, y2, z2+1.12, string.sub( GetPlayerName(id), 1, 15) .. " [ " .. GetPlayerServerId(id) .. " ] ", 255, 255, 255, 160 )
                        DrawText3D(x2, y2, z2+1.015,  "Passive Mode", 0, 204, 0, 150 )

						if NetworkIsPlayerTalking(id) then
                            DrawMarker(25,x2,y2,z2 - takeaway, 0, 0, 0, 0, 0, 0, 1.0, 1.0, 10.3, 0, 160, 205, 80, 0, 0, 2, 0, 0, 0, 0)
                        else
                            DrawMarker(25, x2,y2,z2 - takeaway, 0, 0, 0, 0, 0, 0, 1.0, 1.0, 0.5, 192, 192, 192, 20, 0, 0, 2, 0, 0, 0, 0)
						end
                    end
                end  
            end
        end
        Citizen.Wait(0)
    end
end)
