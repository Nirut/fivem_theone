Config = {}


Config.PedAbleToWalkWhileSwapping = true
Config.UnarmedHash = -1569615261

local st_anim = false

Citizen.CreateThread(function()

	local animDict = 'reaction@intimidation@1h'

	local animIntroName = 'intro'
	local animOutroName = 'outro'

	local animFlag = 0

	RequestAnimDict(animDict)
	  
	while not HasAnimDictLoaded(animDict) do
		Citizen.Wait(100)
	end

	local lastWeapon = nil

	while true do
		Citizen.Wait(0)

		if not IsPedInAnyVehicle(GetPlayerPed(-1), true) then

			if Config.PedAbleToWalkWhileSwapping then
				animFlag = 48
			else
				animFlag = 0
			end

			-- fix swimming no weapon
			if IsPedSwimming(GetPlayerPed(-1)) then
				SetCurrentPedWeapon(GetPlayerPed(-1), Config.UnarmedHash, true)
				lastWeapon = nil
			end

			if GetSelectedPedWeapon(GetPlayerPed(-1) ) ~= Config.UnarmedHash and lastWeapon ~= GetSelectedPedWeapon(GetPlayerPed(-1) ) and st_anim == false then
				local weapon = GetSelectedPedWeapon(GetPlayerPed(-1) )

				st_anim = true
				SetCurrentPedWeapon(GetPlayerPed(-1), Config.UnarmedHash, true)
				TaskPlayAnim(GetPlayerPed(-1), animDict, animIntroName, 8.0, -8.0, 2100, animFlag, 0, false, false, false)

				Citizen.Wait(1000)
				SetCurrentPedWeapon(GetPlayerPed(-1), weapon, true)
				lastWeapon = GetSelectedPedWeapon(GetPlayerPed(-1))
				st_anim = false
			end
		end
	end
end)