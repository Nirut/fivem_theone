ESX = nil
local mode_passive = false

Citizen.CreateThread(function()
    while ESX == nil do
        TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
        Citizen.Wait(0)
    end
    Wait(1000)
    PassiveMode()
end)

 function PassiveMode()

     ESX.UI.Menu.Open(
        'default', GetCurrentResourceName(), 'mode',
        {
          title = 'โหมดการเล่น',
          align = 'center',
          elements = {
            {label = 'โหมดปกติ', value = 'normal'},
            {label = 'Passive Mode (โหมดปลอดภัย)',  value = 'passive'},
          },
        },
        function (data, menu)
          if data.current.value == 'normal' then   
             mode_passive = false
             menu.close()            
          elseif data.current.value == 'passive' then 
             mode_passive = true

              
             menu.close()            
          end

        end,
        function (data, menu)
          menu.close()
        end
      )
end

-- CLIENTSIDED --

Citizen.CreateThread(function()
  while true do
    Citizen.Wait(0)
    if mode_passive then
      SetCurrentPedWeapon(GetPlayerPed(-1),GetHashKey("WEAPON_UNARMED"),true)
      SetEntityAlpha(GetPlayerPed(-1), 190 )
      NetworkSetFriendlyFireOption(false)
      SetCanAttackFriendly(PlayerPedId(), false, false)   
    else
      SetEntityAlpha(GetPlayerPed(-1), 255 ) 
      NetworkSetFriendlyFireOption(true)
      SetCanAttackFriendly(GetPlayerPed(-1), true, false)
    end
  end
end)


 --Godmode die only player kill
Citizen.CreateThread(function()
 
    while true do
        Citizen.Wait(1)

        local playerPed = GetPlayerPed(-1)

        SetEntityInvincible(playerPed, true)
        SetPlayerInvincible(PlayerId(), true)
        SetPedCanRagdoll(playerPed, true)
        SetPedCanRagdollFromPlayerImpact(playerPed, true)
        SetPedCanBeKnockedOffVehicle(playerPed, false)
        SetEntityCanBeDamaged(playerPed, true)
        SetEntityOnlyDamagedByPlayer(playerPed, true)
        ResetPedVisibleDamage(playerPed)
        SetEntityProofs(playerPed, false, true, true, true, false, true, 1, false)
        -- SetEntityProofs(playerPed, true (bulletProof), true (fireProof), true (explosionProof), true (collisionProof), true (meleeProof), true, 1, true (drownProof))

        --SetPedCanBeDraggedOut(playerPed, true)
        --SetPedRagdollOnCollision(playerPed, true)
        --SetPedConfigFlag(playerPed, 32, false)
        --ClearPedBloodDamage(playerPed)
        --ClearPedLastWeaponDamage(playerPed)
        --SetEntityAlpha(GetPlayerPed(-1), 210, nil) -- passive mode ตัวใส
    end

end)

RegisterNetEvent('passive:check')
AddEventHandler('passive:check', function(cb)
  cb(999)
end)


