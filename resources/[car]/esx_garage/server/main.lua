ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

RegisterServerEvent('32a598a6-3db4-4d2a-9fb1-b1bc346efb30')
AddEventHandler('32a598a6-3db4-4d2a-9fb1-b1bc346efb30', function(garage, zone, vehicleProps)

	local xPlayer = ESX.GetPlayerFromId(source)

	if vehicleProps == false then

		MySQL.Async.execute(
			'DELETE FROM `user_parkings` WHERE `identifier` = @identifier AND `garage` = @garage AND zone = @zone',
			{
				['@identifier'] = xPlayer.identifier,
				['@garage']     = garage;
				['@zone']       = zone
			}, function(rowsChanged)
				TriggerClientEvent('8e8af900-8d56-4116-af4d-851ff5c3f798', xPlayer.source, _U('veh_released'))
			end
		)

	else

		MySQL.Async.execute(
			'INSERT INTO `user_parkings` (`identifier`, `garage`, `zone`, `vehicle`) VALUES (@identifier, @garage, @zone, @vehicle)',
			{
				['@identifier'] = xPlayer.identifier,
				['@garage']     = garage;
				['@zone']       = zone,
				['vehicle']     = json.encode(vehicleProps)
			}, function(rowsChanged)
				TriggerClientEvent('8e8af900-8d56-4116-af4d-851ff5c3f798', xPlayer.source, _U('veh_stored'))
			end
		)

	end

end)

RegisterServerEvent('234553dd-91e6-4dc3-968d-608539c6248c')
 AddEventHandler('234553dd-91e6-4dc3-968d-608539c6248c', function(vehicleProps)
 
 	local _source = source
 	local xPlayer = ESX.GetPlayerFromId(source)
 
 	MySQL.Async.fetchAll(
 		'SELECT * FROM owned_vehicles WHERE owner = @owner',
 		{
 			['@owner'] = xPlayer.identifier
 		},
 		function(result)
 
 			local foundVehicleId = nil
 
 			for i=1, #result, 1 do
 				
 				local vehicle = json.decode(result[i].vehicle)
 				
 				if vehicle.plate == vehicleProps.plate then
 					foundVehicleId = result[i].id
 					break
 				end
 
 			end
 
 			if foundVehicleId ~= nil then

 				MySQL.Async.execute(
 					'UPDATE owned_vehicles SET vehicle = @vehicle WHERE id = @id',
 					{
						['@vehicle'] = json.encode(vehicleProps),
						['@id']      = foundVehicleId
 					}
 				)
 
 			end
 
 		end
 	)
 
 end)

ESX.RegisterServerCallback('39b64c69-29b9-4d9e-80c5-f100695d13d4', function(source, cb, garage)

	local xPlayer = ESX.GetPlayerFromId(source)

	MySQL.Async.fetchAll(
		'SELECT * FROM `user_parkings` WHERE `identifier` = @identifier AND garage = @garage',
		{
			['@identifier'] = xPlayer.identifier,
			['@garage']     = garage
		},
		function(result)

			local vehicles = {}

			for i=1, #result, 1 do
				table.insert(vehicles, {
					zone    = result[i].zone,
					vehicle = json.decode(result[i].vehicle)
				})
			end

			cb(vehicles)

		end
	)

end)
