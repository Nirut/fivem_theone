ESX = nil

--- PARTICLES ---
p_flame_location = {
	"exhaust",
	"exhaust_2",
} -- vehicle bones for flames
p_flame_particle = "veh_backfire" -- particle name | default: "veh_backfire"
p_flame_particle_asset = "core" -- particle category | default: "core"
p_flame_size = 2.0 -- size of the flames | default: 2.0

--- EFFECTS ---
e_flames = true -- enable flames on nitro | default: true
e_sound = true -- enable sound on nitro | default: true

--- NITRO ---
n_boost = 15.0 -- boost on nitro | default: 15.0

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end
end)


Citizen.CreateThread(function()
	while true do
		Wait(50)
		if IsPedInAnyVehicle(GetPlayerPed(-1), 0) then
			if GetPedInVehicleSeat(GetVehiclePedIsIn(GetPlayerPed(-1), 0), -1) == GetPlayerPed(-1) then
					vehicle = GetVehiclePedIsIn(GetPlayerPed(-1), 0)
					local currentMods = ESX.Game.GetVehicleProperties(vehicle)
					if IsControlPressed(0, 36) and currentMods.modTurbo then
		        		
		       				SetVehicleEnginePowerMultiplier(vehicle, n_boost)
		       				SetVehicleEngineTorqueMultiplier(vehicle, n_boost)
		       				SetEntityMaxSpeed(vehicle, 999.0)
		       				if e_flames == true then     			
		        				TriggerServerEvent("eff_flames", VehToNet(vehicle))
		        			end
		        			if e_sound == true then
		        				TriggerServerEvent("eff_sound_start", VehToNet(vehicle))
		        			end
		
		        	end
		    end
		end
	end
end)


RegisterNetEvent("c_eff_flames")
AddEventHandler("c_eff_flames", function(c_veh)
	for _,bones in pairs(p_flame_location) do
		UseParticleFxAssetNextCall(p_flame_particle_asset)
		createdPart = StartParticleFxLoopedOnEntityBone(p_flame_particle, NetToVeh(c_veh), 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, GetEntityBoneIndexByName(NetToVeh(c_veh), bones), p_flame_size, 0.0, 0.0, 0.0)
		StopParticleFxLooped(createdPart, 1)
	end
end)

RegisterNetEvent("c_eff_sound_start")
AddEventHandler("c_eff_sound_start", function(c_veh)
	SetVehicleBoostActive(NetToVeh(c_veh), 1)
end)