Locales['en'] = {
  ['invoices'] = 'Faturalar',
  ['invoices_item'] = '%s TL',
  ['received_invoice'] = 'Size bir adet fatura kesildi.',
  ['paid_invoice'] = 'Faturanızı ödediniz : ~r~%s TL~s~',
  ['received_payment'] = 'Kestiğiniz bir fatura ödendi: ~r~%s TL~s~',
  ['player_not_online'] = 'Oyuncu sunucuda yok.',
  ['no_money'] = 'Bunu ödeyebilecek paran yok gibi duruyor.',
  ['target_no_money'] = 'Karşındaki insanın bu kadar parayı ödeyemeyecek gibi duruyor!',
}
