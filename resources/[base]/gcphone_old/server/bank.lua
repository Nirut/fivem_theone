--================================================================================================
--==                                VARIABLES - DO NOT EDIT                                     ==
--================================================================================================


TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)


RegisterServerEvent('gcPhone:transfer')
AddEventHandler('gcPhone:transfer', function(to, amountt)
    local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)
    local zPlayer = ESX.GetPlayerFromId(to)
    local balance = 0
    if zPlayer ~= nil then
        balance = xPlayer.getAccount('bank').money
        zbalance = zPlayer.getAccount('bank').money
        if tonumber(_source) == tonumber(to) then
            -- advanced notification with bank icon
            TriggerClientEvent('esx:showAdvancedNotification', _source, 'Bank',
                               'Transfer Money',
                               'You cannot transfer to your self!',
                               'CHAR_BANK_MAZE', 9)
        else
            if balance <= 0 or balance < tonumber(amountt) or tonumber(amountt) <=
                0 then
                -- advanced notification with bank icon
			TriggerClientEvent("pNotify:SendNotification", source,{
                    text = ("Transfer için yeterli paran yok!"),
                    type = "error",
					theme = "metroui",
                    timeout = 5000,
                    layout = "topRight",
					queue = "lmao"
                }
            )
            else
                xPlayer.removeAccountMoney('bank', tonumber(amountt))
                zPlayer.addAccountMoney('bank', tonumber(amountt))
                -- advanced notification with bank icon
					TriggerClientEvent("pNotify:SendNotification", _source,{
                    text = (" " .. to .. " kişisine <b style='color:#59f222'> $" .. amountt .. "</b> transfer yaptın."),
                    type = "alert",
					theme = "metroui",
                    timeout = 2500,
                    layout = "topRight",
					queue = "lmao"
                }
            )
				TriggerClientEvent("pNotify:SendNotification", to,{
                    text = (" " .. _source .. " hesabına <b style='color:#59f222'> $" .. amountt .. "</b> transfer yaptı."),
                    type = "alert",
					theme = "metroui",
                    timeout = 2500,
                    layout = "topRight",
					queue = "lmao"
                }
            )

            end

        end
    end

end)


function myfirstname(phone_number, firstname, cb)
  MySQL.Async.fetchAll("SELECT firstname, phone_number FROM users WHERE users.firstname = @firstname AND users.phone_number = @phone_number", {
    ['@phone_number'] = phone_number,
	['@firstname'] = firstname
  }, function (data)
    cb(data[1])
  end)
end