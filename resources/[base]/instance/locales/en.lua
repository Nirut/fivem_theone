Locales['en'] = {
  ['left_instance'] = 'you have left the property',
  ['invite_expired'] = 'invite expired',
  ['press_to_enter'] = 'press ~INPUT_CONTEXT~ to enter the property',
  ['entered_instance'] = 'you entered the property',
  ['entered_into'] = '%s entered the property',
  ['left_out'] = '%s left the property',
}