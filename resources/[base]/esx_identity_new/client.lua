--==================================================================================
--======               ESX_IDENTITY BY ARKSEYONET @Ark                        ======
--======    YOU CAN FIND ME ON MY DISCORD @Ark - https://discord.gg/cGHHxPX   ======
--======    IF YOU ALTER THIS VERSION OF THE SCRIPT, PLEASE GIVE ME CREDIT    ======
--======     Special Thanks To Alphakush and CMD.Telhada For Help Testing     ======
--==================================================================================

--===============================================
--==                 VARIABLES                 ==
--===============================================
local guiEnabled = false
local myIdentity = {}

--===============================================
--==                 VARIABLES                 ==
--===============================================
function EnableGui(enable)
    SetNuiFocus(enable)
    guiEnabled = enable

    SendNUIMessage({
        type = "enableui",
        enable = enable
    })
end

function openRegistry()
  TriggerEvent('esx_identity:showRegisterIdentity')
end

--===============================================
--==           Show Registration               ==
--===============================================
RegisterNetEvent("esx_identity:showRegisterIdentity")
AddEventHandler("esx_identity:showRegisterIdentity", function()
  EnableGui(true)
end)

--===============================================
--==              Close GUI                    ==
--===============================================
RegisterNUICallback('escape', function(data, cb)
    --EnableGui(false)
end)

--===============================================
--==           Register Callback               ==
--===============================================
RegisterNUICallback('register', function(data, cb)

  local reason = ""
  
  if data.firstname == "" or data.lastname == "" then
    reason = "firstname of lastname !!"
  elseif data.dateofbirth == "" then
    reason = "Invalid date of birth !!"
  elseif data.height then
    local height = tonumber(data.height)
    if height then
      if height > 200 or height < 140 then
        reason = "Unacceptable player height !!"
      end
    else
      reason = "Unacceptable player height !!"
    end
  elseif data.sex == "m" then
    reason = "gender !!"
  end

  if reason == "" then

    guiEnabled = true
    TriggerServerEvent('esx_identity:setIdentity', data)
  
     -- check select sex
     local skin = nil
    if data.sex == "m" then
      skin = json.decode('{"age_1":0,"chain_1":0,"glasses_1":0,"helmet_1":-1,"lipstick_3":0,"complexion_1":0,"chest_3":0,"hair_1":2,"arms_2":0,"lipstick_2":1,"hair_2":0,"decals_2":0,"bodyb_1":0,"bodyb_2":0,"age_2":0,"face":0,"chest_1":0,"ears_1":-1,"decals_1":0,"chain_2":0,"makeup_1":0,"beard_4":0,"complexion_2":0,"eyebrows_1":0,"tshirt_2":0,"shoes_2":0,"bproof_2":0,"hair_color_2":0,"eye_color":0,"torso_2":0,"makeup_3":0,"blemishes_2":0,"bags_2":0,"helmet_2":0,"beard_1":0,"glasses_2":0,"blush_2":0,"skin":0,"blush_3":0,"makeup_4":0,"ears_2":0,"shoes_1":27,"makeup_2":0,"eyebrows_3":0,"lipstick_4":0,"beard_3":0,"eyebrows_2":0,"pants_1":10,"tshirt_1":4,"moles_2":0,"torso_1":10,"watches_1":-1,"watches_2":0,"lipstick_1":0,"pants_2":0,"bracelets_1":-1,"sun_2":0,"chest_2":0,"mask_2":0,"mask_1":0,"moles_1":0,"bproof_1":0,"arms":1,"bags_1":0,"beard_2":0,"hair_color_1":0,"sex":0,"blemishes_1":0,"sun_1":0,"eyebrows_4":0,"bracelets_2":0,"blush_1":0}')
    elseif data.sex == "f" then
      skin = json.decode('{"age_1":0,"chain_1":0,"glasses_1":5,"helmet_1":-1,"lipstick_3":0,"complexion_1":0,"chest_3":0,"hair_1":4,"arms_2":0,"lipstick_2":0,"hair_2":0,"decals_2":0,"bodyb_1":0,"bodyb_2":0,"hair_color_1":55,"face":29,"chest_1":0,"ears_1":-1,"decals_1":0,"chain_2":0,"makeup_1":0,"beard_4":0,"complexion_2":0,"eyebrows_1":0,"tshirt_2":0,"shoes_2":0,"bproof_2":0,"hair_color_2":0,"eye_color":0,"torso_2":0,"makeup_3":0,"blemishes_2":0,"bags_2":0,"helmet_2":0,"mask_2":0,"glasses_2":0,"blush_2":0,"skin":0,"blush_3":0,"makeup_4":0,"ears_2":0,"shoes_1":27,"makeup_2":0,"eyebrows_3":0,"lipstick_4":0,"beard_3":0,"eyebrows_2":0,"pants_1":8,"tshirt_1":41,"moles_2":0,"sun_2":0,"watches_1":0,"bracelets_1":0,"lipstick_1":0,"bproof_1":0,"watches_2":0,"chest_2":0,"beard_1":0,"mask_1":0,"blush_1":0,"moles_1":0,"beard_2":0,"arms":1,"bags_1":0,"bracelets_2":0,"pants_2":0,"age_2":0,"torso_1":7,"sun_1":0,"eyebrows_4":0,"sex":1,"blemishes_1":0}')
    end

    TriggerEvent('skinchanger:loadSkin', skin )
    TriggerServerEvent('esx_skin:save', skin)

    EnableGui(false)
    Citizen.Wait(500)

    TriggerEvent('chat:start') -- enable chat

  else
    --ESX.ShowNotification(reason)
  end
end)



--===============================================
--==                 THREADING                 ==
--===============================================
Citizen.CreateThread(function()
    while true do
        if guiEnabled then
          DisableControlAction(0, 1,   true) -- LookLeftRight
          DisableControlAction(0, 2,   true) -- LookUpDown
          DisableControlAction(0, 106, true) -- VehicleMouseControlOverride
          DisableControlAction(0, 142, true) -- MeleeAttackAlternate
          DisableControlAction(0, 30,  true) -- MoveLeftRight
          DisableControlAction(0, 31,  true) -- MoveUpDown
          DisableControlAction(0, 18, guiEnabled) -- Enter
          DisableControlAction(0, 21,  true) -- disable sprint
          DisableControlAction(0, 24,  true) -- disable attack
          DisableControlAction(0, 25,  true) -- disable aim
          DisableControlAction(0, 47,  true) -- disable weapon
          DisableControlAction(0, 58,  true) -- disable weapon
          DisableControlAction(0, 263, true) -- disable melee
          DisableControlAction(0, 264, true) -- disable melee
          DisableControlAction(0, 257, true) -- disable melee
          DisableControlAction(0, 140, true) -- disable melee
          DisableControlAction(0, 141, true) -- disable melee
          DisableControlAction(0, 142, guiEnabled) -- MeleeAttackAlternate
          DisableControlAction(0, 143, true) -- disable melee
          DisableControlAction(0, 75,  true) -- disable exit vehicle
          DisableControlAction(27, 75, true) -- disable exit vehicle
          DisableControlAction(0, 322, guiEnabled) -- ESC
          DisableControlAction(0, 106, guiEnabled) -- VehicleMouseControlOverride
        end
        Citizen.Wait(0)
    end
end)
