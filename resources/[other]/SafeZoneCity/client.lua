--------------------------------------------------------------------------------------------------------------
------------First off, many thanks to @anders for help with the majority of this script. ---------------------
------------Also shout out to @setro for helping understand pNotify better.              ---------------------
--------------------------------------------------------------------------------------------------------------
------------To configure: Add/replace your own coords in the sectiong directly below.    ---------------------
------------        Goto LINE 90 and change "50" to your desired SafeZone Radius.        ---------------------
------------        Goto LINE 130 to edit the Marker( Holographic circle.)               ---------------------
--------------------------------------------------------------------------------------------------------------
-- Place your own coords here!
local zones = {
	{ ['x'] = -265.01, ['y'] = -962.14, ['z'] = 29.0 }, -- job
	--{ ['x'] = -1688.43811035156, ['y'] = -1073.62536621094, ['z'] = 13.1521873474121 },
	--{ ['x'] = -2195.1352539063, ['y'] = 4288.7290039063, ['z'] = 49.173923492432 }
}

local notifIn = false
local notifOut = false
local closestZone = 1
local area = 1200.0

--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------
-------                              Creating Blips at the locations. 							--------------
-------You can comment out this section if you dont want any blips showing the zones on the map.--------------
--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------

Citizen.CreateThread(function()
	while not NetworkIsPlayerActive(PlayerId()) do
		Citizen.Wait(0)
	end
	
	-- for i = 1, #zones, 1 do
	-- 	local szBlip = AddBlipForCoord(zones[i].x, zones[i].y, zones[i].z)
	-- 	SetBlipAsShortRange(szBlip, true)
	-- 	SetBlipColour(szBlip, 2)  --Change the blip color: https://gtaforums.com/topic/864881-all-blip-color-ids-pictured/
	-- 	SetBlipSprite(szBlip, 398) -- Change the blip itself: https://marekkraus.sk/gtav/blips/list.html
	-- 	BeginTextCommandSetBlipName("STRING")
	-- 	AddTextComponentString("SAFE ZONE") -- What it will say when you hover over the blip on your map.
	-- 	EndTextCommandSetBlipName(szBlip)
	-- end

	local blip = AddBlipForRadius(zones[closestZone].x, zones[closestZone].y, zones[closestZone].z , area) -- you can use a higher number for a bigger zone
	SetBlipHighDetail(blip, true)
	SetBlipColour(blip, 43)
	SetBlipAlpha (blip, 30)


end)

--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------
----------------   Getting your distance from any one of the locations  --------------------------------------
--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------

Citizen.CreateThread(function()
	while not NetworkIsPlayerActive(PlayerId()) do
		Citizen.Wait(0)
	end

	while true do
		local playerPed = GetPlayerPed(-1)
		local x, y, z = table.unpack(GetEntityCoords(playerPed, true))
		local minDistance = 100000
		for i = 1, #zones, 1 do
			dist = Vdist(zones[i].x, zones[i].y, zones[i].z, x, y, z)
			if dist < minDistance then
				minDistance = dist
				closestZone = i
			end
		end
		Citizen.Wait(15000)
	end
end)

--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------
---------   Setting of friendly fire on and off, disabling your weapons, and sending pNoty   -----------------
--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------
ESX = nil
local job = nil
Citizen.CreateThread(function()
  while ESX == nil do
    TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
    Citizen.Wait(0)
  end
end)

Citizen.CreateThread(function()

	Citizen.Wait(5000)

	while not NetworkIsPlayerActive(PlayerId()) do
		Citizen.Wait(0)
	end

	while true do

		Citizen.Wait(0)

			local player = GetPlayerPed(-1)
			local x,y,z = table.unpack(GetEntityCoords(player, true))
			local dist = Vdist(zones[closestZone].x, zones[closestZone].y, zones[closestZone].z, x, y, z)

			if dist <= area then  ------------------------------------------------------------------------------ Here you can change the RADIUS of the Safe Zone. Remember, whatever you put here will DOUBLE because 
				if not notifIn then				
	
					job = ESX.GetPlayerData().job.name

					if ( job ~= "police" ) then
						NetworkSetFriendlyFireOption(false)
						ClearPlayerWantedLevel(PlayerId())
						SetCurrentPedWeapon(player,GetHashKey("WEAPON_UNARMED"),true)
					end

					TriggerEvent("pNotify:SendNotification",{
					text = "<b style='color:#FFFFFF'>คุณได้เข้าสู่เขตปลอดอาวุธ</b>",
					type = "success",
					timeout = (3000),
					layout = "bottomcenter",
					queue = "global"
					})
					
					notifIn = true
					notifOut = false
				end
			else
				if not notifOut then
					NetworkSetFriendlyFireOption(true)
					TriggerEvent("pNotify:SendNotification",{
						text = "<b style='color:#FFFFFF'>คุณออกนอกเขตปลอดอาวุธ</b>",
						type = "error",
						timeout = (3000),
						layout = "bottomcenter",
						queue = "global"
					})
					--exports['mythic_notify']:SendAlert('error', 'คุณออกนอกเขตปลอดอาวุธ !!')
					notifOut = true
					notifIn = false
				end
			end

			-- police can use weapon
			if notifIn and ( job ~= "police" ) then
				--DisableControlAction(2, 37, true) -- disable weapon wheel (Tab)
				--DisablePlayerFiring(player,true) -- Disables firing all together if they somehow bypass inzone Mouse Disable -- ennable punch
		      	DisableControlAction(0, 106, true) -- Disable in-game mouse controls
				--if IsDisabledControlJustPressed(2, 37) then --if Tab is pressed, send error message
					SetCurrentPedWeapon(player,GetHashKey("WEAPON_UNARMED"),true) -- if tab is pressed it will set them to unarmed (this is to cover the vehicle glitch until I sort that all out)
					-- TriggerEvent("pNotify:SendNotification",{
					-- 	text = "<b style='color:#FF3333'>ไม่สามารถใช้อาวุธได้ในเขตปลอดอาวุธ</b>",
					-- 	type = "error",
					-- 	timeout = (3000),
					-- 	layout = "bottomcenter",
					-- 	queue = "global"
					-- })
					--exports['mythic_notify']:SendAlert('error', 'ไม่สามารถใช้อาวุธได้ในเขตปลอดอาวุธ !!')
				--end
				if IsDisabledControlJustPressed(0, 106) then --if LeftClick is pressed, send error message
					SetCurrentPedWeapon(player,GetHashKey("WEAPON_UNARMED"),true) -- If they click it will set them to unarmed
					-- TriggerEvent("pNotify:SendNotification",{
					-- 	text = "<b style='color:#1E90FF'>You can not do that in a Safe Zone</b>",
					-- 	type = "error",
					-- 	timeout = (3000),
					-- 	layout = "bottomcenter",
					-- 	queue = "global"
					-- })
				end
			end
			-- Comment out lines 142 - 145 if you dont want a marker.
		 	if DoesEntityExist(player) then	      --The -1.0001 will place it on the ground flush		-- SIZING CIRCLE |  x    y    z | R   G    B   alpha| *more alpha more transparent*
		 		--DrawMarker(1, zones[closestZone].x, zones[closestZone].y, zones[closestZone].z, 0, 0, 0, 0, 0, 0, 1585.0, 1585.0, 2.0, 51, 255, 51, 155, 0, 0, 2, 0, 0, 0, 0) -- heres what all these numbers are. Honestly you dont really need to mess with any other than what isnt 0.
		 		--DrawMarker(type, float posX, float posY, float posZ, float dirX, float dirY, float dirZ, float rotX, float rotY, float rotZ, float scaleX, float scaleY, float scaleZ, int red, int green, int blue, int alpha, BOOL bobUpAndDown, BOOL faceCamera, int p19(LEAVE AS 2), BOOL rotate, char* textureDict, char* textureName, BOOL drawOnEnts)
		 	end

	end
end)