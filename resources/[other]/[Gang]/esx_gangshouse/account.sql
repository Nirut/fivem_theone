CREATE TABLE `gang_account` (
  `id` int(11) NOT NULL,
  `account` varchar(255) NOT NULL,
  `gang` varchar(255) NOT NULL,
  `amount` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- AUTO_INCREMENT för tabell `gang_account`
--
ALTER TABLE `gang_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

  --
-- Index för tabell `gang_account`
--
ALTER TABLE `gang_account`
  ADD PRIMARY KEY (`id`);
