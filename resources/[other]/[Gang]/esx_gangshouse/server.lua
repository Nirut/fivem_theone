ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
------------- PRICES FOR GANG HOUSE SELL AND BUY 				   -----------
------------- REMEMBER TO CHANGE PRICES IN client_gang.lua as well -----------
local house1 = 1
local house2 = 1
local house3 = 1
local house4 = 1
local house5 = 1
local house6 = 1
local house7 = 1
local house8 = 1
local house9 = 1
----------------------------------------------------
------------------------ VARS --------------------
----------------------------------------------------
local pGang1 = ""
local pGang2 = ""
local curBounty = 0
local curGangGotFlag = ""
local guyWhoGotFlag1 = ""
local guyWhoGotFlag2 = ""
local GangHouse1 = ""
local GangHouse2 = ""
local PvPActive = false
local FlagTaken1 = false
local FlagTaken2 = false
local oldSource = ""
OwnedGangHouse = {}
Gangs = {}
----------------------------------------------------
AddEventHandler('onMySQLReady', function ()
    MySQL.Async.fetchAll('SELECT * FROM gang_owned', {}, function(house)
      for i=1, #house, 1 do
        OwnedGangHouse[house[i].gang] = {
          house = house[i].name
		    }
      end    
    end)
	MySQL.Async.fetchAll('SELECT * FROM gangs', {}, function(gang)
      for i=1, #gang, 1 do
        Gangs[gang[i].name] = {
          label = gang[i].label
		    }
      end    
    end)
end)

RegisterServerEvent('stadus_gh:iWantInfo')
AddEventHandler('stadus_gh:iWantInfo', function(source, gang)
if OwnedGangHouse[gang] == nil then
return
else
TriggerClientEvent('stadus_gh:inforeciever', source, OwnedGangHouse[gang].house)
if gang == pGang1 or gang == pGang2 then
TriggerClientEvent('stadus_gh:gangFightNow', -1, pGang1, pGang2, tostring(OwnedGangHouse[pGang1].house), tostring(OwnedGangHouse[pGang2].house), "on")
end
end
end)

ESX.RegisterServerCallback('stadus_gh:getStockItems', function(source, cb, gangdf)
  local xPlayer = ESX.GetPlayerFromId(source)
  TriggerEvent('esx_addoninventory:getSharedInventory', gangdf, function(inventory)
    cb(inventory.items)
  end)
end)

RegisterServerEvent('stadus_gh:getStockItem')
AddEventHandler('stadus_gh:getStockItem', function(itemName, count, gangff)

  local xPlayer = ESX.GetPlayerFromId(source)

  TriggerEvent('esx_addoninventory:getSharedInventory', gangff, function(inventory)

    local item = inventory.getItem(itemName)

    if item.count >= count then
      inventory.removeItem(itemName, count)
      xPlayer.addInventoryItem(itemName, count)
    else
      TriggerClientEvent('esx:showNotification', xPlayer.source, "Quantity Invalid")
    end

    TriggerClientEvent('esx:showNotification', xPlayer.source, "You have withdrawn " .. count .. ' ' .. item.label)

  end)

end)

ESX.RegisterServerCallback('stadus_gh:getPlayerInventory', function(source, cb)
  local xPlayer    = ESX.GetPlayerFromId(source)
  local items      = xPlayer.inventory
  cb({
    items      = items
  })
end)

ESX.RegisterServerCallback('stadus_gh:getArmoryWeapons', function(source, cb, gang)
  local xPlayer = ESX.GetPlayerFromId(source)
	curGang = gang
  TriggerEvent('esx_datastore:getSharedDataStore', curGang, function(store)
    local weapons = store.get('weapons')
    if weapons == nil then
      weapons = {}
    end
    cb(weapons)
  end)
end)

ESX.RegisterServerCallback('stadus_gh:getPermWeapons', function(source, cb, gang)
  local xPlayer = ESX.GetPlayerFromId(source)
  local permWeapons = {}
	curGang = gang
	  TriggerEvent('esx_datastore:getSharedDataStore', curGang.."_perm", function(store)
    local weapons = store.get('weapons')
    if weapons == nil then
      weapons = {}
    end
    cb(weapons)
  end)
end)

ESX.RegisterServerCallback('stadus_gh:removeArmoryWeapon', function(source, cb, weaponName, gang)
  local xPlayer = ESX.GetPlayerFromId(source)
	curGang = gang
  xPlayer.addWeapon(weaponName, 1000)
  TriggerEvent('esx_datastore:getSharedDataStore', gang, function(store)
    local weapons = store.get('weapons')

    if weapons == nil then
      weapons = {}
    end

    local foundWeapon = false

    for i=1, #weapons, 1 do
      if weapons[i].name == weaponName then
        weapons[i].count = (weapons[i].count > 0 and weapons[i].count - 1 or 0)
        foundWeapon = true
      end
    end

    if not foundWeapon then
      table.insert(weapons, {
        name  = weaponName,
        count = 0
      })
    end

     store.set('weapons', weapons)
     cb()
  end)
end)

ESX.RegisterServerCallback('stadus_gh:addArmoryWeapon', function(source, cb, weaponName, gang)
  local xPlayer = ESX.GetPlayerFromId(source)

	curGang = gang
	xPlayer.removeWeapon(weaponName)

  TriggerEvent('esx_datastore:getSharedDataStore', curGang, function(store)
    local weapons = store.get('weapons')

    if weapons == nil then
      weapons = {}
    end

    local foundWeapon = false

    for i=1, #weapons, 1 do
      if weapons[i].name == weaponName then
        weapons[i].count = weapons[i].count + 1
        foundWeapon = true
      end
    end

    if not foundWeapon then
      table.insert(weapons, {
        name  = weaponName,
        count = 1
      })
    end
     store.set('weapons', weapons)
     cb()
  end)
end)

local lastItem = ""
RegisterServerEvent('stadus_gh:putStockItems')
AddEventHandler('stadus_gh:putStockItems', function(itemName, count, gang)
  local xPlayer = ESX.GetPlayerFromId(source)
  TriggerEvent('esx_addoninventory:getSharedInventory', gang, function(inventory)
    local item = inventory.getItem(itemName)
	if item.count <= 200 and count <= 200 and item.count >= 0 then
    if item.count >= 0 and count >= 0 then
	if lastItem == itemName or item.count >= 200 then
	TriggerClientEvent('esx:showNotification', xPlayer.source, "Too much of the same item...")
	lastItem = ""
	return
	else
      xPlayer.removeInventoryItem(itemName, count)
      inventory.addItem(itemName, count)
	  lastItem = itemName
	   TriggerClientEvent('esx:showNotification', xPlayer.source, "You added " .. count .. ' ~y~' .. item.label)
	   return
	   end
    else
      TriggerClientEvent('esx:showNotification', xPlayer.source, "quantity invalid")
    end
	else
	TriggerClientEvent('esx:showNotification', xPlayer.source, "You already have too much of that item...")
	end
  end)
end)

RegisterServerEvent("stadus_gh:sellGangHouse")
AddEventHandler("stadus_gh:sellGangHouse", function(source, gang, rank)
local xPlayer = ESX.GetPlayerFromId(source)
	if OwnedGangHouse[gang].house ~= nil then
	if tonumber(rank) >= tonumber(1) then 
	
  MySQL.Async.execute('DELETE FROM `gang_owned` WHERE  `gang`=@gang', 
  {
  ['@gang'] = gang
  }, function ()
  end)
TriggerClientEvent('esx:showNotification', source, "You sold your ~y~ganghouse~s~.")
TriggerClientEvent('esx:showNotification', source, "You can still exit, but you can't enter again.")

else
TriggerClientEvent('esx:showNotification', source, "You are not the owner of the gang")
return
end
end

end)

RegisterServerEvent("stadus_gh:giveGangHouse")
AddEventHandler("stadus_gh:giveGangHouse", function(source, newgang, gang, rank)
local xPlayer = ESX.GetPlayerFromId(source)

	curGang = gang
	if OwnedGangHouse[curgang].house ~= nil then
		if Gangs[newgang] ~= nil then
			if tonumber(rank) >= 1 then 
  MySQL.Async.execute('UPDATE `gang_owned` SET `gang` = @newgang WHERE `gang` = @gang', 
  {
  ['@newgang'] = newgang,
  ['@gang'] = curGang
  }, function ()
  end)
TriggerClientEvent('esx:showNotification', source, "You gave your ~y~ganghouse~s~ to ~y~"..newgang..".")
TriggerClientEvent('esx:showNotification', source, "You can still exit, but you can't enter again.~.")

else
TriggerClientEvent('esx:showNotification', source, "You are not the owner of the gang")
return
end
else
TriggerClientEvent('esx:showNotification', source, "The gang ~y~"..newgang.."~s~ doesn't exists.")
return
end
else
TriggerClientEvent('esx:showNotification', source, "You don't own a ~y~gang house~s~.")
return
end
end)

RegisterServerEvent("stadus_gh:addmoney")
AddEventHandler("stadus_gh:addmoney", function(source, account, amount, gang)
local xPlayer = ESX.GetPlayerFromId(source)
		local accounts = xPlayer.getAccount('black_money')
		local accountmoney = tonumber(accounts.money)
	   curGang = gang
		if accountmoney >= amount then
							TriggerEvent('esx_addonaccount:getSharedAccount', curGang, function(accountd)
						if tonumber(accountd.money) >= 0 and amount >= 0 then
						accountd.addMoney(amount)
						xPlayer.removeAccountMoney('black_money', amount)
						TriggerClientEvent('esx:showNotification', source, "You deposited ~g~$"..amount.."")
						else
						TriggerClientEvent('esx:showNotification', source, "You don't have enough money.")
						end
						end)
else
TriggerClientEvent('esx:showNotification', source, "You don't have that kind of money...")
return
end
end)

RegisterServerEvent("stadus_gh:withdrawmoney")
AddEventHandler("stadus_gh:withdrawmoney", function(source, account, amount, gangs, ranks)
		local xPlayer = ESX.GetPlayerFromId(source)
		local accounts = xPlayer.getAccount('black_money')
		local accountmoney = tonumber(accounts.money)
	curGang = gangs
	MyRank = ranks

	if tonumber(ranks) >= 1 then 
					TriggerEvent('esx_addonaccount:getSharedAccount', curGang, function(account)
						if tonumber(account.money) >= tonumber(amount) and tonumber(account.money) >= tonumber(0) and amount >= 0 then
						account.removeMoney(amount)
						xPlayer.addAccountMoney('black_money', amount)
						TriggerClientEvent('esx:showNotification', source, "You withdrew ~g~$"..amount.."")
						else
						TriggerClientEvent('esx:showNotification', source, "You don't have enough money.")
						end
						end)
else
TriggerClientEvent('esx:showNotification', source, "You are not the owner of the gang")
return
end
end)

ESX.RegisterServerCallback('stadus_gh:getHouseAcc', function(source, cb, gang)
  local xPlayer = ESX.GetPlayerFromId(source)
  local blackMoney = 0
	TriggerEvent('esx_addonaccount:getSharedAccount', gang, function(account)
		  cb({
    moneys = tonumber(account.money)
		})
	end)
end)

ESX.RegisterServerCallback('stadus_gh:getCurrGangHouse', function(source, cb, gang)
  local xPlayer = ESX.GetPlayerFromId(source)
  local _source = source
	curGang = gang
	if curGang == "none" or curGang == nil then
	cb("nope")
	else
		if OwnedGangHouse[curGang].house == nil then
		cb("nope")
		else
		cb(OwnedGangHouse[curGang].house)
		end
	end
end)

RegisterServerEvent('stadus_gh:pickUpFlag')
AddEventHandler('stadus_gh:pickUpFlag', function(source, currhouse, gang)
  local gangatm = ""
  local myGangHouse = ""
  local xPlayer  = ESX.GetPlayerFromId(source) 
  if PvPActive == true then
	gangatm = gang
		if OwnedGangHouse[gang].house == currhouse then
				TriggerClientEvent('esx:showNotification', source, "You can't pick up ~r~your own~s~ flag...")
				return
		end
		if gangatm == pGang1 then
			if FlagTaken1 == false then
		TriggerClientEvent('esx:showNotification', -1, "The gang ~y~"..gangatm.."~s~ picked up the flag from ~r~"..pGang2.."")
				guyWhoGotFlag1 = xPlayer.identifier
				FlagTaken1 = true
				curGangGotFlag = gangatm
				xPlayer.addInventoryItem("flag1", 1)
										else
		TriggerClientEvent('esx:showNotification', source, "Someone already took the flag..")
			end
		elseif gangatm == pGang2 then
			if FlagTaken2 == false then
		TriggerClientEvent('esx:showNotification', -1, "The gang ~y~"..gangatm.."~s~ picked up the flag from ~r~"..pGang1.."")
				guyWhoGotFlag2 = xPlayer.identifier
				FlagTaken2 = true
				curGangGotFlag = gangatm
				xPlayer.addInventoryItem("flag2", 1)
			else
		TriggerClientEvent('esx:showNotification', source, "Someone already took the flag..")
			end
		end
		else
		TriggerClientEvent('esx:showNotification', source, "You can't pick up the flag.")
		return
	end
end)

RegisterServerEvent('stadus_gh:leaveFlag')
AddEventHandler('stadus_gh:leaveFlag', function(source, currhouse, gang)
  local gangatm = ""
  local xPlayer  = ESX.GetPlayerFromId(source)
	gangatm = gang
  if PvPActive == true then
	
	myGangHouse = OwnedGangHouse[gangatm].house 
	
			if myGangHouse ~= currhouse then
				TriggerClientEvent('esx:showNotification', source, "You can't leave the flag here...")
				return
		end
		curGangGotFlag = gangatm
		if guyWhoGotFlag1 == xPlayer.identifier or guyWhoGotFlag2 == xPlayer.identifier then
		if gangatm == pGang1 then
			
			if FlagTaken1 == true then
		TriggerClientEvent('esx:showNotification', -1, "The gang ~y~"..gangatm.."~s~ leaved the flag from ~r~"..pGang2.."")
		TriggerClientEvent('stadus_gh:gangFightNow', -1, pGang1, pGang2, tostring(OwnedGangHouse[pGang1].house), tostring(OwnedGangHouse[pGang2].house), "off")
					TriggerClientEvent('gangPvp:won', -1, pGang1, pGang2, curBounty*2)
					PvPActive = false
		guyWhoGotFlag1 = xPlayer.identifier
					TriggerEvent('esx_addonaccount:getSharedAccount', gangatm, function(account)
						account.addMoney(curBounty*2)
						pGang1 = ""
						pGang2 = ""
						end)	
		FlagTaken1 = false
		xPlayer.removeInventoryItem("flag1", 1)
				else
		TriggerClientEvent('esx:showNotification', source, "Someone already took the flag..")
			end
			elseif gangatm == pGang2 then
			TriggerClientEvent('esx:showNotification', -1, "The gang ~y~"..gangatm.."~s~ leaved the flag from ~r~"..pGang1.."")
				guyWhoGotFlag2 = xPlayer.identifier
				FlagTaken2 = false
				TriggerClientEvent('gangPvp:won', -1, pGang2, pGang1, curBounty*2)
					TriggerEvent('esx_addonaccount:getSharedAccount', gangatm, function(account)
						account.addMoney(curBounty*2)
						end)
					PvPActive = false
				TriggerClientEvent('stadus_gh:gangFightNow', -1, pGang1, pGang2, tostring(OwnedGangHouse[pGang1].house), tostring(OwnedGangHouse[pGang2].house), "off")
				xPlayer.removeInventoryItem("flag2", 1)
				pGang1 = ""
				pGang2 = ""
							else
		TriggerClientEvent('esx:showNotification', source, "You can't pick up the flag from yourself...")
		end
		end

			else
		TriggerClientEvent('esx:showNotification', source, "You can't leave the flag.")
		    end
end)

RegisterServerEvent('stadus_gh:buyItem')
AddEventHandler('stadus_gh:buyItem', function(itemName, price, zone)
  local _source = source
  local xPlayer  = ESX.GetPlayerFromId(source)
  
 if xPlayer.get('money') >= price then
    xPlayer.removeMoney(price)
    xPlayer.addWeapon(itemName, 42)
    TriggerClientEvent('esx:showNotification', _source, "You bought " .. ESX.GetWeaponLabel(itemName))
  else
    TriggerClientEvent('esx:showNotification', _source, "Not enough money")
  end
end)

RegisterServerEvent('stadus_gh:buypermItem')
AddEventHandler('stadus_gh:buypermItem', function(itemName, price, gang, discount)
  local _source = source
  local xPlayer  = ESX.GetPlayerFromId(source)
 if xPlayer.get('money') >= price then
  TriggerEvent('esx_datastore:getSharedDataStore', gang.."_perm", function(store)
    local weapons = store.get('weapons')
    if weapons == nil then
      weapons = {}
    end
    local foundWeapon = false

    for i=1, #weapons, 1 do
      if weapons[i].name == itemName then
        weapons[i].count = weapons[i].count + 1
        foundWeapon = true
      end
    end

    if not foundWeapon then
      table.insert(weapons, {
        name  = itemName,
				count = 1,
				prices = discount
      })
    end
     store.set('weapons', weapons)
  end)
  		xPlayer.removeMoney(price)

    TriggerClientEvent('esx:showNotification', _source, "You bought " .. ESX.GetWeaponLabel(itemName) .. " permanent for your gang house.")
  else
    TriggerClientEvent('esx:showNotification', _source, "Not enough money")
  end
end)

RegisterServerEvent('stadus_gh:getPermItem')
AddEventHandler('stadus_gh:getPermItem', function(itemName, pricesis)
	local xPlayer  = ESX.GetPlayerFromId(source)
	 if xPlayer.getAccount('black_money').money >= tonumber(pricesis) then
		xPlayer.addWeapon(itemName, 400)
		xPlayer.removeAccountMoney('black_money', tonumber(pricesis))
		TriggerClientEvent('esx:showNotification', source, "You bought " .. ESX.GetWeaponLabel(itemName) .. " from your gang house, price: ~g~$"..pricesis)
	 else
		TriggerClientEvent('esx:showNotification', source, "You can't afford " .. ESX.GetWeaponLabel(itemName) .. "!")
 	end
end)

ESX.RegisterServerCallback('stadus_gh:isPlayerInGangFight', function(source, cb, gangcur)
  local xPlayer = ESX.GetPlayerFromId(source)
		if guyWhoGotFlag1 == xPlayer.identifier then
		xPlayer.removeInventoryItem("flag1", 1)
		TriggerClientEvent('esx:showNotification', -1, "~p~PVP FIGHT:~s~ The ~y~flag 1~s~ has been dropped and returned to the gang house!")
		guyWhoGotFlag1 = ""
		 FlagTaken1 = false
		end
		if guyWhoGotFlag2 == xPlayer.identifier then
		FlagTaken2 = false
		xPlayer.removeInventoryItem("flag2", 1)
		TriggerClientEvent('esx:showNotification', -1, "~p~PVP FIGHT:~s~ The ~y~flag 2~s~ has been dropped and returned to the gang house!")
		guyWhoGotFlag2 = ""
		end
		
	if PvPActive == true then
	if gangcur == pGang1 or gangcur == pGang2 then
		cb("yes")
		else
		cb("no")
	end	
		else
	cb("no")
	end
end)

RegisterServerEvent("stadus_gh:iWantPvp")
AddEventHandler("stadus_gh:iWantPvp", function(source, money, gang2, gang1, myRank)
  local xPlayer = ESX.GetPlayerFromId(source)
		oldSource = source
		if tonumber(myRank) >= 1 then
	if Gangs[gang2] ~= nil then
		if PvPActive == false then
		pGang1 = gang1
		pGang2 = gang2
		curBounty = money
		TriggerClientEvent('esx:showNotification', source, "You need to wait for the leader in the other gang to accept.")
		else
		TriggerClientEvent('esx:showNotification', source, "There is already a PvP fight.")
		end
		else
		TriggerClientEvent('esx:showNotification', source, "The gang: "..gang2.." doesn't exists.")
	end
	else
	TriggerClientEvent('esx:showNotification', source, "You're not the leader of ~y~"..gang1.."~s~.")
	return
	end
end)

RegisterServerEvent('stadus_gh:iAcceptPvp')
AddEventHandler('stadus_gh:iAcceptPvp', function(source, myGangNames, myGangRanks)
 local xPlayer = ESX.GetPlayerFromId(source)
	if tonumber(myGangRanks) == 1 then
			if OwnedGangHouse[pGang1].house == nil or OwnedGangHouse[pGang2].house == nil then
				return
			else
			if PvPActive == false then
		if myGangNames == pGang2 then
		TriggerClientEvent('esx:showNotification', source, "You have accepted the PvP fight against ~y~"..pGang1..".")
			TriggerClientEvent('gangPvp:show', -1, pGang1, pGang2, curBounty*2)
			TriggerClientEvent('stadus_gh:gangFightChat', -1, pGang1, pGang2)
					PvPActive = true
					TriggerEvent('esx_addonaccount:getSharedAccount', pGang1, function(account)
						if tonumber(account.money) >= tonumber(curBounty) and tonumber(account.money) >= tonumber(0) then
						account.removeMoney(curBounty)
						else
						TriggerClientEvent('esx:showNotification', source, "You can't afford the fight...")
						end
						end)
							
					TriggerEvent('esx_addonaccount:getSharedAccount', pGang2, function(account)
						if tonumber(account.money) >= tonumber(curBounty) and tonumber(account.money) >= tonumber(0) then
						account.removeMoney(curBounty)
						else
						TriggerClientEvent('esx:showNotification', source, "The other gang can't afford the fight...")
						end
						end)
				TriggerClientEvent('esx:showNotification', oldSource, "You spent ~g~$"..curBounty.." ~s~on the gang fight, the money has been taken.")
				TriggerClientEvent('esx:showNotification', source, "You spent ~g~$"..curBounty.." ~s~on the gang fight, the money has been taken.")
				
				TriggerClientEvent('stadus_gh:gangFightNow', -1, pGang1, pGang2, tostring(OwnedGangHouse[pGang1].house), tostring(OwnedGangHouse[pGang2].house), "on")
		else
		TriggerClientEvent('esx:showNotification', source, "You can't accept a fight against no one...")
		end
		else
		TriggerClientEvent('esx:showNotification', source, "There is already a fight currently...")
		end
	end
end
end)

RegisterServerEvent("stadus_gh:policeNeedGang")
AddEventHandler("stadus_gh:policeNeedGang", function(source, housed)
	MySQL.Async.fetchAll('SELECT * FROM `gang_owned` WHERE `name` = @name', {['@name'] = tostring(housed)}, function(gangNameIs)
		if gangNameIs[1] and gangNameIs then
			policeSearchGang = gangNameIs[1].gang
		TriggerClientEvent('esx:showNotification', source, "~b~SEARCH WARRANT:~s~ The owner of the "..housed.." is ~y~"..policeSearchGang)
		TriggerClientEvent('stadus_gh:policeNeedInfo', source, policeSearchGang)
		return
	 else
		TriggerClientEvent('esx:showNotification', source, "There is not a gang the owns the ~y~gang "..housed)
		return
		end
		end)
end)

RegisterServerEvent("stadus_gh:buyHouse1")
AddEventHandler("stadus_gh:buyHouse1", function(source, gangd, rankd)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	local cash = xPlayer.get('money')
 MySQL.Async.fetchAll('SELECT * FROM `gang_owned` WHERE `name` = @name', {['@name'] = "house1"}, function(gangOwned)
	if gangOwned[1] and gangOwned then
	TriggerClientEvent('esx:showNotification', _source, "Someone already owns that gang house.")
	return
 else
if cash >= tonumber(house1) then
	xPlayer.removeMoney(tonumber(house1))
	testGang(source, "house1", gangd, rankd)
else
	TriggerClientEvent('esx:showNotification', _source, "You can't afford this...")
	end
	end
	end)
end)

RegisterServerEvent("stadus_gh:buyHouse2")
AddEventHandler("stadus_gh:buyHouse2", function(source, gangd, rankd)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	local cash = xPlayer.get('money')
 MySQL.Async.fetchAll('SELECT * FROM `gang_owned` WHERE `name` = @name', {['@name'] = "house2"}, function(gangOwned)
	if gangOwned[1] and gangOwned then
	TriggerClientEvent('esx:showNotification', _source, "Someone already owns that gang house.")
	return
 else
if cash >= tonumber(house2) then
	xPlayer.removeMoney(tonumber(house2))
	testGang(source, "house2", gangd, rankd)
else
	TriggerClientEvent('esx:showNotification', _source, "You can't afford this...")
	end
	end
	end)
end)

RegisterServerEvent("stadus_gh:buyHouse3")
AddEventHandler("stadus_gh:buyHouse3", function(source, gangd, rankd)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	local cash = xPlayer.get('money')
 MySQL.Async.fetchAll('SELECT * FROM `gang_owned` WHERE `name` = @name', {['@name'] = "house3"}, function(gangOwned)
	if gangOwned[1] and gangOwned then
	TriggerClientEvent('esx:showNotification', _source, "Someone already owns that gang house.")
	return
 else
if cash >= tonumber(house3) then
	xPlayer.removeMoney(tonumber(house3))
	testGang(source, "house3", gangd, rankd)
else
	TriggerClientEvent('esx:showNotification', _source, "You can't afford this...")
	end
	end
	end)
end)
RegisterServerEvent("stadus_gh:buyHouse4")
AddEventHandler("stadus_gh:buyHouse4", function(source, gangd, rankd)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	local cash = xPlayer.get('money')
 MySQL.Async.fetchAll('SELECT * FROM `gang_owned` WHERE `name` = @name', {['@name'] = "house4"}, function(gangOwned)
	if gangOwned[1] and gangOwned then
	TriggerClientEvent('esx:showNotification', _source, "Someone already owns that gang house.")
	return
 else
if cash >= tonumber(house4) then
	xPlayer.removeMoney(tonumber(house4))
	testGang(source, "house4", gangd, rankd)
else
	TriggerClientEvent('esx:showNotification', _source, "You can't afford this...")
	end
	end
	end)
end)
RegisterServerEvent("stadus_gh:buyHouse5")
AddEventHandler("stadus_gh:buyHouse5", function(source, gangd, rankd)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	local cash = xPlayer.get('money')
 MySQL.Async.fetchAll('SELECT * FROM `gang_owned` WHERE `name` = @name', {['@name'] = "house5"}, function(gangOwned)
	if gangOwned[1] and gangOwned then
	TriggerClientEvent('esx:showNotification', _source, "Someone already owns that gang house.")
	return
 else
if cash >= tonumber(house5) then
	xPlayer.removeMoney(tonumber(house5))
	testGang(source, "house5", gangd, rankd)
else
	TriggerClientEvent('esx:showNotification', _source, "You can't afford this...")
	end
	end
	end)
end)
RegisterServerEvent("stadus_gh:buyHouse6")
AddEventHandler("stadus_gh:buyHouse6", function(source, gangd, rankd)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	local cash = xPlayer.get('money')
 MySQL.Async.fetchAll('SELECT * FROM `gang_owned` WHERE `name` = @name', {['@name'] = "house6"}, function(gangOwned)
	if gangOwned[1] and gangOwned then
	TriggerClientEvent('esx:showNotification', _source, "Someone already owns that gang house.")
	return
 else
if cash >= tonumber(house6) then
	xPlayer.removeMoney(tonumber(house6))
	testGang(source, "house6", gangd, rankd)
else
	TriggerClientEvent('esx:showNotification', _source, "You can't afford this...")
	end
	end
	end)
end)

function testGang(source, type, gangd, rankd)
  local _source = source
  local xPlayer = ESX.GetPlayerFromId(source)
	if tonumber(rankd) >= tonumber(1) then
	TriggerClientEvent('esx:showNotification', _source, "You just bought a gang house to your gang ~y~"..gangd.."~s~!")
	TriggerClientEvent('esx:showNotification', _source, "~r~WARNING!!! ~s~The gang house need a ~r~server restart~s~ to work!")
	addDBType(source, gangd, type)
	else
	TriggerClientEvent('esx:showNotification', _source, "You're not the gang leader!")
	end
end

function addDBType(source, _gang, type)
  local _source = source
  local xPlayer = ESX.GetPlayerFromId(source)
  local _label = label
  
  MySQL.Async.execute('INSERT INTO `essentialmode`.`gang_owned` (`name`, `gang`) VALUES (@name, @gang);', 
  {
  ['@name'] = type,
  ['@gang'] = _gang
  }, function ()
  end)
  	MySQL.Async.fetchAll('SELECT * FROM `addon_account` WHERE `name` = @gang', {['@gang'] = _gang}, function(ownGang)
	if (ownGang[1] and ownGang) then
  else
      MySQL.Async.execute('INSERT INTO `essentialmode`.`addon_account` (`name`, `label`, `shared`) VALUES (@name, @label, @shared);', 
  {
  ['@name'] = _gang,
  ['@label'] = _gang,
  ['@shared'] = 1
  }, function ()
  end)
  end
    	MySQL.Async.fetchAll('SELECT * FROM `addon_account` WHERE `name` = @gang', {['@gang'] = _gang}, function(ownGangs)
	if (ownGangs[1] and ownGangs) then
  else
    MySQL.Async.execute('INSERT INTO `essentialmode`.`addon_inventory` (`name`, `label`, `shared`) VALUES (@name, @label, @shared);', 
  {
  ['@name'] = _gang,
  ['@label'] = _gang,
  ['@shared'] = 1
  }, function ()
  end)    
  end
  
  MySQL.Async.fetchAll('SELECT * FROM `datastore` WHERE `name` = @gang', {['@gang'] = _gang}, function(ownGangss)
	if (ownGangss[1] and ownGangss) then
  else
  MySQL.Async.execute('INSERT INTO `essentialmode`.`datastore` (`name`, `label`, `shared`) VALUES (@name, @label, @shared);', 
  {
  ['@name'] = _gang,
  ['@label'] = _gang,
  ['@shared'] = 1
  }, function ()
  end)
  MySQL.Async.execute('INSERT INTO `essentialmode`.`datastore` (`name`, `label`, `shared`) VALUES (@name, @label, @shared);', 
  {
  ['@name'] = _gang.."_perm",
  ['@label'] = _gang.."_perm",
  ['@shared'] = 1
  }, function ()
  end)
  end
			end)
		end)
	end)
end

TriggerEvent('es:addGroupCommand', 'gangpvpaccept', 'user', function(source, args, user)
  local player = args[1]
  TriggerClientEvent('stadus_gh:beforeacceptPvp', source)
end, function(source, args, user)
  TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "Insufficient Permissions.")
end, {help = 'ganginvite', params = {{name = " ", help = 'Accept gang fight'}}})

TriggerEvent('es:addGroupCommand', 'gangpvp', 'user', function(source, args, user)
  local player = args[1]
  local gang = args[1]
  local money = args[2]
  if tonumber(money) <= 0 or money == nil or gang == nil then
	TriggerClientEvent('esx:showNotification', source, "Something is invalid..")
  return
  else
  TriggerClientEvent('stadus_gh:beforePvp', source, money, gang)
  end  
end, function(source, args, user)
  TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "Insufficient Permissions.")
end, {help = 'ganginvite', params = {{name = "gang", help = 'Gang name'}, {name = "money", help = 'money'}}})
