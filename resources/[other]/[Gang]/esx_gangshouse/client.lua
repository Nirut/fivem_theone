ESX = nil
local PlayerData                = {}
local Time                      = 0
local FirstSpawn              = true
local myGang = ""
local currhouse = ""
local myGangrank = ""
local myGanghouse = ""
local vsGangHouse = ""
local policeWarrantHouseGang = ""
local PvpIsNow = false

--------------------------------------------------------------

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end
end)

RegisterNetEvent('stadus_gh:policeNeedInfo')
AddEventHandler('stadus_gh:policeNeedInfo', function(warranthouseis)
policeWarrantHouseGang = warranthouseis
print("warranthouseis "..warranthouseis)
end)


RegisterNetEvent('stadus_gh:inforeciever')
AddEventHandler('stadus_gh:inforeciever', function(info)
myGanghouse = info
VehicleSpawnerHouse = info

print("myGanghouse is "..myGanghouse)
ganghouseblip(myGanghouse)
end)

RegisterNetEvent('stadus_gh:policeWarrant')
AddEventHandler('stadus_gh:policeWarrant', function(houseis)
print("houseis "..houseis)
TriggerServerEvent('stadus_gh:policeNeedGang', GetPlayerServerId(PlayerId()), houseis)
Warrantpolicehouse(houseis)
end)

RegisterNetEvent('stadus_gh:beforePvp')
AddEventHandler('stadus_gh:beforePvp',function(money, gang)
TriggerServerEvent('stadus_gh:iWantPvp', GetPlayerServerId(PlayerId()), money, gang, myGang, myGangrank)
end)

RegisterNetEvent('stadus_gh:beforeacceptPvp')
AddEventHandler('stadus_gh:beforeacceptPvp',function()
print("beforeacceptPvp")
TriggerServerEvent('stadus_gh:iAcceptPvp', GetPlayerServerId(PlayerId()), myGang, myGangrank)
end)

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
    PlayerData = xPlayer
	 ESX.SetTimeout(5000, function()
	myGang = tostring(exports['esx_gangs']:getGangName())
	myGangrank = tostring(exports['esx_gangs']:getGangRank())
	if myGang == "none" then
	return
	else
	TriggerServerEvent('stadus_gh:iWantInfo', GetPlayerServerId(PlayerId()), myGang)
	end
	end)
end)

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)
  PlayerData.job = job
end)

function getGangHouseName()
  if myGanghouse ~= nil then
	return myGanghouse
  end  
end

function Warrantpolicehouse(type)
	if type == "house1" then
		currhouse = type
		local x = -683.8 --house1 ENTER
		local y = -1170.73
		local z = 10.11		
		local blip1 = AddBlipForCoord(x, y, z)
		SetBlipSprite (blip1, 492)
		SetBlipDisplay(blip1, 4)
		SetBlipScale  (blip1, 1.0)
		SetBlipColour (blip1, 38)
		SetBlipAsShortRange(blip1, true)
		BeginTextCommandSetBlipName("STRING")
		AddTextComponentString("Search Warrant House")
		EndTextCommandSetBlipName(blip1)	
	end

	if type == "house2" then	
		currhouse = type
		local x3 = 939.0 --house2 ENTER
		local y3 = -1877.0
		local z3 = 32.1		
		local blip = AddBlipForCoord(x3, y3, z3)
		SetBlipSprite (blip, 492)
		SetBlipDisplay(blip, 4)
		SetBlipScale  (blip, 1.0)
		SetBlipColour (blip, 38)
		SetBlipAsShortRange(blip, true)
		BeginTextCommandSetBlipName("STRING")
		AddTextComponentString("Search Warrant House")
		EndTextCommandSetBlipName(blip)	
	end
	
	if type == "house3" then	
		currhouse = type
		local blip = AddBlipForCoord(-1472.662, -49.877, 54.638)
		SetBlipSprite (blip, 492)
		SetBlipDisplay(blip, 4)
		SetBlipScale  (blip, 1.0)
		SetBlipColour (blip, 38)
		SetBlipAsShortRange(blip, true)
		BeginTextCommandSetBlipName("STRING")
		AddTextComponentString("Search Warrant House")
		EndTextCommandSetBlipName(blip)	
	end
	
	if type == "house4" then	
		currhouse = type
		local blip = AddBlipForCoord(-145.166, 885.909, 239.020)
		SetBlipSprite (blip, 492)
		SetBlipDisplay(blip, 4)
		SetBlipScale  (blip, 1.0)
		SetBlipColour (blip, 38)
		SetBlipAsShortRange(blip, true)
		BeginTextCommandSetBlipName("STRING")
		AddTextComponentString("Search Warrant House")
		EndTextCommandSetBlipName(blip)	
	end
	
	if type == "house5" then	
		currhouse = type
		local blip = AddBlipForCoord(-561.130, 402.777, 101.805)
		SetBlipSprite (blip, 492)
		SetBlipDisplay(blip, 4)
		SetBlipScale  (blip, 1.0)
		SetBlipColour (blip, 38)
		SetBlipAsShortRange(blip, true)
		BeginTextCommandSetBlipName("STRING")
		AddTextComponentString("Search Warrant House")
		EndTextCommandSetBlipName(blip)	
	end
	
	if type == "house6" then	
		currhouse = type
		local blip = AddBlipForCoord(-1294.343, 454.621, 97.527)
		SetBlipSprite (blip, 492)
		SetBlipDisplay(blip, 4)
		SetBlipScale  (blip, 1.0)
		SetBlipColour (blip, 38)
		SetBlipAsShortRange(blip, true)
		BeginTextCommandSetBlipName("STRING")
		AddTextComponentString("Search Warrant House")
		EndTextCommandSetBlipName(blip)	
		end	
		
	if type == "house7" then	
		currhouse = type
		local blip = AddBlipForCoord(223.182, -176.176, 57.917)
		SetBlipSprite (blip, 492)
		SetBlipDisplay(blip, 4)
		SetBlipScale  (blip, 1.0)
		SetBlipColour (blip, 38)
		SetBlipAsShortRange(blip, true)
		BeginTextCommandSetBlipName("STRING")
		AddTextComponentString("Search Warrant House")
		EndTextCommandSetBlipName(blip)	
	end
	
	if type == "house8" then	
		currhouse = type
		local blip = AddBlipForCoord(-853.185, 695.749, 148.786)
		SetBlipSprite (blip, 492)
		SetBlipDisplay(blip, 4)
		SetBlipScale  (blip, 1.0)
		SetBlipColour (blip, 38)
		SetBlipAsShortRange(blip, true)
		BeginTextCommandSetBlipName("STRING")
		AddTextComponentString("Search Warrant House")
		EndTextCommandSetBlipName(blip)	
	end
	
	if type == "house9" then	
		currhouse = type
		local blip = AddBlipForCoord(-97.261, 988.158, 235.756)
		SetBlipSprite (blip, 492)
		SetBlipDisplay(blip, 4)
		SetBlipScale  (blip, 1.0)
		SetBlipColour (blip, 38)
		SetBlipAsShortRange(blip, true)
		BeginTextCommandSetBlipName("STRING")
		AddTextComponentString("Search Warrant House")
		EndTextCommandSetBlipName(blip)	
	end
	
	if type == "house10" then	
		currhouse = type
		local blip = AddBlipForCoord(-444.394, 343.162, 105.581)
		SetBlipSprite (blip, 492)
		SetBlipDisplay(blip, 4)
		SetBlipScale  (blip, 1.0)
		SetBlipColour (blip, 38)
		SetBlipAsShortRange(blip, true)
		BeginTextCommandSetBlipName("STRING")
		AddTextComponentString("Search Warrant House")
		EndTextCommandSetBlipName(blip)	
	end
	
	if type == "house11" then	
		currhouse = type
		local blip = AddBlipForCoord(-526.778, 517.118, 112.942)
		SetBlipSprite (blip, 492)
		SetBlipDisplay(blip, 4)
		SetBlipScale  (blip, 1.0)
		SetBlipColour (blip, 38)
		SetBlipAsShortRange(blip, true)
		BeginTextCommandSetBlipName("STRING")
		AddTextComponentString("Search Warrant House")
		EndTextCommandSetBlipName(blip)	
	end
	
	if type == "house12" then	
		currhouse = type
		local blip = AddBlipForCoord(-459.221, 537.119, 121.459)
		SetBlipSprite (blip, 492)
		SetBlipDisplay(blip, 4)
		SetBlipScale  (blip, 1.0)
		SetBlipColour (blip, 38)
		SetBlipAsShortRange(blip, true)
		BeginTextCommandSetBlipName("STRING")
		AddTextComponentString("Search Warrant House")
		EndTextCommandSetBlipName(blip)	
	end
	
	if type == "house13" then	
		currhouse = type
		local blip = AddBlipForCoord(-70.490, 359.242, 112.445)
		SetBlipSprite (blip, 492)
		SetBlipDisplay(blip, 4)
		SetBlipScale  (blip, 1.0)
		SetBlipColour (blip, 38)
		SetBlipAsShortRange(blip, true)
		BeginTextCommandSetBlipName("STRING")
		AddTextComponentString("Search Warrant House")
		EndTextCommandSetBlipName(blip)	
	end
	
	if type == "house14" then	
		currhouse = type
		local blip = AddBlipForCoord(-1580.164, -33.963, 57.565)
		SetBlipSprite (blip, 492)
		SetBlipDisplay(blip, 4)
		SetBlipScale  (blip, 1.0)
		SetBlipColour (blip, 38)
		SetBlipAsShortRange(blip, true)
		BeginTextCommandSetBlipName("STRING")
		AddTextComponentString("Search Warrant House")
		EndTextCommandSetBlipName(blip)	
	end
	
	if type == "house15" then	
		currhouse = type
		local blip = AddBlipForCoord(-1587.769, 15.749, 61.253)
		SetBlipSprite (blip, 492)
		SetBlipDisplay(blip, 4)
		SetBlipScale  (blip, 1.0)
		SetBlipColour (blip, 38)
		SetBlipAsShortRange(blip, true)
		BeginTextCommandSetBlipName("STRING")
		AddTextComponentString("Search Warrant House")
		EndTextCommandSetBlipName(blip)	
	end
end

function ganghouseblip(type)
	if type == "house1" then
		currhouse = type
		local x = -683.8 --house1 ENTER
		local y = -1170.73
		local z = 10.11		
		local blip1 = AddBlipForCoord(x, y, z)
		SetBlipSprite (blip1, 492)
		SetBlipDisplay(blip1, 4)
		SetBlipScale  (blip1, 1.0)
		SetBlipColour (blip1, 4)
		SetBlipAsShortRange(blip1, true)
		BeginTextCommandSetBlipName("STRING")
		AddTextComponentString("My Gang house")
		EndTextCommandSetBlipName(blip1)	
	end

	if type == "house2" then	
		currhouse = type
		local x3 = 939.0 --house2 ENTER
		local y3 = -1877.0
		local z3 = 32.1		
		local blip = AddBlipForCoord(x3, y3, z3)
		SetBlipSprite (blip, 492)
		SetBlipDisplay(blip, 4)
		SetBlipScale  (blip, 1.0)
		SetBlipColour (blip, 4)
		SetBlipAsShortRange(blip, true)
		BeginTextCommandSetBlipName("STRING")
		AddTextComponentString("My Gang house")
		EndTextCommandSetBlipName(blip)	
	end
	
	if type == "house3" then	
		currhouse = type
		local blip = AddBlipForCoord(-1472.662, -49.877, 54.638)
		SetBlipSprite (blip, 492)
		SetBlipDisplay(blip, 4)
		SetBlipScale  (blip, 1.0)
		SetBlipColour (blip, 4)
		SetBlipAsShortRange(blip, true)
		BeginTextCommandSetBlipName("STRING")
		AddTextComponentString("My Gang house")
		EndTextCommandSetBlipName(blip)	
	end

	if type == "house4" then	
		currhouse = type
		local blip = AddBlipForCoord(-145.166, 885.909, 239.020)
		SetBlipSprite (blip, 492)
		SetBlipDisplay(blip, 4)
		SetBlipScale  (blip, 1.0)
		SetBlipColour (blip, 4)
		SetBlipAsShortRange(blip, true)
		BeginTextCommandSetBlipName("STRING")
		AddTextComponentString("My Gang house")
		EndTextCommandSetBlipName(blip)	
	end
	
	if type == "house5" then	
		currhouse = type
		local blip = AddBlipForCoord(-561.130, 402.777, 101.805)
		SetBlipSprite (blip, 492)
		SetBlipDisplay(blip, 4)
		SetBlipScale  (blip, 1.0)
		SetBlipColour (blip, 4)
		SetBlipAsShortRange(blip, true)
		BeginTextCommandSetBlipName("STRING")
		AddTextComponentString("My Gang house")
		EndTextCommandSetBlipName(blip)	
	end
	
	if type == "house6" then	
		currhouse = type
		local blip = AddBlipForCoord(-1294.343, 454.621, 97.527)
		SetBlipSprite (blip, 492)
		SetBlipDisplay(blip, 4)
		SetBlipScale  (blip, 1.0)
		SetBlipColour (blip, 4)
		SetBlipAsShortRange(blip, true)
		BeginTextCommandSetBlipName("STRING")
		AddTextComponentString("My Gang house")
		EndTextCommandSetBlipName(blip)	
	end
		
	if type == "house7" then	
		currhouse = type
		local blip = AddBlipForCoord(223.182, -176.176, 57.917)
		SetBlipSprite (blip, 492)
		SetBlipDisplay(blip, 4)
		SetBlipScale  (blip, 1.0)
		SetBlipColour (blip, 4)
		SetBlipAsShortRange(blip, true)
		BeginTextCommandSetBlipName("STRING")
		AddTextComponentString("My Gang house")
		EndTextCommandSetBlipName(blip)	
	end
	
	if type == "house8" then	
		currhouse = type
		local blip = AddBlipForCoord(-853.185, 695.749, 148.786)
		SetBlipSprite (blip, 492)
		SetBlipDisplay(blip, 4)
		SetBlipScale  (blip, 1.0)
		SetBlipColour (blip, 4)
		SetBlipAsShortRange(blip, true)
		BeginTextCommandSetBlipName("STRING")
		AddTextComponentString("My Gang house")
		EndTextCommandSetBlipName(blip)	
	end
	
	if type == "house9" then	
		currhouse = type
		local blip = AddBlipForCoord(-97.261, 988.158, 235.756)
		SetBlipSprite (blip, 492)
		SetBlipDisplay(blip, 4)
		SetBlipScale  (blip, 1.0)
		SetBlipColour (blip, 4)
		SetBlipAsShortRange(blip, true)
		BeginTextCommandSetBlipName("STRING")
		AddTextComponentString("My Gang house")
		EndTextCommandSetBlipName(blip)	
	end
	
	if type == "house10" then	
		currhouse = type
		local blip = AddBlipForCoord(-444.394, 343.162, 105.581)
		SetBlipSprite (blip, 492)
		SetBlipDisplay(blip, 4)
		SetBlipScale  (blip, 1.0)
		SetBlipColour (blip, 4)
		SetBlipAsShortRange(blip, true)
		BeginTextCommandSetBlipName("STRING")
		AddTextComponentString("My Gang house")
		EndTextCommandSetBlipName(blip)	
	end
	
	if type == "house11" then	
		currhouse = type
		local blip = AddBlipForCoord(-526.778, 517.118, 112.942)
		SetBlipSprite (blip, 492)
		SetBlipDisplay(blip, 4)
		SetBlipScale  (blip, 1.0)
		SetBlipColour (blip, 4)
		SetBlipAsShortRange(blip, true)
		BeginTextCommandSetBlipName("STRING")
		AddTextComponentString("My Gang house")
		EndTextCommandSetBlipName(blip)	
	end
	
	if type == "house12" then	
		currhouse = type
		local blip = AddBlipForCoord(-459.221, 537.119, 121.459)
		SetBlipSprite (blip, 492)
		SetBlipDisplay(blip, 4)
		SetBlipScale  (blip, 1.0)
		SetBlipColour (blip, 4)
		SetBlipAsShortRange(blip, true)
		BeginTextCommandSetBlipName("STRING")
		AddTextComponentString("My Gang house")
		EndTextCommandSetBlipName(blip)	
	end
	
	if type == "house13" then	
		currhouse = type
		local blip = AddBlipForCoord(-70.490, 359.242, 112.445)
		SetBlipSprite (blip, 492)
		SetBlipDisplay(blip, 4)
		SetBlipScale  (blip, 1.0)
		SetBlipColour (blip, 4)
		SetBlipAsShortRange(blip, true)
		BeginTextCommandSetBlipName("STRING")
		AddTextComponentString("My Gang house")
		EndTextCommandSetBlipName(blip)	
	end
	
	if type == "house14" then	
		currhouse = type
		local blip = AddBlipForCoord(-1580.164, -33.963, 57.565)
		SetBlipSprite (blip, 492)
		SetBlipDisplay(blip, 4)
		SetBlipScale  (blip, 1.0)
		SetBlipColour (blip, 4)
		SetBlipAsShortRange(blip, true)
		BeginTextCommandSetBlipName("STRING")
		AddTextComponentString("My Gang house")
		EndTextCommandSetBlipName(blip)	
	end
	
	if type == "house15" then	
		currhouse = type
		local blip = AddBlipForCoord(-1587.769, 15.749, 61.253)
		SetBlipSprite (blip, 492)
		SetBlipDisplay(blip, 4)
		SetBlipScale  (blip, 1.0)
		SetBlipColour (blip, 4)
		SetBlipAsShortRange(blip, true)
		BeginTextCommandSetBlipName("STRING")
		AddTextComponentString("My Gang house")
		EndTextCommandSetBlipName(blip)	
	end
end
RegisterNetEvent('stadus_gh:gangFightChat')
AddEventHandler('stadus_gh:gangFightChat',function(gang1, gang2)
    TriggerEvent('chatMessage', '', {255,255,255}, "^5Gang Fight: ^1"..gang1.."^2 vs ^3"..gang2..".")
	TriggerEvent('stadus:removecarspvp')
end)


RegisterNetEvent('stadus_gh:gangFightNow')
AddEventHandler('stadus_gh:gangFightNow',function(gang1s, gang2s, house1s, house2s, state)
	print("current state "..state)
	print("ghouse2 "..house2s)
	local neededResponse = ""
	print("ghouse1 "..house1s)
	print("fightmyGang "..myGang)
	print("myGangHouse "..myGanghouse)
	if myGang == gang1s or myGang == gang2s then
	if myGanghouse == house1s then
	vsGangHouse = house2s
	print("vsGangHouse1 "..vsGangHouse)
	else
	vsGangHouse = house1s
	print("vsGangHouse2 "..vsGangHouse)
	end
	else
	return
	end
ESX.TriggerServerCallback('stadus_gh:isPlayerInGangFight', function(response)

	if response == "yes" then
	if vsGangHouse == "house2" then	
	local x3 = 939.0 --house2 ENTER
	local y3 = -1877.0
	local z3 = 32.1		
	local blip2 = AddBlipForCoord(x3, y3, z3)
	SetBlipSprite (blip2, 437)
	SetBlipDisplay(blip2, 4)
	SetBlipScale  (blip2, 1.0)
	SetBlipColour (blip2, 49)
	SetBlipAsShortRange(blip2, true)
	BeginTextCommandSetBlipName("STRING")
	AddTextComponentString("Other Gang house")
	EndTextCommandSetBlipName(blip2)	
	end
		if vsGangHouse == "house3" then		
	local blip2 = AddBlipForCoord(-1472.662, -49.877, 54.638)
	SetBlipSprite (blip2, 437)
	SetBlipDisplay(blip2, 4)
	SetBlipScale  (blip2, 1.0)
	SetBlipColour (blip2, 49)
	SetBlipAsShortRange(blip2, true)
	BeginTextCommandSetBlipName("STRING")
	AddTextComponentString("Other Gang house")
	EndTextCommandSetBlipName(blip2)	
	end
	if vsGangHouse == "house1" then	

	local x = -683.8 --house1 ENTER
	local y = -1170.73
	local z = 10.11		
	local blip2 = AddBlipForCoord(x, y, z)
		SetBlipSprite (blip2, 437)
	SetBlipDisplay(blip2, 4)
		SetBlipScale  (blip2, 1.0)
		SetBlipColour (blip2, 49)
		SetBlipAsShortRange(blip2, true)
		BeginTextCommandSetBlipName("STRING")
		AddTextComponentString("Other Gang house")
		EndTextCommandSetBlipName(blip2)	
	end
	if vsGangHouse == "house4" then	

		local blip2 = AddBlipForCoord(-145.166, 885.909, 239.020)
			SetBlipSprite (blip2, 437)
		SetBlipDisplay(blip2, 4)
			SetBlipScale  (blip2, 1.0)
			SetBlipColour (blip2, 49)
			SetBlipAsShortRange(blip2, true)
			BeginTextCommandSetBlipName("STRING")
			AddTextComponentString("Other Gang house")
			EndTextCommandSetBlipName(blip2)	
		end
		if vsGangHouse == "house5" then	

			local blip2 = AddBlipForCoord(-561.130, 402.777, 101.805)
				SetBlipSprite (blip2, 437)
			SetBlipDisplay(blip2, 4)
				SetBlipScale  (blip2, 1.0)
				SetBlipColour (blip2, 49)
				SetBlipAsShortRange(blip2, true)
				BeginTextCommandSetBlipName("STRING")
				AddTextComponentString("Other Gang house")
				EndTextCommandSetBlipName(blip2)	
			end

			if vsGangHouse == "house6" then	

				local blip2 = AddBlipForCoord(-1294.343, 454.621, 97.527)
					SetBlipSprite (blip2, 437)
				SetBlipDisplay(blip2, 4)
					SetBlipScale  (blip2, 1.0)
					SetBlipColour (blip2, 49)
					SetBlipAsShortRange(blip2, true)
					BeginTextCommandSetBlipName("STRING")
					AddTextComponentString("Other Gang house")
					EndTextCommandSetBlipName(blip2)	
				end
				if vsGangHouse == "house7" then	

				local blip2 = AddBlipForCoord(223.182, -176.176, 57.917)
					SetBlipSprite (blip2, 437)
				SetBlipDisplay(blip2, 4)
					SetBlipScale  (blip2, 1.0)
					SetBlipColour (blip2, 49)
					SetBlipAsShortRange(blip2, true)
					BeginTextCommandSetBlipName("STRING")
					AddTextComponentString("Other Gang house")
					EndTextCommandSetBlipName(blip2)	
				end
				if vsGangHouse == "house8" then	

				local blip2 = AddBlipForCoord(-853.185, 695.749, 148.786)
					SetBlipSprite (blip2, 437)
				SetBlipDisplay(blip2, 4)
					SetBlipScale  (blip2, 1.0)
					SetBlipColour (blip2, 49)
					SetBlipAsShortRange(blip2, true)
					BeginTextCommandSetBlipName("STRING")
					AddTextComponentString("Other Gang house")
					EndTextCommandSetBlipName(blip2)	
				end
				if vsGangHouse == "house9" then	

				local blip2 = AddBlipForCoord(-97.261, 988.158, 235.756)
					SetBlipSprite (blip2, 437)
				SetBlipDisplay(blip2, 4)
					SetBlipScale  (blip2, 1.0)
					SetBlipColour (blip2, 49)
					SetBlipAsShortRange(blip2, true)
					BeginTextCommandSetBlipName("STRING")
					AddTextComponentString("Other Gang house")
					EndTextCommandSetBlipName(blip2)	
				end
				if vsGangHouse == "house10" then	

				local blip2 = AddBlipForCoord(-444.394, 343.162, 105.581)
					SetBlipSprite (blip2, 437)
				SetBlipDisplay(blip2, 4)
					SetBlipScale  (blip2, 1.0)
					SetBlipColour (blip2, 49)
					SetBlipAsShortRange(blip2, true)
					BeginTextCommandSetBlipName("STRING")
					AddTextComponentString("Other Gang house")
					EndTextCommandSetBlipName(blip2)	
				end
				if vsGangHouse == "house11" then	

				local blip2 = AddBlipForCoord(-526.778, 517.118, 112.942)
					SetBlipSprite (blip2, 437)
				SetBlipDisplay(blip2, 4)
					SetBlipScale  (blip2, 1.0)
					SetBlipColour (blip2, 49)
					SetBlipAsShortRange(blip2, true)
					BeginTextCommandSetBlipName("STRING")
					AddTextComponentString("Other Gang house")
					EndTextCommandSetBlipName(blip2)	
				end
				if vsGangHouse == "house12" then	

				local blip2 = AddBlipForCoord(-459.221, 537.119, 121.459)
					SetBlipSprite (blip2, 437)
				SetBlipDisplay(blip2, 4)
					SetBlipScale  (blip2, 1.0)
					SetBlipColour (blip2, 49)
					SetBlipAsShortRange(blip2, true)
					BeginTextCommandSetBlipName("STRING")
					AddTextComponentString("Other Gang house")
					EndTextCommandSetBlipName(blip2)	
				end
				if vsGangHouse == "house13" then	

				local blip2 = AddBlipForCoord(-70.490, 359.242, 112.445)
					SetBlipSprite (blip2, 437)
				SetBlipDisplay(blip2, 4)
					SetBlipScale  (blip2, 1.0)
					SetBlipColour (blip2, 49)
					SetBlipAsShortRange(blip2, true)
					BeginTextCommandSetBlipName("STRING")
					AddTextComponentString("Other Gang house")
					EndTextCommandSetBlipName(blip2)	
				end
				if vsGangHouse == "house14" then	

				local blip2 = AddBlipForCoord(-1580.164, -33.963, 57.565)
					SetBlipSprite (blip2, 437)
				SetBlipDisplay(blip2, 4)
					SetBlipScale  (blip2, 1.0)
					SetBlipColour (blip2, 49)
					SetBlipAsShortRange(blip2, true)
					BeginTextCommandSetBlipName("STRING")
					AddTextComponentString("Other Gang house")
					EndTextCommandSetBlipName(blip2)	
				end
				if vsGangHouse == "house15" then	

				local blip2 = AddBlipForCoord(-1587.769, 15.749, 61.253)
					SetBlipSprite (blip2, 437)
				SetBlipDisplay(blip2, 4)
					SetBlipScale  (blip2, 1.0)
					SetBlipColour (blip2, 49)
					SetBlipAsShortRange(blip2, true)
					BeginTextCommandSetBlipName("STRING")
					AddTextComponentString("Other Gang house")
					EndTextCommandSetBlipName(blip2)	
				end
	else
	return
	end	
	if state == "off" then
		if DoesBlipExist(blip2) then
	RemoveBlip(blip2)
	print("RemoveBlip")
		else
			print("BLIP NOT EXIST")
end

end
		end, myGang)
end)



-- FUNCITON OpenShopMenu has the WEAPON CONFIG --
-- 
-------------------------------------------------
		
		local pressedyet = false
		
Citizen.CreateThread(
	function()
		while true do
		Citizen.Wait(1)
			
					local x = -683.8 --house1 ENTER
					local y = -1170.73
					local z = 10.11		

					local x2 = 997.58 -- house1 EXIT
					local y2 = -3158.0
					local z2 = -39.91
					
			local playerPos = GetEntityCoords(GetPlayerPed(-1), true)
			
			if (Vdist(playerPos.x, playerPos.y, playerPos.z, x, y, z) < 20.0) then
				DrawMarker(2, x, y, z, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, x, y, z) < 1.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to get into ~g~gang house")
						if IsControlPressed(0,  38) and (GetGameTimer() - Time) > 300 then
						if pressedyet == false then
							pressedyet = true	
						if myGanghouse == "house1" then
							ESX.ShowNotification("Welcome back!")
							tpInHouse1()	
							Time      = GetGameTimer()
						elseif vsGangHouse == "house1" then	
													ESX.ShowNotification("Welcome back!")
							tpInHouse1()	
							Time      = GetGameTimer()
						elseif PlayerData.job ~= nil and PlayerData.job.name == "police" then	
							ESX.TriggerServerCallback('esx_ambulancejob:getItemAmount', function(qtty)
							if qtty > 0 then
							tpInHouse1()
							TriggerServerEvent('esx_policejob:removewarrant', GetPlayerServerId(PlayerId()), "house1")
							end 
							end, 'warrant1')
							else
							ESX.ShowNotification("You don't own this property")
						end
					end
				end
			end
			end
			
			if (Vdist(playerPos.x, playerPos.y, playerPos.z, -667.16, -1186.82, 9.61) < 20.0) and VehicleSpawnerHouse == "house1" then			-- vehicle deleter house 1
				DrawMarker(1, -667.16, -1186.82, 9.61, 0, 0, 0, 0, 0, 0, 3.0001, 3.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -667.16, -1186.82, 9.61) < 5.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to remove your ~r~vehicle~s~")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							    local playerPed = GetPlayerPed(-1)
								local vehicle = GetVehiclePedIsIn(playerPed, false)
								ESX.Game.DeleteVehicle(vehicle)
							
					end
				end
			end		

				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -679.49, -1172.54, 10.61) < 20.0) and VehicleSpawnerHouse == "house1" then			-- vehicle spawner house 1
				DrawMarker(1, -679.49, -1172.54, 9.61, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -679.49, -1172.54, 10.61) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to get a ~y~Gang House Car~s~")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							if VehicleSpawnerHouse == "house1" then
							OpenVehicleSpawnerMenu()
							else
							print("VehicleSpawnerHouse IS NULL")
						end
					end
				end
			end
			
			if (Vdist(playerPos.x, playerPos.y, playerPos.z, x2, y2, z2) < 20.0) then			
				DrawMarker(1, x2, y2, z2, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, x2, y2, z2) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to get out of the ~g~gang house")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							tpOutHouse1()
					end
				end
			end
			
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 998.09, -3164.8, -34.07) < 20.0) then			-- HOUSE 1 ACCOUNT
				DrawMarker(1, 998.09, -3164.8, -35.07, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 998.09, -3164.8, -34.07) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to check the ~g~gang house ~y~stash")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							          OpenGangAccounts()
					end
				end
			end
			
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 1007.58, -3164.72, -34.67) < 20.0) then			-- HOUSE 1 Leader menu
				DrawMarker(1, 1007.58, -3164.72, -35.27, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 1007.58, -3164.72, -34.67) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to ~r~access~s~ the ~y~leader menu")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							          OpenLeaderMenu()
					end
				end
			end			
	
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 1008.71, -3171.19, -38.87) < 20.0) then			-- HOUSE 1 Weapon Stock menu
				DrawMarker(1, 1008.71, -3171.19, -39.87, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 1008.71, -3171.19, -38.87) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to ~r~access~s~ the ~y~weapon stock")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							          OpenGangHouseMenu()
					end
				end
			end
	
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 1009.17, -3157.51, -38.91) < 20.0) then			-- HOUSE 1 Flag
				DrawMarker(23, 1009.17, -3157.51, -39.81, 0, 0, 0, 0, 0, 0, 3.0001, 3.0001, 2.0001, 255, 0, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 1009.17, -3157.51, -38.91) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to ~r~pick up~s~ the ~y~flag")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
									OpenFlagMenu()
					end
				end
			end
			
						local x3 = 939.0 --house2 ENTER
						local y3 = -1877.0
						local z3 = 32.1		
					
					local x4 = 1120.95 -- house2 EXIT
					local y4 = -3152.31
					local z4 = -38.1
		
			if (Vdist(playerPos.x, playerPos.y, playerPos.z, x3, y3, z3) < 20.0) then
				DrawMarker(0, x3, y3, z3, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, x3, y3, z3) < 1.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to get into ~g~gang house")
						if IsControlPressed(0,  38) and (GetGameTimer() - Time) > 300 then
											if pressedyet == false then
							pressedyet = true
						if myGanghouse == "house2" then
							ESX.ShowNotification("Welcome back!")
							tpInHouse2()	
							Time      = GetGameTimer()
						elseif vsGangHouse == "house2" then	
													ESX.ShowNotification("Welcome back!")
							tpInHouse2()	
							Time      = GetGameTimer()
						elseif PlayerData.job ~= nil and PlayerData.job.name == "police" then	
							ESX.TriggerServerCallback('esx_ambulancejob:getItemAmount', function(qtty)
							if qtty > 0 then
							tpInHouse2()
							TriggerServerEvent('esx_policejob:removewarrant', GetPlayerServerId(PlayerId()), "house2")
							end 
							end, 'warrant2')
														else
							ESX.ShowNotification("You don't own this property")
						end
					end
				end
			end
		end

			if (Vdist(playerPos.x, playerPos.y, playerPos.z, x4, y4, z4) < 20.0) then			
				DrawMarker(1, x4, y4, z4, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, x4, y4, z4) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to get out of the ~g~gang house")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)          
							tpOutHouse2()
					end
				end
			end
			if (Vdist(playerPos.x, playerPos.y, playerPos.z, 944.54, -1871.64, 31.02) < 20.0) and VehicleSpawnerHouse == "house2" then			-- vehicle deleter house 2
				DrawMarker(1, 944.54, -1871.64, 30.02, 0, 0, 0, 0, 0, 0, 3.0001, 3.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 944.54, -1871.64, 31.02) < 5.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to remove your ~r~vehicle~s~")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							    local playerPed = GetPlayerPed(-1)
								local vehicle = GetVehiclePedIsIn(playerPed, false)
								ESX.Game.DeleteVehicle(vehicle)
							
					end
				end
			end		

				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 938.9, -1886.9, 31.14) < 20.0) and VehicleSpawnerHouse == "house2" then			-- vehicle spawner house 2
				DrawMarker(1, 938.9, -1886.9, 30.14, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 938.9, -1886.9, 31.14) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to get a ~y~Gang House Car~s~")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							if VehicleSpawnerHouse == "house2" then
							OpenVehicleSpawnerMenu()
							else
							print("VehicleSpawnerHouse IS NULL")
						end
					end
				end
			end
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 1115.61, -3161.8, -36.87) < 20.0) then			-- HOUSE 2 ACCOUNT
				DrawMarker(1, 1115.61, -3161.8, -37.87, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 1115.61, -3161.8, -36.87) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to check the ~g~gang house ~y~stash")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							          OpenGangAccounts()
						end
					end
				end
			
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 1111.42, -3152.72, -38.87) < 20.0) then			-- HOUSE 2 Leader menu
				DrawMarker(1, 1111.42, -3152.72, -38.67, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 1111.42, -3152.72, -38.87) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to ~r~access~s~ the ~y~leader menu")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							          OpenLeaderMenu()
							end
						end
					end			
	
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 1121.98, -3162.13, -36.87) < 20.0) then			-- HOUSE 2 Weapon Stock menu
				DrawMarker(1, 1121.98, -3162.13, -37.87, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 1121.98, -3162.13, -36.87) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to ~r~access~s~ the ~y~weapon stock")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							          OpenGangHouseMenu()
							end
						end
					end
	
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 1102.47, -3154.51, -37.52) < 20.0) then			-- HOUSE 2 flag menu
				DrawMarker(23, 1102.47, -3154.51, -38.42, 0, 0, 0, 0, 0, 0, 3.0001, 3.0001, 2.0001, 255, 0, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 1102.47, -3154.51, -37.52) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to ~r~pick up~s~ the ~y~flag")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
									OpenFlagMenu()
					end
				end
			end
	if (Vdist(playerPos.x, playerPos.y, playerPos.z, -1472.662, -49.877, 54.638) < 20.0) then -- house3
				DrawMarker(0, -1472.662, -49.877, 54.638, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -1472.662, -49.877, 54.638) < 2.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to get into ~g~gang house")
						if IsControlPressed(0,  38) and (GetGameTimer() - Time) > 300 then
											if pressedyet == false then
							pressedyet = true
						if myGanghouse == "house3" then
							ESX.ShowNotification("Welcome back!")
							tpInHouse3()	
							Time      = GetGameTimer()
						elseif vsGangHouse == "house3" then	
													ESX.ShowNotification("Welcome back!")
							tpInHouse3()	
							Time      = GetGameTimer()
						elseif PlayerData.job ~= nil and PlayerData.job.name == "police" then	
							ESX.TriggerServerCallback('esx_ambulancejob:getItemAmount', function(qtty)
							if qtty > 0 then
							tpInHouse3()
							TriggerServerEvent('esx_policejob:removewarrant', GetPlayerServerId(PlayerId()), "house3")
							end 
							end, 'warrant3')
														else
							ESX.ShowNotification("You don't own this property")
						end
					end
				end
			end
		end
					if (Vdist(playerPos.x, playerPos.y, playerPos.z, 894.477, -3245.821, -98.258) < 20.0) then			-- house3
				DrawMarker(1, 894.477, -3245.821, -99.258, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 894.477, -3245.821, -98.258) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to get out of the ~g~gang house")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)          
							tpOutHouse3()
					end
				end
			end

			if (Vdist(playerPos.x, playerPos.y, playerPos.z, -1462.453, -12.128, 53.500) < 20.0) and VehicleSpawnerHouse == "house3" then			-- vehicle deleter house 3
				DrawMarker(1, -1462.453, -12.128, 53.500, 0, 0, 0, 0, 0, 0, 3.0001, 3.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -1462.453, -12.128, 53.500) < 5.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to remove your ~r~vehicle~s~")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							    local playerPed = GetPlayerPed(-1)
								local vehicle = GetVehiclePedIsIn(playerPed, false)
								ESX.Game.DeleteVehicle(vehicle)
							
					end
				end
			end		

				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -1464.814, -34.026, 54.200) < 20.0) and VehicleSpawnerHouse == "house3" then			-- vehicle spawner house 3
				DrawMarker(1, -1464.814, -34.026, 54.200, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -1464.814, -34.026, 54.200) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to get a ~y~Gang House Car~s~")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							if VehicleSpawnerHouse == "house3" then
							OpenVehicleSpawnerMenu()
							else
							print("VehicleSpawnerHouse IS NULL")
						end
					end
				end
			end

						if (Vdist(playerPos.x, playerPos.y, playerPos.z, 906.26, -3207.09, -98.19) < 20.0) then			-- HOUSE 3 ACCOUNT
				DrawMarker(1, 906.26, -3207.09, -98.19, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 906.26, -3207.09, -98.19) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to check the ~g~gang house ~y~stash")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							          OpenGangAccounts()
						end
					end
				end
			
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 916.369, -3221.514, -99.010) < 20.0) then			-- HOUSE 3 Leader menu
				DrawMarker(1, 916.369, -3221.514, -99.010, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 916.369, -3221.514, -99.010) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to ~r~access~s~ the ~y~leader menu")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							          OpenLeaderMenu()
							end
						end
					end			
	
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 890.35, -3210.8, -99.01) < 20.0) then			-- HOUSE 3 Weapon Stock menu
				DrawMarker(1, 890.35, -3210.8, -99.01, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 890.35, -3210.8, -99.01) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to ~r~access~s~ the ~y~weapon stock")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							          OpenGangHouseMenu()
							end
						end
					end
	
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 866.63, -3187.19, -96.85) < 40.0) then			-- HOUSE 3 flag menu
				DrawMarker(23, 866.63, -3187.19, -97.15, 0, 0, 0, 0, 0, 0, 3.0001, 3.0001, 2.0001, 255, 0, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 866.63, -3187.19, -96.45) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to ~r~pick up~s~ the ~y~flag")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
									OpenFlagMenu()
					end
				end
			end


			if (Vdist(playerPos.x, playerPos.y, playerPos.z, -145.166, 885.909, 239.020) < 20.0) then -- house4
				DrawMarker(0, -145.166, 885.909, 239.020, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -145.166, 885.909, 239.020) < 2.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to get into ~g~gang house")
						if IsControlPressed(0,  38) and (GetGameTimer() - Time) > 300 then
											if pressedyet == false then
							pressedyet = true
						if myGanghouse == "house4" then
							ESX.ShowNotification("Welcome back!")
							tpInHouse4()	
							Time      = GetGameTimer()
						elseif vsGangHouse == "house4" then	
													ESX.ShowNotification("Welcome back!")
							tpInHouse4()	
							Time      = GetGameTimer()
						elseif PlayerData.job ~= nil and PlayerData.job.name == "police" then	
							ESX.TriggerServerCallback('esx_ambulancejob:getItemAmount', function(qtty)
							if qtty > 0 then
								tpInHouse4()
							TriggerServerEvent('esx_policejob:removewarrant', GetPlayerServerId(PlayerId()), "house4")
							end 
							end, 'warrant4')
														else
							ESX.ShowNotification("You don't own this property")
						end
					end
				end
			end
		end
					if (Vdist(playerPos.x, playerPos.y, playerPos.z, 970.908, -2987.682, -39.646) < 20.0) then			-- house4
				DrawMarker(1, 970.908, -2987.682, -40.646, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 970.908, -2987.682, -39.646) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to get out of the ~g~gang house")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)          
							tpOutHouse4()
					end
				end
			end

			if (Vdist(playerPos.x, playerPos.y, playerPos.z, -136.920, 902.755, 235.690) < 20.0) and VehicleSpawnerHouse == "house4" then			-- vehicle deleter house 4
				DrawMarker(1, -136.920, 902.755, 235.690, 0, 0, 0, 0, 0, 0, 3.0001, 3.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -136.920, 902.755, 235.690) < 5.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to remove your ~r~vehicle~s~")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							    local playerPed = GetPlayerPed(-1)
								local vehicle = GetVehiclePedIsIn(playerPed, false)
								ESX.Game.DeleteVehicle(vehicle)
							
					end
				end
			end		

				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -134.330, 896.347, 235.657) < 20.0) and VehicleSpawnerHouse == "house4" then			-- vehicle spawner house 4
				DrawMarker(1, -134.330, 896.347, 235.657, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -134.330, 896.347, 235.657) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to get a ~y~Gang House Car~s~")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							if VehicleSpawnerHouse == "house4" then
							OpenVehicleSpawnerMenu()
							else
							print("VehicleSpawnerHouse IS NULL")
						end
					end
				end
			end

						if (Vdist(playerPos.x, playerPos.y, playerPos.z, 964.875, -3003.302, -39.639) < 20.0) then			-- HOUSE 4 ACCOUNT
				DrawMarker(1, 964.875, -3003.302, -40.639, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 964.875, -3003.302, -39.639) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to check the ~g~gang house ~y~stash")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							          OpenGangAccounts()
						end
					end
				end
			
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 959.149, -3005.597, -39.639) < 20.0) then			-- HOUSE 4 Leader menu
				DrawMarker(1, 959.149, -3005.597, -40.639, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 959.149, -3005.597, -39.639) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to ~r~access~s~ the ~y~leader menu")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							          OpenLeaderMenu()
							end
						end
					end			
	
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 962.546, -2994.020, -39.646) < 20.0) then			-- HOUSE 4 Weapon Stock menu
				DrawMarker(1, 962.546, -2994.020, -40.646, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 962.546, -2994.020, -39.646) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to ~r~access~s~ the ~y~weapon stock")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							          OpenGangHouseMenu()
							end
						end
					end
	
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 954.938, -2990.780, -39.646) < 40.0) then			-- HOUSE 4 flag menu
				DrawMarker(23, 954.938, -2990.780, -40.546, 0, 0, 0, 0, 0, 0, 3.0001, 3.0001, 2.0001, 255, 0, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 954.938, -2990.780, -39.646) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to ~r~pick up~s~ the ~y~flag")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
									OpenFlagMenu()
					end
				end
			end

						if (Vdist(playerPos.x, playerPos.y, playerPos.z, -561.130, 402.777, 101.805) < 20.0) then -- house5
				DrawMarker(0, -561.130, 402.777, 101.805, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -561.130, 402.777, 101.805) < 2.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to get into ~g~gang house")
						if IsControlPressed(0,  38) and (GetGameTimer() - Time) > 300 then
											if pressedyet == false then
							pressedyet = true
						if myGanghouse == "house5" then
							ESX.ShowNotification("Welcome back!")
							tpInHouse5()	
							Time      = GetGameTimer()
						elseif vsGangHouse == "house5" then	
													ESX.ShowNotification("Welcome back!")
							tpInHouse5()	
							Time      = GetGameTimer()
						elseif PlayerData.job ~= nil and PlayerData.job.name == "police" then	
							ESX.TriggerServerCallback('esx_ambulancejob:getItemAmount', function(qtty)
							if qtty > 0 then
								tpInHouse5()
							TriggerServerEvent('esx_policejob:removewarrant', GetPlayerServerId(PlayerId()), "house5")
							end 
							end, 'warrant5')
														else
							ESX.ShowNotification("You don't own this property")
						end
					end
				end
			end
		end
					if (Vdist(playerPos.x, playerPos.y, playerPos.z, -174.05, 496.8, 137.67) < 20.0) then			-- house5
				DrawMarker(1, -174.05, 496.8, 136.67, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -174.05, 496.8, 137.67) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to get out of the ~g~gang house")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)          
							tpOutHouse5()
					end
				end
			end

			if (Vdist(playerPos.x, playerPos.y, playerPos.z, -577.720, 405.192, 100.661) < 20.0) and VehicleSpawnerHouse == "house5" then			-- vehicle deleter house 5
				DrawMarker(1, -577.720, 405.192, 100.661, 0, 0, 0, 0, 0, 0, 3.0001, 3.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -577.720, 405.192, 100.661) < 5.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to remove your ~r~vehicle~s~")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							    local playerPed = GetPlayerPed(-1)
								local vehicle = GetVehiclePedIsIn(playerPed, false)
								ESX.Game.DeleteVehicle(vehicle)
							
					end
				end
			end		

				if (Vdist(playerPos.x, playerPos.y, playerPos.z,  -566.722, 402.517, 100.662) < 20.0) and VehicleSpawnerHouse == "house5" then			-- vehicle spawner house 5
				DrawMarker(1,  -566.722, 402.517, 100.662, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z,  -566.722, 402.517, 100.662) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to get a ~y~Gang House Car~s~")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							if VehicleSpawnerHouse == "house5" then
							OpenVehicleSpawnerMenu()
							else
							print("VehicleSpawnerHouse IS NULL")
						end
					end
				end
			end

						if (Vdist(playerPos.x, playerPos.y, playerPos.z, -167.350, 487.899, 133.843) < 20.0) then			-- HOUSE 5 ACCOUNT
				DrawMarker(1, -167.350, 487.899, 132.843, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -167.350, 487.899, 133.843) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to check the ~g~gang house ~y~stash")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							          OpenGangAccounts()
						end
					end
				end
			
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -170.860, 500.286, 130.043) < 20.0) then			-- HOUSE 5 Leader menu
				DrawMarker(1, -170.860, 500.286, 129.043, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -170.860, 500.286, 130.043) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to ~r~access~s~ the ~y~leader menu")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							          OpenLeaderMenu()
							end
						end
					end			
	
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -168.394, 482.550, 133.878) < 20.0) then			-- HOUSE 5 Weapon Stock menu
				DrawMarker(1, -168.394, 482.550, 132.878, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -168.394, 482.550, 133.878) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to ~r~access~s~ the ~y~weapon stock")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							          OpenGangHouseMenu()
							end
						end
					end
	
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -173.296, 491.716, 130.043) < 40.0) then			-- HOUSE 5 flag menu
				DrawMarker(23, -173.296, 491.716, 129.143, 0, 0, 0, 0, 0, 0, 3.0001, 3.0001, 2.0001, 255, 0, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -173.296, 491.716, 130.043) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to ~r~pick up~s~ the ~y~flag")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
									OpenFlagMenu()
					end
				end
			end

			if (Vdist(playerPos.x, playerPos.y, playerPos.z, -1294.343, 454.621, 97.527) < 20.0) then -- house6
				DrawMarker(0, -1294.343, 454.621, 97.527, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -1294.343, 454.621, 97.527) < 2.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to get into ~g~gang house")
						if IsControlPressed(0,  38) and (GetGameTimer() - Time) > 300 then
											if pressedyet == false then
							pressedyet = true
						if myGanghouse == "house6" then
							ESX.ShowNotification("Welcome back!")
							tpInHouse6()	
							Time      = GetGameTimer()
						elseif vsGangHouse == "house6" then	
													ESX.ShowNotification("Welcome back!")
							tpInHouse6()	
							Time      = GetGameTimer()
						elseif PlayerData.job ~= nil and PlayerData.job.name == "police" then	
							ESX.TriggerServerCallback('esx_ambulancejob:getItemAmount', function(qtty)
							if qtty > 0 then
								tpInHouse6()
							TriggerServerEvent('esx_policejob:removewarrant', GetPlayerServerId(PlayerId()), "house6")
							end 
							end, 'warrant6')
														else
							ESX.ShowNotification("You don't own this property")
						end
					end
				end
			end
		end
					if (Vdist(playerPos.x, playerPos.y, playerPos.z, -1289.711, 449.046, 97.902) < 20.0) then			-- house6
				DrawMarker(1, -1289.711, 449.046, 96.902, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -1289.711, 449.046, 97.902) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to get out of the ~g~gang house")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)          
							tpOutHouse6()
					end
				end
			end

			if (Vdist(playerPos.x, playerPos.y, playerPos.z, -1297.998, 470.548, 97.853) < 20.0) and VehicleSpawnerHouse == "house6" then			-- vehicle deleter house 6
				DrawMarker(1, -1297.998, 470.548, 96.853, 0, 0, 0, 0, 0, 0, 3.0001, 3.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -1297.998, 470.548, 97.853) < 5.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to remove your ~r~vehicle~s~")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							    local playerPed = GetPlayerPed(-1)
								local vehicle = GetVehiclePedIsIn(playerPed, false)
								ESX.Game.DeleteVehicle(vehicle)
							
					end
				end
			end		

				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -1300.127, 454.551, 97.631) < 20.0) and VehicleSpawnerHouse == "house6" then			-- vehicle spawner house 6
				DrawMarker(1, -1300.127, 454.551, 96.631, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -1300.127, 454.551, 97.631) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to get a ~y~Gang House Car~s~")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							if VehicleSpawnerHouse == "house6" then
							OpenVehicleSpawnerMenu()
							else
							print("VehicleSpawnerHouse IS NULL")
						end
					end
				end
			end

						if (Vdist(playerPos.x, playerPos.y, playerPos.z, -1286.117, 438.181, 94.094) < 20.0) then			-- HOUSE 6 ACCOUNT
				DrawMarker(1, -1286.117, 438.181, 93.094, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -1286.117, 438.181, 94.094) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to check the ~g~gang house ~y~stash")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							          OpenGangAccounts()
						end
					end
				end
			
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -1291.056, 451.140, 90.294) < 20.0) then			-- HOUSE 6 Leader menu
				DrawMarker(1, -1291.056, 451.140, 89.294, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -1291.056, 451.140, 90.294) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to ~r~access~s~ the ~y~leader menu")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							          OpenLeaderMenu()
							end
						end
					end			
	
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -1288.182, 432.973, 94.129) < 20.0) then			-- HOUSE 6 Weapon Stock menu
				DrawMarker(1, -1288.182, 432.973, 93.129, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -1288.182, 432.973, 94.129) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to ~r~access~s~ the ~y~weapon stock")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							          OpenGangHouseMenu()
							end
						end
					end
	
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -1287.861, 458.066, 90.294) < 40.0) then			-- HOUSE 6 flag menu
				DrawMarker(23, -1287.861, 458.066, 89.394, 0, 0, 0, 0, 0, 0, 3.0001, 3.0001, 2.0001, 255, 0, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -1287.861, 458.066, 90.294) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to ~r~pick up~s~ the ~y~flag")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
									OpenFlagMenu()
					end
				end
			end
		
		if (Vdist(playerPos.x, playerPos.y, playerPos.z, 223.182, -176.176, 57.917) < 20.0) then -- house7
				DrawMarker(0, 223.182, -176.176, 57.917, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 223.182, -176.176, 57.917) < 2.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to get into ~g~gang house")
						if IsControlPressed(0,  38) and (GetGameTimer() - Time) > 300 then
											if pressedyet == false then
							pressedyet = true
						if myGanghouse == "house7" then
							ESX.ShowNotification("Welcome back!")
							tpInHouse7()	
							Time      = GetGameTimer()
						elseif vsGangHouse == "house7" then	
													ESX.ShowNotification("Welcome back!")
							tpInHouse7()	
							Time      = GetGameTimer()
						elseif PlayerData.job ~= nil and PlayerData.job.name == "police" then	
							ESX.TriggerServerCallback('esx_ambulancejob:getItemAmount', function(qtty)
							if qtty > 0 then
								tpInHouse7()
							TriggerServerEvent('esx_policejob:removewarrant', GetPlayerServerId(PlayerId()), "house7")
							end 
							end, 'warrant7')
														else
							ESX.ShowNotification("You don't own this property")
						end
					end
				end
			end
		end
					if (Vdist(playerPos.x, playerPos.y, playerPos.z, -1451.460, -523.948, 56.929) < 20.0) then			-- house7
				DrawMarker(1, -1451.460, -523.948, 55.929, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -1451.460, -523.948, 56.929) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to get out of the ~g~gang house")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)          
							tpOutHouse7()
					end
				end
			end

			if (Vdist(playerPos.x, playerPos.y, playerPos.z, 215.15, -167.7, 56.853) < 20.0) and VehicleSpawnerHouse == "house7" then			-- vehicle deleter house 7
				DrawMarker(1, 215.15, -167.7, 55.853, 0, 0, 0, 0, 0, 0, 3.0001, 3.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 215.15, -167.7, 56.853) < 5.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to remove your ~r~vehicle~s~")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							    local playerPed = GetPlayerPed(-1)
								local vehicle = GetVehiclePedIsIn(playerPed, false)
								ESX.Game.DeleteVehicle(vehicle)
							
					end
				end
			end		

				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 210.02, -167.42, 56.34) < 20.0) and VehicleSpawnerHouse == "house7" then			-- vehicle spawner house 7
				DrawMarker(1, 210.02, -167.42, 55.34, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 210.02, -167.42, 56.34) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to get a ~y~Gang House Car~s~")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							if VehicleSpawnerHouse == "house7" then
							OpenVehicleSpawnerMenu()
							else
							print("VehicleSpawnerHouse IS NULL")
						end
					end
				end
			end

						if (Vdist(playerPos.x, playerPos.y, playerPos.z, -1474.103, -529.948, 50.721) < 20.0) then			-- HOUSE 7 ACCOUNT
				DrawMarker(1, -1474.103, -529.948, 49.721, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -1474.103, -529.948, 50.721) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to check the ~g~gang house ~y~stash")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							          OpenGangAccounts()
						end
					end
				end
			
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -1471.002, -538.639, 50.721) < 20.0) then			-- HOUSE 7 Leader menu
				DrawMarker(1, -1471.002, -538.639, 50.721, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -1471.002, -538.639, 50.721) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to ~r~access~s~ the ~y~leader menu")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							          OpenLeaderMenu()
							end
						end
					end			
	
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -1457.002, -530.170, 56.937) < 20.0) then			-- HOUSE 7 Weapon Stock menu
				DrawMarker(1, -1457.002, -530.170, 55.937, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -1457.002, -530.170, 56.937) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to ~r~access~s~ the ~y~weapon stock")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							          OpenGangHouseMenu()
							end
						end
					end
	
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -1467.759, -537.831, 50.732) < 40.0) then			-- HOUSE 7 flag menu
				DrawMarker(23, -1467.759, -537.831, 49.832, 0, 0, 0, 0, 0, 0, 3.0001, 3.0001, 2.0001, 255, 0, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -1467.759, -537.831, 50.732) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to ~r~pick up~s~ the ~y~flag")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
									OpenFlagMenu()
					end
				end
			end
			
			if (Vdist(playerPos.x, playerPos.y, playerPos.z, -853.185, 695.749, 148.786) < 20.0) then -- house8
				DrawMarker(0, -853.185, 695.749, 148.786, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -853.185, 695.749, 148.786) < 2.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to get into ~g~gang house")
						if IsControlPressed(0,  38) and (GetGameTimer() - Time) > 300 then
											if pressedyet == false then
							pressedyet = true
						if myGanghouse == "house8" then
							ESX.ShowNotification("Welcome back!")
							tpInHouse8()	
							Time      = GetGameTimer()
						elseif vsGangHouse == "house8" then	
													ESX.ShowNotification("Welcome back!")
							tpInHouse8()	
							Time      = GetGameTimer()
						elseif PlayerData.job ~= nil and PlayerData.job.name == "police" then	
							ESX.TriggerServerCallback('esx_ambulancejob:getItemAmount', function(qtty)
							if qtty > 0 then
								tpInHouse8()
							TriggerServerEvent('esx_policejob:removewarrant', GetPlayerServerId(PlayerId()), "house8")
							end 
							end, 'warrant8')
														else
							ESX.ShowNotification("You don't own this property")
						end
					end
				end
			end
		end
					if (Vdist(playerPos.x, playerPos.y, playerPos.z, -859.943, 690.973, 151.900) < 20.0) then			-- house8
				DrawMarker(1, -859.943, 690.973, 151.900, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -859.943, 690.973, 151.900) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to get out of the ~g~gang house")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)          
							tpOutHouse8()
					end
				end
			end

			if (Vdist(playerPos.x, playerPos.y, playerPos.z, -873.709, 698.049, 148.700) < 20.0) and VehicleSpawnerHouse == "house8" then			-- vehicle deleter house 8
				DrawMarker(1, -873.709, 698.049, 148.700, 0, 0, 0, 0, 0, 0, 3.0001, 3.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -873.709, 698.049, 148.700) < 5.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to remove your ~r~vehicle~s~")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							    local playerPed = GetPlayerPed(-1)
								local vehicle = GetVehiclePedIsIn(playerPed, false)
								ESX.Game.DeleteVehicle(vehicle)
							
					end
				end
			end		

				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -865.042, 696.856, 148.100) < 20.0) and VehicleSpawnerHouse == "house8" then			-- vehicle spawner house 8
				DrawMarker(1, -865.042, 696.856, 148.100, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -865.042, 696.856, 148.100) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to get a ~y~Gang House Car~s~")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							if VehicleSpawnerHouse == "house8" then
							OpenVehicleSpawnerMenu()
							else
							print("VehicleSpawnerHouse IS NULL")
						end
					end
				end
			end

						if (Vdist(playerPos.x, playerPos.y, playerPos.z, -855.326, 680.032, 148.100) < 20.0) then			-- HOUSE 8 ACCOUNT
				DrawMarker(1, -855.326, 680.032, 148.100, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -855.326, 680.032, 148.100) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to check the ~g~gang house ~y~stash")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							          OpenGangAccounts()
						end
					end
				end
			
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -851.487, 673.408, 148.100) < 20.0) then			-- HOUSE 8 Leader menu
				DrawMarker(1, -851.487, 673.408, 148.100, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -851.487, 673.408, 148.100) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to ~r~access~s~ the ~y~leader menu")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							          OpenLeaderMenu()
							end
						end
					end			
	
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -851.695, 674.261, 151.700) < 20.0) then			-- HOUSE 8 Weapon Stock menu
				DrawMarker(1, -851.695, 674.261, 151.700, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -851.695, 674.261, 151.700) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to ~r~access~s~ the ~y~weapon stock")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							          OpenGangHouseMenu()
							end
						end
					end
	
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -855.427, 684.500, 148.500) < 40.0) then			-- HOUSE 8 flag menu
				DrawMarker(23, -855.427, 684.500, 148.500, 0, 0, 0, 0, 0, 0, 3.0001, 3.0001, 2.0001, 255, 0, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -855.427, 684.500, 148.500) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to ~r~pick up~s~ the ~y~flag")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
									OpenFlagMenu()
					end
				end
			end
			
			--House9
			if (Vdist(playerPos.x, playerPos.y, playerPos.z, -97.261, 988.158, 235.756) < 20.0) then -- house9
				DrawMarker(0, -97.261, 988.158, 235.756, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -97.261, 988.158, 235.756) < 2.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to get into ~g~gang house")
						if IsControlPressed(0,  38) and (GetGameTimer() - Time) > 300 then
											if pressedyet == false then
							pressedyet = true
						if myGanghouse == "house9" then
							ESX.ShowNotification("Welcome back!")
							tpInHouse9()	
							Time      = GetGameTimer()
						elseif vsGangHouse == "house9" then	
													ESX.ShowNotification("Welcome back!")
							tpInHouse9()	
							Time      = GetGameTimer()
						elseif PlayerData.job ~= nil and PlayerData.job.name == "police" then	
							ESX.TriggerServerCallback('esx_ambulancejob:getItemAmount', function(qtty)
							if qtty > 0 then
								tpInHouse9()
							TriggerServerEvent('esx_policejob:removewarrant', GetPlayerServerId(PlayerId()), "house9")
							end 
							end, 'warrant9')
														else
							ESX.ShowNotification("You don't own this property")
						end
					end
				end
			end
		end
					if (Vdist(playerPos.x, playerPos.y, playerPos.z, -785.083, 323.693, 211.100) < 20.0) then			-- house9
				DrawMarker(1, -785.083, 323.693, 211.100, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -785.083, 323.693, 211.100) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to get out of the ~g~gang house")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)          
							tpOutHouse9()
					end
				end
			end

			if (Vdist(playerPos.x, playerPos.y, playerPos.z, -127.430, 1007.208, 234.900) < 20.0) and VehicleSpawnerHouse == "house9" then			-- vehicle deleter house 9
				DrawMarker(1, -127.430, 1007.208, 234.900, 0, 0, 0, 0, 0, 0, 3.0001, 3.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -127.430, 1007.208, 234.900) < 5.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to remove your ~r~vehicle~s~")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							    local playerPed = GetPlayerPed(-1)
								local vehicle = GetVehiclePedIsIn(playerPed, false)
								ESX.Game.DeleteVehicle(vehicle)
							
					end
				end
			end		

				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -111.479, 1000.306, 234.900) < 20.0) and VehicleSpawnerHouse == "house9" then			-- vehicle spawner house 9
				DrawMarker(1, -111.479, 1000.306, 234.900, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -111.479, 1000.306, 234.900) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to get a ~y~Gang House Car~s~")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							if VehicleSpawnerHouse == "house9" then
							OpenVehicleSpawnerMenu()
							else
							print("VehicleSpawnerHouse IS NULL")
						end
					end
				end
			end

						if (Vdist(playerPos.x, playerPos.y, playerPos.z, -793.416, 325.708, 209.400) < 20.0) then			-- HOUSE 9 ACCOUNT
				DrawMarker(1, -793.416, 325.708, 209.400, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -793.416, 325.708, 209.400) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to check the ~g~gang house ~y~stash")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							          OpenGangAccounts()
						end
					end
				end
			
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -793.943, 335.690, 209.400) < 20.0) then			-- HOUSE 9 Leader menu
				DrawMarker(1, -793.943, 335.690, 209.400, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -793.943, 335.690, 209.400) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to ~r~access~s~ the ~y~leader menu")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							          OpenLeaderMenu()
							end
						end
					end			
	
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -765.766, 327.880, 210.000) < 20.0) then			-- HOUSE 9 Weapon Stock menu
				DrawMarker(1, -765.766, 327.880, 210.000, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -765.766, 327.880, 210.000) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to ~r~access~s~ the ~y~weapon stock")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							          OpenGangHouseMenu()
							end
						end
					end
	
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -797.225, 330.869, 210.000) < 40.0) then			-- HOUSE 9 flag menu
				DrawMarker(23, -797.225, 330.869, 210.000, 0, 0, 0, 0, 0, 0, 3.0001, 3.0001, 2.0001, 255, 0, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -797.225, 330.869, 210.000) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to ~r~pick up~s~ the ~y~flag")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
									OpenFlagMenu()
					end
				end
			end
			
			--House10
			if (Vdist(playerPos.x, playerPos.y, playerPos.z, -444.394, 343.162, 105.581) < 20.0) then -- house10
				DrawMarker(0, -444.394, 343.162, 105.581, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -444.394, 343.162, 105.581) < 2.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to get into ~g~gang house")
						if IsControlPressed(0,  38) and (GetGameTimer() - Time) > 300 then
											if pressedyet == false then
							pressedyet = true
						if myGanghouse == "house10" then
							ESX.ShowNotification("Welcome back!")
							tpInHouse10()	
							Time      = GetGameTimer()
						elseif vsGangHouse == "house10" then	
													ESX.ShowNotification("Welcome back!")
							tpInHouse10()	
							Time      = GetGameTimer()
						elseif PlayerData.job ~= nil and PlayerData.job.name == "police" then	
							ESX.TriggerServerCallback('esx_ambulancejob:getItemAmount', function(qtty)
							if qtty > 0 then
								tpInHouse10()
							TriggerServerEvent('esx_policejob:removewarrant', GetPlayerServerId(PlayerId()), "house10")
							end 
							end, 'warrant10')
														else
							ESX.ShowNotification("You don't own this property")
						end
					end
				end
			end
		end
					if (Vdist(playerPos.x, playerPos.y, playerPos.z, 117.290, 559.914, 183.300) < 20.0) then			-- house10
				DrawMarker(1, 117.290, 559.914, 183.300, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 117.290, 559.914, 183.300) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to get out of the ~g~gang house")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)          
							tpOutHouse10()
					end
				end
			end

			if (Vdist(playerPos.x, playerPos.y, playerPos.z, -432.395, 343.007, 104.800) < 20.0) and VehicleSpawnerHouse == "house10" then			-- vehicle deleter house 10
				DrawMarker(1, -432.395, 343.007, 104.800, 0, 0, 0, 0, 0, 0, 3.0001, 3.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -432.395, 343.007, 104.800) < 5.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to remove your ~r~vehicle~s~")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							    local playerPed = GetPlayerPed(-1)
								local vehicle = GetVehiclePedIsIn(playerPed, false)
								ESX.Game.DeleteVehicle(vehicle)
							
					end
				end
			end		

				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -424.660, 342.341, 105.000) < 20.0) and VehicleSpawnerHouse == "house10" then			-- vehicle spawner house 10
				DrawMarker(1, -424.660, 342.341, 105.000, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -424.660, 342.341, 105.000) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to get a ~y~Gang House Car~s~")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							if VehicleSpawnerHouse == "house10" then
							OpenVehicleSpawnerMenu()
							else
							print("VehicleSpawnerHouse IS NULL")
						end
					end
				end
			end

						if (Vdist(playerPos.x, playerPos.y, playerPos.z, 122.160, 548.976, 179.300) < 20.0) then			-- HOUSE 10 ACCOUNT
				DrawMarker(1, 122.160, 548.976, 179.300, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 122.160, 548.976, 179.300) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to check the ~g~gang house ~y~stash")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							          OpenGangAccounts()
						end
					end
				end
			
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 126.174, 542.343, 179.300) < 20.0) then			-- HOUSE 10 Leader menu
				DrawMarker(1, 126.174, 542.343, 179.300, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 126.174, 542.343, 179.300) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to ~r~access~s~ the ~y~leader menu")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							          OpenLeaderMenu()
							end
						end
					end			
	
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 125.864, 541.297, 182.800) < 20.0) then			-- HOUSE 10 Weapon Stock menu
				DrawMarker(1, 125.864, 541.297, 182.800, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 125.864, 541.297, 182.800) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to ~r~access~s~ the ~y~weapon stock")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							          OpenGangHouseMenu()
							end
						end
					end
	
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 117.870, 568.830, 176.000) < 40.0) then			-- HOUSE 10 flag menu
				DrawMarker(23, 117.870, 568.830, 176.000, 0, 0, 0, 0, 0, 0, 3.0001, 3.0001, 2.0001, 255, 0, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 117.870, 568.830, 176.000) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to ~r~pick up~s~ the ~y~flag")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
									OpenFlagMenu()
					end
				end
			end
			
			--House11
			if (Vdist(playerPos.x, playerPos.y, playerPos.z, -526.778, 517.118, 112.942) < 20.0) then -- house11
				DrawMarker(0, -526.778, 517.118, 112.942, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -526.778, 517.118, 112.942) < 2.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to get into ~g~gang house")
						if IsControlPressed(0,  38) and (GetGameTimer() - Time) > 300 then
											if pressedyet == false then
							pressedyet = true
						if myGanghouse == "house11" then
							ESX.ShowNotification("Welcome back!")
							tpInHouse11()	
							Time      = GetGameTimer()
						elseif vsGangHouse == "house11" then	
													ESX.ShowNotification("Welcome back!")
							tpInHouse11()	
							Time      = GetGameTimer()
						elseif PlayerData.job ~= nil and PlayerData.job.name == "police" then	
							ESX.TriggerServerCallback('esx_ambulancejob:getItemAmount', function(qtty)
							if qtty > 0 then
								tpInHouse11()
							TriggerServerEvent('esx_policejob:removewarrant', GetPlayerServerId(PlayerId()), "house11")
							end 
							end, 'warrant11')
														else
							ESX.ShowNotification("You don't own this property")
						end
					end
				end
			end
		end
					if (Vdist(playerPos.x, playerPos.y, playerPos.z, -781.670, 323.785, 175.800) < 20.0) then			-- house11
				DrawMarker(1, -781.670, 323.785, 175.800, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -781.670, 323.785, 175.800) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to get out of the ~g~gang house")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)          
							tpOutHouse11()
					end
				end
			end

			if (Vdist(playerPos.x, playerPos.y, playerPos.z, -524.079, 525.922, 111.300) < 20.0) and VehicleSpawnerHouse == "house11" then			-- vehicle deleter house 11
				DrawMarker(1, -524.079, 525.922, 111.300, 0, 0, 0, 0, 0, 0, 3.0001, 3.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -524.079, 525.922, 111.300) < 5.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to remove your ~r~vehicle~s~")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							    local playerPed = GetPlayerPed(-1)
								local vehicle = GetVehiclePedIsIn(playerPed, false)
								ESX.Game.DeleteVehicle(vehicle)
							
					end
				end
			end		

				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -526.567, 535.725, 110.300) < 20.0) and VehicleSpawnerHouse == "house11" then			-- vehicle spawner house 11
				DrawMarker(1, -526.567, 535.725, 110.300, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -526.567, 535.725, 110.300) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to get a ~y~Gang House Car~s~")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							if VehicleSpawnerHouse == "house11" then
							OpenVehicleSpawnerMenu()
							else
							print("VehicleSpawnerHouse IS NULL")
						end
					end
				end
			end

						if (Vdist(playerPos.x, playerPos.y, playerPos.z, -759.167, 325.422, 169.500) < 20.0) then			-- HOUSE 11 ACCOUNT
				DrawMarker(1, -759.167, 325.422, 169.500, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -759.167, 325.422, 169.500) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to check the ~g~gang house ~y~stash")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							          OpenGangAccounts()
						end
					end
				end
			
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -760.133, 316.715, 169.500) < 20.0) then			-- HOUSE 11 Leader menu
				DrawMarker(1, -760.133, 316.715, 169.500, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -760.133, 316.715, 169.500) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to ~r~access~s~ the ~y~leader menu")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							          OpenLeaderMenu()
							end
						end
					end			
	
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -773.657, 325.640, 175.800) < 20.0) then			-- HOUSE 11 Weapon Stock menu
				DrawMarker(1, -773.657, 325.640, 175.800, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -773.657, 325.640, 175.800) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to ~r~access~s~ the ~y~weapon stock")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							          OpenGangHouseMenu()
							end
						end
					end
	
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -770.261, 324.184, 169.600) < 40.0) then			-- HOUSE 11 flag menu
				DrawMarker(23, -770.261, 324.184, 169.600, 0, 0, 0, 0, 0, 0, 3.0001, 3.0001, 2.0001, 255, 0, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -770.261, 324.184, 169.600) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to ~r~pick up~s~ the ~y~flag")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
									OpenFlagMenu()
					end
				end
			end
			
			--House12
			if (Vdist(playerPos.x, playerPos.y, playerPos.z, -459.221, 537.119, 121.459) < 20.0) then -- house12
				DrawMarker(0, -459.221, 537.119, 121.459, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -459.221, 537.119, 121.459) < 2.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to get into ~g~gang house")
						if IsControlPressed(0,  38) and (GetGameTimer() - Time) > 300 then
											if pressedyet == false then
							pressedyet = true
						if myGanghouse == "house12" then
							ESX.ShowNotification("Welcome back!")
							tpInHouse12()	
							Time      = GetGameTimer()
						elseif vsGangHouse == "house12" then	
													ESX.ShowNotification("Welcome back!")
							tpInHouse12()	
							Time      = GetGameTimer()
						elseif PlayerData.job ~= nil and PlayerData.job.name == "police" then	
							ESX.TriggerServerCallback('esx_ambulancejob:getItemAmount', function(qtty)
							if qtty > 0 then
								tpInHouse12()
							TriggerServerEvent('esx_policejob:removewarrant', GetPlayerServerId(PlayerId()), "house12")
							end 
							end, 'warrant12')
														else
							ESX.ShowNotification("You don't own this property")
						end
					end
				end
			end
		end
					if (Vdist(playerPos.x, playerPos.y, playerPos.z, 373.590, 423.331, 144.400) < 20.0) then			-- house12
				DrawMarker(1, 373.590, 423.331, 144.400, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 373.590, 423.331, 144.400) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to get out of the ~g~gang house")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)          
							tpOutHouse12()
					end
				end
			end

			if (Vdist(playerPos.x, playerPos.y, playerPos.z, -470.260, 539.028, 120.200) < 20.0) and VehicleSpawnerHouse == "house12" then			-- vehicle deleter house 12
				DrawMarker(1, -470.260, 539.028, 120.200, 0, 0, 0, 0, 0, 0, 3.0001, 3.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -470.260, 539.028, 120.200) < 5.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to remove your ~r~vehicle~s~")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							    local playerPed = GetPlayerPed(-1)
								local vehicle = GetVehiclePedIsIn(playerPed, false)
								ESX.Game.DeleteVehicle(vehicle)
							
					end
				end
			end		

				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -475.459, 548.466, 118.800) < 20.0) and VehicleSpawnerHouse == "house12" then			-- vehicle spawner house 12
				DrawMarker(1, -475.459, 548.466, 118.800, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -475.459, 548.466, 118.800) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to get a ~y~Gang House Car~s~")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							if VehicleSpawnerHouse == "house12" then
							OpenVehicleSpawnerMenu()
							else
							print("VehicleSpawnerHouse IS NULL")
						end
					end
				end
			end

						if (Vdist(playerPos.x, playerPos.y, playerPos.z, 374.439, 411.738, 141.000) < 20.0) then			-- HOUSE 12 ACCOUNT
				DrawMarker(1, 374.439, 411.738, 141.000, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 374.439, 411.738, 141.500) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to check the ~g~gang house ~y~stash")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							          OpenGangAccounts()
						end
					end
				end
			
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 375.842, 404.135, 141.000) < 20.0) then			-- HOUSE 12 Leader menu
				DrawMarker(1, 375.842, 404.135, 141.000, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 375.842, 404.135, 141.000) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to ~r~access~s~ the ~y~leader menu")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							          OpenLeaderMenu()
							end
						end
					end			
	
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 376.216, 404.966, 144.500) < 20.0) then			-- HOUSE 12 Weapon Stock menu
				DrawMarker(1, 376.216, 404.966, 144.500, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 376.216, 404.966, 144.500) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to ~r~access~s~ the ~y~weapon stock")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							          OpenGangHouseMenu()
							end
						end
					end
	
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 376.876, 431.501, 137.500) < 40.0) then			-- HOUSE 12 flag menu
				DrawMarker(23, 376.876, 431.501, 137.500, 0, 0, 0, 0, 0, 0, 3.0001, 3.0001, 2.0001, 255, 0, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 376.876, 431.501, 137.500) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to ~r~pick up~s~ the ~y~flag")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
									OpenFlagMenu()
					end
				end
			end
			
			--House13
			if (Vdist(playerPos.x, playerPos.y, playerPos.z, -70.490, 359.242, 112.445) < 20.0) then -- house13
				DrawMarker(0, -70.490, 359.242, 112.445, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -70.490, 359.242, 112.445) < 2.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to get into ~g~gang house")
						if IsControlPressed(0,  38) and (GetGameTimer() - Time) > 300 then
											if pressedyet == false then
							pressedyet = true
						if myGanghouse == "house13" then
							ESX.ShowNotification("Welcome back!")
							tpInHouse13()	
							Time      = GetGameTimer()
						elseif vsGangHouse == "house13" then	
													ESX.ShowNotification("Welcome back!")
							tpInHouse13()	
							Time      = GetGameTimer()
						elseif PlayerData.job ~= nil and PlayerData.job.name == "police" then	
							ESX.TriggerServerCallback('esx_ambulancejob:getItemAmount', function(qtty)
							if qtty > 0 then
								tpInHouse13()
							TriggerServerEvent('esx_policejob:removewarrant', GetPlayerServerId(PlayerId()), "house13")
							end 
							end, 'warrant13')
														else
							ESX.ShowNotification("You don't own this property")
						end
					end
				end
			end
		end
					if (Vdist(playerPos.x, playerPos.y, playerPos.z, 469.158, 4800.400, -59.500) < 20.0) then			-- house13
				DrawMarker(1, 469.158, 4800.400, -59.500, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 469.158, 4800.400, -59.500) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to get out of the ~g~gang house")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)          
							tpOutHouse13()
					end
				end
			end

			if (Vdist(playerPos.x, playerPos.y, playerPos.z, -60.873, 324.996, 109.400) < 20.0) and VehicleSpawnerHouse == "house13" then			-- vehicle deleter house 13
				DrawMarker(1, -60.873, 324.996, 109.400, 0, 0, 0, 0, 0, 0, 3.0001, 3.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -60.873, 324.996, 109.400) < 5.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to remove your ~r~vehicle~s~")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							    local playerPed = GetPlayerPed(-1)
								local vehicle = GetVehiclePedIsIn(playerPed, false)
								ESX.Game.DeleteVehicle(vehicle)
							
					end
				end
			end		

				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -50.330, 348.606, 111.400) < 20.0) and VehicleSpawnerHouse == "house13" then			-- vehicle spawner house 13
				DrawMarker(1, -50.330, 348.606, 111.400, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -50.330, 348.606, 111.400) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to get a ~y~Gang House Car~s~")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							if VehicleSpawnerHouse == "house13" then
							OpenVehicleSpawnerMenu()
							else
							print("VehicleSpawnerHouse IS NULL")
						end
					end
				end
			end

						if (Vdist(playerPos.x, playerPos.y, playerPos.z, 362.640, 4821.316, -60.400) < 20.0) then			-- HOUSE 13 ACCOUNT
				DrawMarker(1, 362.640, 4821.316, -60.400, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 362.640, 4821.316, -60.400) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to check the ~g~gang house ~y~stash")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							          OpenGangAccounts()
						end
					end
				end
			
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 325.726, 4827.145, -60.500) < 20.0) then			-- HOUSE 13 Leader menu
				DrawMarker(1, 325.726, 4827.145, -60.500, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 325.726, 4827.145, -60.500) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to ~r~access~s~ the ~y~leader menu")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							          OpenLeaderMenu()
							end
						end
					end			
	
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 417.266, 4809.716, -60.400) < 20.0) then			-- HOUSE 13 Weapon Stock menu
				DrawMarker(1, 417.266, 4809.716, -60.400, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 417.266, 4809.716, -60.400) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to ~r~access~s~ the ~y~weapon stock")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							          OpenGangHouseMenu()
							end
						end
					end
	
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 352.291, 4874.372, -61.300) < 40.0) then			-- HOUSE 13 flag menu
				DrawMarker(23, 352.291, 4874.372, -61.300, 0, 0, 0, 0, 0, 0, 3.0001, 3.0001, 2.0001, 255, 0, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 352.291, 4874.372, -61.300) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to ~r~pick up~s~ the ~y~flag")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
									OpenFlagMenu()
					end
				end
			end
			
			--House14
			if (Vdist(playerPos.x, playerPos.y, playerPos.z, -1580.164, -33.963, 57.565) < 20.0) then -- house14
				DrawMarker(0, -1580.164, -33.963, 57.565, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -1580.164, -33.963, 57.5652) < 2.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to get into ~g~gang house")
						if IsControlPressed(0,  38) and (GetGameTimer() - Time) > 300 then
											if pressedyet == false then
							pressedyet = true
						if myGanghouse == "house14" then
							ESX.ShowNotification("Welcome back!")
							tpInHouse14()	
							Time      = GetGameTimer()
						elseif vsGangHouse == "house14" then	
													ESX.ShowNotification("Welcome back!")
							tpInHouse14()	
							Time      = GetGameTimer()
						elseif PlayerData.job ~= nil and PlayerData.job.name == "police" then	
							ESX.TriggerServerCallback('esx_ambulancejob:getItemAmount', function(qtty)
							if qtty > 0 then
								tpInHouse14()
							TriggerServerEvent('esx_policejob:removewarrant', GetPlayerServerId(PlayerId()), "house14")
							end 
							end, 'warrant14')
														else
							ESX.ShowNotification("You don't own this property")
						end
					end
				end
			end
		end
					if (Vdist(playerPos.x, playerPos.y, playerPos.z, 794.246, -2992.228, -40.000) < 20.0) then			-- house14
				DrawMarker(1, 794.246, -2992.228, -40.000, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 794.246, -2992.228, -40.000) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to get out of the ~g~gang house")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)          
							tpOutHouse14()
					end
				end
			end

			if (Vdist(playerPos.x, playerPos.y, playerPos.z, -1590.931, -58.817, 55.400) < 20.0) and VehicleSpawnerHouse == "house14" then			-- vehicle deleter house 14
				DrawMarker(1, -1590.931, -58.817, 55.400, 0, 0, 0, 0, 0, 0, 3.0001, 3.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -1590.931, -58.817, 55.400) < 5.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to remove your ~r~vehicle~s~")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							    local playerPed = GetPlayerPed(-1)
								local vehicle = GetVehiclePedIsIn(playerPed, false)
								ESX.Game.DeleteVehicle(vehicle)
							
					end
				end
			end		

				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -1585.729, -55.483, 55.400) < 20.0) and VehicleSpawnerHouse == "house14" then			-- vehicle spawner house 14
				DrawMarker(1, -1585.729, -55.483, 55.400, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -1585.729, -55.483, 55.400) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to get a ~y~Gang House Car~s~")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							if VehicleSpawnerHouse == "house14" then
							OpenVehicleSpawnerMenu()
							else
							print("VehicleSpawnerHouse IS NULL")
						end
					end
				end
			end

						if (Vdist(playerPos.x, playerPos.y, playerPos.z, 783.728, -3015.120, -40.000) < 20.0) then			-- HOUSE 14 ACCOUNT
				DrawMarker(1, 783.728, -3015.120, -40.000, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 783.728, -3015.120, -40.000) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to check the ~g~gang house ~y~stash")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							          OpenGangAccounts()
						end
					end
				end
			
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 783.347, -3015.271, -34.600) < 20.0) then			-- HOUSE 14 Leader menu
				DrawMarker(1, 783.347, -3015.271, -34.600, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 783.347, -3015.271, -34.600) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to ~r~access~s~ the ~y~leader menu")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							          OpenLeaderMenu()
							end
						end
					end			
	
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 785.580, -2998.734, -40.000) < 20.0) then			-- HOUSE 14 Weapon Stock menu
				DrawMarker(1, 785.580, -2998.734, -40.000, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 785.580, -2998.734, -40.000) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to ~r~access~s~ the ~y~weapon stock")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							          OpenGangHouseMenu()
							end
						end
					end
	
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 785.279, -3012.508, -28.800) < 40.0) then			-- HOUSE 14 flag menu
				DrawMarker(23, 785.279, -3012.508, -28.800, 0, 0, 0, 0, 0, 0, 3.0001, 3.0001, 2.0001, 255, 0, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, 785.279, -3012.508, -28.800) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to ~r~pick up~s~ the ~y~flag")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
									OpenFlagMenu()
					end
				end
			end
			
			--House15
			if (Vdist(playerPos.x, playerPos.y, playerPos.z, -1587.769, 15.749, 61.253) < 20.0) then -- house15
				DrawMarker(0, -1587.769, 15.749, 61.253, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -1587.769, 15.749, 61.253) < 2.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to get into ~g~gang house")
						if IsControlPressed(0,  38) and (GetGameTimer() - Time) > 300 then
											if pressedyet == false then
							pressedyet = true
						if myGanghouse == "house15" then
							ESX.ShowNotification("Welcome back!")
							tpInHouse15()	
							Time      = GetGameTimer()
						elseif vsGangHouse == "house15" then	
													ESX.ShowNotification("Welcome back!")
							tpInHouse15()	
							Time      = GetGameTimer()
						elseif PlayerData.job ~= nil and PlayerData.job.name == "police" then	
							ESX.TriggerServerCallback('esx_ambulancejob:getItemAmount', function(qtty)
							if qtty > 0 then
								tpInHouse15()
							TriggerServerEvent('esx_policejob:removewarrant', GetPlayerServerId(PlayerId()), "house15")
							end 
							end, 'warrant15')
														else
							ESX.ShowNotification("You don't own this property")
						end
					end
				end
			end
		end
					if (Vdist(playerPos.x, playerPos.y, playerPos.z, -1452.402, -540.556, 73.200) < 20.0) then			-- house15
				DrawMarker(1, -1452.402, -540.556, 73.200, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -1452.402, -540.556, 73.200) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to get out of the ~g~gang house")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)          
							tpOutHouse15()
					end
				end
			end

			if (Vdist(playerPos.x, playerPos.y, playerPos.z, -1555.952, 19.170, 57.400) < 20.0) and VehicleSpawnerHouse == "house15" then			-- vehicle deleter house 15
				DrawMarker(1, -1555.952, 19.170, 57.400, 0, 0, 0, 0, 0, 0, 3.0001, 3.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -1555.952, 19.170, 57.400) < 5.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to remove your ~r~vehicle~s~")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							    local playerPed = GetPlayerPed(-1)
								local vehicle = GetVehiclePedIsIn(playerPed, false)
								ESX.Game.DeleteVehicle(vehicle)
							
					end
				end
			end		

				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -1548.708, 25.011, 57.400) < 20.0) and VehicleSpawnerHouse == "house15" then			-- vehicle spawner house 14
				DrawMarker(1, -1548.708, 25.011, 57.400, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -1548.708, 25.011, 57.400) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to get a ~y~Gang House Car~s~")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							if VehicleSpawnerHouse == "house15" then
							OpenVehicleSpawnerMenu()
							else
							print("VehicleSpawnerHouse IS NULL")
						end
					end
				end
			end

						if (Vdist(playerPos.x, playerPos.y, playerPos.z, -1448.900, -548.505, 71.600) < 20.0) then			-- HOUSE 15 ACCOUNT
				DrawMarker(1, -1448.900, -548.505, 71.600, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -1448.900, -548.505, 71.600) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to check the ~g~gang house ~y~stash")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							          OpenGangAccounts()
						end
					end
				end
			
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -1456.884, -554.747, 71.600) < 20.0) then			-- HOUSE 15 Leader menu
				DrawMarker(1, -1456.884, -554.747, 71.600, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -1456.884, -554.747, 71.600) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to ~r~access~s~ the ~y~leader menu")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							          OpenLeaderMenu()
							end
						end
					end			
	
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -1466.864, -527.634, 72.400) < 20.0) then			-- HOUSE 15 Weapon Stock menu
				DrawMarker(1, -1466.864, -527.634, 72.400, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 165, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -1466.864, -527.634, 72.400) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to ~r~access~s~ the ~y~weapon stock")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
							          OpenGangHouseMenu()
							end
						end
					end
	
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -1449.557, -556.197, 72.200) < 40.0) then			-- HOUSE 15 flag menu
				DrawMarker(23, -1449.557, -556.197, 72.200, 0, 0, 0, 0, 0, 0, 3.0001, 3.0001, 2.0001, 255, 0, 0, 165, 0, 0, 0, 1)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, -1449.557, -556.197, 72.200) < 3.0) then
						DisplayHelpText("Press ~INPUT_PICKUP~ to ~r~pick up~s~ the ~y~flag")
					if (IsControlPressed(0, 38)) then
							Citizen.Wait(100)
									OpenFlagMenu()
					end
				end
			end
	end	
	end)
	
	function DisplayHelpText(str)
	SetTextComponentFormat("STRING")
	AddTextComponentString(str)
	DisplayHelpTextFromStringLabel(0, 0, 1, -1)
	end
			function tpInHouse1(source)
		pressedyet = false
		local randomnmr32 = math.random(1,3)
			if randomnmr32 == 1 then
				SetEntityCoords(GetPlayerPed(-1), 998.4809, -3164.711, -38.90733)
				elseif randomnmr32 == 2 then
					SetEntityCoords(GetPlayerPed(-1), 1000.83, -3168.63, -38.91)
				elseif randomnmr32 == 3 then
					SetEntityCoords(GetPlayerPed(-1), 1003.52, -3162.72, -38.91)
				end
		end			
		
		function tpOutHouse1(source)
			local randomnmr62 = math.random(1,3)
			pressedyet = false
			if randomnmr62 == 1 then
			SetEntityCoords(GetPlayerPed(-1), -676.31, -1177.92, 10.61)
			elseif randomnmr62 == 2 then
				SetEntityCoords(GetPlayerPed(-1), -672.06, -1176.72, 10.61)
			elseif randomnmr62 == 3 then
				SetEntityCoords(GetPlayerPed(-1), -677.22, -1166.21, 10.81)
			end
		end	

		function tpInHouse2(source)
		pressedyet = false
				local randomnmr422 = math.random(1,3)
			if randomnmr422 == 1 then
				SetEntityCoords(GetPlayerPed(-1), 1123.47, -3147.65, -37.06)
				elseif randomnmr422 == 2 then
				SetEntityCoords(GetPlayerPed(-1), 1115.84, -3151.21, -37.06)
				elseif randomnmr422 == 3 then
				SetEntityCoords(GetPlayerPed(-1), 1108.53, -3161.76, -37.52)
				end
		end			
		
		function tpOutHouse2(source)
			local randomnmr61 = math.random(1,3)
			if randomnmr61 == 1 then
			SetEntityCoords(GetPlayerPed(-1), 941.76, -1881.03, 31.09)
			elseif randomnmr61 == 2 then
				SetEntityCoords(GetPlayerPed(-1), 941.47, -1884.13, 31.09)
			elseif randomnmr61 == 3 then
				SetEntityCoords(GetPlayerPed(-1), 942.7, -1876.73, 31.09)
			end
		end	
		
		function tpInHouse3(source)
		local randomnmrf = math.random(1,3)
		pressedyet = false
		if randomnmrf == 1 then
		SetEntityCoords(GetPlayerPed(-1), 889.333, -3244.580, -98.270)
		elseif randomnmrf == 2 then
			SetEntityCoords(GetPlayerPed(-1),  889.952, -3238.821, -98.278)
		elseif randomnmrf == 3 then
			SetEntityCoords(GetPlayerPed(-1), 889.619, -3247.203, -98.281)
		end
		end			
		
		function tpOutHouse3(source)
			local randomnmr1 = math.random(1,3)
			if randomnmr1 == 1 then
			SetEntityCoords(GetPlayerPed(-1), -1474.705, -51.359, 54.638)
			elseif randomnmr1 == 2 then
				SetEntityCoords(GetPlayerPed(-1), -1479.447, -51.572, 54.639)
			elseif randomnmr1 == 3 then
				SetEntityCoords(GetPlayerPed(-1), -1475.456, -57.368, 54.638)
			end
		end	
		function tpInHouse4(source)
		pressedyet = false
				local randomnmr = math.random(1,3)
			if randomnmr == 1 then
				SetEntityCoords(GetPlayerPed(-1), 970.858, -2991.115, -39.646)
				elseif randomnmr == 2 then
					SetEntityCoords(GetPlayerPed(-1), 976.2, -2992.115, -39.65)
				elseif randomnmr == 3 then
					SetEntityCoords(GetPlayerPed(-1), 974.858, -2998.57, -39.65)
				end
			end			
			
			function tpOutHouse4(source)
				local randomnmr6p = math.random(1,3)
				if randomnmr6p == 1 then
				SetEntityCoords(GetPlayerPed(-1), -144.470, 885.032, 239.020)
				elseif randomnmr6p == 2 then
					SetEntityCoords(GetPlayerPed(-1), -163.988, 890.838, 237.139)
				elseif randomnmr6p == 3 then
					SetEntityCoords(GetPlayerPed(-1), -128.357, 859.911, 232.699)
				end
			end	

			function tpInHouse5(source)
				local randomnmr5 = math.random(1,3)
				pressedyet = false
				if randomnmr5 == 1 then
				SetEntityCoords(GetPlayerPed(-1), -170.251, 477.986, 137.244)
				elseif randomnmr5 == 2 then
					SetEntityCoords(GetPlayerPed(-1), -166.527, 479.092, 137.248)
				elseif randomnmr5 == 3 then
					SetEntityCoords(GetPlayerPed(-1), -166.734, 480.973, 137.265)
				end
				end			
				
				function tpOutHouse5(source)
					local randomnmr6 = math.random(1,3)
					if randomnmr6 == 1 then
					SetEntityCoords(GetPlayerPed(-1), -562.296, 405.559, 100.662)
					elseif randomnmr6 == 2 then
						SetEntityCoords(GetPlayerPed(-1), -570.774, 408.176, 100.663)
					elseif randomnmr6 == 3 then
						SetEntityCoords(GetPlayerPed(-1), -580.299, 395.557, 100.788)
					end
				end	

				function tpInHouse6(source)
				pressedyet = false
					local randomnmr8 = math.random(1,3)
					pressedyet = false
					if randomnmr8 == 1 then
					SetEntityCoords(GetPlayerPed(-1), -1290.64, 445.46, 97.902)
					elseif randomnmr8 == 2 then
						SetEntityCoords(GetPlayerPed(-1), -1288.93, 445.41, 97.9)
					elseif randomnmr8 == 3 then
						SetEntityCoords(GetPlayerPed(-1), -1290.029, 443.26, 97.9)
					end
					end			
					
					function tpOutHouse6(source)
						local randomnmr7 = math.random(1,3)
						if randomnmr7 == 1 then
						SetEntityCoords(GetPlayerPed(-1), -1297.724, 454.842, 97.561)
						elseif randomnmr7 == 2 then
							SetEntityCoords(GetPlayerPed(-1), -1290.455, 455.335, 97.646)
						elseif randomnmr7 == 3 then
							SetEntityCoords(GetPlayerPed(-1), -1303.029, 455.83, 97.94)
						end
					end	
					
				function tpInHouse7(source)
				pressedyet = false
					local randomnmr82 = math.random(1,3)
					pressedyet = false
					if randomnmr82 == 1 then
					SetEntityCoords(GetPlayerPed(-1), -1456.776, -517.487, 56.929)
					elseif randomnmr82 == 2 then
						SetEntityCoords(GetPlayerPed(-1), -1452.857, -520.743, 56.929)
					elseif randomnmr82 == 3 then
						SetEntityCoords(GetPlayerPed(-1), -1454.287, -517.720, 56.929)
					end
					end			
					
					function tpOutHouse7(source)
						local randomnmr79 = math.random(1,3)
						if randomnmr79 == 1 then
						SetEntityCoords(GetPlayerPed(-1), 218.860, -174.584, 57.917)
						elseif randomnmr79 == 2 then
							SetEntityCoords(GetPlayerPed(-1), 209.034, -166.416, 56.349)
						elseif randomnmr79 == 3 then
							SetEntityCoords(GetPlayerPed(-1), 219.243, -162.280, 57.031)
						end
					end	
					
				function tpInHouse8(source)
				pressedyet = false
					local randomnmr81 = math.random(1,3)
					pressedyet = false
					if randomnmr81 == 1 then
					SetEntityCoords(GetPlayerPed(-1), -853.234, 687.986, 152.852)
					elseif randomnmr81 == 2 then
						SetEntityCoords(GetPlayerPed(-1), -858.839, 684.412, 152.852)
					elseif randomnmr81 == 3 then
						SetEntityCoords(GetPlayerPed(-1), -854.103, 691.086, 151.052)
					end
					end			
					
					function tpOutHouse8(source)
						local randomnmr83 = math.random(1,3)
						if randomnmr83 == 1 then
						SetEntityCoords(GetPlayerPed(-1), -818.163, 704.666, 147.394)
						elseif randomnmr83 == 2 then
							SetEntityCoords(GetPlayerPed(-1), -839.577, 721.898, 150.051)
						elseif randomnmr83 == 3 then
							SetEntityCoords(GetPlayerPed(-1), -874.589, 695.843, 149.889)
						end
					end

				function tpInHouse9(source)
				pressedyet = false
					local randomnmr95 = math.random(1,3)
					pressedyet = false
					if randomnmr95 == 1 then
					SetEntityCoords(GetPlayerPed(-1), -780.645, 322.663, 211.997)
					elseif randomnmr95 == 2 then
						SetEntityCoords(GetPlayerPed(-1), -781.633, 323.611, 211.997)
					elseif randomnmr95 == 3 then
						SetEntityCoords(GetPlayerPed(-1), -780.010, 324.678, 211.997)
					end
					end			
					
					function tpOutHouse9(source)
						local randomnmr96 = math.random(1,3)
						if randomnmr96 == 1 then
						SetEntityCoords(GetPlayerPed(-1), -85.431, 987.780, 234.398)
						elseif randomnmr96 == 2 then
							SetEntityCoords(GetPlayerPed(-1), -95.800, 988.283, 235.756)
						elseif randomnmr96 == 3 then
							SetEntityCoords(GetPlayerPed(-1), -98.758, 939.154, 233.028)
						end
					end
					
					function tpInHouse10(source)
				pressedyet = false
					local randomnmr100 = math.random(1,3)
					pressedyet = false
					if randomnmr100 == 1 then
					SetEntityCoords(GetPlayerPed(-1), 117.565, 556.642, 184.301)
					elseif randomnmr100 == 2 then
						SetEntityCoords(GetPlayerPed(-1), 119.184, 551.436, 184.097)
					elseif randomnmr100 == 3 then
						SetEntityCoords(GetPlayerPed(-1), 123.936, 556.860, 184.297)
					end
					end			
					
					function tpOutHouse10(source)
						local randomnmr101 = math.random(1,3)
						if randomnmr101 == 1 then
						SetEntityCoords(GetPlayerPed(-1), -450.938, 343.804, 104.274)
						elseif randomnmr101 == 2 then
							SetEntityCoords(GetPlayerPed(-1), -436.467, 343.354, 105.745)
						elseif randomnmr101 == 3 then
							SetEntityCoords(GetPlayerPed(-1), -423.545, 342.453, 106.102)
						end
					end
					
					function tpInHouse11(source)
				pressedyet = false
					local randomnmr110 = math.random(1,3)
					pressedyet = false
					if randomnmr110 == 1 then
					SetEntityCoords(GetPlayerPed(-1), -781.629, 317.012, 176.803)
					elseif randomnmr110 == 2 then
						SetEntityCoords(GetPlayerPed(-1), -781.724, 316.029, 176.803)
					elseif randomnmr110 == 3 then
						SetEntityCoords(GetPlayerPed(-1), -781.843, 318.008, 176.803)
					end
					end			
					
					function tpOutHouse11(source)
						local randomnmr111 = math.random(1,3)
						if randomnmr111 == 1 then
						SetEntityCoords(GetPlayerPed(-1), -534.116, 517.009, 112.833)
						elseif randomnmr111 == 2 then
							SetEntityCoords(GetPlayerPed(-1), -529.631, 520.693, 112.742)
						elseif randomnmr111 == 3 then
							SetEntityCoords(GetPlayerPed(-1), -522.229, 528.986, 112.282)
						end
					end

					function tpInHouse12(source)
				pressedyet = false
					local randomnmr120 = math.random(1,3)
					pressedyet = false
					if randomnmr120 == 1 then
					SetEntityCoords(GetPlayerPed(-1), 371.910, 415.020, 145.699)
					elseif randomnmr120 == 2 then
						SetEntityCoords(GetPlayerPed(-1), 376.491, 418.478, 145.900)
					elseif randomnmr120 == 3 then
						SetEntityCoords(GetPlayerPed(-1), 372.800, 418.498, 145.903)
					end
					end			
					
					function tpOutHouse12(source)
						local randomnmr121 = math.random(1,3)
						if randomnmr121 == 1 then
						SetEntityCoords(GetPlayerPed(-1), -454.874, 537.655, 121.436)
						elseif randomnmr121 == 2 then
							SetEntityCoords(GetPlayerPed(-1), -464.580, 539.915, 121.365)
						elseif randomnmr121 == 3 then
							SetEntityCoords(GetPlayerPed(-1), -470.293, 541.882, 120.902)
						end
					end
					
					function tpInHouse13(source)
				pressedyet = false
					local randomnmr130 = math.random(1,3)
					pressedyet = false
					if randomnmr130 == 1 then
					SetEntityCoords(GetPlayerPed(-1), 488.000, 4791.835, -58.393)
					elseif randomnmr130 == 2 then
						SetEntityCoords(GetPlayerPed(-1), 485.942, 4797.335, -58.389)
					elseif randomnmr130 == 3 then
						SetEntityCoords(GetPlayerPed(-1), 482.847, 4815.255, -58.382)
					end
					end			
					
					function tpOutHouse13(source)
						local randomnmr131 = math.random(1,3)
						if randomnmr131 == 1 then
						SetEntityCoords(GetPlayerPed(-1), -78.995, 344.798, 112.444)
						elseif randomnmr131 == 2 then
							SetEntityCoords(GetPlayerPed(-1), -60.257, 360.061, 113.056)
						elseif randomnmr131 == 3 then
							SetEntityCoords(GetPlayerPed(-1), -40.919, 353.898, 114.755)
						end
					end
					
					function tpInHouse14(source)
				pressedyet = false
					local randomnmr140 = math.random(1,3)
					pressedyet = false
					if randomnmr140 == 1 then
					SetEntityCoords(GetPlayerPed(-1), 797.848, -2994.378, -38.999)
					elseif randomnmr140 == 2 then
						SetEntityCoords(GetPlayerPed(-1), 795.362, -2997.284, -38.999)
					elseif randomnmr140 == 3 then
						SetEntityCoords(GetPlayerPed(-1), 792.183, -2996.982, -38.999)
					end
					end			
					
					function tpOutHouse14(source)
						local randomnmr141 = math.random(1,3)
						if randomnmr141 == 1 then
						SetEntityCoords(GetPlayerPed(-1), -1573.624, -34.893, 57.146)
						elseif randomnmr141 == 2 then
							SetEntityCoords(GetPlayerPed(-1), -1581.071, -17.476, 56.871)
						elseif randomnmr141 == 3 then
							SetEntityCoords(GetPlayerPed(-1), -1584.268, -58.803, 56.489)
						end
					end
					
					function tpInHouse15(source)
				pressedyet = false
					local randomnmr150 = math.random(1,3)
					pressedyet = false
					if randomnmr150 == 1 then
					SetEntityCoords(GetPlayerPed(-1), -1455.645, -535.798, 74.044)
					elseif randomnmr150 == 2 then
						SetEntityCoords(GetPlayerPed(-1), -1454.833, -538.705, 74.044)
					elseif randomnmr150 == 3 then
						SetEntityCoords(GetPlayerPed(-1), -1453.863, -536.241, 74.044)
					end
					end			
					
					function tpOutHouse15(source)
						local randomnmr151 = math.random(1,3)
						if randomnmr151 == 1 then
						SetEntityCoords(GetPlayerPed(-1), -1587.736, 12.268, 61.082)
						elseif randomnmr151 == 2 then
							SetEntityCoords(GetPlayerPed(-1), -1599.027, 15.896, 61.082)
						elseif randomnmr151 == 3 then
							SetEntityCoords(GetPlayerPed(-1), -1566.542, 11.004, 59.577)
						end
					end
					
		function testmyPos(source)
			local playerPos = GetEntityCoords(GetPlayerPed(-1), true)
			if (Vdist(playerPos.x, playerPos.y, playerPos.z, 1009.17, -3157.51, -38.91) < 40.0) then
				return "house1"
			elseif (Vdist(playerPos.x, playerPos.y, playerPos.z, 1102.47, -3154.51, -37.52) < 40.0) then
			return "house2"
			elseif (Vdist(playerPos.x, playerPos.y, playerPos.z, 866.63, -3187.19, -96.85) < 120.0) then
			return "house3"
			elseif (Vdist(playerPos.x, playerPos.y, playerPos.z, 954.938, -2990.780, -39.646) < 120.0) then
			return "house4"
			elseif (Vdist(playerPos.x, playerPos.y, playerPos.z, -173.296, 491.716, 130.043) < 100.0) then
			return "house5"
			elseif (Vdist(playerPos.x, playerPos.y, playerPos.z, -1287.861, 458.066, 90.294) < 100.0) then
			return "house6"		
			elseif (Vdist(playerPos.x, playerPos.y, playerPos.z, -1467.759, -537.831, 50.732) < 100.0) then
			return "house7"
			elseif (Vdist(playerPos.x, playerPos.y, playerPos.z, -855.427, 684.500, 149.053) < 100.0) then
			return "house8"
			elseif (Vdist(playerPos.x, playerPos.y, playerPos.z, -797.225, 330.869, 210.796) < 120.0) then
			return "house9"
			elseif (Vdist(playerPos.x, playerPos.y, playerPos.z, -770.363, 324.177, 217.059) < 120.0) then
			return "house10"
			elseif (Vdist(playerPos.x, playerPos.y, playerPos.z, -770.261, 324.184, 170.000) < 120.0) then
			return "house11"
			elseif (Vdist(playerPos.x, playerPos.y, playerPos.z, 376.876, 431.501, 138.300) < 120.0) then
			return "house12"
			elseif (Vdist(playerPos.x, playerPos.y, playerPos.z, 352.291, 4874.372, -60.793) < 120.0) then
			return "house13"
			elseif (Vdist(playerPos.x, playerPos.y, playerPos.z, 785.279, -3012.508, -28.308) < 120.0) then
			return "house14"
			elseif (Vdist(playerPos.x, playerPos.y, playerPos.z, -1449.557, -556.197, 72.852) < 120.0) then
			return "house15"
			end	
		
		end			

		
function OpenVehicleSpawnerMenu()
  local vehicles = Vehicles

  ESX.UI.Menu.CloseAll()


    local elements = {}
local coords = 0
if VehicleSpawnerHouse == 'house1' then
print("vehilce is house1")
coords = Config.Zones.VehicleSpawnPoint_h1.Pos
    for i=1, #house1Vehicles, 1 do
      local vehicled = house1Vehicles[i]
      table.insert(elements, {label = vehicled.label, value = vehicled.name})
    end
end
if VehicleSpawnerHouse == 'house2' then
	coords = Config.Zones.VehicleSpawnPoint_h2.Pos
    for i=1, #house2Vehicles, 1 do
      local vehicled = house2Vehicles[i]
      table.insert(elements, {label = vehicled.label, value = vehicled.name})
    end
end
if VehicleSpawnerHouse == 'house3' then
	coords = Config.Zones.VehicleSpawnPoint_h3.Pos
    for i=1, #house3Vehicles, 1 do
      local vehicled = house3Vehicles[i]
      table.insert(elements, {label = vehicled.label, value = vehicled.name})
    end
end

if VehicleSpawnerHouse == 'house4' then
	coords = Config.Zones.VehicleSpawnPoint_h4.Pos
    for i=1, #house4Vehicles, 1 do
      local vehicled = house4Vehicles[i]
      table.insert(elements, {label = vehicled.label, value = vehicled.name})
    end
end

if VehicleSpawnerHouse == 'house5' then
	coords = Config.Zones.VehicleSpawnPoint_h5.Pos
    for i=1, #house5Vehicles, 1 do
      local vehicled = house5Vehicles[i]
      table.insert(elements, {label = vehicled.label, value = vehicled.name})
    end
end

if VehicleSpawnerHouse == 'house6' then
	coords = Config.Zones.VehicleSpawnPoint_h6.Pos
    for i=1, #house6Vehicles, 1 do
      local vehicled = house6Vehicles[i]
      table.insert(elements, {label = vehicled.label, value = vehicled.name})
    end
end

if VehicleSpawnerHouse == 'house7' then
	coords = Config.Zones.VehicleSpawnPoint_h7.Pos
    for i=1, #house7Vehicles, 1 do
      local vehicled = house7Vehicles[i]
      table.insert(elements, {label = vehicled.label, value = vehicled.name})
    end
end

if VehicleSpawnerHouse == 'house8' then
	coords = Config.Zones.VehicleSpawnPoint_h8.Pos
    for i=1, #house8Vehicles, 1 do
      local vehicled = house8Vehicles[i]
      table.insert(elements, {label = vehicled.label, value = vehicled.name})
    end
end

if VehicleSpawnerHouse == 'house9' then
	coords = Config.Zones.VehicleSpawnPoint_h9.Pos
    for i=1, #house9Vehicles, 1 do
      local vehicled = house9Vehicles[i]
      table.insert(elements, {label = vehicled.label, value = vehicled.name})
    end
end

if VehicleSpawnerHouse == 'house10' then
	coords = Config.Zones.VehicleSpawnPoint_h10.Pos
    for i=1, #house10Vehicles, 1 do
      local vehicled = house10Vehicles[i]
      table.insert(elements, {label = vehicled.label, value = vehicled.name})
    end
end

if VehicleSpawnerHouse == 'house11' then
	coords = Config.Zones.VehicleSpawnPoint_h11.Pos
    for i=1, #house11Vehicles, 1 do
      local vehicled = house11Vehicles[i]
      table.insert(elements, {label = vehicled.label, value = vehicled.name})
    end
end

if VehicleSpawnerHouse == 'house12' then
	coords = Config.Zones.VehicleSpawnPoint_h12.Pos
    for i=1, #house12Vehicles, 1 do
      local vehicled = house12Vehicles[i]
      table.insert(elements, {label = vehicled.label, value = vehicled.name})
    end
end

if VehicleSpawnerHouse == 'house13' then
	coords = Config.Zones.VehicleSpawnPoint_h13.Pos
    for i=1, #house13Vehicles, 1 do
      local vehicled = house13Vehicles[i]
      table.insert(elements, {label = vehicled.label, value = vehicled.name})
    end
end

if VehicleSpawnerHouse == 'house14' then
	coords = Config.Zones.VehicleSpawnPoint_h14.Pos
    for i=1, #house14Vehicles, 1 do
      local vehicled = house14Vehicles[i]
      table.insert(elements, {label = vehicled.label, value = vehicled.name})
    end
end

if VehicleSpawnerHouse == 'house15' then
	coords = Config.Zones.VehicleSpawnPoint_h15.Pos
    for i=1, #house15Vehicles, 1 do
      local vehicled = house15Vehicles[i]
      table.insert(elements, {label = vehicled.label, value = vehicled.name})
    end
end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'vehicle_spawner',
      {
        title    = "เรียกรถแก๊ง",
        align    = 'top-left',
        elements = elements,
      },
      function(data, menu)

        menu.close()

        local model = data.current.value

        local vehicle = GetClosestVehicle(1029.42, -2955.59, 6.69,  3.0,  0,  71)

        if not DoesEntityExist(vehicle) then
			local playerPed = GetPlayerPed(-1)
			ESX.Game.SpawnVehicle(model, coords, 225.0, function(vehicle)
				TaskWarpPedIntoVehicle(playerPed,  vehicle, -1)
			end)
	end
      end,
      function(data, menu)

        menu.close()

        CurrentAction     = 'menu_vehicle_spawner'
        CurrentActionMsg  = "Vehicle spawn"
        CurrentActionData = {station = station, partNum = partNum}

      end
    )

  

end
		
function OpenFlagMenu()
  local elements = {}
      table.insert(elements, {label = "เก็บธง", value = 'pickup'})
      table.insert(elements, {label = "วางธง", value = 'leave'})


  ESX.UI.Menu.CloseAll()

  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'taxi_actions',
    {
      title    = 'ธงประจำบ้าน',
      elements = elements
    },
    function(data, menu)

	if data.current.value == 'pickup' then
		TriggerServerEvent('stadus_gh:pickUpFlag', GetPlayerServerId(PlayerId()), testmyPos(source), myGang)
		menu.close()
    elseif data.current.value == 'leave' then
			TriggerServerEvent('stadus_gh:leaveFlag', GetPlayerServerId(PlayerId()), testmyPos(source), myGang)
		menu.close()
      end
	  
    end,
    function(data, menu)

      menu.close()

    end
  )
end
		
function OpenLeaderMenu()
  local elements = {}
      table.insert(elements, {label = "ขายบ้าน", value = 'sell'})
      table.insert(elements, {label = "ส่งบ้านให้แก๊งอื่น", value = 'give'})


  ESX.UI.Menu.CloseAll()

  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'taxi_actions',
    {
      title    = 'จัดการบ้าน',
      elements = elements
    },
    function(data, menu)

	if data.current.value == 'sell' then
		TriggerServerEvent('stadus_gh:sellGangHouse', GetPlayerServerId(PlayerId()), myGang, myGangrank)
	
    elseif data.current.value == 'give' then
	ESX.ShowNotification("~r~WARNING!!!~s~ If you write wrong gang, then it's ~r~no going~s~ back...")
		        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'wash',
          {
            title = "amount"
          },
          function(data, menu)
            if data.value == nil then
              ESX.ShowNotification("The amount is not valid")
            else
				TriggerServerEvent('stadus_gh:giveGangHouse', GetPlayerServerId(PlayerId()), data.value, myGang, myGangrank)

              menu.close()

            end

          end,
          function(data, menu)
            menu.close()
          end
        )
		menu.close()
      end
	  
    end,
    function(data, menu)

      menu.close()


    end
  )
end
		
function OpenGangAccounts()

	if PlayerData.job.name == "police" and PlayerData.job.name ~= nil then
		ESX.TriggerServerCallback('stadus_gh:getHouseAcc', function(inventory)
			local elements = {}
					table.insert(elements, {label = "เงินสกปรก $" .. inventory.moneys, type = 'item_account', value = 'black_money'})
					table.insert(elements, {label = "ฝากเงิน", type = 'item_account', value = 'deposit'})
					table.insert(elements, {label = "ถอนเงิน ", type = 'item_account', value = 'withdraw'})
		
		
			ESX.UI.Menu.CloseAll()
		
			ESX.UI.Menu.Open(
				'default', GetCurrentResourceName(), 'taxi_actions',
				{
					title    = 'จัดการเงินในบ้าน',
					elements = elements
				},
				function(data, menu)
		
					if data.current.value == 'deposit' then
								ESX.UI.Menu.Open(
							'dialog', GetCurrentResourceName(), 'wash',
							{
								title = "amount"
							},
							function(data, menu)
		
								local amount = tonumber(data.value)
		
								if amount == nil then
									ESX.ShowNotification("The amount is not valid")
								else
						print(amount)
						TriggerServerEvent('stadus_gh:addmoney', GetPlayerServerId(PlayerId()), "black_money", amount, policeWarrantHouseGang)
		
									menu.close()
		
								end
		
							end,
							function(data, menu)
								menu.close()
							end
						)
				menu.close()
					end
		
							if data.current.value == 'withdraw' then
								ESX.UI.Menu.Open(
							'dialog', GetCurrentResourceName(), 'wash',
							{
								title = "amount"
							},
							function(data, menu)
		
								local amount = tonumber(data.value)
		
								if amount == nil then
									ESX.ShowNotification("The amount is not valid")
								else
						print(amount)
						TriggerServerEvent('stadus_gh:withdrawmoney', GetPlayerServerId(PlayerId()), "black_money", amount, policeWarrantHouseGang, 1)
		
									menu.close()
		
								end
		
							end,
							function(data, menu)
								menu.close()
							end
						)
				menu.close()
					end
				
				end,
				function(data, menu)
		
					menu.close()
		
		
				end
			)
		end, policeWarrantHouseGang)
	else
ESX.TriggerServerCallback('stadus_gh:getHouseAcc', function(inventory)
  local elements = {}
      table.insert(elements, {label = "เงินสกปรก $" .. inventory.moneys, type = 'item_account', value = 'black_money'})
      table.insert(elements, {label = "ฝากเงิน", type = 'item_account', value = 'deposit'})
      table.insert(elements, {label = "ถอนเงิน ", type = 'item_account', value = 'withdraw'})


  ESX.UI.Menu.CloseAll()

  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'taxi_actions',
    {
      title    = 'จัดการเงินในบ้าน',
      elements = elements
    },
    function(data, menu)

      if data.current.value == 'deposit' then
		        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'wash',
          {
            title = "amount"
          },
          function(data, menu)

            local amount = tonumber(data.value)

            if amount == nil then
              ESX.ShowNotification("The amount is not valid")
            else
				print(amount)
				TriggerServerEvent('stadus_gh:addmoney', GetPlayerServerId(PlayerId()), "black_money", amount, myGang)

              menu.close()

            end

          end,
          function(data, menu)
            menu.close()
          end
        )
		menu.close()
      end

	        if data.current.value == 'withdraw' then
		        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'wash',
          {
            title = "amount"
          },
          function(data, menu)

            local amount = tonumber(data.value)

            if amount == nil then
              ESX.ShowNotification("The amount is not valid")
            else
				print(amount)
				TriggerServerEvent('stadus_gh:withdrawmoney', GetPlayerServerId(PlayerId()), "black_money", amount, myGang, myGangrank)

              menu.close()

            end

          end,
          function(data, menu)
            menu.close()
          end
        )
		menu.close()
      end
	  
    end,
    function(data, menu)

      menu.close()


    end
  )
end, myGang)
end
end
	
function OpenCloakroomMenu()

  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'cloakroom',
    {
      title    = "เปลี่ยนชุดแก๊ง",
      align    = 'top-left',
      elements = {
        {label = "ชุดประชาชน", value = 'citizen_wear'},
        {label = "ชุดแก๊ง", value = 'gang_wear'},
      },
    },
    function(data, menu)

      menu.close()

      if data.current.value == 'citizen_wear' then

        ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin, jobSkin)
          TriggerEvent('skinchanger:loadSkin', skin)
        end)

      end

      if data.current.value == 'gang_wear' then			

        ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin, jobSkin)
					if tonumber(myGangrank) >= 1 and VehicleSpawnerHouse == "house1" then
						if skin.sex == 0 then
						curSkin = house1ClothesBoss_M
					else
						curSkin = house1ClothesBoss_F
					end
				end
				if tonumber(myGangrank) == 0 and VehicleSpawnerHouse == "house1" then
					if skin.sex == 0 then
					curSkin = house1ClothesMember_M
				else
					curSkin = house1ClothesMember_F
				end
			end
			if tonumber(myGangrank) >= 1 and VehicleSpawnerHouse == "house2" then
				if skin.sex == 0 then
				curSkin = house2ClothesBoss_M
			else
				curSkin = house2ClothesBoss_F
			end
		end
		if tonumber(myGangrank) == 0 and VehicleSpawnerHouse == "house2" then
			if skin.sex == 0 then
			curSkin = house2ClothesMember_M
		else
			curSkin = house2ClothesMember_F
		end
	end
						if tonumber(myGangrank) >= 1 and VehicleSpawnerHouse == "house3" then
						if skin.sex == 0 then
						curSkin = house3ClothesBoss_M
					else
						curSkin = house3ClothesBoss_F
					end
				end
				if tonumber(myGangrank) == 0 and VehicleSpawnerHouse == "house3" then
					if skin.sex == 0 then
					curSkin = house3ClothesMember_M
				else
					curSkin = house3ClothesMember_F
				end
			end
			if tonumber(myGangrank) >= 1 and VehicleSpawnerHouse == "house4" then
				if skin.sex == 0 then
				curSkin = house4ClothesBoss_M
			else
				curSkin = house4ClothesBoss_F
			end
		end
		if tonumber(myGangrank) == 0 and VehicleSpawnerHouse == "house4" then
			if skin.sex == 0 then
			curSkin = house4ClothesMember_M
		else
			curSkin = house4ClothesMember_F
		end
	end
				if tonumber(myGangrank) >= 1 and VehicleSpawnerHouse == "house5" then
				if skin.sex == 0 then
				curSkin = house5ClothesBoss_M
			else
				curSkin = house5ClothesBoss_F
			end
		end
		if tonumber(myGangrank) == 0 and VehicleSpawnerHouse == "house5" then
			if skin.sex == 0 then
			curSkin = house5ClothesMember_M
		else
			curSkin = house5ClothesMember_F
		end
	end
				if tonumber(myGangrank) >= 1 and VehicleSpawnerHouse == "house6" then
				if skin.sex == 0 then
				curSkin = house6ClothesBoss_M
			else
				curSkin = house6ClothesBoss_F
			end
		end
		if tonumber(myGangrank) == 0 and VehicleSpawnerHouse == "house6" then
			if skin.sex == 0 then
			curSkin = house6ClothesMember_M
		else
			curSkin = house6ClothesMember_F
		end
	end
	
	
				if tonumber(myGangrank) >= 1 and VehicleSpawnerHouse == "house7" then
				if skin.sex == 0 then
				curSkin = house7ClothesBoss_M
			else
				curSkin = house7ClothesBoss_F
			end
		end
		if tonumber(myGangrank) == 0 and VehicleSpawnerHouse == "house7" then
			if skin.sex == 0 then
			curSkin = house7ClothesMember_M
		else
			curSkin = house7ClothesMember_F
		end
	end
	
	
	
		if tonumber(myGangrank) >= 1 and VehicleSpawnerHouse == "house8" then
				if skin.sex == 0 then
				curSkin = house8ClothesBoss_M
			else
				curSkin = house8ClothesBoss_F
			end
		end
		if tonumber(myGangrank) == 0 and VehicleSpawnerHouse == "house8" then
			if skin.sex == 0 then
			curSkin = house8ClothesMember_M
		else
			curSkin = house8ClothesMember_F
		end
	end
	
	
		if tonumber(myGangrank) >= 1 and VehicleSpawnerHouse == "house9" then
				if skin.sex == 0 then
				curSkin = house9ClothesBoss_M
			else
				curSkin = house9ClothesBoss_F
			end
		end
		if tonumber(myGangrank) == 0 and VehicleSpawnerHouse == "house9" then
			if skin.sex == 0 then
			curSkin = house9ClothesMember_M
		else
			curSkin = house9ClothesMember_F
		end
	end
	
	
		if tonumber(myGangrank) >= 1 and VehicleSpawnerHouse == "house10" then
				if skin.sex == 0 then
				curSkin = house10ClothesBoss_M
			else
				curSkin = house10ClothesBoss_F
			end
		end
		if tonumber(myGangrank) == 0 and VehicleSpawnerHouse == "house10" then
			if skin.sex == 0 then
			curSkin = house10ClothesMember_M
		else
			curSkin = house10ClothesMember_F
		end
	end
	
		if tonumber(myGangrank) >= 1 and VehicleSpawnerHouse == "house11" then
				if skin.sex == 0 then
				curSkin = house11ClothesBoss_M
			else
				curSkin = house11ClothesBoss_F
			end
		end
		if tonumber(myGangrank) == 0 and VehicleSpawnerHouse == "house11" then
			if skin.sex == 0 then
			curSkin = house11ClothesMember_M
		else
			curSkin = house11ClothesMember_F
		end
	end
	
		if tonumber(myGangrank) >= 1 and VehicleSpawnerHouse == "house12" then
				if skin.sex == 0 then
				curSkin = house12ClothesBoss_M
			else
				curSkin = house12ClothesBoss_F
			end
		end
		if tonumber(myGangrank) == 0 and VehicleSpawnerHouse == "house12" then
			if skin.sex == 0 then
			curSkin = house12ClothesMember_M
		else
			curSkin = house12ClothesMember_F
		end
	end
	
		if tonumber(myGangrank) >= 1 and VehicleSpawnerHouse == "house13" then
				if skin.sex == 0 then
				curSkin = house13ClothesBoss_M
			else
				curSkin = house13ClothesBoss_F
			end
		end
		if tonumber(myGangrank) == 0 and VehicleSpawnerHouse == "house13" then
			if skin.sex == 0 then
			curSkin = house13ClothesMember_M
		else
			curSkin = house13ClothesMember_F
		end
	end
	
	if tonumber(myGangrank) >= 1 and VehicleSpawnerHouse == "house14" then
				if skin.sex == 0 then
				curSkin = house14ClothesBoss_M
			else
				curSkin = house14ClothesBoss_F
			end
		end
		if tonumber(myGangrank) == 0 and VehicleSpawnerHouse == "house14" then
			if skin.sex == 0 then
			curSkin = house14ClothesMember_M
		else
			curSkin = house14ClothesMember_F
		end
	end
	
		if tonumber(myGangrank) >= 1 and VehicleSpawnerHouse == "house15" then
				if skin.sex == 0 then
				curSkin = house15ClothesBoss_M
			else
				curSkin = house15ClothesBoss_F
			end
		end
		if tonumber(myGangrank) == 0 and VehicleSpawnerHouse == "house15" then
			if skin.sex == 0 then
			curSkin = house15ClothesMember_M
		else
			curSkin = house15ClothesMember_F
		end
	end

	
					if skin.sex == 0 then
            TriggerEvent('skinchanger:loadClothes', skin, json.decode(curSkin))
					else
            TriggerEvent('skinchanger:loadClothes', skin, json.decode(curSkin))
          end

        end)

      end

    end,
    function(data, menu)
      menu.close()
    end
  )

end

function OpenRoomInventoryMenu(property, owner)
	ESX.TriggerServerCallback('stadus_gh:getPropertyInventory', function(inventory)

    local elements = {}

    table.insert(elements, {label = "Svart " .. inventory.blackMoney, type = 'item_account', value = 'black_money'})



    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'room_inventory',
      {
        title    = "Gangs house",
        align    = 'top-left',
        elements = elements,
      },
      function(data, menu)

        menu.close()


      end,
      function(data, menu)
        menu.close()
      end
    )

  end, "")

end


function OpenGangHouseMenu()

  local elements = {
    {label = "ฝากอาวุธ", value = 'wput_stock'},
    {label = "เบิกอาวุธ", value = 'wget_stock'},
    {label = "ฝากสิ่งของ", value = 'put_stock'},
    {label = "เบิกสิ่งของ", value = 'get_stock'},
    {label = "ซื้อโมเดลปืน", value = 'permweapons'},
	{label = "ซื้ออาวุธปืน", value = 'buyweapon'},
	{label = "เปลี่ยนชุดแก๊ง", value = 'clothing'}
  }
  ESX.UI.Menu.CloseAll()

  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'taxi_actions',
    {
      title    = 'โกดังบ้านแก๊ง',
      elements = elements
    },
    function(data, menu)

			if data.current.value == 'clothing' then
				OpenCloakroomMenu()
			end

      if data.current.value == 'put_stock' then
        OpenPutStocksMenu()
      end

      if data.current.value == 'get_stock' then
        OpenGetStocksMenu()
      end    

		  if data.current.value == 'wput_stock' then
        OpenPutWeaponMenu()
      end

      if data.current.value == 'wget_stock' then
        OpenGetWeaponMenu()
      end   

	 		if data.current.value == 'permweapons' then
        OpenPermBuyMenu()
      end
	  
      if data.current.value == 'buyweapon' then
				OpenPermWeaponMenu()
      end


    end,
    function(data, menu)

      menu.close()

    end
  )

end

function OpenPermBuyMenu()
	local elements = {}
if VehicleSpawnerHouse == "house1" or VehicleSpawnerHouse == "house7" or VehicleSpawnerHouse == "house2" then
	table.insert(elements,{label = "Pistol $"..permPistol.."", value = 'WEAPON_PISTOL'})
	table.insert(elements,{label = "Micro SMG $"..permAssualtSmg.."", value = 'WEAPON_MICROSMG'})
	table.insert(elements,{label = "Assualt Rifle $"..permARifle.."", value = 'WEAPON_ASSAULTRIFLE'})
	table.insert(elements,{label = "Pump shotgun $"..permPump.."", value = 'WEAPON_PUMPSHOTGUN'})
	
	elseif VehicleSpawnerHouse == "house12" or VehicleSpawnerHouse == "house10" then	
	table.insert(elements,{label = "Pistol $"..permPistol.."", value = 'WEAPON_PISTOL'})
	table.insert(elements,{label = "Micro SMG $"..permAssualtSmg.."", value = 'WEAPON_MICROSMG'})
	table.insert(elements,{label = "Assualt Rifle $"..permARifle.."", value = 'WEAPON_ASSAULTRIFLE'})
	table.insert(elements,{label = "Pump shotgun $"..permPump.."", value = 'WEAPON_PUMPSHOTGUN'})
	table.insert(elements,{label = "AP Pistol $"..permAPpistol.."", value = 'WEAPON_APPISTOL'})
	table.insert(elements,{label = "ASSAULT SMG $"..permARSmg.."", value = 'WEAPON_ASSAULTSMG'})
	table.insert(elements,{label = "Sawnoff Shotgun $"..permSawn.."", value = 'WEAPON_SAWNOFFSHOTGUN'})
	table.insert(elements,{label = "SPECIAL CARBINE $"..permSpecialC.."", value = 'WEAPON_SPECIALCARBINE'})
	
	elseif VehicleSpawnerHouse == "house5" or VehicleSpawnerHouse == "house6" then	
	table.insert(elements,{label = "Pistol $"..permPistol.."", value = 'WEAPON_PISTOL'})
	table.insert(elements,{label = "Micro SMG $"..permAssualtSmg.."", value = 'WEAPON_MICROSMG'})
	table.insert(elements,{label = "Assualt Rifle $"..permARifle.."", value = 'WEAPON_ASSAULTRIFLE'})
	table.insert(elements,{label = "Pump shotgun $"..permPump.."", value = 'WEAPON_PUMPSHOTGUN'})
	table.insert(elements,{label = "AP Pistol $"..permAPpistol.."", value = 'WEAPON_APPISTOL'})
	table.insert(elements,{label = "ASSAULT SMG $"..permARSmg.."", value = 'WEAPON_ASSAULTSMG'})
	table.insert(elements,{label = "Sawnoff Shotgun $"..permSawn.."", value = 'WEAPON_SAWNOFFSHOTGUN'})
	table.insert(elements,{label = "SPECIAL CARBINE $"..permSpecialC.."", value = 'WEAPON_SPECIALCARBINE'})
	table.insert(elements,{label = "MP5 $"..permMP5.."", value = 'WEAPON_SMG'})
	table.insert(elements,{label = "Carbine Rifle $"..permCarAR.."", value = 'WEAPON_CARBINERIFLE'})
	table.insert(elements,{label = "Gusenberg $"..permGusenBurg.."", value = 'WEAPON_GUSENBERG'})
	table.insert(elements,{label = "Bullpuprifle $"..permBullPupR.."", value = 'WEAPON_BULLPUPRIFLE'})
	
	elseif VehicleSpawnerHouse == "house3" or VehicleSpawnerHouse == "house4" or VehicleSpawnerHouse == "house9" or VehicleSpawnerHouse == "house11" 
	or VehicleSpawnerHouse == "house13" or VehicleSpawnerHouse == "house14" or VehicleSpawnerHouse == "house15" then	
	table.insert(elements,{label = "Pistol $"..permPistol.."", value = 'WEAPON_PISTOL'})
	table.insert(elements,{label = "Micro SMG $"..permAssualtSmg.."", value = 'WEAPON_MICROSMG'})
	table.insert(elements,{label = "Assualt Rifle $"..permARifle.."", value = 'WEAPON_ASSAULTRIFLE'})
	table.insert(elements,{label = "Pump shotgun $"..permPump.."", value = 'WEAPON_PUMPSHOTGUN'})
	table.insert(elements,{label = "AP Pistol $"..permAPpistol.."", value = 'WEAPON_APPISTOL'})
	table.insert(elements,{label = "ASSAULT SMG $"..permARSmg.."", value = 'WEAPON_ASSAULTSMG'})
	table.insert(elements,{label = "Sawnoff Shotgun $"..permSawn.."", value = 'WEAPON_SAWNOFFSHOTGUN'})
	table.insert(elements,{label = "SPECIAL CARBINE $"..permSpecialC.."", value = 'WEAPON_SPECIALCARBINE'})
	table.insert(elements,{label = "MP5 $"..permMP5.."", value = 'WEAPON_SMG'})
	table.insert(elements,{label = "Carbine Rifle $"..permCarAR.."", value = 'WEAPON_CARBINERIFLE'})
	table.insert(elements,{label = "Gusenberg $"..permGusenBurg.."", value = 'WEAPON_GUSENBERG'})
	table.insert(elements,{label = "Bullpuprifle $"..permBullPupR.."", value = 'WEAPON_BULLPUPRIFLE'})
	table.insert(elements,{label = "Advanced Rifle $"..permAdvRifle.."", value = 'WEAPON_ADVANCEDRIFLE'})
	table.insert(elements,{label = "Assualt Shotgun $"..permAssualtShotgun.."", value = 'WEAPON_ASSAULTSHOTGUN'})
	table.insert(elements,{label = "Pistol 50 $"..permPistol50.."", value = 'WEAPON_PISTOL50'})
	table.insert(elements,{label = "CombatPDW $"..permCombatPDW.."", value = 'WEAPON_COMBATPDW'})
end

  ESX.UI.Menu.CloseAll()

  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'taxi_actions',
    {
      title    = 'เลือกซื้อโมเดลปืน',
      elements = elements
    },
    function(data, menu)

	if data.current.value == "WEAPON_PISTOL" then
		TriggerServerEvent('stadus_gh:buypermItem', data.current.value, permPistol, myGang, discWEAPON_PISTOL)
	end
	if data.current.value == "WEAPON_MICROSMG" then
		TriggerServerEvent('stadus_gh:buypermItem', data.current.value, permAssualtSmg, myGang, discWEAPON_MICROSMG)
	end
	if data.current.value == "WEAPON_ASSAULTRIFLE" then
		TriggerServerEvent('stadus_gh:buypermItem', data.current.value, permARifle, myGang, discWEAPON_ASSAULTRIFLE)
	end
	if data.current.value == "WEAPON_PUMPSHOTGUN" then
		TriggerServerEvent('stadus_gh:buypermItem', data.current.value, permPump, myGang, discWEAPON_PUMPSHOTGUN)
	end
	if data.current.value == "WEAPON_APPISTOL" then
		TriggerServerEvent('stadus_gh:buypermItem', data.current.value, permAPpistol, myGang, discAPpistol)
	end
	if data.current.value == "WEAPON_ASSAULTSMG" then
		TriggerServerEvent('stadus_gh:buypermItem', data.current.value, permARSmg, myGang, discARSmg)
	end
	if data.current.value == "WEAPON_SAWNOFFSHOTGUN" then
		TriggerServerEvent('stadus_gh:buypermItem', data.current.value, permSawn, myGang, discSawn)
	end
	if data.current.value == "WEAPON_SPECIALCARBINE" then
		TriggerServerEvent('stadus_gh:buypermItem', data.current.value, permSpecialC, myGang, discSpecialC)
	end
	if data.current.value == "WEAPON_SMG" then
		TriggerServerEvent('stadus_gh:buypermItem', data.current.value, permMP5, myGang, discMP5)
	end
	if data.current.value == "WEAPON_CARBINERIFLE" then
		TriggerServerEvent('stadus_gh:buypermItem', data.current.value, permCarAR, myGang, discCarAR)
	end
	if data.current.value == "WEAPON_GUSENBERG" then
		TriggerServerEvent('stadus_gh:buypermItem', data.current.value, permGusenBurg, myGang, discGusenBurg)
	end
	if data.current.value == "WEAPON_BULLPUPRIFLE" then
		TriggerServerEvent('stadus_gh:buypermItem', data.current.value, permBullPupR, myGang, discBullPupR)
	end
	if data.current.value == "WEAPON_ADVANCEDRIFLE" then
		TriggerServerEvent('stadus_gh:buypermItem', data.current.value, permAdvRifle, myGang, discAdvRifle)
	end
	if data.current.value == "WEAPON_ASSAULTSHOTGUN" then
		TriggerServerEvent('stadus_gh:buypermItem', data.current.value, permAssualtShotgun, myGang, discAssualtShotgun)
	end
	if data.current.value == "WEAPON_PISTOL50" then
		TriggerServerEvent('stadus_gh:buypermItem', data.current.value, permPistol50, myGang, discPistol50)
	end
	if data.current.value == "WEAPON_COMBATPDW" then
		TriggerServerEvent('stadus_gh:buypermItem', data.current.value, permCombatPDW, myGang, discCombatPDW)
	end

    end,
    function(data, menu)

      menu.close()

    end
  )

end

function OpenPermWeaponMenu()
  ESX.TriggerServerCallback('stadus_gh:getPermWeapons', function(weapons)

    local elements = {}

    for i=1, #weapons, 1 do
      if weapons[i].count > 0 then
        table.insert(elements, {label = 'x' .. weapons[i].count .. ' ' .. ESX.GetWeaponLabel(weapons[i].name)..' $'..weapons[i].prices, value = weapons[i].name, price = weapons[i].prices})
      end
    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'armory_get_weapon',
      {
        title    = "ซื้ออาวุธปืน",
        align    = 'top-left',
        elements = elements,
      },
      function(data, menu)

        menu.close()
		
	TriggerServerEvent('stadus_gh:getPermItem', data.current.value, data.current.price)

      end,
      function(data, menu)
        menu.close()
      end
    )

  end, myGang)
end

function OpenPutWeaponMenu()

  local elements   = {}
  local playerPed  = GetPlayerPed(-1)
  local weaponList = ESX.GetWeaponList()

  for i=1, #weaponList, 1 do

    local weaponHash = GetHashKey(weaponList[i].name)

    if HasPedGotWeapon(playerPed,  weaponHash,  false) and weaponList[i].name ~= 'WEAPON_UNARMED' then
      local ammo = GetAmmoInPedWeapon(playerPed, weaponHash)
      table.insert(elements, {label = weaponList[i].label, value = weaponList[i].name})
    end

  end

  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'armory_put_weapon',
    {
      title    = "เลือกอาวุธที่จะฝาก",
      align    = 'top-left',
      elements = elements,
    },
    function(data, menu)

      menu.close()

      ESX.TriggerServerCallback('stadus_gh:addArmoryWeapon', function()
        OpenPutWeaponMenu()
      end, data.current.value, myGang)

    end,
    function(data, menu)
      menu.close()
    end
  )

end


function OpenGetWeaponMenu()

  ESX.TriggerServerCallback('stadus_gh:getArmoryWeapons', function(weapons)

    local elements = {}

    for i=1, #weapons, 1 do
      if weapons[i].count > 0 then
        table.insert(elements, {label = 'x' .. weapons[i].count .. ' ' .. ESX.GetWeaponLabel(weapons[i].name), value = weapons[i].name})
      end
    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'armory_get_weapon',
      {
        title    = "เลือกอาวุธที่จะเบิก",
        align    = 'top-left',
        elements = elements,
      },
      function(data, menu)

        menu.close()

        ESX.TriggerServerCallback('stadus_gh:removeArmoryWeapon', function()
          OpenGetWeaponMenu()
        end, data.current.value, myGang)

      end,
      function(data, menu)
        menu.close()
      end
    )

  end, myGang)

end
local lastcheckDeposit = 0
local lastItem = ""

function OpenPutStocksMenu()
	if PlayerData.job.name == "police" and PlayerData.job.name ~= nil then
		ESX.TriggerServerCallback('stadus_gh:getPlayerInventory', function(inventory)

			local elements = {}
	
			for i=1, #inventory.items, 1 do
	
				local item = inventory.items[i]
	
				if item.count > 0 then
					table.insert(elements, {label = item.label .. ' x' .. item.count, type = 'item_standard', value = item.name})
				end
	
			end
	
			ESX.UI.Menu.Open(
				'default', GetCurrentResourceName(), 'stocks_menu',
				{
					title    = "เลือกสิ่งของที่จะฝาก",
					elements = elements
				},
				function(data, menu)
	
					local itemName = data.current.value
	
					ESX.UI.Menu.Open(
						'dialog', GetCurrentResourceName(), 'stocks_menu_put_item_count',
						{
							title = "amount"
						},
						function(data2, menu2)
	
							local count = tonumber(data2.value)
	
							if count == nil then
								ESX.ShowNotification("Quantity invalid")
							else
								menu2.close()
								menu.close()
								OpenPutStocksMenu()
	
								TriggerServerEvent('stadus_gh:putStockItems', itemName, count, policeWarrantHouseGang)
							end
	
						end,
						function(data2, menu2)
							menu2.close()
						end
					)
	
				end,
				function(data, menu)
					menu.close()
				end
			)
	
		end)
	else
	
  ESX.TriggerServerCallback('stadus_gh:getPlayerInventory', function(inventory)

    local elements = {}

    for i=1, #inventory.items, 1 do

      local item = inventory.items[i]

      if item.count > 0 then
        table.insert(elements, {label = item.label .. ' x' .. item.count, type = 'item_standard', value = item.name})
      end

    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'stocks_menu',
      {
        title    = "เลือกสิ่งของที่จะฝาก",
        elements = elements
      },
      function(data, menu)

        local itemName = data.current.value

        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'stocks_menu_put_item_count',
          {
            title = "amount"
          },
          function(data2, menu2)
			
            local count = tonumber(data2.value)
			
            if count == nil then
              ESX.ShowNotification("Quantity invalid")
            else
				if lastcheckDeposit >= 200 then
				menu.close()
				menu2.close()
				ESX.ShowNotification("You have deposited too much items, you need to wait...")
				Citizen.Wait(2000)
				lastcheckDeposit = 0
				return
				else
				TriggerServerEvent('stadus_gh:putStockItems', itemName, count, myGang)
				lastcheckDeposit = count
			  		menu.close()
					menu2.close()
					return
			  end
            end

          end,
          function(data2, menu2)
            menu2.close()
          end
        )

      end,
      function(data, menu)
        menu.close()
      end
    )

  end)
end
end

function OpenGetStocksMenu()

	if PlayerData.job.name == "police" and PlayerData.job.name ~= nil then
		print("policeWarrantHouseGangGETSTOCK "..policeWarrantHouseGang)

		ESX.TriggerServerCallback('stadus_gh:getStockItems', function(items)

			print(json.encode(items))
	
			local elements = {}
	
			for i=1, #items, 1 do
				table.insert(elements, {label = 'x' .. items[i].count .. ' ' .. items[i].label, value = items[i].name})
			end
	
			ESX.UI.Menu.Open(
				'default', GetCurrentResourceName(), 'stocks_menu',
				{
					title    = 'เลือกสิ่งของที่จะเบิก',
					elements = elements
				},
				function(data, menu)
	
					local itemName = data.current.value
	
					ESX.UI.Menu.Open(
						'dialog', GetCurrentResourceName(), 'stocks_menu_get_item_count',
						{
							title = "quantity"
						},
						function(data2, menu2)
	
							local count = tonumber(data2.value)
	
							if count == nil then
								ESX.ShowNotification("Quantity Invalid")
							else
								menu2.close()
								menu.close()
								OpenGetStocksMenu()
	
								TriggerServerEvent('stadus_gh:getStockItem', itemName, count, policeWarrantHouseGang)
							end
	
						end,
						function(data2, menu2)
							menu2.close()
						end
					)
	
				end,
				function(data, menu)
					menu.close()
				end
			)
	
		end, policeWarrantHouseGang)

	else

  ESX.TriggerServerCallback('stadus_gh:getStockItems', function(items)

    print(json.encode(items))

    local elements = {}

    for i=1, #items, 1 do
      table.insert(elements, {label = 'x' .. items[i].count .. ' ' .. items[i].label, value = items[i].name})
    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'stocks_menu',
      {
        title    = 'เลือกสิ่งของที่จะเบิก',
        elements = elements
      },
      function(data, menu)

        local itemName = data.current.value

        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'stocks_menu_get_item_count',
          {
            title = "quantity"
          },
          function(data2, menu2)

            local count = tonumber(data2.value)

            if count == nil then
              ESX.ShowNotification("Quantity Invalid")
            else
              menu2.close()
              menu.close()
             
							TriggerServerEvent('stadus_gh:getStockItem', itemName, count, myGang)
							OpenGetStocksMenu()

            end

          end,
          function(data2, menu2)
            menu2.close()
          end
        )

      end,
      function(data, menu)
        menu.close()
      end
    )

  end, myGang)
end
end

local Faketimer = 0
local CarFaketimer = 0

function CleanCarsHouses(PvpIsNows)
print(PvpIsNows)
	ClearAreaOfVehicles(-683.8, -1170.7, 10.1, 2000000.0, false, false, false, false, false) --house1
	ClearAreaOfVehicles(938.9, -1886.9, 31.1, 2000000.0, false, false, false, false, false) --house2
	ClearAreaOfVehicles(-1472.662, -49.877, 54.638, 2000000.0, false, false, false, false, false) --house3
	ClearAreaOfVehicles(-129.8, 900.7, 235.7, 2000000.0, false, false, false, false, false) --house4
	ClearAreaOfVehicles(-175.1, 502.4, 137.4, 2000000.0, false, false, false, false, false) --house5
	ClearAreaOfVehicles(-1294.3, 454.6, 97.5, 2000000.0, false, false, false, false, false) --house6
	ClearAreaOfVehicles(218.21, -166.63, 56.59, 2000000.0, false, false, false, false, false) --house7
	ClearAreaOfVehicles(-853.185, 695.749, 148.786, 2000000.0, false, false, false, false, false) --house8
	ClearAreaOfVehicles(-97.261, 988.158, 235.756, 2000000.0, false, false, false, false, false) --house9
	ClearAreaOfVehicles(117.870, 568.830, 176.697, 2000000.0, false, false, false, false, false) --house10
	ClearAreaOfVehicles(-526.778, 517.118, 112.942, 2000000.0, false, false, false, false, false) --house11
	ClearAreaOfVehicles(-459.221, 537.119, 121.459, 2000000.0, false, false, false, false, false) --house12
	ClearAreaOfVehicles(-1536.971, 130.521, 57.371, 2000000.0, false, false, false, false, false) --house13
	ClearAreaOfVehicles(-429.497, 1109.778, 327.682, 2000000.0, false, false, false, false, false) --house14
	ClearAreaOfVehicles(-1587.769, 15.749, 61.253, 2000000.0, false, false, false, false, false) --house15
	Citizen.Trace('Vehicles are cleared!')
end

local timeToWaitToCleanUp = 0

Citizen.CreateThread(function()

  while true do

  if PvpIsNow == true then
  timeToWaitToCleanUp = 240
  else
  timeToWaitToCleanUp = 1200
  end
  
  Citizen.Wait(0)
if CarFaketimer >= timeToWaitToCleanUp then
CleanCarsHouses(PvpIsNow)
CarFaketimer = 0
end
end
end)

Citizen.CreateThread(function()

RegisterNetEvent('gangPvp:show')
AddEventHandler('gangPvp:show',function(gang1, gang2, money)
	Faketimer = 0
	scaleform = RequestScaleformMovie("mp_big_message_freemode")
	
	while not HasScaleformMovieLoaded(scaleform) do
		Citizen.Wait(0)
	end
	
 PushScaleformMovieFunction(scaleform, "SHOW_SHARD_WASTED_MP_MESSAGE")
        PushScaleformMovieFunctionParameterString("~r~Gang Fight")
        PushScaleformMovieFunctionParameterString("~p~"..gang1.." ~s~VS ~o~"..gang2.."~s~. Bounty:~g~ $"..money.." ~s~The fight will begin shortly, please ~r~don't~s~ interact with the either of the ~y~gangs~s~.")
        PopScaleformMovieFunctionVoid()
	 
	DrawScaleformMovieFullscreen(scaleform, 255, 255, 255, 255, 0)

    while true do
	Citizen.Wait(0)
	DrawScaleformMovieFullscreen(scaleform, 255, 255, 255, 255, 0)
		if Faketimer >= 8 then
			UnloadScaleform(scaleform)
			Faketimer = 0
			break
		end
	end
        UnloadScaleform(scaleform)
end)
end)

Citizen.CreateThread(function()
RegisterNetEvent('gangPvp:won')
AddEventHandler('gangPvp:won',function(gang1, gang2, money)
	Faketimer = 0
	scaleform = RequestScaleformMovie("mp_big_message_freemode")
	TriggerEvent('stadus:removecarsnopvp')
	while not HasScaleformMovieLoaded(scaleform) do
		Citizen.Wait(0)
	end
	
 PushScaleformMovieFunction(scaleform, "SHOW_SHARD_WASTED_MP_MESSAGE")
        PushScaleformMovieFunctionParameterString("~p~"..gang1.." ~s~Won the fight")
        PushScaleformMovieFunctionParameterString("~s~ against ~o~"..gang2.."~s~. Bounty:~g~ $"..money.." ~s~Good job!")
        PopScaleformMovieFunctionVoid()
	 
	DrawScaleformMovieFullscreen(scaleform, 255, 255, 255, 255, 0)

    while true do
	Citizen.Wait(0)
	DrawScaleformMovieFullscreen(scaleform, 255, 255, 255, 255, 0)
		if Faketimer >= 8 then
			UnloadScaleform(scaleform)
			Faketimer = 0
			break
		end
		if IsControlPressed(1, 18) then
		UnloadScaleform(scaleform)
		end
	end
        UnloadScaleform(scaleform)
end)
end)

Citizen.CreateThread(function() -- Thread for  timer
	while true do 
		Citizen.Wait(1000)
		Faketimer = Faketimer + 1 
		CarFaketimer = CarFaketimer + 1 
	end 
end)
