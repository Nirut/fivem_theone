resource_manifest_version '44febabe-d386-4d18-afbe-5e627f4af937'

description 'ESX Gang house system | made by Stadus'

version '1.0.1'

client_scripts {
	'client_gang.lua',
	'client.lua',
	'config.lua',
	'GUI.lua'

}

server_scripts {
	'@mysql-async/lib/MySQL.lua',
	'config.lua',
	'server.lua'
}

exports {
  'getGangHouseName'
}
