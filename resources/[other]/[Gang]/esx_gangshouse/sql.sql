CREATE TABLE `gang_owned` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `gang` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumpning av Data i tabell `gang_owned`
--

-- --------------------------------------------------------

--
-- Tabellstruktur `gang_special`
--

CREATE TABLE `gang_special` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `gang` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- AUTO_INCREMENT för tabell `gang_owned`
--
ALTER TABLE `gang_owned`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT för tabell `gang_special`
--
ALTER TABLE `gang_special`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

  --
-- Index för tabell `gang_owned`
--
ALTER TABLE `gang_owned`
  ADD PRIMARY KEY (`id`);

--
-- Index för tabell `gang_special`
--
ALTER TABLE `gang_special`
  ADD PRIMARY KEY (`id`);
