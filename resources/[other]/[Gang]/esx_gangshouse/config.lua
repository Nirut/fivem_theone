Config                      = {}

------------------------------------------------
------------ PERMANENT WEAPONS PRICES ----------
------------------------------------------------
permPistol = 0
permAssualtSmg = 0
permARifle = 0
permPump = 0
permAPpistol = 0
permARSmg = 0
permSawn = 0
permSpecialC = 0
permMP5 = 0
permCarAR = 0
permGusenBurg = 0
permBullPupR = 0
permAdvRifle = 0
permAssualtShotgun = 0
permPistol50 = 0
permCombatPDW = 0

------------------------------------------------
------------ BUY WEAPONS MENU PRICES  ----------
------------------------------------------------

discWEAPON_PISTOL = 0
discWEAPON_MICROSMG = 0
discWEAPON_ASSAULTRIFLE = 0
discWEAPON_PUMPSHOTGUN = 0
discAPpistol = 0
discARSmg = 0
discSawn = 0
discSpecialC = 0
discMP5 = 0
discCarAR = 0
discGusenBurg = 0
discBullPupR = 0
discAdvRifle = 0
discAssualtShotgun = 0
discPistol50 = 0
discCombatPDW = 0

------------------------------------------------
------------------------------------------------

	house1Vehicles = {	--Hunter 1
      { name = 'zentorno1',  label = 'รถแก๊งฟรี' },
	  { name = 'trezor12',  label = 'trezor12' }
    }

    house2Vehicles = {	--1ST 2
      { name = 'zentorno1',  label = 'รถยนต์ประจำแก๊ง' },
      { name = 'cheetah',  label = 'cheetah ติดต่อ ทรงพล' }
    }

    house3Vehicles = {	--AAA 3
      { name = 'zentorno1',  label = 'รถประจำแก๊ง' },
    }

    house4Vehicles = {	--Jub 4
	 { name = '',  label = 'รถโดเนทแก๊ง01' },
	 { name = '',  label = 'รถโดเนทแก๊ง02' },
    } 
	  
	house5Vehicles = {	--Clitoris 5
      { name = 'zentorno1',  label = 'รถแก๊งฟรี' },
    } 
	   
	house6Vehicles = {	 --Report 6
      { name = 'zentorno1',  label = 'รถยนต์ประจำแก๊งฟรี' },
    } 
	
	house7Vehicles = {	 --PIZZA 7
      { name = 'zentorno1',  label = 'คันฟ้า' },
      { name = 'senna',  label = 'คันแดง' },
      { name = 'C7',  label = 'มาใหม่' },
    } 
	
	house8Vehicles = {	 --Communist 8
      { name = 'zentorno1',  label = 'รถแก๊งฟรี' },
    } 
	
	house9Vehicles = {	 --SoYer 9
      { name = 'zentorno1',  label = 'รถแก๊งฟรี' },
    } 
	
	house10Vehicles = {	 --DES 10
      { name = 'r6',  label = 'รถมอเตอร์ไซต์ประจำแก๊ง' },
      { name = '',  label = 'รถประจำแก๊ง' }
    } 
	
	house11Vehicles = {	 --Temx 11
      { name = 'zentorno1',  label = 'รถประจำแก๊ง' },
    }
	
    house12Vehicles = {	--1ST 12
	  { name = 'zentorno1',  label = 'รถแก๊ง1000' } ,
    }
	
	house13Vehicles = {	--Brazil 13
      { name = 'zentorno1',  label = 'รถแก๊งฟรี' },
    }
	
	house14Vehicles = {	--Pa KhotSadi 14
      { name = 'zentorno1',  label = 'คันฟ้า' },
      { name = 'senna',  label = 'คันแดง' },
    }
	
	house15Vehicles = {	--repot 15
      { name = 'zentorno1',  label = 'รถแก๊งฟรี' },
    }

  house1ClothesMember_M = '{"tshirt_2":0,"hair_color_1":0,"glasses_2":0,"chain_1":0,"chain_2":0,"shoes_1":0,"shoes_2":0,"torso_2":0,"hair_color_2":0,"pants_1":0,"glasses_1":0,"hair_1":0,"sex":0,"decals_2":0,"tshirt_1":0,"helmet_1":0,"helmet_2":0,"arms":0,"face":0,"decals_1":0,"torso_1":0,"hair_2":0,"skin":0,"pants_2":0","mask_1":0,"mask_2":0}'
  house1ClothesMember_F = '{"tshirt_2":0,"hair_color_1":0,"glasses_2":0,"chain_1":0,"chain_2":0,"shoes_1":0,"shoes_2":0,"torso_2":0,"hair_color_2":0,"pants_1":0,"glasses_1":0,"hair_1":0,"sex":0,"decals_2":0,"tshirt_1":0,"helmet_1":0,"helmet_2":0,"arms":0,"face":0,"decals_1":0,"torso_1":0,"hair_2":0,"skin":0,"pants_2":0","mask_1":0,"mask_2":0}'
 
  house1ClothesBoss_M =   '{"tshirt_2":0,"hair_color_1":0,"glasses_2":0,"chain_1":0,"chain_2":0,"shoes_1":0,"shoes_2":0,"torso_2":0,"hair_color_2":0,"pants_1":0,"glasses_1":0,"hair_1":0,"sex":0,"decals_2":0,"tshirt_1":0,"helmet_1":0,"helmet_2":0,"arms":0,"face":0,"decals_1":0,"torso_1":0,"hair_2":0,"skin":0,"pants_2":0,"mask_1":0,"mask_2":0}'
  house1ClothesBoss_F =   '{"tshirt_2":0,"hair_color_1":0,"glasses_2":0,"chain_1":0,"chain_2":0,"shoes_1":0,"shoes_2":0,"torso_2":0,"hair_color_2":0,"pants_1":0,"glasses_1":0,"hair_1":0,"sex":0,"decals_2":0,"tshirt_1":0,"helmet_1":0,"helmet_2":0,"arms":0,"face":0,"decals_1":0,"torso_1":0,"hair_2":0,"skin":0,"pants_2":0,"mask_1":0,"mask_2":0}'
  

  house2ClothesMember_M = '{"tshirt_2":0,"hair_color_1":0,"glasses_2":0,"chain_1":0,"chain_2":0,"shoes_1":0,"shoes_2":0,"torso_2":0,"hair_color_2":0,"pants_1":0,"glasses_1":0,"hair_1":0,"sex":0,"decals_2":0,"tshirt_1":0,"helmet_1":0,"helmet_2":0,"arms":0,"face":0,"decals_1":0,"torso_1":0,"hair_2":0,"skin":0,"pants_2":0","mask_1":0,"mask_2":0}'
  house2ClothesMember_F = '{"tshirt_2":0,"hair_color_1":0,"glasses_2":0,"chain_1":0,"chain_2":0,"shoes_1":0,"shoes_2":0,"torso_2":0,"hair_color_2":0,"pants_1":0,"glasses_1":0,"hair_1":0,"sex":0,"decals_2":0,"tshirt_1":0,"helmet_1":0,"helmet_2":0,"arms":0,"face":0,"decals_1":0,"torso_1":0,"hair_2":0,"skin":0,"pants_2":0","mask_1":0,"mask_2":0}'
 
  house2ClothesBoss_M =   '{"tshirt_2":0,"hair_color_1":0,"glasses_2":0,"chain_1":0,"chain_2":0,"shoes_1":0,"shoes_2":0,"torso_2":0,"hair_color_2":0,"pants_1":0,"glasses_1":0,"hair_1":0,"sex":0,"decals_2":0,"tshirt_1":0,"helmet_1":0,"helmet_2":0,"arms":0,"face":0,"decals_1":0,"torso_1":0,"hair_2":0,"skin":0,"pants_2":0,"mask_1":0,"mask_2":0}'
  house2ClothesBoss_F =   '{"tshirt_2":0,"hair_color_1":0,"glasses_2":0,"chain_1":0,"chain_2":0,"shoes_1":0,"shoes_2":0,"torso_2":0,"hair_color_2":0,"pants_1":0,"glasses_1":0,"hair_1":0,"sex":0,"decals_2":0,"tshirt_1":0,"helmet_1":0,"helmet_2":0,"arms":0,"face":0,"decals_1":0,"torso_1":0,"hair_2":0,"skin":0,"pants_2":0,"mask_1":0,"mask_2":0}'
    
  house3ClothesMember_M = '{"tshirt_2":0,"hair_color_1":0,"glasses_2":0,"shoes_1":32,"shoes_2":2,"torso_2":11,"hair_color_2":0,"pants_1":26,"glasses_1":7,"hair_1":0,"sex":0,"decals_2":0,"tshirt_1":15,"helmet_1":77,"helmet_2":0,"arms":1,"face":0,"decals_1":0,"torso_1":87,"hair_2":0,"skin":0,"pants_2":0,"mask_1":111,"mask_2":4}'
  house3ClothesMember_F = '{"tshirt_2":0,"hair_color_1":0,"glasses_2":3,"shoes_1":32,"shoes_2":2,"torso_2":11,"hair_color_2":0,"pants_1":27,"glasses_1":11,"hair_1":0,"sex":0,"decals_2":0,"tshirt_1":2,"helmet_1":76,"helmet_2":0,"arms":2,"face":0,"decals_1":0,"torso_1":81,"hair_2":0,"skin":0,"pants_2":0,"mask_1":111,"mask_2":4}'
  
  house3ClothesBoss_M = '{"tshirt_2":0,"hair_color_1":0,"glasses_2":0,"shoes_1":26,"shoes_2":0,"torso_2":1,"hair_color_2":0,"pants_1":26,"glasses_1":7,"hair_1":0,"sex":0,"decals_2":0,"tshirt_1":15,"helmet_1":45,"helmet_2":2,"arms":8,"face":0,"decals_1":0,"torso_1":150,"hair_2":0,"skin":0,"pants_2":0}'
  house3ClothesBoss_F = '{"tshirt_2":0,"hair_color_1":0,"glasses_2":3,"shoes_1":32,"shoes_2":2,"torso_2":5,"hair_color_2":0,"pants_1":27,"glasses_1":11,"hair_1":0,"sex":0,"decals_2":0,"tshirt_1":2,"helmet_1":76,"helmet_2":8,"arms":2,"face":0,"decals_1":0,"torso_1":140,"hair_2":0,"skin":0,"pants_2":0}'
    
  house4ClothesMember_M = '{"tshirt_2":0,"hair_color_1":0,"glasses_2":0,"shoes_1":28,"shoes_2":2,"torso_2":0,"hair_color_2":0,"pants_1":28,"glasses_1":0,"hair_1":0,"sex":0,"decals_2":0,"tshirt_1":15,"helmet_1":-1,"helmet_2":0,"arms":15,"face":0,"decals_1":0,"torso_1":15,"hair_2":0,"skin":0,"pants_2":12","mask_1":51,"mask_2":1}'
  house4ClothesMember_F = '{"tshirt_2":0,"hair_color_1":0,"glasses_2":0,"shoes_1":42,"shoes_2":4,"torso_2":0,"hair_color_2":0,"pants_1":27,"glasses_1":0,"hair_1":0,"sex":0,"decals_2":0,"tshirt_1":15,"helmet_1":-1,"helmet_2":0,"arms":15,"face":0,"decals_1":0,"torso_1":15,"hair_2":0,"skin":0,"pants_2":9","mask_1":51,"mask_2":1}'
 
  house4ClothesBoss_M = '{"tshirt_2":0,"hair_color_1":0,"glasses_2":0,"shoes_1":28,"shoes_2":2,"torso_2":5,"hair_color_2":0,"pants_1":28,"glasses_1":0,"hair_1":0,"sex":0,"decals_2":0,"tshirt_1":15,"helmet_1":-1,"helmet_2":0,"arms":14,"face":0,"decals_1":0,"torso_1":70,"hair_2":0,"skin":0,"pants_2":12,"mask_1":51,"mask_2":1}'
  house4ClothesBoss_F = '{"tshirt_2":0,"hair_color_1":0,"glasses_2":0,"shoes_1":0,"shoes_2":0,"torso_2":0,"hair_color_2":0,"pants_1":0,"glasses_1":0,"hair_1":0,"sex":0,"decals_2":0,"tshirt_1":0,"helmet_1":0,"helmet_2":0,"arms":0,"face":0,"decals_1":0,"torso_1":0,"hair_2":0,"skin":0,"pants_2":0,"mask_1":51,"mask_2":1}'
   
  house5ClothesMember_M = '{"tshirt_2":1,"hair_color_1":0,"glasses_2":3,"chain_1":0,"chain_2":0,"shoes_1":45,"shoes_2":3,"torso_2":3,"hair_color_2":0,"pants_1":68,"glasses_1":1,"hair_1":0,"sex":0,"decals_2":1,"tshirt_1":96,"helmet_1":55,"helmet_2":0,"arms":17,"face":0,"decals_1":8,"torso_1":68,"hair_2":0,"skin":0,"pants_2":3","mask_1":0,"mask_2":0}'
  house5ClothesMember_F = '{"tshirt_2":0,"hair_color_1":0,"glasses_2":2,"chain_1":85,"chain_2":0,"shoes_1":1,"shoes_2":12,"torso_2":4,"hair_color_2":0,"pants_1":25,"glasses_1":2,"hair_1":0,"sex":0,"decals_2":0,"tshirt_1":159,"helmet_1":-1,"helmet_2":0,"arms":64,"face":0,"decals_1":0,"torso_1":125,"hair_2":0,"skin":0,"pants_2":5","mask_1":0,"mask_2":0}'
 
  house5ClothesBoss_M =  '{"tshirt_2":1,"hair_color_1":0,"glasses_2":7,"chain_1":20,"chain_2":0,"shoes_1":46,"shoes_2":2,"torso_2":3,"hair_color_2":0,"pants_1":64,"glasses_1":7,"hair_1":0,"sex":0,"decals_2":1,"tshirt_1":96,"helmet_1":55,"helmet_2":0,"arms":17,"face":0,"decals_1":8,"torso_1":69,"hair_2":0,"skin":0,"pants_2":4,"mask_1":0,"mask_2":0}'
  house5ClothesBoss_F =  '{"tshirt_2":0,"hair_color_1":0,"glasses_2":0,"chain_1":2,"chain_2":5,"shoes_1":19,"shoes_2":6,"torso_2":0,"hair_color_2":0,"pants_1":28,"glasses_1":11,"hair_1":0,"sex":0,"decals_2":0,"tshirt_1":34,"helmet_1":44,"helmet_2":0,"arms":11,"face":0,"decals_1":0,"torso_1":0,"hair_2":0,"skin":0,"pants_2":0,"mask_1":0,"mask_2":0}'
    
  house6ClothesMember_M = '{"tshirt_2":0,"hair_color_1":0,"glasses_2":0,"chain_1":0,"chain_2":0,"shoes_1":0,"shoes_2":0,"torso_2":0,"hair_color_2":0,"pants_1":0,"glasses_1":0,"hair_1":0,"sex":0,"decals_2":0,"tshirt_1":0,"helmet_1":0,"helmet_2":0,"arms":0,"face":0,"decals_1":0,"torso_1":0,"hair_2":0,"skin":0,"pants_2":0","mask_1":0,"mask_2":0}'
  house6ClothesMember_F = '{"tshirt_2":0,"hair_color_1":0,"glasses_2":0,"chain_1":0,"chain_2":0,"shoes_1":0,"shoes_2":0,"torso_2":0,"hair_color_2":0,"pants_1":0,"glasses_1":0,"hair_1":0,"sex":0,"decals_2":0,"tshirt_1":0,"helmet_1":0,"helmet_2":0,"arms":0,"face":0,"decals_1":0,"torso_1":0,"hair_2":0,"skin":0,"pants_2":0","mask_1":0,"mask_2":0}'
 
  house6ClothesBoss_M =   '{"tshirt_2":0,"hair_color_1":0,"glasses_2":0,"chain_1":0,"chain_2":0,"shoes_1":0,"shoes_2":0,"torso_2":0,"hair_color_2":0,"pants_1":0,"glasses_1":0,"hair_1":0,"sex":0,"decals_2":0,"tshirt_1":0,"helmet_1":0,"helmet_2":0,"arms":0,"face":0,"decals_1":0,"torso_1":0,"hair_2":0,"skin":0,"pants_2":0,"mask_1":0,"mask_2":0}'
  house6ClothesBoss_F =   '{"tshirt_2":0,"hair_color_1":0,"glasses_2":0,"chain_1":0,"chain_2":0,"shoes_1":0,"shoes_2":0,"torso_2":0,"hair_color_2":0,"pants_1":0,"glasses_1":0,"hair_1":0,"sex":0,"decals_2":0,"tshirt_1":0,"helmet_1":0,"helmet_2":0,"arms":0,"face":0,"decals_1":0,"torso_1":0,"hair_2":0,"skin":0,"pants_2":0,"mask_1":0,"mask_2":0}'
  
  house7ClothesMember_M = '{"tshirt_2":0,"hair_color_1":0,"glasses_2":0,"chain_1":0,"chain_2":0,"shoes_1":0,"shoes_2":0,"torso_2":0,"hair_color_2":0,"pants_1":0,"glasses_1":0,"hair_1":0,"sex":0,"decals_2":0,"tshirt_1":0,"helmet_1":0,"helmet_2":0,"arms":0,"face":0,"decals_1":0,"torso_1":0,"hair_2":0,"skin":0,"pants_2":0","mask_1":0,"mask_2":0}'
  house7ClothesMember_F = '{"tshirt_2":0,"hair_color_1":0,"glasses_2":0,"chain_1":0,"chain_2":0,"shoes_1":0,"shoes_2":0,"torso_2":0,"hair_color_2":0,"pants_1":0,"glasses_1":0,"hair_1":0,"sex":0,"decals_2":0,"tshirt_1":0,"helmet_1":0,"helmet_2":0,"arms":0,"face":0,"decals_1":0,"torso_1":0,"hair_2":0,"skin":0,"pants_2":0","mask_1":0,"mask_2":0}'
 
  house7ClothesBoss_M =   '{"tshirt_2":0,"hair_color_1":0,"glasses_2":0,"chain_1":0,"chain_2":0,"shoes_1":0,"shoes_2":0,"torso_2":0,"hair_color_2":0,"pants_1":0,"glasses_1":0,"hair_1":0,"sex":0,"decals_2":0,"tshirt_1":0,"helmet_1":0,"helmet_2":0,"arms":0,"face":0,"decals_1":0,"torso_1":0,"hair_2":0,"skin":0,"pants_2":0,"mask_1":0,"mask_2":0}'
  house7ClothesBoss_F =   '{"tshirt_2":0,"hair_color_1":0,"glasses_2":0,"chain_1":0,"chain_2":0,"shoes_1":0,"shoes_2":0,"torso_2":0,"hair_color_2":0,"pants_1":0,"glasses_1":0,"hair_1":0,"sex":0,"decals_2":0,"tshirt_1":0,"helmet_1":0,"helmet_2":0,"arms":0,"face":0,"decals_1":0,"torso_1":0,"hair_2":0,"skin":0,"pants_2":0,"mask_1":0,"mask_2":0}'
  
  house8ClothesMember_M = '{"tshirt_2":0,"hair_color_1":0,"glasses_2":0,"chain_1":0,"chain_2":0,"shoes_1":0,"shoes_2":0,"torso_2":0,"hair_color_2":0,"pants_1":0,"glasses_1":0,"hair_1":0,"sex":0,"decals_2":0,"tshirt_1":0,"helmet_1":0,"helmet_2":0,"arms":0,"face":0,"decals_1":0,"torso_1":0,"hair_2":0,"skin":0,"pants_2":0","mask_1":0,"mask_2":0}'
  house8ClothesMember_F = '{"tshirt_2":0,"hair_color_1":0,"glasses_2":0,"chain_1":0,"chain_2":0,"shoes_1":0,"shoes_2":0,"torso_2":0,"hair_color_2":0,"pants_1":0,"glasses_1":0,"hair_1":0,"sex":0,"decals_2":0,"tshirt_1":0,"helmet_1":0,"helmet_2":0,"arms":0,"face":0,"decals_1":0,"torso_1":0,"hair_2":0,"skin":0,"pants_2":0","mask_1":0,"mask_2":0}'
 
  house8ClothesBoss_M =   '{"tshirt_2":0,"hair_color_1":0,"glasses_2":0,"chain_1":0,"chain_2":0,"shoes_1":0,"shoes_2":0,"torso_2":0,"hair_color_2":0,"pants_1":0,"glasses_1":0,"hair_1":0,"sex":0,"decals_2":0,"tshirt_1":0,"helmet_1":0,"helmet_2":0,"arms":0,"face":0,"decals_1":0,"torso_1":0,"hair_2":0,"skin":0,"pants_2":0,"mask_1":0,"mask_2":0}'
  house8ClothesBoss_F =   '{"tshirt_2":0,"hair_color_1":0,"glasses_2":0,"chain_1":0,"chain_2":0,"shoes_1":0,"shoes_2":0,"torso_2":0,"hair_color_2":0,"pants_1":0,"glasses_1":0,"hair_1":0,"sex":0,"decals_2":0,"tshirt_1":0,"helmet_1":0,"helmet_2":0,"arms":0,"face":0,"decals_1":0,"torso_1":0,"hair_2":0,"skin":0,"pants_2":0,"mask_1":0,"mask_2":0}'
    
  house9ClothesMember_M = '{"tshirt_2":0,"hair_color_1":0,"glasses_2":6,"chain_1":0,"chain_2":0,"shoes_1":59,"shoes_2":20,"torso_2":20,"hair_color_2":0,"pants_1":34,"glasses_1":15,"hair_1":0,"sex":0,"decals_2":0,"tshirt_1":131,"helmet_1":107,"helmet_2":20,"arms":59,"face":0,"decals_1":0,"torso_1":222,"hair_2":0,"skin":0,"pants_2":0","mask_1":0,"mask_2":0}'
  house9ClothesMember_F = '{"tshirt_2":0,"hair_color_1":0,"glasses_2":5,"chain_1":0,"chain_2":0,"shoes_1":62,"shoes_2":20,"torso_2":20,"hair_color_2":0,"pants_1":102,"glasses_1":5,"hair_1":0,"sex":0,"decals_2":0,"tshirt_1":161,"helmet_1":106,"helmet_2":20,"arms":59,"face":0,"decals_1":0,"torso_1":232,"hair_2":0,"skin":0,"pants_2":0","mask_1":0,"mask_2":0}'
 
  house9ClothesBoss_M =   '{"tshirt_2":0,"hair_color_1":0,"glasses_2":6,"chain_1":0,"chain_2":0,"shoes_1":59,"shoes_2":20,"torso_2":20,"hair_color_2":0,"pants_1":34,"glasses_1":15,"hair_1":0,"sex":0,"decals_2":0,"tshirt_1":131,"helmet_1":107,"helmet_2":20,"arms":59,"face":0,"decals_1":0,"torso_1":222,"hair_2":0,"skin":0,"pants_2":0","mask_1":0,"mask_2":0}'
  house9ClothesBoss_F =   '{"tshirt_2":0,"hair_color_1":0,"glasses_2":5,"chain_1":0,"chain_2":0,"shoes_1":62,"shoes_2":20,"torso_2":20,"hair_color_2":0,"pants_1":102,"glasses_1":5,"hair_1":0,"sex":0,"decals_2":0,"tshirt_1":161,"helmet_1":106,"helmet_2":20,"arms":59,"face":0,"decals_1":0,"torso_1":232,"hair_2":0,"skin":0,"pants_2":0","mask_1":0,"mask_2":0}'
	
  house10ClothesMember_M = '{"tshirt_2":0,"hair_color_1":0,"glasses_2":0,"chain_1":0,"chain_2":0,"shoes_1":0,"shoes_2":0,"torso_2":0,"hair_color_2":0,"pants_1":0,"glasses_1":0,"hair_1":0,"sex":0,"decals_2":0,"tshirt_1":0,"helmet_1":0,"helmet_2":0,"arms":0,"face":0,"decals_1":0,"torso_1":0,"hair_2":0,"skin":0,"pants_2":0","mask_1":0,"mask_2":0}'
  house10ClothesMember_F = '{"tshirt_2":0,"hair_color_1":0,"glasses_2":0,"chain_1":0,"chain_2":0,"shoes_1":0,"shoes_2":0,"torso_2":0,"hair_color_2":0,"pants_1":0,"glasses_1":0,"hair_1":0,"sex":0,"decals_2":0,"tshirt_1":0,"helmet_1":0,"helmet_2":0,"arms":0,"face":0,"decals_1":0,"torso_1":0,"hair_2":0,"skin":0,"pants_2":0","mask_1":0,"mask_2":0}'
 
  house10ClothesBoss_M =   '{"tshirt_2":0,"hair_color_1":0,"glasses_2":0,"chain_1":0,"chain_2":0,"shoes_1":0,"shoes_2":0,"torso_2":0,"hair_color_2":0,"pants_1":0,"glasses_1":0,"hair_1":0,"sex":0,"decals_2":0,"tshirt_1":0,"helmet_1":0,"helmet_2":0,"arms":0,"face":0,"decals_1":0,"torso_1":0,"hair_2":0,"skin":0,"pants_2":0,"mask_1":0,"mask_2":0}'
  house10ClothesBoss_F =   '{"tshirt_2":0,"hair_color_1":0,"glasses_2":0,"chain_1":0,"chain_2":0,"shoes_1":0,"shoes_2":0,"torso_2":0,"hair_color_2":0,"pants_1":0,"glasses_1":0,"hair_1":0,"sex":0,"decals_2":0,"tshirt_1":0,"helmet_1":0,"helmet_2":0,"arms":0,"face":0,"decals_1":0,"torso_1":0,"hair_2":0,"skin":0,"pants_2":0,"mask_1":0,"mask_2":0}'
  
  house11ClothesMember_M = '{"tshirt_2":3,"hair_color_1":0,"glasses_2":3,"chain_1":20,"chain_2":0,"shoes_1":8,"shoes_2":0,"torso_2":3,"hair_color_2":0,"pants_1":28,"glasses_1":0,"hair_1":0,"sex":0,"decals_2":0,"tshirt_1":26,"helmet_1":-1,"helmet_2":0,"arms":82,"face":0,"decals_1":2,"torso_1":23,"hair_2":0,"skin":0,"pants_2":8","mask_1":94,"mask_2":2}'
  house11ClothesMember_F = '{"tshirt_2":0,"hair_color_1":0,"glasses_2":0,"chain_1":0,"chain_2":0,"shoes_1":0,"shoes_2":0,"torso_2":0,"hair_color_2":0,"pants_1":0,"glasses_1":0,"hair_1":0,"sex":0,"decals_2":0,"tshirt_1":0,"helmet_1":0,"helmet_2":0,"arms":0,"face":0,"decals_1":0,"torso_1":0,"hair_2":0,"skin":0,"pants_2":0","mask_1":0,"mask_2":0}'
 
  house11ClothesBoss_M =   '{"tshirt_2":0,"hair_color_1":0,"glasses_2":5,"chain_1":0,"chain_2":0,"shoes_1":42,"shoes_2":2,"torso_2":18,"hair_color_2":0,"pants_1":29,"glasses_1":5,"hair_1":0,"sex":0,"decals_2":0,"tshirt_1":15,"helmet_1":10,"helmet_2":0,"arms":15,"face":0,"decals_1":0,"torso_1":200,"hair_2":0,"skin":0,"pants_2":2,"mask_1":3,"mask_2":0}'
  house11ClothesBoss_F =   '{"tshirt_2":0,"hair_color_1":0,"glasses_2":0,"chain_1":0,"chain_2":0,"shoes_1":0,"shoes_2":0,"torso_2":0,"hair_color_2":0,"pants_1":0,"glasses_1":0,"hair_1":0,"sex":0,"decals_2":0,"tshirt_1":0,"helmet_1":0,"helmet_2":0,"arms":0,"face":0,"decals_1":0,"torso_1":0,"hair_2":0,"skin":0,"pants_2":0,"mask_1":0,"mask_2":0}'
  

  house12ClothesMember_M = '{"tshirt_2":0,"hair_color_1":0,"glasses_2":0,"chain_1":0,"chain_2":0,"shoes_1":0,"shoes_2":0,"torso_2":0,"hair_color_2":0,"pants_1":0,"glasses_1":0,"hair_1":0,"sex":0,"decals_2":0,"tshirt_1":0,"helmet_1":0,"helmet_2":0,"arms":0,"face":0,"decals_1":0,"torso_1":0,"hair_2":0,"skin":0,"pants_2":0","mask_1":0,"mask_2":0}'
  house12ClothesMember_F = '{"tshirt_2":0,"hair_color_1":0,"glasses_2":0,"chain_1":0,"chain_2":0,"shoes_1":0,"shoes_2":0,"torso_2":0,"hair_color_2":0,"pants_1":0,"glasses_1":0,"hair_1":0,"sex":0,"decals_2":0,"tshirt_1":0,"helmet_1":0,"helmet_2":0,"arms":0,"face":0,"decals_1":0,"torso_1":0,"hair_2":0,"skin":0,"pants_2":0","mask_1":0,"mask_2":0}'
 
  house12ClothesBoss_M =   '{"tshirt_2":0,"hair_color_1":0,"glasses_2":0,"chain_1":0,"chain_2":0,"shoes_1":0,"shoes_2":0,"torso_2":0,"hair_color_2":0,"pants_1":0,"glasses_1":0,"hair_1":0,"sex":0,"decals_2":0,"tshirt_1":0,"helmet_1":0,"helmet_2":0,"arms":0,"face":0,"decals_1":0,"torso_1":0,"hair_2":0,"skin":0,"pants_2":0,"mask_1":0,"mask_2":0}'
  house12ClothesBoss_F =   '{"tshirt_2":0,"hair_color_1":0,"glasses_2":0,"chain_1":0,"chain_2":0,"shoes_1":0,"shoes_2":0,"torso_2":0,"hair_color_2":0,"pants_1":0,"glasses_1":0,"hair_1":0,"sex":0,"decals_2":0,"tshirt_1":0,"helmet_1":0,"helmet_2":0,"arms":0,"face":0,"decals_1":0,"torso_1":0,"hair_2":0,"skin":0,"pants_2":0,"mask_1":0,"mask_2":0}'
  
  house13ClothesMember_M = '{"tshirt_2":1,"hair_color_1":0,"glasses_2":8,"chain_1":12,"chain_2":2,"shoes_1":21,"shoes_2":0,"torso_2":2,"hair_color_2":0,"pants_1":28,"glasses_1":7,"hair_1":0,"sex":0,"decals_2":0,"tshirt_1":10,"helmet_1":-1,"helmet_2":0,"arms":8,"face":0,"decals_1":0,"torso_1":60,"hair_2":0,"skin":0,"pants_2":0","mask_1":0,"mask_2":0}'
  house13ClothesMember_F = '{"tshirt_2":17,"hair_color_1":0,"glasses_2":3,"chain_1":22,"chain_2":0,"shoes_1":6,"shoes_2":0,"torso_2":2,"hair_color_2":0,"pants_1":47,"glasses_1":11,"hair_1":0,"sex":0,"decals_2":0,"tshirt_1":104,"helmet_1":-1,"helmet_2":0,"arms":5,"face":0,"decals_1":0,"torso_1":53,"hair_2":0,"skin":0,"pants_2":0","mask_1":0,"mask_2":0}'
 
  house13ClothesBoss_M =   '{"tshirt_2":0,"hair_color_1":0,"glasses_2":0,"chain_1":0,"chain_2":0,"shoes_1":31,"shoes_2":1,"torso_2":0,"hair_color_2":0,"pants_1":26,"glasses_1":8,"hair_1":0,"sex":0,"decals_2":0,"tshirt_1":15,"helmet_1":66,"helmet_2":0,"arms":15,"face":0,"decals_1":0,"torso_1":15,"hair_2":0,"skin":0,"pants_2":0,"mask_1":0,"mask_2":0}'
  house13ClothesBoss_F =   '{"tshirt_2":0,"hair_color_1":0,"glasses_2":0,"chain_1":0,"chain_2":0,"shoes_1":0,"shoes_2":0,"torso_2":0,"hair_color_2":0,"pants_1":0,"glasses_1":0,"hair_1":0,"sex":0,"decals_2":0,"tshirt_1":0,"helmet_1":0,"helmet_2":0,"arms":0,"face":0,"decals_1":0,"torso_1":0,"hair_2":0,"skin":0,"pants_2":0,"mask_1":0,"mask_2":0}'
  

  house14ClothesMember_M = '{"tshirt_2":0,"hair_color_1":0,"glasses_2":0,"chain_1":0,"chain_2":0,"shoes_1":0,"shoes_2":0,"torso_2":0,"hair_color_2":0,"pants_1":0,"glasses_1":0,"hair_1":0,"sex":0,"decals_2":0,"tshirt_1":0,"helmet_1":0,"helmet_2":0,"arms":0,"face":0,"decals_1":0,"torso_1":0,"hair_2":0,"skin":0,"pants_2":0","mask_1":0,"mask_2":0}'
  house14ClothesMember_F = '{"tshirt_2":0,"hair_color_1":0,"glasses_2":0,"chain_1":0,"chain_2":0,"shoes_1":0,"shoes_2":0,"torso_2":0,"hair_color_2":0,"pants_1":0,"glasses_1":0,"hair_1":0,"sex":0,"decals_2":0,"tshirt_1":0,"helmet_1":0,"helmet_2":0,"arms":0,"face":0,"decals_1":0,"torso_1":0,"hair_2":0,"skin":0,"pants_2":0","mask_1":0,"mask_2":0}'
 
  house14ClothesBoss_M =   '{"tshirt_2":0,"hair_color_1":0,"glasses_2":0,"chain_1":0,"chain_2":0,"shoes_1":0,"shoes_2":0,"torso_2":0,"hair_color_2":0,"pants_1":0,"glasses_1":0,"hair_1":0,"sex":0,"decals_2":0,"tshirt_1":0,"helmet_1":0,"helmet_2":0,"arms":0,"face":0,"decals_1":0,"torso_1":0,"hair_2":0,"skin":0,"pants_2":0,"mask_1":0,"mask_2":0}'
  house14ClothesBoss_F =   '{"tshirt_2":0,"hair_color_1":0,"glasses_2":0,"chain_1":0,"chain_2":0,"shoes_1":0,"shoes_2":0,"torso_2":0,"hair_color_2":0,"pants_1":0,"glasses_1":0,"hair_1":0,"sex":0,"decals_2":0,"tshirt_1":0,"helmet_1":0,"helmet_2":0,"arms":0,"face":0,"decals_1":0,"torso_1":0,"hair_2":0,"skin":0,"pants_2":0,"mask_1":0,"mask_2":0}'
  
  house15ClothesMember_M = '{"tshirt_2":0,"hair_color_1":0,"glasses_2":0,"chain_1":0,"chain_2":0,"shoes_1":0,"shoes_2":0,"torso_2":0,"hair_color_2":0,"pants_1":0,"glasses_1":0,"hair_1":0,"sex":0,"decals_2":0,"tshirt_1":0,"helmet_1":0,"helmet_2":0,"arms":0,"face":0,"decals_1":0,"torso_1":0,"hair_2":0,"skin":0,"pants_2":0","mask_1":0,"mask_2":0}'
  house15ClothesMember_F = '{"tshirt_2":0,"hair_color_1":0,"glasses_2":0,"chain_1":0,"chain_2":0,"shoes_1":0,"shoes_2":0,"torso_2":0,"hair_color_2":0,"pants_1":0,"glasses_1":0,"hair_1":0,"sex":0,"decals_2":0,"tshirt_1":0,"helmet_1":0,"helmet_2":0,"arms":0,"face":0,"decals_1":0,"torso_1":0,"hair_2":0,"skin":0,"pants_2":0","mask_1":0,"mask_2":0}'
 
  house15ClothesBoss_M =   '{"tshirt_2":0,"hair_color_1":0,"glasses_2":0,"chain_1":0,"chain_2":0,"shoes_1":0,"shoes_2":0,"torso_2":0,"hair_color_2":0,"pants_1":0,"glasses_1":0,"hair_1":0,"sex":0,"decals_2":0,"tshirt_1":0,"helmet_1":0,"helmet_2":0,"arms":0,"face":0,"decals_1":0,"torso_1":0,"hair_2":0,"skin":0,"pants_2":0,"mask_1":0,"mask_2":0}'
  house15ClothesBoss_F =   '{"tshirt_2":0,"hair_color_1":0,"glasses_2":0,"chain_1":0,"chain_2":0,"shoes_1":0,"shoes_2":0,"torso_2":0,"hair_color_2":0,"pants_1":0,"glasses_1":0,"hair_1":0,"sex":0,"decals_2":0,"tshirt_1":0,"helmet_1":0,"helmet_2":0,"arms":0,"face":0,"decals_1":0,"torso_1":0,"hair_2":0,"skin":0,"pants_2":0,"mask_1":0,"mask_2":0}'

Config.Zones = {

  VehicleSpawnPoint_h1 = {
    Pos   = {x = -644.45, y = -1195.99, z = 10.61},
    Size  = {x = 1.5, y = 1.5, z = 1.0},
    Type  = -1
  },

  VehicleSpawnPoint_h2 = {
    Pos   = {x = 942.89, y = -1887.14, z = 31.08},
    Size  = {x = 1.5, y = 1.5, z = 1.0},
    Type  = -1
  },

  VehicleSpawnPoint_h3 = {
    Pos   = {x = -1464.224, y = -25.811, z = 54.646},
    Size  = {x = 1.5, y = 1.5, z = 1.0},
    Type  = -1
  },

  VehicleSpawnPoint_h4 = {
    Pos   = {x = -142.248, y = 910.956, z = 235.805},
    Size  = {x = 1.5, y = 1.5, z = 1.0},
    Type  = -1
  },

  VehicleSpawnPoint_h5 = {
    Pos   = {x = -577.720, y = 405.192, z = 100.661},
    Size  = {x = 1.5, y = 1.5, z = 1.0},
    Type  = -1
  },

  VehicleSpawnPoint_h6 = {
    Pos   = {x =  -1296.205, y = 459.835, z = 97.362},
    Size  = {x = 1.5, y = 1.5, z = 1.0},
    Type  = -1
  }, 
  VehicleSpawnPoint_h7 = {
    Pos   = {x =  225.07, y = -168.85, z = 56.41},
    Size  = {x = 1.5, y = 1.5, z = 1.0},
    Type  = -1
  }, 
  VehicleSpawnPoint_h8 = {
    Pos   = {x =  -864.938, y = 706.174, z = 149.258},
    Size  = {x = 1.5, y = 1.5, z = 1.0},
    Type  = -1
  }, 
  VehicleSpawnPoint_h9 = {
    Pos   = {x = -123.113, y = 996.824, z = 235.752},
    Size  = {x = 1.5, y = 1.5, z = 1.0},
    Type  = -1
  }, 
  VehicleSpawnPoint_h10 = {
    Pos   = {x = -432.799, y = 347.900, z = 105.903},
    Size  = {x = 1.5, y = 1.5, z = 1.0},
    Type  = -1
  }, 
   VehicleSpawnPoint_h11 = {
    Pos   = {x = -529.230, y = 531.278, z = 111.551},
    Size  = {x = 1.5, y = 1.5, z = 1.0},
    Type  = -1
  }, 
  VehicleSpawnPoint_h12 = {
    Pos   = {x = -470.196, y = 544.537, z = 120.371},
    Size  = {x = 1.5, y = 1.5, z = 1.0},
    Type  = -1
  }, 
    VehicleSpawnPoint_h13 = {
    Pos   = {x = -56.223, y = 341.882, z = 112.023},
    Size  = {x = 1.5, y = 1.5, z = 1.0},
    Type  = -1
  }, 
  VehicleSpawnPoint_h14 = {
    Pos   = {x = -1575.531, y = -58.972, z = 56.491},
    Size  = {x = 1.5, y = 1.5, z = 1.0},
    Type  = -1
  }, 
  VehicleSpawnPoint_h15 = {
    Pos   = {x = -1554.370, y = 24.623, z = 58.572},
    Size  = {x = 1.5, y = 1.5, z = 1.0},
    Type  = -1
  }, 
}
