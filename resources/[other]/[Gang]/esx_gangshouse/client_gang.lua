--[[
------------------
- made by Stadus -
-    StadusRP    -
-   client.lua   -
-  stadus.party  -
------------------
]]--

------------- PRICES FOR GANG HOUSE SELL -----------

local house1 = 1
local house2 = 1
local house3 = 1
local house4 = 1
local house5 = 1
local house6 = 1
local house7 = 1
local house8 = 1
local house9 = 1

		local x1 = -54.55
		local y1 = -871.20
		local z1 = 41.54

----------------------------------------------------

local Keys = {
  ["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
  ["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
  ["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
  ["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
  ["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
  ["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
  ["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
  ["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
  ["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

local emplacement = {
{name="Ganghouse Buy", id=374, colour=20, x=x1, y=y1, z=z1},
}
local options = {
    x = 0.1,
    y = 0.2,
    width = 0.2,
    height = 0.04,
    scale = 0.4,
    font = 0,
    menu_title = "Ganghouse", -- title menu // titre menu
    menu_subtitle = "Menu", -- subtitle menu // sous titre menu
    color_r = 155, -- R
    color_g = 89, -- G
    color_b = 182,  -- B
}

local showBlip = true -- Show blip on map
local maxDirty = 200000 -- Player allowed to laundering per 100 000 // Le joueur ne peut blanchir que 100 000$ par 100 000$
local openKey = 51 -- PRESS E TO OPEN MENU 
local GUI                     = {}
local HasAlreadyEnteredMarker = false
local LastZone                = nil
local PlayerData              = {}
local CurrentAction           = nil
local CurrentActionMsg        = ''
local CurrentActionData       = {}
local IsInShopMenu            = false
local Categories              = {}
local Vehicles                = {}
local LastVehicles            = {}
local CurrentVehicleData      = nil

ESX                           = nil
GUI.Time                      = 0

local myGang = ""
local myGangrank = ""
	
Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end
end)

-- Show blip
Citizen.CreateThread(function()
 if (showBlip == true) then
    for _, item in pairs(emplacement) do
      item.blip = AddBlipForCoord(item.x, item.y, item.z)
      SetBlipSprite(item.blip, item.id)
      SetBlipColour(item.blip, item.colour)
      SetBlipAsShortRange(item.blip, true)
      BeginTextCommandSetBlipName("STRING")
      AddTextComponentString(item.name)
      EndTextCommandSetBlipName(item.blip)
    end
 end
end)

--- Location
Citizen.CreateThread(
	function()
	--X, Y, Z coords 

		while true do
			Citizen.Wait(0)
			local playerPos = GetEntityCoords(GetPlayerPed(-1), true)
			if (Vdist(playerPos.x, playerPos.y, playerPos.z, x1, y1, z1) < 20.0) then
				DrawMarker(0, x1, y1, z1, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 10, 10,165, 0, 0, 0,0)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, x1, y1, z1) < 2.0) then						
					DisplayHelpText('Press ~INPUT_CONTEXT~ to look ~y~which~s~ ~g~gang houses~s~ that exists!')
					if (IsControlJustReleased(1, openKey)) then 
						BlanchirMenu()
						Menu.hidden = not Menu.hidden
					end
					
					Menu.renderGUI(options) 
					else
					Menu.hidden = true
				end
				
			end
		end
end)



---- FUNCTIONS ----
function Notify(text)
	SetNotificationTextEntry('STRING')
	AddTextComponentString(text)
	DrawNotification(false, false)
end

function DisplayHelpText(str)
	SetTextComponentFormat("STRING")
	AddTextComponentString(str)
	DisplayHelpTextFromStringLabel(0, 0, 1, -1)
end
---------------------
---- Menu
function BlanchirMenu()
   options.menu_subtitle = "Places"
    ClearMenu()
	Menu.addButton("Ganghouse1 - ~g~"..house1.." $", "ganghouse1", -1)
	Menu.addButton("Ganghouse2 - ~g~"..house2.." $", "ganghouse2", -1)
	Menu.addButton("Ganghouse3 - ~g~"..house3.." $", "ganghouse3", -1)
	Menu.addButton("Ganghouse4 - ~g~"..house4.." $", "ganghouse4", -1)
	Menu.addButton("Ganghouse5 - ~g~"..house5.." $", "ganghouse5", -1)
	Menu.addButton("Ganghouse6 - ~g~"..house6.." $", "ganghouse6", -1)
	Menu.addButton("Close", "CloseMenu", nil)
end

function ganghouse1()

myGang = tostring(exports['esx_gangs']:getGangName())
myGangrank = tostring(exports['esx_gangs']:getGangRank())
TriggerServerEvent('stadus_gh:buyHouse1', GetPlayerServerId(PlayerId()), myGang, myGangrank)
Menu.hidden = true
end

function ganghouse2()
myGang = tostring(exports['esx_gangs']:getGangName())
myGangrank = tostring(exports['esx_gangs']:getGangRank())
TriggerServerEvent('stadus_gh:buyHouse2', GetPlayerServerId(PlayerId()), myGang, myGangrank)
Menu.hidden = true
end

function ganghouse3()
myGang = tostring(exports['esx_gangs']:getGangName())
myGangrank = tostring(exports['esx_gangs']:getGangRank())
TriggerServerEvent('stadus_gh:buyHouse3', GetPlayerServerId(PlayerId()), myGang, myGangrank)
Menu.hidden = true
end

function ganghouse4()
	myGang = tostring(exports['esx_gangs']:getGangName())
	myGangrank = tostring(exports['esx_gangs']:getGangRank())
	TriggerServerEvent('stadus_gh:buyHouse4', GetPlayerServerId(PlayerId()), myGang, myGangrank)
	Menu.hidden = true
end

function ganghouse5()
	myGang = tostring(exports['esx_gangs']:getGangName())
	myGangrank = tostring(exports['esx_gangs']:getGangRank())
	TriggerServerEvent('stadus_gh:buyHouse5', GetPlayerServerId(PlayerId()), myGang, myGangrank)
	Menu.hidden = true
end

function ganghouse6()
	myGang = tostring(exports['esx_gangs']:getGangName())
	myGangrank = tostring(exports['esx_gangs']:getGangRank())
	TriggerServerEvent('stadus_gh:buyHouse6', GetPlayerServerId(PlayerId()), myGang, myGangrank)
	Menu.hidden = true
end

function OpenShopMenu()
Menu.hidden = true
end

function CloseMenu()
    Menu.hidden = true
end
