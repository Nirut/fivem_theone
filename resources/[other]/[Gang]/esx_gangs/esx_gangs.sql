CREATE TABLE `gangs` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  `color` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumpning av Data i tabell `gangs`
--

INSERT INTO `gangs` (`id`, `name`, `label`, `color`) VALUES
(1, 'none', 'Unaffiliated', '');

-- --------------------------------------------------------

--
-- Tabellstruktur `gang_grades`
--

CREATE TABLE `gang_grades` (
  `id` int(11) NOT NULL,
  `gang_name` varchar(255) NOT NULL,
  `grade` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumpning av Data i tabell `gang_grades`
--

INSERT INTO `gang_grades` (`id`, `gang_name`, `grade`, `name`, `label`) VALUES
(1, 'none', 0, 'none', 'Unaffiliated');

ALTER TABLE `gangs`
  ADD PRIMARY KEY (`id`);

--
-- Index för tabell `gang_grades`
--
ALTER TABLE `gang_grades`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `gangs`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
  
ALTER TABLE `gang_grades`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

ALTER TABLE `users`
ADD COLUMN `gang` VARCHAR(255) NULL,
ADD COLUMN `gang_grade` INT(11) NULL;