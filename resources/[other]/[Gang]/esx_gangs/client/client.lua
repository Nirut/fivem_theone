--================================================================================================
--==                                VARIABLES - DO NOT EDIT                                     ==
--================================================================================================
ESX                         = nil
inMenu                      = true
local showblips = true
local atbank = false
local bankMenu = true

--================================================================================================
--==                                THREADING - DO NOT EDIT                                     ==
--================================================================================================

--===============================================
--==           Base ESX Threading              ==
--===============================================
Citizen.CreateThread(function()
  while ESX == nil do
    TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
    Citizen.Wait(0)
  end
end)





--===============================================
--==             Core Threading                ==
--===============================================
if bankMenu then
	Citizen.CreateThread(function()
  while true do
    Wait(0)

		if IsControlJustPressed(1, 56) then
		ESX.TriggerServerCallback('stadusrp_drugs:getGangName', function(own)
		if own ~= "none" then
			inMenu = true
			SetNuiFocus(true, true)
			SendNUIMessage({type = 'openGeneral'})
			TriggerServerEvent('bank:balance')
			local ped = GetPlayerPed(-1)
			
			else
		--	print("nein")
			
			end
			end)
		end
        
    if IsControlJustPressed(1, 322) then
	  inMenu = false
      SetNuiFocus(false, false)
      SendNUIMessage({type = 'close'})
    end
	end
  end)
end



--===============================================
--==           Deposit Event                   ==
--===============================================
RegisterNetEvent('currentbalance1')
AddEventHandler('currentbalance1', function(balance)
	local id = PlayerId()
	local playerName = GetPlayerName(id)
	ESX.TriggerServerCallback('stadusrp_drugs:getGangName', function(own)
		SendNUIMessage({
			type = "balanceHUD",
			balance = balance,
			gang = own
			})
	end)
end)


RegisterNetEvent('esx_gangs:setGPSWeed')
AddEventHandler('esx_gangs:setGPSWeed', function(source, place)

SetNewWaypoint(143.62, -1656.93)

end)

RegisterNetEvent('esx_gangs:setGPSKranen')
AddEventHandler('esx_gangs:setGPSKranen', function(source, place)

SetNewWaypoint(-1187.53, -456.87)

end)

RegisterNetEvent('esx_gangs:setGPSWash')
AddEventHandler('esx_gangs:setGPSWash', function(source, place)

SetNewWaypoint(-966.98, -267.25)

end)

RegisterNetEvent('esx_gangs:setGPSVapen')
AddEventHandler('esx_gangs:setGPSVapen', function(source, place)

SetNewWaypoint(285.61, 2846.77)

end)

RegisterNetEvent('esx_gangs:setGPSMeth')
AddEventHandler('esx_gangs:setGPSMeth', function(source, place)

SetNewWaypoint(-1402.79, -451.84)

end)

RegisterNetEvent('esx_gangs:setGPSCoke')
AddEventHandler('esx_gangs:setGPSCoke', function(source, place)

SetNewWaypoint(-2952.68, 50.42)

end)




--===============================================
--==               NUIFocusoff                 ==
--===============================================
RegisterNUICallback('NUIFocusOff', function()
  inMenu = false
  SetNuiFocus(false, false)
  SendNUIMessage({type = 'closeAll'})
end)

function DisplayHelpText(str)
	SetTextComponentFormat("STRING")
	AddTextComponentString(str)
	DisplayHelpTextFromStringLabel(0, 0, 1, -1)
end