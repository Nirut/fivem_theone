local Keys = {
  ["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
  ["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
  ["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
  ["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
  ["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
  ["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
  ["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
  ["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
  ["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

local GUI           = {}
GUI.Time            = 0
local PlayerData = {}
PlayerData.gang = {
  name = nil,
  gang_grade = nil,
  gang_label = nil,
  gang_grade_label = nil,
  gang_grade_name = nil,
  money = 0,
  locker = nil,
  garage = nil
}

local myGangName = ""
local myGangRank = ""

ESX              = nil

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end
	--print ('ESX received from server')
end)

--[[ Blip
local emplacement = {
{name="Gangcreation", id=58, colour=3, x=-949.31, y=334.05, z=4.12},
}--]]

Citizen.CreateThread(function()

    for _, item in pairs(emplacement) do
      item.blip = AddBlipForCoord(item.x, item.y, item.z)
      SetBlipSprite(item.blip, item.id)
      SetBlipColour(item.blip, item.colour)
      SetBlipAsShortRange(item.blip, true)
      BeginTextCommandSetBlipName("STRING")
      AddTextComponentString(item.name)
      EndTextCommandSetBlipName(item.blip)
    
 end
end)

--- Location
Citizen.CreateThread(
	function()
	--X, Y, Z coords 
		local x = -949.31
		local y = 334.05
		local z = 71.12
		while true do
			Citizen.Wait(0)
			local playerPos = GetEntityCoords(GetPlayerPed(-1), true)
			if (Vdist(playerPos.x, playerPos.y, playerPos.z, x, y, z) < 100.0) then
				DrawMarker(0, x, y, z, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 255, 10, 10,165, 0, 0, 0,0)
				if (Vdist(playerPos.x, playerPos.y, playerPos.z, x, y, z) < 4.0) then						
				--	Notify("~r~OBS!!!~s~ Du kan inte ha mellanrum i ditt ~y~Organisations namn!")
					DisplayHelpText('Press ~INPUT_CONTEXT~ to ~g~create~s~ your ~y~gang')
					if (IsControlJustReleased(1, 51)) then 
						TriggerServerEvent('esx_gangs:InBeforeGang', GetPlayerServerId(PlayerId()))
						--Menu.hidden = not Menu.hidden
					end
					--Menu.renderGUI(options) 
				end

			end
		end
end)

--- Menu
function CreateGang(gang)
			DisplayOnscreenKeyboard(1, "FMMC_KEY_TIP8S", "", "", "", "", "", 30)
			while (UpdateOnscreenKeyboard() == 0) do
				DisableAllControlActions(0);
				Wait(0);
			end
			if (GetOnscreenKeyboardResult()) then
				print(GetOnscreenKeyboardResult())
				local res = tonumber(GetOnscreenKeyboardResult())
				if(res ~= nil and res ~= 0 and res <= 20) then 
					amount = res		
                else
               --  Notify("~s~Du har ~r~inte tillräckligt~s~ med pengar för att skapa en ~y~Organisation")				
				end
			end

			TriggerServerEvent("esx_gangs:TrycreateGang", GetPlayerServerId(PlayerId()), GetOnscreenKeyboardResult())
end

function getGangName()
  if myGangName ~= nil then
	return myGangName
  end  
end

function getGangRank()
  if myGangRank ~= nil then
	return myGangRank
  end  
end

RegisterNetEvent('esx_gangs:gangLoaded')
AddEventHandler('esx_gangs:gangLoaded', function(gangData, Gangs)
    PlayerData.gang.name = gangData.gang
    PlayerData.gang.gang_grade = gangData.gang_grade
    ESX.Gangs = Gangs
    print ('GangData recieved')
    print (PlayerData.gang.name)
    print (PlayerData.gang.gang_grade)
	myGangName = PlayerData.gang.name
	myGangRank = PlayerData.gang.gang_grade
    PlayerData.gang.gang_label = ESX.Gangs[gangData.gang].label
    PlayerData.gang.gang_grade_label = ESX.Gangs[gangData.gang].ranks[gangData.gang_grade].label
    PlayerData.gang.gang_grade_name = ESX.Gangs[gangData.gang].ranks[gangData.gang_grade].name

    local gangTpl = ''

    
    --HUD Gang Display         (name, index, priority, html, data)
    ESX.UI.HUD.RegisterElement('gang', 3, 0, gangTpl, {
        gang_label       = '',
        gang_grade_label = ''
    })

    ESX.UI.HUD.UpdateElement('gang', {
        gang_label          = PlayerData.gang.gang_label, --these are the labels for the gang data appropriate to rank/gang
        gang_grade_label    = PlayerData.gang.gang_grade_label
    })
    
end)




RegisterNetEvent('esx_gangs:UpdateGang')
AddEventHandler('esx_gangs:UpdateGang', function(gang, grade)
print("oke")
local grade_label = ""
 -- PlayerData.gang = gangData
  print ('Gang Updated')
 -- print (PlayerData.gang.gang_label)
 -- print (PlayerData.gang.gang_grade_label)
  print(gang)
  print(grade)
  
  if grade == 0 then
  grade_label = "สมาชิกแก๊ง"
  end 
  
  if grade == 1 then
  grade_label = "หัวหน้าแก๊ง"
  end
  
    ESX.UI.HUD.UpdateElement('gang', {
        gang_label          = gang, --these are the labels for the gang data appropriate to rank/gang
        gang_grade_label    = grade_label
    })

end)

RegisterNetEvent('esx_gangs:GangMembers')
AddEventHandler('esx_gangs:GangMembers', function(members)
   PlayerData.gang.members = members
end)

Citizen.CreateThread(function()
					if (IsControlJustReleased(1, 51)) then 
					print("sdsdsdsddsds")
					end

	function UpdateSquadMembers()
		ptable = GetPlayers()
		for id, Player in ipairs(ptable) do
			isTeamMate = false
			for i,theTeammate in ipairs(curSquadMembers) do
				if Player == GetPlayerFromServerId(theTeammate) then
					if playersDB[Player].blip then RemoveBlip(playersDB[Player].blip) end
					isTeamMate = true
					local ped = GetPlayerPed(GetPlayerFromServerId(theTeammate))
					local blip = AddBlipForEntity(ped)
					SetBlipSprite(blip, 1)
					Citizen.InvokeNative(0x5FBCA48327B914DF, blip, true)
					SetBlipNameToPlayerName(blip, Player)
					SetBlipScale(blip, 0.85)
					playersDB[Player].blip = blip
				end
			end
			if isTeamMate == false then
				if playersDB[Player].blip then
					RemoveBlip(playersDB[Player].blip)
				end
			end
		end
	end
end)

function Notify(text)
	SetNotificationTextEntry('STRING')
	AddTextComponentString(text)
	DrawNotification(false, false)
end

function DisplayHelpText(str)
	SetTextComponentFormat("STRING")
	AddTextComponentString(str)
	DisplayHelpTextFromStringLabel(0, 0, 1, -1)
end

local guiEnabled = false
local myIdentity = {}
local myIdentifiers = {}
local hasIdentity = false

--===============================================
--==                 VARIABLES                 ==
--===============================================
function EnableGui(enable)

    SetNuiFocus(enable)
    guiEnabled = enable

    SendNUIMessage({
        type = "enableui",
        enable = enable
    })
	
end


--===============================================
--==           Show Registration               ==
--===============================================
RegisterNetEvent("esx_gangs:showGangRegister")
AddEventHandler("esx_gangs:showGangRegister", function()

  EnableGui(true)
  
end)

--===============================================
--==              Close GUI                    ==
--===============================================
RegisterNUICallback('escape', function(data, cb)

  if hasIdentity == true then
  
    EnableGui(false)
	
  else
  
     TriggerEvent("chatMessage", "^1[GANG]", {255, 255, 0}, "You have to enter all fields..")
	 
  end
  
end)

--===============================================
--==           Register Callback               ==
--===============================================
RegisterNUICallback('register', function(data, cb)

  myGang = data
  
  if myGang.gang ~= '' and myGang.gangid ~= '' then
  
	print(myGang.gang)
	print(myGang.gangid)
	
	local gang = myGang.gang
	local gangid = myGang.gangid
	
    EnableGui(false)
	TriggerServerEvent('esx_gangs:TrycreateGang', GetPlayerServerId(PlayerId()), gang, gangid)
    Wait (500)
	
  else
  
    TriggerEvent("chatMessage", "^1[GANG]", {255, 255, 0}, "Please fill in all of the fields.")
	
  end
  
end)

--===============================================
--==                 THREADING                 ==
--===============================================
Citizen.CreateThread(function()

    while true do
	
        if guiEnabled then

          DisableControlAction(0, 1, guiEnabled) -- LookLeftRight
          DisableControlAction(0, 2, guiEnabled) -- LookUpDown
          DisableControlAction(0, 106, guiEnabled) -- VehicleMouseControlOverride			
          DisableControlAction(0, 142, true) -- MeleeAttackAlternate
          DisableControlAction(0, 30,  true) -- MoveLeftRight
          DisableControlAction(0, 31,  true) -- MoveUpDown
          DisableControlAction(0, 21,  true) -- disable sprint
          DisableControlAction(0, 24,  true) -- disable attack
          DisableControlAction(0, 25,  true) -- disable aim
          DisableControlAction(0, 47,  true) -- disable weapon
          DisableControlAction(0, 58,  true) -- disable weapon
          DisableControlAction(0, 263, true) -- disable melee
          DisableControlAction(0, 264, true) -- disable melee
          DisableControlAction(0, 257, true) -- disable melee
          DisableControlAction(0, 140, true) -- disable melee
          DisableControlAction(0, 141, true) -- disable melee
          DisableControlAction(0, 143, true) -- disable melee
          DisableControlAction(0, 75,  true) -- disable exit vehicle
          DisableControlAction(27, 75, true) -- disable exit vehicle

          if IsDisabledControlJustReleased(0, 142) then -- MeleeAttackAlternate
		  
            SendNUIMessage({
              type = "click"
            })
			
            end
			
        end
		
        Citizen.Wait(0)
		
    end
	
end)
