ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

ESX.Gangs = {}

AddEventHandler('onMySQLReady', function ()

  MySQL.Async.fetchAll('SELECT * FROM gangs', {}, function(gangs)
    MySQL.Async.fetchAll('SELECT * FROM gang_grades', {}, function(grades)

      for i=1, #gangs, 1 do
        ESX.Gangs[gangs[i].name] = {
          label = gangs[i].label,
		  color = json.decode(gangs[i].color),
          ranks = {}
        }
      end

      for i=1, #grades, 1 do
        ESX.Gangs[grades[i].gang_name].ranks[grades[i].grade] = {
            name = grades[i].name,
            label = grades[i].label
        }
		--print (grades[i].gang_name)
	--	print (grades[i].label)
      end    
    end)
  end)  
end)

function ReloadGangs()

MySQL.Async.fetchAll('SELECT * FROM gangs', {}, function(gangs)
    MySQL.Async.fetchAll('SELECT * FROM gang_grades', {}, function(grades)

      for i=1, #gangs, 1 do
        ESX.Gangs[gangs[i].name] = {
          label = gangs[i].label,
		  color = json.decode(gangs[i].color),
          ranks = {}
        }
	--	print(gangs[i].name)
      end

      for i=1, #grades, 1 do
        ESX.Gangs[grades[i].gang_name].ranks[grades[i].grade] = {
            name = grades[i].name,
            label = grades[i].label
        }
		--print (grades[i].gang_name)
	--	print (grades[i].label)
      end    
    end)
  end)
  print("Server reloaded gang")
end

function ReloadClientGang(source, label)
  local gangData = {}  
	local xPlayers = ESX.GetPlayers()
Citizen.Wait(3000)
	--for i=1, #xPlayers, 1 do
		local xPlayer = ESX.GetPlayerFromId(source)
  MySQL.Async.fetchAll('SELECT * FROM `users` WHERE `identifier` = @identifier', {['@identifier'] = xPlayer.identifier}, function(gangInfo)
  
    gangData.gang = gangInfo[1].gang
--	print(gangData.gang)
	gangData.gang_grade = gangInfo[1].gang_grade
	--print(gangData.gang_grade)
  TriggerClientEvent('esx_gangs:UpdateGang', source, label, gangData.gang_grade)
 -- print(xPlayer.getName())
		end)
	--end
end

RegisterServerEvent('esx_gangs:getGangMembers')--Fetches all players in one gang
RegisterServerEvent('esx_gangs:getGangbyIdentifier')--Fetches all players in one gang
RegisterServerEvent('esx_gangs:getPlayerGang')--Fetches player current gang and rank
RegisterServerEvent('esx_gangs:setPlayerGang')--Sets player gang and rank
RegisterServerEvent('esx_gangs:getGangLocker')--Gets weapons in gang locker
RegisterServerEvent('esx_gangs:modifyGangLocker')--Modifies gang weapon locker

AddEventHandler('esx:playerLoaded', function(source) 
  local _source = source
  local xPlayer  = ESX.GetPlayerFromId(_source)
  local gangData = {}
  MySQL.Async.fetchAll('SELECT * FROM `users` WHERE `identifier` = @identifier', {['@identifier'] = xPlayer.identifier}, function(gangInfo)
    gangData.gang = gangInfo[1].gang
	gangData.gang_grade = gangInfo[1].gang_grade
  TriggerClientEvent('esx_gangs:gangLoaded', _source, gangData, ESX.Gangs)
end)
end)


ESX.RegisterServerCallback('esx_gangs:getGangName', function(source, cb)
  local xPlayer = ESX.GetPlayerFromId(source)
  local _source = source
  local gangData = {}
  MySQL.Async.fetchAll('SELECT * FROM `users` WHERE `identifier` = @identifier', {['@identifier'] = xPlayer.identifier}, function(gangInfo)
    gangData.gang = gangInfo[1].gang
--	print(gangData.gang)
	gangData.gang_grade = gangInfo[1].gang_grade
		cb(gangData.gang)
	--print("work")
	end)
end)


RegisterServerEvent('esx_gangs:InBeforeGang')--Modifies gang weapon locker
AddEventHandler('esx_gangs:InBeforeGang', function(source)
  local xPlayer = ESX.GetPlayerFromId(source)

  local createFee = 999999999
  if xPlayer.get('money') >= createFee then
  	xPlayer.removeMoney(createFee)
TriggerClientEvent('esx_gangs:showGangRegister', source)
else
	TriggerClientEvent('esx:showNotification', source, "~r~You don't have enough money!~s~ Come back when you have: ~g~$"..createFee.."")
end
end)

RegisterServerEvent('esx_gangs:TrycreateGang')--Modifies gang weapon locker
AddEventHandler('esx_gangs:TrycreateGang', function(source, label, gang)
  local _source = source
  local xPlayer = ESX.GetPlayerFromId(_source)
--print(label)
--print(gang)

	TriggerEvent('esx_gangs:createGang', source, label, gang)
	TriggerEvent('esx_gangs:createGangGrade', source, gang, gang)
	TriggerEvent('esx_gangs:addPlayerGang', source, gang, grade)
	--TriggerClientEvent('esx_gangs:UpdateGang', source, gangData)
	ReloadGangs()
	TriggerClientEvent('esx:showNotification', source, "Your gang ~y~"..label.."~s~ is now activated!")
--	ReloadClientGang()
	ReloadClientGang(source, label)
end)

RegisterServerEvent('esx_gangs:createGang')--Modifies gang weapon locker
AddEventHandler('esx_gangs:createGang', function(source, gLabel, gGang)
  local _source = source
  local xPlayer = ESX.GetPlayerFromId(source)
  local _gang = gGang
  local _label = gLabel
  MySQL.Async.execute('INSERT INTO `essentialmode`.`gangs` (`name`, `label`) VALUES (@name, @label);', 
  {
  ['@name'] = _gang,
  ['@label'] = _label
  }, function ()
  end)
  
  --print("worked")
end)

RegisterServerEvent('esx_gangs:createGangGrade')
AddEventHandler('esx_gangs:createGangGrade', function(source, gang, label)
  local _source = source
  local xPlayer = ESX.GetPlayerFromId(source)
  local _gang = gang
  local _label = "สมาชิกแก๊ง"
  local _labelb = "หัวหน้าแก๊ง"
  local _gradeb = 1
  local _grade = 0
  
  MySQL.Async.execute('INSERT INTO `essentialmode`.`gang_grades` (`gang_name`, `grade`, `name`, `label`) VALUES (@gang_name, @grade, @name, @label);', 
  {
  ['@gang_name'] = _gang,
  ['@grade'] = _grade,
  ['@name'] = _label,
  ['@label'] = _label
  }, function ()
  end)
  
  MySQL.Async.execute('INSERT INTO `essentialmode`.`gang_grades` (`gang_name`, `grade`, `name`, `label`) VALUES (@gang_name, @grade, @name, @label);', 
  {
  ['@gang_name'] = _gang,
  ['@grade'] = _gradeb,
  ['@name'] = _labelb,
  ['@label'] = _labelb
  }, function ()
  end)

  --print("grades worked")
end)

AddEventHandler('esx_gangs:getGangMembers', function(source, gang)

  local _source = source
  local cbmembers = {}
  MySQL.Async.fetchAll('SELECT `identifier`,`gang_grade`,`firstname`,`lastname` FROM `users` WHERE `gang` = @gang',{['@gang'] = gang}, function(members)
    for i=1, #members, 1 do
	  cbmembers[i] = {
	    identifier = members[i].identifier,
		grade = members[i].gang_grade,
		fname = members[i].firstname,
		lname = members[i].lastname
	  }
	end
  TriggerClientEvent('esx_gangs:GangMembers', _source, cbmembers)
  end)
end)


AddEventHandler('esx_gangs:addPlayerGang', function(source, gang, grade)
  local _source = source
  local xPlayer = ESX.GetPlayerFromId(_source)
  local _gang = gang
  local _grade = grade
  MySQL.Async.execute('UPDATE `users` SET `gang` = @gang, `gang_grade` = @grade WHERE `identifier` = @identifier', 
  {
  ['@gang'] = _gang,
  ['@grade'] = 1,
  ['@identifier'] = xPlayer.identifier
  }, function ()
  end)
  --[[
  local gangData = {
    name = _gang,
	gang_grade = tonumber(_grade),
	name = ESX.Gangs[gang].label,
	gang_grade_label = ESX.Gangs[gang].ranks[tonumber(grade)].label,
	gang_grade_name = ESX.Gangs[gang].ranks[tonumber(grade)].name
  } ]]--
  TriggerClientEvent('esx_gangs:UpdateGang', source, gang, 1)
end)

AddEventHandler('esx_gangs:setPlayerGang', function(source, player, gang, grade)
  local _source = source
  local xPlayer = ESX.GetPlayerFromId(player)
  local _gang = gang
  local _grade = grade
  MySQL.Async.execute('UPDATE `users` SET `gang` = @gang, `gang_grade` = @grade WHERE `identifier` = @identifier', 
  {
  ['@gang'] = _gang,
  ['@grade'] = _grade,
  ['@identifier'] = xPlayer.identifier
  }, function ()
  end)
  TriggerClientEvent('esx_gangs:UpdateGang', player, gang, grade)
end)

AddEventHandler('esx_gangs:invitePlayerGang', function(source, player, gang, grade)
  local _source = source
  local xPlayer = ESX.GetPlayerFromId(player)
  local _gang = gang
  local _grade = grade
  MySQL.Async.execute('UPDATE `users` SET `gang` = @gang, `gang_grade` = @grade WHERE `identifier` = @identifier', 
  {
  ['@gang'] = _gang,
  ['@grade'] = _grade,
  ['@identifier'] = xPlayer.identifier
  }, function ()
  end)
  
  local gangData = {
    name = _gang,
	gang_grade = tonumber(_grade),
	name = ESX.Gangs[gang].label,
	gang_grade_label = ESX.Gangs[gang].ranks[tonumber(grade)].label,
	gang_grade_name = ESX.Gangs[gang].ranks[tonumber(grade)].name
  }
  TriggerClientEvent ('esx_gangs:UpdateGang', player, gangData)
end)

function testGang(source, player)
  local _source = source
  local xPlayer = ESX.GetPlayerFromId(source)
	--print(xPlayer.identifier)

  local gangData = {}
  MySQL.Async.fetchAll('SELECT * FROM `users` WHERE `identifier` = @identifier', {['@identifier'] = xPlayer.identifier}, function(gangInfo)
    gangData.gang = gangInfo[1].gang
	--print(gangData.gang)
	gangData.gang_grade = gangInfo[1].gang_grade
	--print(gangData.gang_grade)
	
	if gangData.gang_grade == 1 then
	--print("yes")
	TriggerClientEvent('esx:showNotification', player, "You have been invited to the gang: ~y~".. gangData.gang .."")
	inviteGang(source, player, gangData.gang)
	else
	TriggerClientEvent('esx:showNotification', _source, "~r~You're not the owner of the gang!")
	end
	
end)
	

end

function checkPlayerGang(source, player, place)
  local _source = source
  local xPlayer = ESX.GetPlayerFromId(source)
	--print(xPlayer.identifier)
	--print(place)
  local gangData = {}
  local placesData = {}
  MySQL.Async.fetchAll('SELECT * FROM `users` WHERE `identifier` = @identifier', {['@identifier'] = xPlayer.identifier}, function(gangInfo)
    gangData.gang = gangInfo[1].gang
	--print(gangData.gang)
	gangData.gang_grade = gangInfo[1].gang_grade
	--print(gangData.gang_grade)
	MySQL.Async.fetchAll('SELECT * FROM `gang_owned` WHERE `gang` = @gang', {['@gang'] = gangData.gang}, function(gangOwned)
    placesData.gang = gangOwned[1].gang
    placesData.name = gangOwned[1].name
	--print(placesData.gang)
	--gangData.gang_grade = gangInfo[1].gang_grade
--	print(placesData.name)
	
			for _,i in pairs(gangOwned) do
			placesData.name = gangOwned[_].name
		--	print(placesData.name)
			if place == "weed" then
			if placesData.name == "weed" then
			--print("yeeee")
			--cb("weed")
			TriggerClientEvent('esx_gangs:setGPSWeed', source)
			--table.insert(placesData, {vehicle = vehicle, state = v.state})
			end
		end
		if place == "wash" then
			if placesData.name == "wash" then
			TriggerClientEvent('esx_gangs:setGPSWash', source)
			end
		end	
		if place == "vapen" then
			if placesData.name == "vapen" then
			TriggerClientEvent('esx_gangs:setGPSVapen', source)
			end
		end	
		if place == "kranen" then
			if placesData.name == "kranen" then
				TriggerClientEvent('esx_gangs:setGPSKranen', source)
			end
		end
		if place == "coke" then
			if placesData.name == "coke" then
				TriggerClientEvent('esx_gangs:setGPSCoke', source)
			end
		end
		if place == "meth" then
			if placesData.name == "meth" then
				TriggerClientEvent('esx_gangs:setGPSMeth', source)
			end
		end
		end
	end)
end)

	
end


function inviteGang(source, player, gang)
--print(gang)
local grade = 0
 TriggerEvent('esx_gangs:setPlayerGang', source, player, gang, grade)
end

function gangLeave(source, player, gang)
--print(gang)
local grade = 0
local gang = "none"
 TriggerEvent('esx_gangs:setPlayerGang', source, player, gang, grade)
end

function gangLeaves(source, player, gang)
--print(gang)
local grade = 0
local gang = "none"
 TriggerEvent('esx_gangs:setPlayerGang', source, source, gang, grade)
end
--setgang cmd
TriggerEvent('es:addGroupCommand', 'setgang', 'admin', function(source, args, user)
  local player = args[1]
  local gang = args[2]
  local grade = args[3]
  TriggerEvent('esx_gangs:setPlayerGang', source, player, gang, grade)
  
end, function(source, args, user)
  TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "Insufficient Permissions.")
end, {help = 'setgang', params = {{name = "id", help = 'PlayerID'}, {name = "gang", help = 'Gang'}, {name = "grade_id", help = 'Grade'}}})

TriggerEvent('es:addGroupCommand', 'ganginvite', 'user', function(source, args, user)
  local player = args[1]
  testGang(source, player)
  --TriggerEvent('esx_gangs:esx_gangs:getGangbyIdentifier', source)
  --TriggerEvent('esx_gangs:setPlayerGang', source, player, gang, grade)
  
end, function(source, args, user)
  TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "Insufficient Permissions.")
end, {help = 'ganginvite', params = {{name = "id", help = 'Spelare ID'}}})

TriggerEvent('es:addGroupCommand', 'gangkick', 'user', function(source, args, user)
  local player = args[1]
  gangLeave(source, player)
  --TriggerEvent('esx_gangs:esx_gangs:getGangbyIdentifier', source)
  --TriggerEvent('esx_gangs:setPlayerGang', source, player, gang, grade)
  
end, function(source, args, user)
  TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "Insufficient Permissions.")
end, {help = 'ganginvite', params = {{name = "id", help = 'Spelare ID'}}})

TriggerEvent('es:addGroupCommand', 'gangleave', 'user', function(source, args, user)
  local player = args[1]
  gangLeaves(source, player)
  --TriggerEvent('esx_gangs:esx_gangs:getGangbyIdentifier', source)
  --TriggerEvent('esx_gangs:setPlayerGang', source, player, gang, grade)
  
end, function(source, args, user)
  TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "Insufficient Permissions.")
end, {help = 'ganginvite', params = {{name = " ", help = 'Leave gang'}}})


TriggerEvent('es:addGroupCommand', 'ganggps', 'user', function(source, args, user)
  local player = args[1]
  local place = args[1]
  checkPlayerGang(source, player, place)
  --TriggerEvent('esx_gangs:esx_gangs:getGangbyIdentifier', source)
  --TriggerEvent('esx_gangs:setPlayerGang', source, player, gang, grade)
  
end, function(source, args, user)
  TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "Insufficient Permissions.")
end, {help = 'ganginvite', params = {{name = "id", help = 'Spelare ID'}}})

