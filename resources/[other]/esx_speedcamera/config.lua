Config = {}
Config.Locale = 'en'
-- BELOW IS YOUR SETTINGS, CHANGE THEM TO WHATEVER YOU'D LIKE & MORE SETTINGS WILL COME IN THE FUTURE! --
Config.useBilling = true --OPTIONS: (true/false)
Config.useCameraSound = true -- OPTIONS: (true/false)
Config.useFlashingScreen = true -- OPTIONS: (true/false)
Config.useBlips = false -- OPTIONS: (true/false)
Config.alertPolice = false -- OPTIONS: (true/false)
Config.alertSpeed = 150 -- OPTIONS: (1-5000 KMH)
Config.SixtyFine = 100 -- DESIRED FINE AMOUNT FOR 60 KMH ZONE
Config.EightyFine = 500 -- DESIRED FINE AMOUNT FOR 80 KMH ZONE
Config.OneHundredTwentyFine = 750 -- DESIRED FINE AMOUNT FOR 120 KMH ZONE

-- ABOVE IS YOUR SETTINGS, CHANGE THEM TO WHATEVER YOU'D LIKE & MORE SETTINGS WILL COME IN THE FUTURE!  --



--------------------------------------------------------------------------
-- ADD AUTHORIZED VEHICLES THAT YOU WISH TO NOT GET CAUGHT AND TICKETED --
--------------------------------------------------------------------------

-- AUTHORIZED VEHICLES (START) --
Config.AuthorizedVehicles = {
	{model = 'ambulance'},
	{model = 'police'},
	{model = 'police2'},
	{model = 'police3'},
	{model = 'police4'},
	{model = 'policeb'},
	{model = 'polmav'},
	{model = '2015polstang'},
	{model = 'chgr'},
	{model = 'pdram'},
	{model = 'r1custom'},
	--
	{model = '00excursion'},
	{model = '350zrb'},
	{model = '911rwb'},
	{model = 'acs8'},
	{model = 'zl12017'},
	{model = 'hcbr17'},
	{model = 'dk350z'},
	{model = 'evoltex'},
	{model = 'fq360'},
	{model = 'fqrally'},
	{model = '2f2fgtr34'},
	{model = '2f2fgts'},
	{model = '2f2fmk4'},
	{model = '2f2fmle7'},
	{model = 'ff4wrx'},
	{model = 'fnf4r34'},
	{model = 'fnflan'},
	{model = 'fnfmits'},
	{model = 'fnfmk4'},
	{model = 'fnfrx7'},
	{model = 'fnfcivic'},
	{model = 'gt3rs'},
	{model = 'corvette'},
	{model = 'gtr7a'},
	{model = 'gtr7b'},
	{model = 'gtrlb'},
	{model = 'hondacivictr'},
	{model = '18performante'},
	{model = 'lancerevox'},
	{model = 'lw458s'},
	{model = 'lykan'},
	{model = 'p1lm'},
	{model = 'mst'},
	{model = 'skyline'},
	{model = 'gtx'},
	{model = 'pruf'},
	{model = 'r1'},
	{model = 'r6'},
	{model = 'subwrx'},
	{model = 'z15clean'},
	{model = 'coquette'},
	{model = 'massacro'},
	{model = 'lwgtr'},
	{model = 'f150'}
}
-- AUTHORIZED VEHICLES (END) --


---------------------------------------------
-- ADD BLIP LOCATION FOR YOUR SPEEDCAMERA --
---------------------------------------------

-- BLIP FOR SPEEDCAMERA (START) --
Config.blips = {
	-- 60KM/H ZONES
	
	-- 80KM/H ZONES
	{title="Speedcamera (140KM/H)", colour=1, id=1, x = 2506.0671, y = 4145.2431, z = 38.1054}, -- 80KM/H ZONE
	{title="Speedcamera (140KM/H)", colour=1, id=1, x = -631.5, y = -372.25, z = 34.81}, -- 80KM/H ZONE
	{title="Speedcamera (140KM/H)", colour=1, id=1, x = 471.0, y = -313.5, z = 47.01}, -- 80KM/H ZONE
	{title="Speedcamera (140KM/H)", colour=1, id=1, x = -221.5, y = 261.0, z = 92.08}, -- 80KM/H ZONE
	{title="Speedcamera (140KM/H)", colour=1, id=1, x = -391.04, y = -1832.69, z = 21.42}, -- 80KM/H ZONE
	{title="Speedcamera (140KM/H)", colour=1, id=1, x = 275.64, y = -1865.5, z = 26.86}, -- 80KM/H ZONE
	{title="Speedcamera (140KM/H)", colour=1, id=1, x = -1407.25, y = -69.0, z = 52.77}, -- 80KM/H ZONE
	{title="Speedcamera (140KM/H)", colour=1, id=1, x = -524.2645, y = -1776.3569, z = 21.3384}, -- 60KM/H ZONE
	{title="Speedcamera (140KM/H)", colour=1, id=1, x = 222.25, y = -1040.5, z = 29.35}, -- 60KM/H ZONE
	{title="Speedcamera (140KM/H)", colour=1, id=1, x = 783.1, y = -1005.26, z = 26.14}, -- 60KM/H ZONE
	{title="Speedcamera (140KM/H)", colour=1, id=1, x = -186.75, y = -892.25, z = 29.34}, -- 60KM/H ZONE
	{title="Speedcamera (140KM/H)", colour=1, id=1, x = -1079.75, y = -763.0, z = 19.35}, -- 60KM/H ZONE
	{title="Speedcamera (140KM/H)", colour=1, id=1, x = 108.41, y = -574.74, z = 31.51}, -- 60KM/H ZONE
	{title="Speedcamera (140KM/H)", colour=1, id=1, x = 154.9, y = -1396.57, z = 29.3}, -- 60KM/H ZONE
	
	-- 120KM/H ZONES
	{title="Speedcamera (180KM/H)", colour=1, id=1, x = 1584.9281, y = -993.4557, z = 59.3923}, -- 120KM/H ZONE
	{title="Speedcamera (180KM/H)", colour=1, id=1, x = 2442.2006, y = -134.6004, z = 88.7765}, -- 120KM/H ZONE
	{title="Speedcamera (180KM/H)", colour=1, id=1, x = 1334.53, y = 599.83, z = 80.07}, -- 120KM/H ZONE
	{title="Speedcamera (180KM/H)", colour=1, id=1, x = 1306.75, y = 6487.25, z = 20.09}, -- 120KM/H ZONE
	{title="Speedcamera (180KM/H)", colour=1, id=1, x = -1618.75, y = 4877.25, z = 61.04}, -- 120KM/H ZONE
	{title="Speedcamera (180KM/H)", colour=1, id=1, x = -2684.5, y = 2454.8, z = 16.67}, -- 120KM/H ZONE
	{title="Speedcamera (180KM/H)", colour=1, id=1, x = -2268.58, y = -334.46, z = 13.48}, -- 120KM/H ZONE
	{title="Speedcamera (180KM/H)", colour=1, id=1, x = 2871.7951, y = 3540.5795, z = 53.0930} -- 120KM/H ZONE
}
-- BLIP FOR SPEEDCAMERA (END)


------------------------------------------
-- USE SAME COORDS (X,Y,Z) AS THE BLIPS --
------------------------------------------

-- SPEEDCAMERA LOCATION (X,Y,Z) (START) --
Config.Speedcamera60Zone = {
}

Config.Speedcamera80Zone = {
    {x = 2506.0671,y = 4145.2431,z = 38.1054},
    {x = 471.0, y = -313.5, z = 47.01},
    {x = -1407.25, y = -69.0, z = 52.77},
    {x = -221.5, y = 261.0, z = 92.08},
	{x = -524.2645,y = -1776.3569,z = 21.3384},
    {x = -186.75, y = -892.25, z = 29.34},
    {x = 783.1, y = -1005.26, z = 26.14},
    {x = 108.41, y = -574.74, z = 31.51},
    {x = 154.9, y = -1396.57, z = 29.3},
    {x = -1079.75, y = -763.0, z = 19.35},
    {x = 222.25, y = -1040.5, z = 29.35},
    {x = -391.04, y = -1832.69, z = 21.42},
    {x = 275.64, y = -1865.5, z = 26.86},
    {x = -631.5, y = -372.25, z = 34.81}
}

Config.Speedcamera120Zone = {
    {x = 1584.9281,y = -993.4557,z = 59.3923},
    {x = 2442.2006,y = -134.6004,z = 88.7765},
    {x = 1334.53, y = 599.83, z = 80.07},
    {x = 1306.75, y = 6487.25, z = 20.09},
    {x = -2684.5, y = 2454.8, z = 16.67},
    {x = -2268.58, y = -334.46, z = 13.48},
    {x = 2871.7951,y = 3540.5795,z = 53.0930}
}
-- SPEEDCAMERA LOCATION (X,Y,Z) (END) --
