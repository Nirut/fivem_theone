GTA V - Lamborghini (Zentorno) Police - LSPD V3.0
First Modding Police car BY AitGamers - Mister Brooks

Important :
Any contents of the attached file may not be redistributed in any manner without proper credit given to AitGamers - Mister Brooks

How to install :
1.  Run OpenIV
2.  Navigate to GTAV\mods(if use openiv asi)\update\x64\dlcpacks\patchday3ng\dlc.rpf\x64\levels\gta5\vehicles.rpf
3.  First, Backup Police2.yft Police2.ytd Police2_hi.yft Police2_hi.ytd
4.  Now, Extract and replace all files from (Models folder) to this location in step2
5.  Again, Navigate to GTAV\mods(if use openiv asi)\x64e.rpf\levels\gta5\vehicles.rpf
6.  Backup again and Replace same files like step3 and step4, but now to location in step5
7.  Now, Navigate to GTAV\mods(if use openiv asi)\update\update.rpf\common\data 
8.  Backup handling.meta
9.  Extract and Replace handling.meta from (Handling folder) to this location in step7
10. Important. When join GTAV, first spawn car has spawned without siren, spawn the car again to spawned with siren (use any trainer to spawn car, like Simple Trainer) (i'll fix it later)
11. Recommended. You can change second color with Trainer
Enjoy ;)