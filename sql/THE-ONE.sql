-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.8-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for essentialmode
CREATE DATABASE IF NOT EXISTS `essentialmode` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `essentialmode`;

-- Dumping structure for table essentialmode.addon_account
CREATE TABLE IF NOT EXISTS `addon_account` (
  `name` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `shared` int(11) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.addon_account: ~8 rows (approximately)
/*!40000 ALTER TABLE `addon_account` DISABLE KEYS */;
INSERT INTO `addon_account` (`name`, `label`, `shared`) VALUES
	('caution', 'caution', 0),
	('property_black_money', 'Money Sale Property', 0),
	('society_ambulance', 'หน่วยแพทย์', 1),
	('society_cardealer', 'Concessionnaire', 1),
	('society_foodtruck', 'Foodtruck', 1),
	('society_mechanic', 'Mechanic', 1),
	('society_police', 'เจ้าหน้าที่ตำรวจ', 1),
	('society_taxi', 'Taxi', 1);
/*!40000 ALTER TABLE `addon_account` ENABLE KEYS */;

-- Dumping structure for table essentialmode.addon_account_data
CREATE TABLE IF NOT EXISTS `addon_account_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `money` double NOT NULL,
  `owner` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5879 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.addon_account_data: ~8 rows (approximately)
/*!40000 ALTER TABLE `addon_account_data` DISABLE KEYS */;
INSERT INTO `addon_account_data` (`id`, `account_name`, `money`, `owner`) VALUES
	(5869, 'society_ambulance', 0, NULL),
	(5870, 'society_cardealer', 0, NULL),
	(5871, 'society_foodtruck', 0, NULL),
	(5872, 'society_mechanic', 0, NULL),
	(5873, 'society_police', 0, NULL),
	(5874, 'society_taxi', 0, NULL),
	(5875, 'caution', 0, 'steam:11000013ac515f5'),
	(5876, 'property_black_money', 0, 'steam:11000013ac515f5'),
	(5877, 'property_black_money', 0, 'steam:110000104a1abc7'),
	(5878, 'caution', 0, 'steam:110000104a1abc7');
/*!40000 ALTER TABLE `addon_account_data` ENABLE KEYS */;

-- Dumping structure for table essentialmode.addon_inventory
CREATE TABLE IF NOT EXISTS `addon_inventory` (
  `name` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `shared` int(11) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.addon_inventory: ~7 rows (approximately)
/*!40000 ALTER TABLE `addon_inventory` DISABLE KEYS */;
INSERT INTO `addon_inventory` (`name`, `label`, `shared`) VALUES
	('property', 'Property', 0),
	('society_ambulance', 'หน่วยแพทย์', 1),
	('society_cardealer', 'Concesionnaire', 1),
	('society_mafia', 'Mafia', 1),
	('society_mechanic', 'Mechanic', 1),
	('society_police', 'เจ้าหน้าที่ตำรวจ', 1),
	('society_taxi', 'Taxi', 1);
/*!40000 ALTER TABLE `addon_inventory` ENABLE KEYS */;

-- Dumping structure for table essentialmode.addon_inventory_items
CREATE TABLE IF NOT EXISTS `addon_inventory_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inventory_name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `count` int(11) NOT NULL,
  `owner` varchar(60) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=301 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.addon_inventory_items: ~117 rows (approximately)
/*!40000 ALTER TABLE `addon_inventory_items` DISABLE KEYS */;
INSERT INTO `addon_inventory_items` (`id`, `inventory_name`, `name`, `count`, `owner`) VALUES
	(57, 'society_police', 'shovel', 0, NULL),
	(58, 'society_police', 'weed_pooch', 389, NULL),
	(59, 'society_police', 'repairkit', 0, NULL),
	(60, 'society_police', 'opium', 409, NULL),
	(61, 'society_police', 'bread', 1, NULL),
	(62, 'society_police', 'clip', 443, NULL),
	(63, 'society_police', 'cannabis', 2492, NULL),
	(64, 'society_police', 'opium_pooch', 24, NULL),
	(65, 'society_police', 'weed', 7, NULL),
	(66, 'society_police', 'medikit', 0, NULL),
	(67, 'society_police', 'bulletproof', 189, NULL),
	(68, 'society_police', 'bottle', 0, NULL),
	(69, 'society_police', 'lrod', 1, NULL),
	(70, 'society_police', 'lbait', 79, NULL),
	(72, 'society_police', 'leather', 0, NULL),
	(73, 'society_taxi', 'cannabis', 0, NULL),
	(74, 'society_police', 'laranja', 0, NULL),
	(75, 'society_taxi', 'weed_pooch', 0, NULL),
	(83, 'society_mechanic', 'gazbottle', 0, NULL),
	(84, 'society_mechanic', 'carotool', 0, NULL),
	(85, 'society_mechanic', 'fixtool', 0, NULL),
	(86, 'society_police', 'acabbage', 0, NULL),
	(100, 'society_police', 'meth_pooch', 74, NULL),
	(101, 'society_police', 'meth', 611, NULL),
	(104, 'society_police', 'scissor', 1, NULL),
	(105, 'society_police', 'oil_a', 0, NULL),
	(106, 'society_police', 'sand', 0, NULL),
	(107, 'society_police', 'turtlebait', 0, NULL),
	(108, 'society_police', 'hatchet_lj', 1, NULL),
	(112, 'society_police', 'wood', 0, NULL),
	(153, 'society_police', 'lfish', 14, NULL),
	(154, 'society_police', 'chicken', 28, NULL),
	(155, 'society_police', 'knife_chicken', 0, NULL),
	(156, 'society_mecano', 'shovel', 0, NULL),
	(157, 'society_mecano', 'drill', 0, NULL),
	(160, 'society_mecano', 'knife_chicken', 1, NULL),
	(161, 'society_mecano', 'lrod', 1, NULL),
	(210, 'society_mechanic', 'mushroom', 0, NULL),
	(211, 'society_mechanic', 'milk_engine', 1, NULL),
	(212, 'society_mechanic', 'drill', 0, NULL),
	(213, 'society_mechanic', 'sickle', 1, NULL),
	(214, 'society_mechanic', 'hatchet_lj', 1, NULL),
	(226, 'property', 'stone', 1, 'steam:11000010bcd9aa5'),
	(227, 'property', 'marijuana_cigarette', 1, 'steam:11000010bcd9aa5'),
	(228, 'property', 'clip', 1, 'steam:11000010bcd9aa5'),
	(229, 'property', 'turtlebait', 1, 'steam:11000010bcd9aa5'),
	(230, 'property', 'oxygen_mask', 1, 'steam:11000010bcd9aa5'),
	(231, 'property', 'weed_pooch', 0, 'steam:11000010bcd9aa5'),
	(232, 'property', 'weed_pooch', 165, 'steam:110000115151cb4'),
	(233, 'property', 'oxygen_mask', 10, 'steam:110000115151cb4'),
	(234, 'property', 'cupcake', 0, 'steam:110000114b5f1de'),
	(235, 'property', 'fixkit', 37, 'steam:110000136506bc2'),
	(236, 'property', 'wrench_blueprint', 1, 'steam:110000136506bc2'),
	(237, 'property', 'snspistol_blueprint', 1, 'steam:110000136506bc2'),
	(238, 'property', 'SteelScrap', 60, 'steam:110000136506bc2'),
	(239, 'property', 'shovel', 1, 'steam:110000136506bc2'),
	(240, 'property', 'knife_chicken', 1, 'steam:110000136506bc2'),
	(241, 'property', 'cocacola', 0, 'steam:110000114b5f1de'),
	(242, 'property', 'iron', 60, 'steam:110000114b5f1de'),
	(243, 'property', 'chest_a', 10, 'steam:110000114b5f1de'),
	(244, 'property', 'diamond', 6, 'steam:110000114b5f1de'),
	(245, 'property', 'lighter', 0, 'steam:1100001328007ca'),
	(246, 'property', 'SteelScrap', 10, 'steam:1100001328007ca'),
	(247, 'property', 'SteelScrap', 18, 'steam:11000013ba816d4'),
	(248, 'property', 'SteelScrap', 5, 'steam:11000013b9dd243'),
	(249, 'property', 'stone', 5, 'steam:11000013b9dd243'),
	(250, 'property', 'gold', 79, 'steam:11000010bcd9aa5'),
	(251, 'property', 'SteelScrap', 2264, 'steam:11000010bcd9aa5'),
	(252, 'property', 'diamond', 30, 'steam:11000010bcd9aa5'),
	(253, 'property', 'chest_a', 304, 'steam:11000010bcd9aa5'),
	(254, 'society_police', 'coffe', 3, NULL),
	(255, 'property', 'cannabis', 1000, 'steam:11000011223dd27'),
	(256, 'property', 'weed_pooch', 92, 'steam:11000011223dd27'),
	(257, 'property', 'fixkit', 10, 'steam:11000011223dd27'),
	(258, 'property', 'rice_pro', 200, 'steam:11000011223dd27'),
	(259, 'property', 'clip', 5, 'steam:11000011223dd27'),
	(260, 'property', 'petrol', 1, 'steam:11000011b14adbc'),
	(261, 'property', 'copper', 2, 'steam:11000011b14adbc'),
	(262, 'property', 'gold', 1, 'steam:11000011b14adbc'),
	(263, 'property', 'fish', 19, 'steam:11000011b14adbc'),
	(264, 'property', 'gold', 0, 'steam:11000013cf1acff'),
	(265, 'property', 'stone', 1, 'steam:11000013cf1acff'),
	(266, 'society_mechanic', 'sandwich', 0, NULL),
	(267, 'property', 'jumelles', 1, 'steam:11000010bc9b236'),
	(268, 'property', 'sportlunch', 13, 'steam:11000010bc9b236'),
	(269, 'property', 'protein_shake', 20, 'steam:11000010bc9b236'),
	(270, 'property', 'powerade', 22, 'steam:11000010bc9b236'),
	(271, 'property', 'copper', 2, 'steam:1100001321288c8'),
	(272, 'property', 'sickle', 1, 'steam:110000136506bc2'),
	(273, 'property', 'drill', 1, 'steam:110000136506bc2'),
	(274, 'property', 'bong', 1, 'steam:110000136506bc2'),
	(275, 'property', 'diamond', 18, 'steam:11000011223dd27'),
	(276, 'property', 'iron', 249, 'steam:11000011223dd27'),
	(277, 'property', 'gold', 73, 'steam:11000011223dd27'),
	(278, 'society_mechanic', 'cannabis', 0, NULL),
	(279, 'society_mechanic', 'weed_pooch', 0, NULL),
	(280, 'property', 'copper', 100, 'steam:11000011223dd27'),
	(281, 'property', 'SteelScrap', 250, 'steam:11000011223dd27'),
	(282, 'property', 'phone', 0, 'steam:1100001335deef6'),
	(283, 'society_mechanic', 'pork', 9, NULL),
	(284, 'society_mechanic', 'drugs', 0, NULL),
	(285, 'property', 'anti', 30, 'steam:1100001139ef872'),
	(286, 'property', 'iron', 1000, 'steam:1100001139ef872'),
	(287, 'property', 'diamond', 500, 'steam:1100001139ef872'),
	(288, 'property', 'copper', 1000, 'steam:1100001139ef872'),
	(289, 'property', 'marijuana', 500, 'steam:1100001139ef872'),
	(290, 'property', 'SteelScrap', 607, 'steam:1100001139ef872'),
	(291, 'property', 'doubleaction_blueprint', 1, 'steam:1100001139ef872'),
	(292, 'property', 'snspistol_blueprint', 1, 'steam:1100001139ef872'),
	(293, 'property', 'switchblade_blueprint', 1, 'steam:1100001139ef872'),
	(294, 'property', 'wrench_blueprint', 1, 'steam:1100001139ef872'),
	(295, 'property', 'snspistol_blueprint', 1, 'steam:110000115dde3a7'),
	(296, 'property', 'cannabis', 2, 'steam:110000115c53a68'),
	(297, 'property', 'marijuana', 47, 'steam:110000115c53a68'),
	(298, 'property', 'weed_pooch', 13, 'steam:110000115c53a68'),
	(299, 'property', 'cannabis', 100, 'steam:11000010e09d44f'),
	(300, 'property', 'bcabbage', 0, 'steam:110000104a1abc7');
/*!40000 ALTER TABLE `addon_inventory_items` ENABLE KEYS */;

-- Dumping structure for table essentialmode.baninfo
CREATE TABLE IF NOT EXISTS `baninfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(25) COLLATE utf8mb4_bin DEFAULT 'no info',
  `license` varchar(50) COLLATE utf8mb4_bin DEFAULT 'no info',
  `liveid` varchar(21) COLLATE utf8mb4_bin DEFAULT 'no info',
  `xblid` varchar(21) COLLATE utf8mb4_bin DEFAULT 'no info',
  `discord` varchar(30) COLLATE utf8mb4_bin DEFAULT 'no info',
  `playerip` varchar(25) COLLATE utf8mb4_bin DEFAULT '0.0.0.0',
  `playername` varchar(32) COLLATE utf8mb4_bin DEFAULT 'no info',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.baninfo: ~0 rows (approximately)
/*!40000 ALTER TABLE `baninfo` DISABLE KEYS */;
/*!40000 ALTER TABLE `baninfo` ENABLE KEYS */;

-- Dumping structure for table essentialmode.banlist
CREATE TABLE IF NOT EXISTS `banlist` (
  `identifier` varchar(25) COLLATE utf8mb4_bin NOT NULL,
  `license` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `liveid` varchar(21) COLLATE utf8mb4_bin DEFAULT NULL,
  `xblid` varchar(21) COLLATE utf8mb4_bin DEFAULT NULL,
  `discord` varchar(30) COLLATE utf8mb4_bin DEFAULT NULL,
  `playerip` varchar(25) COLLATE utf8mb4_bin DEFAULT NULL,
  `targetplayername` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `sourceplayername` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `reason` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `timeat` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `expiration` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `permanent` int(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.banlist: ~0 rows (approximately)
/*!40000 ALTER TABLE `banlist` DISABLE KEYS */;
/*!40000 ALTER TABLE `banlist` ENABLE KEYS */;

-- Dumping structure for table essentialmode.banlisthistory
CREATE TABLE IF NOT EXISTS `banlisthistory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(25) COLLATE utf8mb4_bin NOT NULL,
  `license` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `liveid` varchar(21) COLLATE utf8mb4_bin DEFAULT NULL,
  `xblid` varchar(21) COLLATE utf8mb4_bin DEFAULT NULL,
  `discord` varchar(30) COLLATE utf8mb4_bin DEFAULT NULL,
  `playerip` varchar(25) COLLATE utf8mb4_bin DEFAULT NULL,
  `targetplayername` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `sourceplayername` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `reason` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `timeat` int(11) NOT NULL,
  `added` varchar(40) COLLATE utf8mb4_bin NOT NULL,
  `expiration` int(11) NOT NULL,
  `permanent` int(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.banlisthistory: ~0 rows (approximately)
/*!40000 ALTER TABLE `banlisthistory` DISABLE KEYS */;
/*!40000 ALTER TABLE `banlisthistory` ENABLE KEYS */;

-- Dumping structure for table essentialmode.bans
CREATE TABLE IF NOT EXISTS `bans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.bans: ~0 rows (approximately)
/*!40000 ALTER TABLE `bans` DISABLE KEYS */;
/*!40000 ALTER TABLE `bans` ENABLE KEYS */;

-- Dumping structure for table essentialmode.bansip
CREATE TABLE IF NOT EXISTS `bansip` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.bansip: ~0 rows (approximately)
/*!40000 ALTER TABLE `bansip` DISABLE KEYS */;
/*!40000 ALTER TABLE `bansip` ENABLE KEYS */;

-- Dumping structure for table essentialmode.banslicense
CREATE TABLE IF NOT EXISTS `banslicense` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `license` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.banslicense: ~0 rows (approximately)
/*!40000 ALTER TABLE `banslicense` DISABLE KEYS */;
/*!40000 ALTER TABLE `banslicense` ENABLE KEYS */;

-- Dumping structure for table essentialmode.billing
CREATE TABLE IF NOT EXISTS `billing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `sender` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `target_type` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `amount` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=457 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.billing: ~0 rows (approximately)
/*!40000 ALTER TABLE `billing` DISABLE KEYS */;
/*!40000 ALTER TABLE `billing` ENABLE KEYS */;

-- Dumping structure for table essentialmode.boats
CREATE TABLE IF NOT EXISTS `boats` (
  `name` varchar(60) NOT NULL,
  `model` varchar(60) NOT NULL,
  `price` int(11) NOT NULL,
  `category` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`model`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.boats: ~16 rows (approximately)
/*!40000 ALTER TABLE `boats` DISABLE KEYS */;
INSERT INTO `boats` (`name`, `model`, `price`, `category`) VALUES
	('Dinghy 4Seat', 'dinghy', 25000, 'boat'),
	('Dinghy 2Seat', 'dinghy2', 20000, 'boat'),
	('Dinghy Yacht', 'dinghy4', 25000, 'boat'),
	('Jetmax', 'jetmax', 30000, 'boat'),
	('Marquis', 'marquis', 45000, 'boat'),
	('Seashark', 'seashark', 10000, 'boat'),
	('Seashark Yacht', 'seashark3', 10000, 'boat'),
	('Speeder', 'speeder', 40000, 'boat'),
	('Squalo', 'squalo', 32000, 'boat'),
	('Submarine', 'submersible', 29000, 'subs'),
	('Kraken', 'submersible2', 31000, 'subs'),
	('Suntrap', 'suntrap', 34000, 'boat'),
	('Toro', 'toro', 38000, 'boat'),
	('Toro Yacht', 'toro2', 38000, 'boat'),
	('Tropic', 'tropic', 27000, 'boat'),
	('Tropic Yacht', 'tropic2', 27000, 'boat');
/*!40000 ALTER TABLE `boats` ENABLE KEYS */;

-- Dumping structure for table essentialmode.boat_categories
CREATE TABLE IF NOT EXISTS `boat_categories` (
  `name` varchar(60) NOT NULL,
  `label` varchar(60) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.boat_categories: ~2 rows (approximately)
/*!40000 ALTER TABLE `boat_categories` DISABLE KEYS */;
INSERT INTO `boat_categories` (`name`, `label`) VALUES
	('boat', 'Boats'),
	('subs', 'Submersibles');
/*!40000 ALTER TABLE `boat_categories` ENABLE KEYS */;

-- Dumping structure for table essentialmode.cardealer_vehicles
CREATE TABLE IF NOT EXISTS `cardealer_vehicles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vehicle` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `price` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.cardealer_vehicles: ~7 rows (approximately)
/*!40000 ALTER TABLE `cardealer_vehicles` DISABLE KEYS */;
INSERT INTO `cardealer_vehicles` (`id`, `vehicle`, `price`) VALUES
	(1, 'bison', 80000),
	(2, 'ek9', 800000),
	(3, 'asea', 60000),
	(4, 'AKUMA', 20000),
	(5, 'blista', 50000),
	(6, 'Tractor', 1500),
	(7, 'fnflan', 500000);
/*!40000 ALTER TABLE `cardealer_vehicles` ENABLE KEYS */;

-- Dumping structure for table essentialmode.characters
CREATE TABLE IF NOT EXISTS `characters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `dateofbirth` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `sex` varchar(1) COLLATE utf8mb4_bin NOT NULL DEFAULT 'M',
  `height` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  `lastdigits` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=196 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.characters: ~0 rows (approximately)
/*!40000 ALTER TABLE `characters` DISABLE KEYS */;
INSERT INTO `characters` (`id`, `identifier`, `firstname`, `lastname`, `dateofbirth`, `sex`, `height`, `lastdigits`) VALUES
	(195, 'steam:110000104a1abc7', 'Abx', 'Abx', '1999-09-09', 'm', '200', NULL);
/*!40000 ALTER TABLE `characters` ENABLE KEYS */;

-- Dumping structure for table essentialmode.communityservice
CREATE TABLE IF NOT EXISTS `communityservice` (
  `identifier` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `actions_remaining` int(10) NOT NULL,
  PRIMARY KEY (`identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.communityservice: ~0 rows (approximately)
/*!40000 ALTER TABLE `communityservice` DISABLE KEYS */;
/*!40000 ALTER TABLE `communityservice` ENABLE KEYS */;

-- Dumping structure for table essentialmode.datastore
CREATE TABLE IF NOT EXISTS `datastore` (
  `name` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `shared` int(11) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.datastore: ~9 rows (approximately)
/*!40000 ALTER TABLE `datastore` DISABLE KEYS */;
INSERT INTO `datastore` (`name`, `label`, `shared`) VALUES
	('property', 'Property', 0),
	('society_ambulance', 'หน่วยแพทย์', 1),
	('society_fbi', 'FBI', 1),
	('society_mafia', 'มาเฟีย', 1),
	('society_police', 'เจ้าหน้าที่ตำรวจ', 1),
	('user_ears', 'Ears', 0),
	('user_glasses', 'Glasses', 0),
	('user_helmet', 'Helmet', 0),
	('user_mask', 'Mask', 0);
/*!40000 ALTER TABLE `datastore` ENABLE KEYS */;

-- Dumping structure for table essentialmode.datastore_data
CREATE TABLE IF NOT EXISTS `datastore_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `owner` varchar(60) COLLATE utf8mb4_bin DEFAULT NULL,
  `data` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_datastore_owner_name` (`owner`,`name`),
  KEY `index_datastore_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=10465 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.datastore_data: ~14 rows (approximately)
/*!40000 ALTER TABLE `datastore_data` DISABLE KEYS */;
INSERT INTO `datastore_data` (`id`, `name`, `owner`, `data`) VALUES
	(10451, 'society_ambulance', NULL, '{}'),
	(10452, 'society_fbi', NULL, '{}'),
	(10453, 'society_mafia', NULL, '{}'),
	(10454, 'society_police', NULL, '{}'),
	(10455, 'user_glasses', 'steam:11000013ac515f5', '{}'),
	(10456, 'user_mask', 'steam:11000013ac515f5', '{}'),
	(10457, 'property', 'steam:11000013ac515f5', '{}'),
	(10458, 'user_helmet', 'steam:11000013ac515f5', '{}'),
	(10459, 'user_ears', 'steam:11000013ac515f5', '{}'),
	(10460, 'property', 'steam:110000104a1abc7', '{}'),
	(10461, 'user_glasses', 'steam:110000104a1abc7', '{}'),
	(10462, 'user_helmet', 'steam:110000104a1abc7', '{}'),
	(10463, 'user_ears', 'steam:110000104a1abc7', '{}'),
	(10464, 'user_mask', 'steam:110000104a1abc7', '{}');
/*!40000 ALTER TABLE `datastore_data` ENABLE KEYS */;

-- Dumping structure for table essentialmode.disc_inventory
CREATE TABLE IF NOT EXISTS `disc_inventory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner` text NOT NULL,
  `type` text DEFAULT NULL,
  `data` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.disc_inventory: ~0 rows (approximately)
/*!40000 ALTER TABLE `disc_inventory` DISABLE KEYS */;
/*!40000 ALTER TABLE `disc_inventory` ENABLE KEYS */;

-- Dumping structure for table essentialmode.disc_inventory_itemdata
CREATE TABLE IF NOT EXISTS `disc_inventory_itemdata` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `description` text DEFAULT NULL,
  `weight` int(11) NOT NULL DEFAULT 0,
  `closeonuse` tinyint(1) NOT NULL DEFAULT 0,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.disc_inventory_itemdata: ~0 rows (approximately)
/*!40000 ALTER TABLE `disc_inventory_itemdata` DISABLE KEYS */;
/*!40000 ALTER TABLE `disc_inventory_itemdata` ENABLE KEYS */;

-- Dumping structure for table essentialmode.economy
CREATE TABLE IF NOT EXISTS `economy` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `item` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `count` int(11) NOT NULL DEFAULT 0,
  `price` int(11) NOT NULL DEFAULT 0,
  `difference` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.economy: ~13 rows (approximately)
/*!40000 ALTER TABLE `economy` DISABLE KEYS */;
INSERT INTO `economy` (`ID`, `item`, `label`, `count`, `price`, `difference`) VALUES
	(1, 'shell_package', 'หอยผัดเผ็ด', 0, 312, 246),
	(2, 'glasses', 'กระจก', 0, 368, 4),
	(3, 'milk_package', 'นมกล่อง', 0, 379, 190),
	(4, 'rice_pro', 'ข้าวสาร', 0, 237, 223),
	(5, 'Packaged_plank', 'ไม้แปรรูป', 0, 360, 452),
	(6, 'petrol_raffin', 'น้ำมัน', 0, 253, 376),
	(7, 'packaged_chicken', 'ไก่แพ็ค', 0, 302, 117),
	(8, 'bcabbage', 'กระหล่ำแปลรูป', 0, 247, 379),
	(9, 'honey_b', 'น้ำผึ้ง', 0, 273, 319),
	(10, 'fish', 'ปลา', 0, 249, 290),
	(11, 'clothe', 'เสื้อผ้า', 0, 376, 410),
	(12, 'chest_a', 'เหรียญทองคำ', 0, 286, 18),
	(13, 'porkpackage', 'เนื้อหมูแพ็ค', 0, 391, 375);
/*!40000 ALTER TABLE `economy` ENABLE KEYS */;

-- Dumping structure for table essentialmode.fine_types
CREATE TABLE IF NOT EXISTS `fine_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=216 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.fine_types: ~169 rows (approximately)
/*!40000 ALTER TABLE `fine_types` DISABLE KEYS */;
INSERT INTO `fine_types` (`id`, `label`, `amount`, `category`) VALUES
	(1, 'ข้บรถชนแล้วหนี', 1000, 0),
	(2, 'ขับรถชนรถคนอื่นพัง', 1000, 0),
	(3, 'ฝ่าไฟแดง', 1000, 0),
	(4, 'ขับรถย้อนศร', 800, 0),
	(5, 'ไม่มีใบขับขี่ทุกชนิด', 500, 0),
	(6, 'ตำรวจเรียกแล้วไม่จอด', 3000, 0),
	(7, 'ขโมยรถ NPC', 1000, 0),
	(8, 'ขโมยรถผู้เล่น', 5000, 0),
	(9, 'ขโมยรถ (หมอ,ตำรวจ,ช่าง)', 5000, 0),
	(10, 'ถืออาวุธในที่สาธารณะ', 500, 0),
	(11, 'จับตัวประกันผู้เล่น', 1000, 1),
	(12, 'จับตัวประกันหมอ', 5000, 1),
	(13, 'จับตัวประกันตำรวจ', 5000, 1),
	(14, 'จับตัวประกันช่าง', 5000, 1),
	(15, 'ยิงปืนในที่สาธารณะ', 1500, 1),
	(16, 'ฆ่าหมอหัวละ', 30000, 1),
	(17, 'ฆ่าตำรวจหัวละ', 30000, 1),
	(18, 'ฆ่าช่าง', 30000, 1),
	(19, 'ฆ่าผู้เล่น', 10000, 1),
	(20, 'หนีการตรวจค้น', 5000, 1),
	(21, 'ปล้นผู้เล่น', 5000, 2),
	(22, 'พูดจาหมิ่นประมาทเจ้าหน้าที่', 500, 2),
	(23, 'ทำร้ายร่างกายผู้เล่น', 1500, 2),
	(24, 'มีสัตว์ผิดกฏหมายในครองครอง[เต่าหรือฉลาม]', 8000, 3),
	(25, 'ยึด สน.', 0, 2),
	(26, 'ชุมนุมใน สน.', 0, 2),
	(27, 'ถ่วงเวลาเจ้าหน้าที่', 500, 2),
	(28, 'ทำลายหลักฐาน', 5000, 2),
	(29, 'สมรู้ร่วมคิดก่อเหตุ', 500, 2),
	(30, 'แจ้งความเท็จ', 2000, 2),
	(31, 'ปลอมตัวเป็นเจ้าหน้าที่', 30000, 2),
	(35, 'ก่อเหตุใน สน.', 5000, 2),
	(36, 'มี Weed ครอบครองไว้เกิน 50 ชิ้น', 20000, 3),
	(37, 'มี Weed ครอบครองไว้น้อยกว่า 50 ชิ้น', 10000, 3),
	(42, 'มี Opium ครอบครอวไว้เกิน 50 ชิ้น', 20000, 3),
	(43, 'มี Opium ครอบครอวไว้น้อยกว่า 50 ชิ้น', 15000, 3),
	(44, 'มีเงินแดงในครอบครองน้อยกว่า 1 หมื่น', 8000, 3),
	(45, 'มีเงินแดงในครอบครองมากกว่า 1 หมื่น', 15000, 3),
	(46, 'มีเงินแดงในครอบครองมากกว่า 5 หมื่น', 50000, 3),
	(47, 'มีเงินแดงในครอบครองมากกว่า 1 แสน', 150000, 3),
	(48, 'ชุบหน่วยงาน', 2000, 4),
	(49, 'ชุบนอกเมือง', 4000, 4),
	(50, 'ชุบในเมือง', 3000, 4),
	(51, 'ชุบในสถานที่ยากจะเข้าถึง', 4600, 4),
	(52, 'ชุบในที่ผิดกฎหมาย', 5000, 4),
	(54, 'ข้ับรถชนแล้วหนี', 5000, 5),
	(55, 'ขับรถชนรถคนอื่นพัง', 7500, 5),
	(56, 'ฝ่าไฟแดง', 2000, 5),
	(57, 'ขับรถย้อนศร', 1500, 5),
	(58, 'ไม่มีใบขับขี่ทุกชนิด', 2500, 5),
	(59, 'ตำรวจเรียกแล้วไม่จอด', 5000, 5),
	(60, 'ขโมยรถ NPC', 5000, 5),
	(61, 'ขโมยรถผู้เล่น', 10000, 5),
	(62, 'ขโมยรถ (หมอ,ตำรวจ,ช่าง)', 27500, 5),
	(63, 'ถืออาวุธในที่สาธารณะ', 7500, 5),
	(64, 'จับตัวประกันผู้เล่น', 15000, 5),
	(65, 'จับตัวประกันหมอ', 100000, 5),
	(66, 'จับตัวประกันตำรวจ', 50000, 5),
	(67, 'จับตัวประกันช่าง', 35000, 5),
	(68, 'ยิงปืนในที่สาธารณะ', 15000, 5),
	(69, 'ฆ่าหมอหัวละ', 125000, 5),
	(70, 'ฆ่าตำรวจหัวละ', 50000, 5),
	(71, 'ฆ่าช่าง', 40000, 5),
	(72, 'ฆ่าผู้เล่น', 22500, 5),
	(73, 'หนีการตรวจค้น', 10000, 5),
	(74, 'ปล้นผู้เล่น', 17500, 5),
	(75, 'พูดจาหมิ่นประมาทเจ้าหน้าที่', 10000, 5),
	(76, 'ทำร้ายร่างกายผู้เล่น', 5000, 5),
	(77, 'นำรถเข้า สน.', 1000, 5),
	(78, 'ยึด สน.', 100000, 5),
	(79, 'ชุมนุมใน สน.', 17500, 5),
	(80, 'ถ่วงเวลาเจ้าหน้าที่', 2500, 5),
	(81, 'ทำลายหลักฐาน', 1000, 5),
	(82, 'สมรู้ร่วมคิดก่อเหตุ', 2500, 5),
	(83, 'แจ้งความเท็จ', 1000, 5),
	(84, 'ปลอมตัวเป็นเจ้าหน้าที่', 50000, 5),
	(85, 'ปล้นร้านค้า', 30000, 2),
	(86, 'ปล้นธนาคารเล็ก', 100000, 5),
	(87, 'ปล้นธนาคารใหญ่', 150000, 5),
	(88, 'ก่อเหตุใน สน', 5000, 5),
	(89, 'ซ่อมรถหน่วยงาน', 500, 6),
	(90, 'ซ่อมรถประชาชนทั่วไป', 1250, 6),
	(91, 'ซ่อมรถประชาชนสปอร์ต', 2000, 6),
	(92, 'ซ่อมรถประชาชนมอไซ', 1000, 6),
	(98, 'ส่งในเมือง', 1000, 8),
	(99, 'ส่งนอกเมือง', 3000, 8),
	(100, 'ส่งในที่ผิดกฎหมาย', 5000, 8),
	(101, 'มีอาวุธผิดกฎหมายไว้ในครอบครอง', 7500, 0),
	(102, 'ไม่มีใบอนุญาติพกปืน', 5000, 0),
	(106, 'การละเมิดของแตร', 1000, 0),
	(107, 'หลอกลวงทางธุรกิจ', 10000, 2),
	(108, 'มีเงินแดงในครอบครองมากกว่า 500K', 500000, 3),
	(109, 'ปิดบังหน้าตา', 800, 0),
	(110, 'ความเร็วเกินกำหนด', 800, 0),
	(111, 'จอดในที่ห้ามจอด', 800, 0),
	(112, 'พกพาอาวุธล่าสัตว์ในที่สาธารณะ', 10000, 2),
	(113, 'ก่อกวนหน้า สน.', 3000, 2),
	(130, 'ขับรถโดยประมาท', 5000, 0),
	(131, 'ขับรถโดยประมาททางอาญา', 10000, 0),
	(132, 'ฝ่าฝืนกฏจราจร ผ่าไฟแดง', 500, 0),
	(133, 'จอดรถผิดที่', 500, 0),
	(134, 'ขับรถโดยไม่มีใบขับขี่', 1500, 0),
	(135, 'ขับรถสภาพไม่เต็ม100%', 2500, 0),
	(136, 'เมาแล้วขับ', 2500, 0),
	(137, 'การแข่งรถโดยไม่ได้รับอนุญาติ', 10000, 0),
	(138, 'ก่อให้เกิตความไม่สงบ', 10000, 3),
	(139, 'รบกวนโดยใช้เสียง', 3500, 3),
	(140, 'ล่วงละเมิด ทางร่างกาย', 20000, 3),
	(141, 'ข้ามถนนโดยประมาท', 1500, 0),
	(142, 'ปฐิเสธการจับกุม', 10000, 0),
	(143, 'เมาในที่สาธารณะ', 3000, 0),
	(144, 'ขัดขวางการทำงานของเจ้าหน้าที่รัฐ', 50000, 3),
	(145, 'ตามสอดส่งเจ้าหน้าที่รัฐมากเกินไป', 5000, 3),
	(146, 'ปฎิเสธข้อกล่าวหา', 3000, 0),
	(147, 'แจ้งข้อมูลเท็จ', 5000, 0),
	(148, 'การละเมิดทางอาญา', 1000, 3),
	(149, 'ทำลายทรัพย์สิน น้อยกว่า $ 1,000', 1000, 3),
	(150, 'ชนแล้วหนีโดยไม่บาดเจ็บ', 800, 3),
	(151, 'การพยายามปล้นสิ่งของมีมูลค่าน้อยกว่า $950', 1000, 1),
	(152, 'มีกัญชา ครอบครอง/1ใบ', 2000, 2),
	(155, 'การปล้นสิ่งของที่มีมูลมากกว่า $950', 1000, 1),
	(156, 'ทำลายทรัพย์สิน มากกว่า $ 1,000', 3000, 3),
	(157, 'แจ้งเหตุ , แจ้งความหรือข้อมูลเท็จ', 3000, 0),
	(158, 'หลบหนีและมีความผิดทางอาญา', 15000, 0),
	(159, 'การแอบอ้างบุคคลว่าอื่นเป็นข้าราชการ', 8000, 3),
	(160, 'ปลอมแปลงหลักฐาน', 5000, 3),
	(161, 'ฟอกเงิน', 50000, 2),
	(162, 'กระทำให้ผู้อื่น รู้สึกถึงอันตราย', 3000, 3),
	(163, 'ชนแล้วหนี บาดเจ็บ', 3000, 3),
	(165, 'ทำร้ายร่างกาย', 20000, 3),
	(166, 'ทำร้ายเจ้าพนักงาน', 3000, 3),
	(167, 'ปล้นรถยนต์', 3000, 1),
	(168, 'การปล้นปืน', 25000, 1),
	(169, 'Weed กัญชา : 50อันขึ้นไป', 30000, 2),
	(170, 'Weed กัญชาแปรรูป : 20อันขึ้นไป', 50000, 2),
	(173, 'แอบซุกซ่อนสิ่งเสพติดไว้ในพาหนะ', 5000, 2),
	(174, 'ครอบครอง สิ่งของสำหรับลักพาตัว', 5000, 1),
	(175, 'ครอบของอาวุธสงครามไปก่อเหตุ', 30000, 2),
	(176, 'เงินผิดกฎหมาย/1000บาท', 1500, 2),
	(177, 'หลบหนีการจับกุม', 50000, 3),
	(178, 'ปฏิเสธที่จะจ่ายค่าปรับ', 500, 0),
	(182, 'พกปืนโดยไม่มีใบอนุญาติ', 5000, 2),
	(183, 'ก่อเหตุจลาจล', 1000, 3),
	(185, 'ปล้นธนาคารโดยใช้อาวุธ', 85000, 1),
	(186, 'ปล้นใช้อาวุธ', 35000, 1),
	(187, 'พยายามปล้นหรือขู่กรรโชก', 30000, 1),
	(188, 'พยานเท็จ', 6000, 0),
	(189, 'ติดสินบนเจ้าพนักงาน', 10000, 3),
	(190, 'ฆาตกรรมเจ้าหน้าที่รัฐ', 100000, 3),
	(191, 'สมรู้ร่วมคิดในการฆาตกรรมเจ้าหน้าที่รัฐ', 75000, 3),
	(192, 'ฆาตกรรมประชาชน', 30000, 3),
	(193, 'สมรู้ร่วมคิดในการฆาตกรรม', 10000, 3),
	(194, 'ช่วยเหลือผู้กระทำผิด', 5000, 3),
	(195, 'ฆ่าโดยไม่ไตร่ตรอง', 15000, 3),
	(196, 'ฆ่าคนตายโดยประมาท', 30000, 3),
	(198, 'ทรมาน', 35000, 3),
	(199, 'กักขังหน่วงเหนี่ยว', 8000, 3),
	(200, 'ลักพาตัว', 150000, 1),
	(201, 'ขโมยสิ่งของเจ้าหน้าที่รัฐ', 10000, 3),
	(206, 'ปิดบังใบหน้า', 10000, 0),
	(207, 'ดูหมิ่นเจ้าหน้าที่ตำรวจ', 10000, 3),
	(208, 'มีกัญชา ครอบครอง/1แพ็ค', 6000, 2),
	(209, 'ชกต่อยบริเวณโรงพยาบาล', 30000, 3),
	(210, 'ทำให้สลบบริเวณโรงพยาบาล', 50000, 3),
	(211, 'ถืออาวุธบริเวณโรงพยาบาล', 30000, 3),
	(212, 'ตั้งใจใส่หน้ากากภายในโรงพยาบาล', 10000, 3),
	(213, 'ไม่จ่ายค่ารักษาพยาบาล', 2500, 3),
	(214, 'ปั่นป่วน ก่อจราจล ทำให้โรงพยาบาลวุ่นวาย', 30000, 3),
	(215, 'ทำลายทรัพย์สินของโรงพยาบาล', 50000, 3);
/*!40000 ALTER TABLE `fine_types` ENABLE KEYS */;

-- Dumping structure for table essentialmode.gangs
CREATE TABLE IF NOT EXISTS `gangs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  `color` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.gangs: ~0 rows (approximately)
/*!40000 ALTER TABLE `gangs` DISABLE KEYS */;
/*!40000 ALTER TABLE `gangs` ENABLE KEYS */;

-- Dumping structure for table essentialmode.gang_grades
CREATE TABLE IF NOT EXISTS `gang_grades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gang_name` varchar(255) NOT NULL,
  `grade` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.gang_grades: ~0 rows (approximately)
/*!40000 ALTER TABLE `gang_grades` DISABLE KEYS */;
/*!40000 ALTER TABLE `gang_grades` ENABLE KEYS */;

-- Dumping structure for table essentialmode.hungerthirst
CREATE TABLE IF NOT EXISTS `hungerthirst` (
  `idSteam` varchar(255) NOT NULL,
  `hunger` int(11) NOT NULL DEFAULT 100,
  `thirst` int(11) NOT NULL DEFAULT 100
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.hungerthirst: ~0 rows (approximately)
/*!40000 ALTER TABLE `hungerthirst` DISABLE KEYS */;
/*!40000 ALTER TABLE `hungerthirst` ENABLE KEYS */;

-- Dumping structure for table essentialmode.items
CREATE TABLE IF NOT EXISTS `items` (
  `name` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `limit` int(11) NOT NULL DEFAULT -1,
  `rare` int(11) NOT NULL DEFAULT 0,
  `can_remove` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.items: ~220 rows (approximately)
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
INSERT INTO `items` (`name`, `label`, `limit`, `rare`, `can_remove`) VALUES
	('AED', 'เครื่องaed', 1, 0, 1),
	('Packaged_plank', 'ไม้แปรรูป', 100, 0, 1),
	('SteelScrap', 'เศษเหล็ก', 1000, 0, 1),
	('WEAPON_ADVANCEDRIFLE', 'Advanced Rifle', 1, 0, 1),
	('WEAPON_APPISTOL', 'AP Pistol', 1, 0, 1),
	('WEAPON_ASSAULTRIFLE', 'Assault Rifle', 1, 0, 1),
	('WEAPON_ASSAULTSHOTGUN', 'Assault Shotgun', 1, 0, 1),
	('WEAPON_ASSAULTSMG', 'Assault SMG', 1, 0, 1),
	('WEAPON_AUTOSHOTGUN', 'Auto Shotgun', 1, 0, 1),
	('WEAPON_BALL', 'Ball', 1, 0, 1),
	('WEAPON_BAT', 'Bat', 1, 0, 1),
	('WEAPON_BATTLEAXE', 'Battle Axe', 1, 0, 1),
	('WEAPON_BOTTLE', 'Bottle', 1, 0, 1),
	('WEAPON_BULLPUPRIFLE', 'Bullpup Rifle', 1, 0, 1),
	('WEAPON_BULLPUPSHOTGUN', 'Bullpup Shotgun', 1, 0, 1),
	('WEAPON_BZGAS', 'BZ Gas', 1, 0, 1),
	('WEAPON_CARBINERIFLE', 'Carbine Rifle', 1, 0, 1),
	('WEAPON_COMBATMG', 'Combat MG', 1, 0, 1),
	('WEAPON_COMBATPDW', 'Combat PDW', 1, 0, 1),
	('WEAPON_COMBATPISTOL', 'Combat Pistol', 1, 0, 1),
	('WEAPON_COMPACTLAUNCHER', 'Compact Launcher', 1, 0, 1),
	('WEAPON_COMPACTRIFLE', 'Compact Rifle', 1, 0, 1),
	('WEAPON_CROWBAR', 'Crowbar', 1, 0, 1),
	('WEAPON_DAGGER', 'Dagger', 1, 0, 1),
	('WEAPON_DBSHOTGUN', 'Double Barrel Shotgun', 1, 0, 1),
	('WEAPON_DIGISCANNER', 'Digiscanner', 1, 0, 1),
	('WEAPON_DOUBLEACTION', 'Double Action Revolver', 1, 0, 1),
	('WEAPON_FIREEXTINGUISHER', 'Fire Extinguisher', 1, 0, 1),
	('WEAPON_FIREWORK', 'Firework Launcher', 1, 0, 1),
	('WEAPON_FLARE', 'Flare', 1, 0, 1),
	('WEAPON_FLAREGUN', 'Flare Gun', 1, 0, 1),
	('WEAPON_FLASHLIGHT', 'Flashlight', 1, 0, 1),
	('WEAPON_GARBAGEBAG', 'Garbage Bag', 1, 0, 1),
	('WEAPON_GOLFCLUB', 'Golf Club', 1, 0, 1),
	('WEAPON_GRENADE', 'Gernade', 1, 0, 1),
	('WEAPON_GRENADELAUNCHER', 'Gernade Launcher', 1, 0, 1),
	('WEAPON_GUSENBERG', 'Gusenberg', 1, 0, 1),
	('WEAPON_HAMMER', 'Hammer', 1, 0, 1),
	('WEAPON_HANDCUFFS', 'Handcuffs', 1, 0, 1),
	('WEAPON_HATCHET', 'Hatchet', 1, 0, 1),
	('WEAPON_HEAVYPISTOL', 'Heavy Pistol', 1, 0, 1),
	('WEAPON_HEAVYSHOTGUN', 'Heavy Shotgun', 1, 0, 1),
	('WEAPON_HEAVYSNIPER', 'Heavy Sniper', 1, 0, 1),
	('WEAPON_HOMINGLAUNCHER', 'Homing Launcher', 1, 0, 1),
	('WEAPON_KNIFE', 'Knife', 1, 0, 1),
	('WEAPON_KNUCKLE', 'Knuckle Dusters ', 1, 0, 1),
	('WEAPON_MACHETE', 'Machete', 1, 0, 1),
	('WEAPON_MACHINEPISTOL', 'Machine Pistol', 1, 0, 1),
	('WEAPON_MARKSMANPISTOL', 'Marksman Pistol', 1, 0, 1),
	('WEAPON_MARKSMANRIFLE', 'Marksman Rifle', 1, 0, 1),
	('WEAPON_MG', 'MG', 1, 0, 1),
	('WEAPON_MICROSMG', 'Micro SMG', 1, 0, 1),
	('WEAPON_MINIGUN', 'Minigun', 1, 0, 1),
	('WEAPON_MINISMG', 'Mini SMG', 1, 0, 1),
	('WEAPON_MOLOTOV', 'Molotov', 1, 0, 1),
	('WEAPON_MUSKET', 'Musket', 1, 0, 1),
	('WEAPON_NIGHTSTICK', 'Police Baton', 1, 0, 1),
	('WEAPON_PETROLCAN', 'Petrol Can', 1, 0, 1),
	('WEAPON_PIPEBOMB', 'Pipe Bomb', 1, 0, 1),
	('WEAPON_PISTOL', 'Pistol', 1, 0, 1),
	('WEAPON_PISTOL50', 'Police .50', 1, 0, 1),
	('WEAPON_POOLCUE', 'Pool Cue', 1, 0, 1),
	('WEAPON_PROXMINE', 'Proximity Mine', 1, 0, 1),
	('WEAPON_PUMPSHOTGUN', 'Pump Shotgun', 1, 0, 1),
	('WEAPON_RAILGUN', 'Rail Gun', 1, 0, 1),
	('WEAPON_REVOLVER', 'Revolver', 1, 0, 1),
	('WEAPON_RPG', 'RPG', 1, 0, 1),
	('WEAPON_SAWNOFFSHOTGUN', 'Sawn Off Shotgun', 1, 0, 1),
	('WEAPON_SMG', 'SMG', 1, 0, 1),
	('WEAPON_SMOKEGRENADE', 'Smoke Gernade', 1, 0, 1),
	('WEAPON_SNIPERRIFLE', 'Sniper Rifle', 1, 0, 1),
	('WEAPON_SNOWBALL', 'Snow Ball', 1, 0, 1),
	('WEAPON_SNSPISTOL', 'SNS Pistol', 1, 0, 1),
	('WEAPON_SPECIALCARBINE', 'Special Rifle', 1, 0, 1),
	('WEAPON_STICKYBOMB', 'Sticky Bombs', 1, 0, 1),
	('WEAPON_STINGER', 'Stinger', 1, 0, 1),
	('WEAPON_STUNGUN', 'Police Taser', 1, 0, 1),
	('WEAPON_SWITCHBLADE', 'Switch Blade', 1, 0, 1),
	('WEAPON_VINTAGEPISTOL', 'Vintage Pistol', 1, 0, 1),
	('WEAPON_WRENCH', 'Wrench', 1, 0, 1),
	('acabbage', 'กระหล่ำสด', 100, 0, 1),
	('alive_chicken', 'เนื้อไก่สด', 70, 0, 1),
	('anti', 'ยาแก้ไอ', 30, 0, 1),
	('bandage', 'ผ้าพันแผล', 20, 0, 1),
	('bcabbage', 'กระหล่ำถูกล้าง', 100, 0, 1),
	('beer', 'เบียร์', 15, 0, 1),
	('blowpipe', 'คบเพลิง', 10, 0, 1),
	('blowtorch', 'Blowtorch', 1, 0, 1),
	('bobbypin', 'Bobbypin', 3, 0, 1),
	('bong', 'บ้องกัญชา', 1, 0, 1),
	('bottle', 'ขวด', 100, 0, 1),
	('boxmilk', 'นม', 20, 0, 1),
	('bread', 'ขนมปัง', 15, 0, 1),
	('britadeira', 'บดหิน', 1, 0, 1),
	('cannabis', 'กัญชา', 100, 0, 1),
	('carokit', 'Body Kit', 1, 0, 1),
	('carotool', 'Tools', 4, 0, 1),
	('chest_a', 'เหรียญทองคำ', -1, 0, 1),
	('chicken', 'ไก่สด', 20, 0, 1),
	('chicken_meat', 'เนื้อไก่สด', 100, 0, 1),
	('chicken_package', 'เนื้อไก่แช่เข็ง', 100, 0, 1),
	('chickenwithoutfeather', 'ไก่ที่ถูกดึงขนออก', 20, 0, 1),
	('chocolate', 'ช็อกโกแลต', 10, 0, 1),
	('cigarett', 'บุหรี่', 20, 0, 1),
	('clip', 'Weapon Clip', 5, 0, 1),
	('clothe', 'ผ้าหนังสัตว์', 40, 0, 1),
	('cocacola', 'แป๊ปซี่', 15, 0, 1),
	('coffe', 'กาแฟ', 15, 0, 1),
	('coke', 'โคเคน', 40, 0, 1),
	('coke_pooch', 'โคเคนสำเร็จ', 20, 0, 1),
	('copper', 'ทองแดง', 100, 0, 1),
	('cow_card', 'บัตรคราฟพิเศษ', 20, 0, 1),
	('craftkit', 'เครื่องมือคราฟ', 1, 0, 1),
	('crazypersoncard', 'บัตรคนบ้า', 1, 0, 1),
	('croquettes', 'อาหารสัตว์', 20, 0, 1),
	('cupcake', 'เค้ก', 15, 0, 1),
	('cutted_wood', '\r\nไม้ที่ถูกตัด', 100, 0, 1),
	('diamond', 'เพชร', 10, 0, 1),
	('diesel', 'ถังน้ำมัน', 100, 0, 1),
	('doubleaction_blueprint', 'Doubleaction Blueprint', 1, 0, 1),
	('drill', 'เครื่องเจาะหิน', 1, 0, 1),
	('essence', 'แก๊ส', 20, 0, 1),
	('fabric', '\r\nผ้า', 80, 0, 1),
	('fish', 'ปลา', 1000, 0, 1),
	('fishbait', 'เหยื่อตกปลา', 30, 0, 1),
	('fishingrod', 'เบ็ดตกปลา', 1, 0, 1),
	('fixkit', 'ชุดซ่อม', 5, 0, 1),
	('fixtool', '\r\nเครื่องมือซ่อม', 6, 0, 1),
	('fuel_a', 'น้ำมัน', 100, 0, 1),
	('fuel_b', '\r\nน้ำมันแปรรูป', 100, 0, 1),
	('gazbottle', '\r\nขวดแก๊ส', 11, 0, 1),
	('glasses', 'กระจก', 10, 0, 1),
	('gold', 'ทอง', 50, 0, 1),
	('gunpowder', 'ดินปืน', -1, 0, 1),
	('gym_membership', 'บัตรสมาชิกยิม', 1, 0, 1),
	('hamajifish', 'ปลาฮามาจิ', 100, 0, 1),
	('hamburger', 'แฮมเบอร์เกอร์', 15, 0, 1),
	('handcuffs', 'กุญแจมือ', 1, 0, 1),
	('hatchet_lj', 'ขวานตัดไม้', 1, 0, 1),
	('honey_a', 'รังผึ้ง', 100, 0, 1),
	('honey_b', 'แพ๊คน้ำผึ้ง', 100, 0, 1),
	('hydrocodone', 'Hydrocodone', 5, 0, 1),
	('icetea', 'ชา', 15, 0, 1),
	('iron', 'เหล็ก', 100, 0, 1),
	('jumelles', 'กล้องส่องทางไกล', 1, 0, 1),
	('knife_chicken', 'มีดฆ่าไก่', 1, 0, 1),
	('leather', 'Leather', 100, 0, 1),
	('lighter', 'ไฟแช๊ค', 1, 0, 1),
	('lockpick', 'Lock pick', 10, 0, 1),
	('marijuana', 'กัญชาแปรรูป', 50, 0, 1),
	('marijuana_cigarette', 'กัญชามวน', 15, 0, 1),
	('meat', 'Meat', 100, 0, 1),
	('meatshark', 'เนื้อฉลาม', 100, 0, 1),
	('medikit', 'ยาชุบชีวิต', 10, 0, 1),
	('meth', 'แอมเฟตามีน', 40, 0, 1),
	('meth_pooch', 'แพ็คแอมเฟตามีน', 20, 0, 1),
	('milk', 'นมวัวสด', 20, 0, 1),
	('milk_engine', 'เครื่องรีดนมวัว', 1, 0, 1),
	('milk_package', 'นมวัวกล่อง', 50, 0, 1),
	('morphine', 'Morphine', 5, 0, 1),
	('mushroom', 'Dirty Mushroom', 100, 0, 1),
	('mushroom_d', 'Mushroom', 100, 0, 1),
	('mushroom_p', 'Mushroom Pack', 100, 0, 1),
	('oxygen_mask', 'หน้ากากออกซิเจน', 5, 0, 1),
	('packaged_chicken', 'แพ๊คไก่', 70, 0, 1),
	('packaged_plank', 'packaged_plank', 100, 0, 1),
	('petrol', 'น้ำมันดิบ', 100, 0, 1),
	('petrol_raffin', 'น้ำมันแปรรูป', 100, 0, 1),
	('phone', 'มือถือ', 1, 0, 1),
	('pork', 'เนื้อหมู', 120, 0, 1),
	('porkpackage', 'เนื้อหมูแพ็ค', 40, 0, 1),
	('powerade', 'Powerade', -1, 0, 1),
	('prawn', 'กุ้งสด', 100, 0, 1),
	('prawnbait', 'กุ้งสำเร็จรูป', 100, 0, 1),
	('pro_wood', 'ไม้แปรรูป', 100, 0, 1),
	('protein_shake', 'Protein Shake', 15, 0, 1),
	('radio', 'radio', 1, 0, 1),
	('rice', 'ข้าวเปลือก', 100, 0, 1),
	('rice_pro', 'ข้าวสาร', 100, 0, 1),
	('rope', 'rope', -1, 0, 1),
	('sand', 'กองทราย', 100, 0, 1),
	('sand_a', 'กองทราย', 100, 0, 1),
	('sand_b', 'แพ็คทราย', 50, 0, 1),
	('sandwich', 'แซนด์วิช', 15, 0, 1),
	('shark', 'ปลาฉลาม', 20, 0, 1),
	('sharkfin', 'หูฉลาม', -1, 0, 1),
	('shell', 'หอยสด', 100, 0, 1),
	('shell_a', 'หอยสด', 100, 0, 1),
	('shell_b', 'หอยสะอาด', 100, 0, 1),
	('shell_c', 'หอยลวก', 100, 0, 1),
	('shell_engine', 'เครื่องมือขุดหอย', 1, 0, 1),
	('shell_package', 'หอยผัดเผ็ด', 50, 0, 1),
	('shovel', 'พลั่วขุดทราย', 1, 0, 1),
	('sickle', 'เคียวเกี่ยวข้าว', 1, 0, 1),
	('skate', 'skate', 1, 0, 1),
	('slaughtered_chicken', 'ไก่สำเร็จรูป', 70, 0, 1),
	('snspistol_blueprint', 'Snspistol Blueprint', 1, 0, 1),
	('sportlunch', 'Sportlunch', 15, 0, 1),
	('squid', 'ปลาหมึก', 100, 0, 1),
	('squidbait', '\r\nเหยื่อปลาหมึก', 100, 0, 1),
	('stone', 'หิน', 50, 0, 1),
	('switchblade_blueprint', 'Switchblade Blueprint', 1, 0, 1),
	('tequila', 'เทคิวล่า', 15, 0, 1),
	('turtle', 'เต่าทะเล', 20, 0, 1),
	('turtlebait', 'เหยื่อตกเต่าทะเล', 30, 0, 1),
	('vicodin', 'Vicodin', 5, 0, 1),
	('vodka', 'วอดก้า', 15, 0, 1),
	('washed_stone', 'หินล้าง', 50, 0, 1),
	('water', 'น้ำ', 10, 0, 1),
	('weaponflashlight', 'Weapon Flashlight', 1, 0, 1),
	('weapongrip', 'Weapon Grip', 1, 0, 1),
	('weaponskin', 'Weapon Skin', 1, 0, 1),
	('weed_pooch', 'กัญชาแพ๊ค', 25, 0, 1),
	('whisky', 'วิสกี้', 15, 0, 1),
	('wine', 'ไวน์', 15, 0, 1),
	('wood', 'ไม้', 100, 0, 1),
	('wool', 'ขนสัตว์', 40, 0, 1),
	('worm', '\r\nหนอน', 100, 0, 1),
	('wrench_blueprint', 'Wrench Blueprint', 1, 0, 1),
	('zippybag', 'ถุงซิบใส', 100, 0, 1);
/*!40000 ALTER TABLE `items` ENABLE KEYS */;

-- Dumping structure for table essentialmode.jail
CREATE TABLE IF NOT EXISTS `jail` (
  `identifier` varchar(100) NOT NULL,
  `isjailed` tinyint(1) DEFAULT NULL,
  `J_Time` datetime NOT NULL,
  `J_Cell` varchar(20) NOT NULL,
  `Jailer` varchar(100) NOT NULL,
  `Jailer_ID` varchar(100) NOT NULL,
  PRIMARY KEY (`identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.jail: ~0 rows (approximately)
/*!40000 ALTER TABLE `jail` DISABLE KEYS */;
/*!40000 ALTER TABLE `jail` ENABLE KEYS */;

-- Dumping structure for table essentialmode.jobs
CREATE TABLE IF NOT EXISTS `jobs` (
  `name` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `whitelisted` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.jobs: ~12 rows (approximately)
/*!40000 ALTER TABLE `jobs` DISABLE KEYS */;
INSERT INTO `jobs` (`name`, `label`, `whitelisted`) VALUES
	('ambulance', 'แพทย์', 1),
	('cardealer', 'Concessionnaire', 1),
	('deliverer', 'Deliverer', 0),
	('fueler', 'ขนส่งน้ำมัน', 1),
	('garbage', '♻พนักงานเก็บขยะ', 0),
	('mafia', 'มาเฟีย', 1),
	('mechanic', 'ช่างซ่อม', 1),
	('offambulance', 'Off-Duty', 1),
	('offpolice', 'Off-Duty', 1),
	('police', 'ตำรวจ', 1),
	('taxi', 'Taxi', 1),
	('unemployed', 'ว่างงาน', 0);
/*!40000 ALTER TABLE `jobs` ENABLE KEYS */;

-- Dumping structure for table essentialmode.job_grades
CREATE TABLE IF NOT EXISTS `job_grades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_name` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `grade` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `salary` int(11) NOT NULL,
  `skin_male` longtext COLLATE utf8mb4_bin NOT NULL,
  `skin_female` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=184 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.job_grades: ~37 rows (approximately)
/*!40000 ALTER TABLE `job_grades` DISABLE KEYS */;
INSERT INTO `job_grades` (`id`, `job_name`, `grade`, `name`, `label`, `salary`, `skin_male`, `skin_female`) VALUES
	(1, 'unemployed', 0, 'unemployed', 'Unemployed', 0, '{}', '{}'),
	(9, 'ambulance', 0, 'ambulance', 'นักเรียนแพทย์', 500, '{"torso_2":3,"makeup_3":0,"moles_2":0,"sun_2":0,"makeup_2":0,"arms":88,"bproof_2":0,"makeup_4":0,"decals_1":57,"hair_color_1":0,"complexion_2":0,"tshirt_1":11,"beard_4":0,"helmet_2":0,"makeup_1":0,"eyebrows_2":10,"complexion_1":0,"skin":0,"pants_1":20,"mask_1":0,"beard_2":10,"age_2":0,"decals_2":0,"blush_2":0,"watches_2":0,"ears_1":-1,"chain_2":0,"hair_color_2":0,"blush_1":0,"hair_1":18,"bproof_1":0,"chain_1":126,"torso_1":23,"blemishes_2":0,"arms_2":0,"mask_2":0,"ears_2":0,"watches_1":-1,"chest_1":0,"beard_3":0,"eyebrows_1":9,"eyebrows_3":0,"pants_2":0,"bags_1":0,"tshirt_2":0,"bracelets_1":-1,"chest_2":0,"sex":0,"glasses_1":0,"bags_2":0,"glasses_2":3,"eyebrows_4":0,"chest_3":0,"age_1":0,"face":0,"helmet_1":122,"bodyb_1":0,"shoes":9,"eye_color":0,"blush_3":0,"shoes_1":4,"shoes_2":2,"bracelets_2":0,"sun_1":0,"blemishes_1":0,"beard_1":10,"lipstick_4":0,"lipstick_3":0,"lipstick_2":0,"lipstick_1":0,"hair_2":0,"moles_1":0,"bodyb_2":0}', '{"mask_2":0,"complexion_1":0,"makeup_4":0,"helmet_2":1,"lipstick_3":0,"age_2":0,"chain_2":0,"moles_1":0,"beard_1":11,"decals_1":66,"lipstick_4":0,"bracelets_2":0,"ears_2":0,"arms":88,"hair_1":18,"bproof_2":0,"torso_2":2,"makeup_3":0,"chain_1":0,"pants_1":12,"glasses_1":4,"lipstick_1":0,"makeup_1":0,"eyebrows_4":0,"beard_4":0,"age_1":0,"blemishes_2":0,"face":0,"beard_3":0,"helmet_1":7,"bodyb_1":0,"eyebrows_1":0,"tshirt_2":0,"bags_2":0,"eyebrows_2":0,"chest_2":0,"bproof_1":0,"lipstick_2":0,"shoes":9,"shoes_1":10,"skin":4,"watches_2":0,"torso_1":10,"blush_3":0,"sun_1":0,"blemishes_1":0,"watches_1":3,"hair_color_1":0,"mask_1":0,"ears_1":-1,"beard_2":10,"shoes_2":1,"bags_1":0,"sun_2":0,"hair_2":1,"arms_2":0,"chest_3":0,"blush_1":0,"makeup_2":0,"eye_color":0,"complexion_2":0,"bracelets_1":-1,"sex":1,"eyebrows_3":0,"decals_2":0,"moles_2":0,"blush_2":0,"glasses_2":3,"hair_color_2":0,"pants_2":8,"bodyb_2":0,"chest_1":0,"tshirt_1":57}'),
	(10, 'ambulance', 1, 'doctor', 'แพทย์ชำนาญ', 800, '{"torso_2":3,"makeup_3":0,"moles_2":0,"sun_2":0,"makeup_2":0,"arms":88,"bproof_2":0,"makeup_4":0,"decals_1":57,"hair_color_1":0,"complexion_2":0,"tshirt_1":11,"beard_4":0,"helmet_2":0,"makeup_1":0,"eyebrows_2":10,"complexion_1":0,"skin":0,"pants_1":20,"mask_1":0,"beard_2":10,"age_2":0,"decals_2":0,"blush_2":0,"watches_2":0,"ears_1":-1,"chain_2":0,"hair_color_2":0,"blush_1":0,"hair_1":18,"bproof_1":0,"chain_1":126,"torso_1":23,"blemishes_2":0,"arms_2":0,"mask_2":0,"ears_2":0,"watches_1":-1,"chest_1":0,"beard_3":0,"eyebrows_1":9,"eyebrows_3":0,"pants_2":0,"bags_1":0,"tshirt_2":0,"bracelets_1":-1,"chest_2":0,"sex":0,"glasses_1":0,"bags_2":0,"glasses_2":3,"eyebrows_4":0,"chest_3":0,"age_1":0,"face":0,"helmet_1":122,"bodyb_1":0,"shoes":9,"eye_color":0,"blush_3":0,"shoes_1":4,"shoes_2":2,"bracelets_2":0,"sun_1":0,"blemishes_1":0,"beard_1":10,"lipstick_4":0,"lipstick_3":0,"lipstick_2":0,"lipstick_1":0,"hair_2":0,"moles_1":0,"bodyb_2":0}', '{"mask_2":0,"complexion_1":0,"makeup_4":0,"helmet_2":1,"lipstick_3":0,"age_2":0,"chain_2":0,"moles_1":0,"beard_1":11,"decals_1":66,"lipstick_4":0,"bracelets_2":0,"ears_2":0,"arms":88,"hair_1":18,"bproof_2":0,"torso_2":2,"makeup_3":0,"chain_1":0,"pants_1":12,"glasses_1":4,"lipstick_1":0,"makeup_1":0,"eyebrows_4":0,"beard_4":0,"age_1":0,"blemishes_2":0,"face":0,"beard_3":0,"helmet_1":7,"bodyb_1":0,"eyebrows_1":0,"tshirt_2":0,"bags_2":0,"eyebrows_2":0,"chest_2":0,"bproof_1":0,"lipstick_2":0,"shoes":9,"shoes_1":10,"skin":4,"watches_2":0,"torso_1":10,"blush_3":0,"sun_1":0,"blemishes_1":0,"watches_1":3,"hair_color_1":0,"mask_1":0,"ears_1":-1,"beard_2":10,"shoes_2":1,"bags_1":0,"sun_2":0,"hair_2":1,"arms_2":0,"chest_3":0,"blush_1":0,"makeup_2":0,"eye_color":0,"complexion_2":0,"bracelets_1":-1,"sex":1,"eyebrows_3":0,"decals_2":0,"moles_2":0,"blush_2":0,"glasses_2":3,"hair_color_2":0,"pants_2":8,"bodyb_2":0,"chest_1":0,"tshirt_1":57}'),
	(11, 'ambulance', 2, 'chief_doctor', 'รองผู้อำนวยการแพทย์', 1000, '{"torso_2":3,"makeup_3":0,"moles_2":0,"sun_2":0,"makeup_2":0,"arms":88,"bproof_2":0,"makeup_4":0,"decals_1":57,"hair_color_1":0,"complexion_2":0,"tshirt_1":11,"beard_4":0,"helmet_2":0,"makeup_1":0,"eyebrows_2":10,"complexion_1":0,"skin":0,"pants_1":20,"mask_1":0,"beard_2":10,"age_2":0,"decals_2":0,"blush_2":0,"watches_2":0,"ears_1":-1,"chain_2":0,"hair_color_2":0,"blush_1":0,"hair_1":18,"bproof_1":0,"chain_1":126,"torso_1":23,"blemishes_2":0,"arms_2":0,"mask_2":0,"ears_2":0,"watches_1":-1,"chest_1":0,"beard_3":0,"eyebrows_1":9,"eyebrows_3":0,"pants_2":0,"bags_1":0,"tshirt_2":0,"bracelets_1":-1,"chest_2":0,"sex":0,"glasses_1":0,"bags_2":0,"glasses_2":3,"eyebrows_4":0,"chest_3":0,"age_1":0,"face":0,"helmet_1":122,"bodyb_1":0,"shoes":9,"eye_color":0,"blush_3":0,"shoes_1":4,"shoes_2":2,"bracelets_2":0,"sun_1":0,"blemishes_1":0,"beard_1":10,"lipstick_4":0,"lipstick_3":0,"lipstick_2":0,"lipstick_1":0,"hair_2":0,"moles_1":0,"bodyb_2":0}', '{"mask_2":0,"complexion_1":0,"makeup_4":0,"helmet_2":1,"lipstick_3":0,"age_2":0,"chain_2":0,"moles_1":0,"beard_1":11,"decals_1":66,"lipstick_4":0,"bracelets_2":0,"ears_2":0,"arms":88,"hair_1":18,"bproof_2":0,"torso_2":2,"makeup_3":0,"chain_1":0,"pants_1":12,"glasses_1":4,"lipstick_1":0,"makeup_1":0,"eyebrows_4":0,"beard_4":0,"age_1":0,"blemishes_2":0,"face":0,"beard_3":0,"helmet_1":7,"bodyb_1":0,"eyebrows_1":0,"tshirt_2":0,"bags_2":0,"eyebrows_2":0,"chest_2":0,"bproof_1":0,"lipstick_2":0,"shoes":9,"shoes_1":10,"skin":4,"watches_2":0,"torso_1":10,"blush_3":0,"sun_1":0,"blemishes_1":0,"watches_1":3,"hair_color_1":0,"mask_1":0,"ears_1":-1,"beard_2":10,"shoes_2":1,"bags_1":0,"sun_2":0,"hair_2":1,"arms_2":0,"chest_3":0,"blush_1":0,"makeup_2":0,"eye_color":0,"complexion_2":0,"bracelets_1":-1,"sex":1,"eyebrows_3":0,"decals_2":0,"moles_2":0,"blush_2":0,"glasses_2":3,"hair_color_2":0,"pants_2":8,"bodyb_2":0,"chest_1":0,"tshirt_1":57}'),
	(12, 'ambulance', 3, 'boss', 'ผู้อำนวยการแพทย์', 2000, '{"torso_2":3,"makeup_3":0,"moles_2":0,"sun_2":0,"makeup_2":0,"arms":88,"bproof_2":0,"makeup_4":0,"decals_1":57,"hair_color_1":0,"complexion_2":0,"tshirt_1":11,"beard_4":0,"helmet_2":0,"makeup_1":0,"eyebrows_2":10,"complexion_1":0,"skin":0,"pants_1":20,"mask_1":0,"beard_2":10,"age_2":0,"decals_2":0,"blush_2":0,"watches_2":0,"ears_1":-1,"chain_2":0,"hair_color_2":0,"blush_1":0,"hair_1":18,"bproof_1":0,"chain_1":126,"torso_1":23,"blemishes_2":0,"arms_2":0,"mask_2":0,"ears_2":0,"watches_1":-1,"chest_1":0,"beard_3":0,"eyebrows_1":9,"eyebrows_3":0,"pants_2":0,"bags_1":0,"tshirt_2":0,"bracelets_1":-1,"chest_2":0,"sex":0,"glasses_1":0,"bags_2":0,"glasses_2":3,"eyebrows_4":0,"chest_3":0,"age_1":0,"face":0,"helmet_1":122,"bodyb_1":0,"shoes":9,"eye_color":0,"blush_3":0,"shoes_1":4,"shoes_2":2,"bracelets_2":0,"sun_1":0,"blemishes_1":0,"beard_1":10,"lipstick_4":0,"lipstick_3":0,"lipstick_2":0,"lipstick_1":0,"hair_2":0,"moles_1":0,"bodyb_2":0}', '{"mask_2":0,"complexion_1":0,"makeup_4":0,"helmet_2":1,"lipstick_3":0,"age_2":0,"chain_2":0,"moles_1":0,"beard_1":11,"decals_1":66,"lipstick_4":0,"bracelets_2":0,"ears_2":0,"arms":88,"hair_1":18,"bproof_2":0,"torso_2":2,"makeup_3":0,"chain_1":0,"pants_1":12,"glasses_1":4,"lipstick_1":0,"makeup_1":0,"eyebrows_4":0,"beard_4":0,"age_1":0,"blemishes_2":0,"face":0,"beard_3":0,"helmet_1":7,"bodyb_1":0,"eyebrows_1":0,"tshirt_2":0,"bags_2":0,"eyebrows_2":0,"chest_2":0,"bproof_1":0,"lipstick_2":0,"shoes":9,"shoes_1":10,"skin":4,"watches_2":0,"torso_1":10,"blush_3":0,"sun_1":0,"blemishes_1":0,"watches_1":3,"hair_color_1":0,"mask_1":0,"ears_1":-1,"beard_2":10,"shoes_2":1,"bags_1":0,"sun_2":0,"hair_2":1,"arms_2":0,"chest_3":0,"blush_1":0,"makeup_2":0,"eye_color":0,"complexion_2":0,"bracelets_1":-1,"sex":1,"eyebrows_3":0,"decals_2":0,"moles_2":0,"blush_2":0,"glasses_2":3,"hair_color_2":0,"pants_2":8,"bodyb_2":0,"chest_1":0,"tshirt_1":57}'),
	(13, 'police', 0, 'recruit', 'นักเรียนตำรวจ', 800, '{"makeup_3":25,"sex":0,"lipstick_3":19,"lipstick_1":0,"eyebrows_3":1,"hair_color_1":1,"decals_2":2,"helmet_1":65,"age_2":7,"eyebrows_4":0,"pants_1":28,"decals_1":8,"makeup_1":0,"tshirt_1":58,"hair_color_2":0,"shoes":1,"mask_1":0,"ears_2":0,"tshirt_2":0,"beard_3":0,"glasses":0,"beard_1":7,"beard_2":7,"skin":0,"lipstick_4":0,"lipstick_2":0,"beard_4":0,"makeup_4":0,"bags_2":0,"eyebrows_1":10,"torso_2":1,"pants_2":0,"mask_2":0,"ears_1":2,"hair_1":12,"bproof_2":1,"helmet_2":0,"face":0,"shoes_1":51,"chain_1":125,"bproof_1":0,"hair_2":0,"makeup_2":0,"glasses_2":2,"glasses_1":15,"torso_1":95,"shoes_2":0,"eyebrows_2":10,"arms":37,"bags_1":0,"chain_2":0,"age_1":5}', '{"mask_2":0,"complexion_1":0,"makeup_4":0,"helmet_2":0,"makeup_2":0,"age_2":0,"chain_2":0,"moles_1":0,"bodyb_2":0,"decals_1":0,"lipstick_4":0,"bracelets_2":0,"hair_color_2":0,"arms":19,"hair_1":18,"bproof_2":0,"torso_2":0,"makeup_3":0,"chain_1":125,"pants_1":35,"glasses_1":0,"lipstick_1":0,"makeup_1":0,"eyebrows_4":0,"blush_1":0,"chest_1":0,"blemishes_2":0,"face":0,"beard_3":0,"helmet_1":46,"bodyb_1":0,"eyebrows_1":9,"tshirt_2":0,"bags_2":0,"eyebrows_2":10,"chest_2":0,"bproof_1":0,"sex":0,"shoes_1":21,"glasses_2":1,"watches_2":0,"torso_1":55,"blush_3":0,"sun_1":0,"blemishes_1":0,"watches_1":-1,"hair_color_1":0,"mask_1":0,"ears_1":2,"beard_2":10,"shoes_2":0,"skin":0,"lipstick_2":0,"hair_2":1,"tshirt_1":129,"sun_2":0,"age_1":0,"lipstick_3":0,"eye_color":0,"complexion_2":0,"beard_1":11,"beard_4":0,"chest_3":0,"decals_2":0,"moles_2":0,"blush_2":0,"ears_2":0,"arms_2":0,"pants_2":0,"eyebrows_3":0,"bags_1":0,"bracelets_1":-1}'),
	(14, 'police', 3, 'lieutenant', 'ผู้กำกับ', 1300, '{"tshirt_2":2,"eyebrows_4":0,"makeup_3":0,"blush_2":0,"arms_2":0,"torso_1":64,"makeup_2":0,"lipstick_1":0,"glasses_2":0,"bags_1":43,"chain_2":0,"watches_1":3,"mask_2":0,"sun_2":0,"beard_3":0,"beard_4":0,"bproof_1":0,"makeup_4":0,"eyebrows_1":0,"chain_1":0,"face":0,"eye_color":0,"beard_1":18,"helmet_2":0,"eyebrows_3":0,"moles_2":0,"sun_1":0,"beard_2":10,"blush_3":0,"chest_2":0,"ears_2":0,"lipstick_4":0,"age_2":0,"hair_1":52,"blemishes_2":0,"chest_1":0,"glasses_1":0,"tshirt_1":2,"age_1":0,"eyebrows_2":10,"ears_1":-1,"sex":0,"hair_2":0,"bags_2":0,"bracelets_2":0,"hair_color_1":0,"torso_2":0,"skin":0,"helmet_1":-1,"chest_3":0,"bracelets_1":-1,"shoes_2":0,"pants_2":4,"complexion_1":0,"arms":6,"watches_2":0,"pants_1":0,"moles_1":0,"bproof_2":0,"decals_2":0,"complexion_2":0,"decals_1":0,"mask_1":0,"blush_1":0,"bodyb_1":0,"blemishes_1":0,"bodyb_2":0,"shoes_1":48,"lipstick_3":0,"lipstick_2":0,"hair_color_2":0,"makeup_1":0}', '{"mask_2":0,"complexion_1":0,"makeup_4":0,"helmet_2":0,"makeup_2":0,"age_2":0,"chain_2":0,"moles_1":0,"bodyb_2":0,"decals_1":0,"lipstick_4":0,"bracelets_2":0,"hair_color_2":0,"arms":19,"hair_1":18,"bproof_2":0,"torso_2":0,"makeup_3":0,"chain_1":125,"pants_1":35,"glasses_1":0,"lipstick_1":0,"makeup_1":0,"eyebrows_4":0,"blush_1":0,"chest_1":0,"blemishes_2":0,"face":0,"beard_3":0,"helmet_1":46,"bodyb_1":0,"eyebrows_1":9,"tshirt_2":0,"bags_2":0,"eyebrows_2":10,"chest_2":0,"bproof_1":0,"sex":0,"shoes_1":21,"glasses_2":1,"watches_2":0,"torso_1":55,"blush_3":0,"sun_1":0,"blemishes_1":0,"watches_1":-1,"hair_color_1":0,"mask_1":0,"ears_1":2,"beard_2":10,"shoes_2":0,"skin":0,"lipstick_2":0,"hair_2":1,"tshirt_1":129,"sun_2":0,"age_1":0,"lipstick_3":0,"eye_color":0,"complexion_2":0,"beard_1":11,"beard_4":0,"chest_3":0,"decals_2":0,"moles_2":0,"blush_2":0,"ears_2":0,"arms_2":0,"pants_2":0,"eyebrows_3":0,"bags_1":0,"bracelets_1":-1}'),
	(17, 'police', 4, 'boss', 'ผู้บังคับบัญชาตำรวจ', 3000, '{"tshirt_2":2,"eyebrows_4":0,"makeup_3":0,"blush_2":0,"arms_2":0,"torso_1":64,"makeup_2":0,"lipstick_1":0,"glasses_2":0,"bags_1":43,"chain_2":0,"watches_1":3,"mask_2":0,"sun_2":0,"beard_3":0,"beard_4":0,"bproof_1":0,"makeup_4":0,"eyebrows_1":0,"chain_1":0,"face":0,"eye_color":0,"beard_1":18,"helmet_2":0,"eyebrows_3":0,"moles_2":0,"sun_1":0,"beard_2":10,"blush_3":0,"chest_2":0,"ears_2":0,"lipstick_4":0,"age_2":0,"hair_1":52,"blemishes_2":0,"chest_1":0,"glasses_1":0,"tshirt_1":2,"age_1":0,"eyebrows_2":10,"ears_1":-1,"sex":0,"hair_2":0,"bags_2":0,"bracelets_2":0,"hair_color_1":0,"torso_2":0,"skin":0,"helmet_1":-1,"chest_3":0,"bracelets_1":-1,"shoes_2":0,"pants_2":4,"complexion_1":0,"arms":6,"watches_2":0,"pants_1":0,"moles_1":0,"bproof_2":0,"decals_2":0,"complexion_2":0,"decals_1":0,"mask_1":0,"blush_1":0,"bodyb_1":0,"blemishes_1":0,"bodyb_2":0,"shoes_1":48,"lipstick_3":0,"lipstick_2":0,"hair_color_2":0,"makeup_1":0}', '{"mask_2":0,"complexion_1":0,"makeup_4":0,"helmet_2":0,"makeup_2":0,"age_2":0,"chain_2":0,"moles_1":0,"bodyb_2":0,"decals_1":0,"lipstick_4":0,"bracelets_2":0,"hair_color_2":0,"arms":19,"hair_1":18,"bproof_2":0,"torso_2":0,"makeup_3":0,"chain_1":125,"pants_1":35,"glasses_1":0,"lipstick_1":0,"makeup_1":0,"eyebrows_4":0,"blush_1":0,"chest_1":0,"blemishes_2":0,"face":0,"beard_3":0,"helmet_1":46,"bodyb_1":0,"eyebrows_1":9,"tshirt_2":0,"bags_2":0,"eyebrows_2":10,"chest_2":0,"bproof_1":0,"sex":0,"shoes_1":21,"glasses_2":1,"watches_2":0,"torso_1":55,"blush_3":0,"sun_1":0,"blemishes_1":0,"watches_1":-1,"hair_color_1":0,"mask_1":0,"ears_1":2,"beard_2":10,"shoes_2":0,"skin":0,"lipstick_2":0,"hair_2":1,"tshirt_1":129,"sun_2":0,"age_1":0,"lipstick_3":0,"eye_color":0,"complexion_2":0,"beard_1":11,"beard_4":0,"chest_3":0,"decals_2":0,"moles_2":0,"blush_2":0,"ears_2":0,"arms_2":0,"pants_2":0,"eyebrows_3":0,"bags_1":0,"bracelets_1":-1}'),
	(31, 'deliverer', 0, 'employee', 'Employee', 0, '{"ears_1":2,"eyebrows_2":10,"chest_1":0,"moles_2":0,"helmet_2":2,"lipstick_3":0,"decals_1":0,"tshirt_1":15,"glasses_1":19,"arms":0,"glasses_2":0,"face":0,"mask_1":0,"pants_1":0,"blemishes_1":0,"blush_3":0,"makeup_1":0,"moles_1":0,"shoes_1":48,"complexion_1":0,"eyebrows_1":9,"beard_3":0,"beard_1":10,"bproof_1":0,"eye_color":0,"hair_2":0,"ears_2":0,"blemishes_2":0,"eyebrows_3":0,"hair_color_1":0,"decals_2":0,"hair_1":18,"shoes_2":0,"chain_2":0,"watches_1":-1,"makeup_2":0,"mask_2":0,"hair_color_2":0,"shoes":35,"arms_2":0,"bracelets_1":-1,"lipstick_4":0,"chest_2":0,"torso_1":22,"bags_2":0,"pants_2":1,"eyebrows_4":0,"chain_1":4,"tshirt_2":0,"age_2":0,"complexion_2":0,"bproof_2":0,"sex":0,"bracelets_2":0,"beard_2":10,"blush_2":0,"torso_2":2,"lipstick_2":0,"sun_2":0,"bags_1":0,"lipstick_1":0,"bodyb_1":0,"age_1":0,"skin":0,"blush_1":0,"watches_2":0,"beard_4":0,"makeup_4":0,"chest_3":0,"bodyb_2":0,"makeup_3":0,"helmet_1":56,"sun_1":0}', '{"ears_1":2,"eyebrows_2":10,"chest_1":0,"moles_2":0,"helmet_2":2,"lipstick_3":0,"decals_1":0,"tshirt_1":15,"glasses_1":19,"arms":0,"glasses_2":0,"face":0,"mask_1":0,"pants_1":0,"blemishes_1":0,"blush_3":0,"makeup_1":0,"moles_1":0,"shoes_1":48,"complexion_1":0,"eyebrows_1":9,"beard_3":0,"beard_1":10,"bproof_1":0,"eye_color":0,"hair_2":0,"ears_2":0,"blemishes_2":0,"eyebrows_3":0,"hair_color_1":0,"decals_2":0,"hair_1":18,"shoes_2":0,"chain_2":0,"watches_1":-1,"makeup_2":0,"mask_2":0,"hair_color_2":0,"shoes":35,"arms_2":0,"bracelets_1":-1,"lipstick_4":0,"chest_2":0,"torso_1":22,"bags_2":0,"pants_2":1,"eyebrows_4":0,"chain_1":4,"tshirt_2":0,"age_2":0,"complexion_2":0,"bproof_2":0,"sex":0,"bracelets_2":0,"beard_2":10,"blush_2":0,"torso_2":2,"lipstick_2":0,"sun_2":0,"bags_1":0,"lipstick_1":0,"bodyb_1":0,"age_1":0,"skin":0,"blush_1":0,"watches_2":0,"beard_4":0,"makeup_4":0,"chest_3":0,"bodyb_2":0,"makeup_3":0,"helmet_1":56,"sun_1":0}'),
	(108, 'garbage', 0, 'employee', 'Employee', 0, '{"tshirt_1":59,"torso_1":89,"arms":31,"pants_1":36,"glasses_1":19,"decals_2":0,"hair_color_2":0,"helmet_2":0,"hair_color_1":0,"face":2,"glasses_2":0,"torso_2":1,"shoes":35,"hair_1":0,"skin":0,"sex":0,"glasses_1":19,"pants_2":0,"hair_2":0,"decals_1":0,"tshirt_2":0,"helmet_1":5}', '{"tshirt_1":36,"torso_1":0,"arms":68,"pants_1":30,"glasses_1":15,"decals_2":0,"hair_color_2":0,"helmet_2":0,"hair_color_1":0,"face":27,"glasses_2":0,"torso_2":11,"shoes":26,"hair_1":5,"skin":0,"sex":1,"glasses_1":15,"pants_2":2,"hair_2":0,"decals_1":0,"tshirt_2":0,"helmet_1":19}'),
	(135, 'fueler', 0, 'employee', 'employee', 0, '{}', '{}'),
	(147, 'taxi', 0, 'recrue', 'Recrue', 300, '{"eyebrows_2":10,"bodyb_1":0,"eye_color":0,"sex":0,"sun_1":0,"face":0,"chest_2":0,"lipstick_4":0,"chest_3":0,"moles_1":0,"lipstick_3":0,"shoes":9,"beard_2":10,"shoes_2":0,"torso_1":4,"bodyb_2":0,"moles_2":0,"tshirt_2":0,"glasses_1":0,"helmet_2":0,"makeup_4":0,"complexion_1":0,"watches_1":3,"hair_color_1":1,"chain_1":12,"makeup_2":0,"beard_4":0,"age_1":0,"watches_2":0,"blush_3":0,"tshirt_1":11,"bracelets_1":-1,"eyebrows_4":0,"makeup_1":0,"hair_1":19,"blemishes_2":0,"bags_1":0,"sun_2":0,"pants_2":0,"chest_1":0,"chain_2":0,"age_2":0,"decals_1":0,"bproof_2":0,"blemishes_1":0,"helmet_1":-1,"ears_2":0,"eyebrows_1":0,"ears_1":-1,"torso_2":0,"bags_2":0,"lipstick_1":0,"arms_2":0,"mask_2":0,"hair_color_2":0,"arms":20,"pants_1":10,"decals_2":0,"mask_1":0,"bproof_1":0,"blush_1":0,"complexion_2":0,"glasses_2":3,"beard_3":0,"shoes_1":21,"makeup_3":0,"eyebrows_3":0,"hair_2":0,"beard_1":11,"bracelets_2":0,"skin":0,"lipstick_2":0,"blush_2":0}', '{"eyebrows_2":10,"bodyb_1":0,"eye_color":0,"sex":0,"sun_1":0,"face":0,"chest_2":0,"lipstick_4":0,"chest_3":0,"moles_1":0,"lipstick_3":0,"shoes":9,"beard_2":10,"shoes_2":0,"torso_1":4,"bodyb_2":0,"moles_2":0,"tshirt_2":0,"glasses_1":0,"helmet_2":0,"makeup_4":0,"complexion_1":0,"watches_1":3,"hair_color_1":1,"chain_1":12,"makeup_2":0,"beard_4":0,"age_1":0,"watches_2":0,"blush_3":0,"tshirt_1":11,"bracelets_1":-1,"eyebrows_4":0,"makeup_1":0,"hair_1":19,"blemishes_2":0,"bags_1":0,"sun_2":0,"pants_2":0,"chest_1":0,"chain_2":0,"age_2":0,"decals_1":0,"bproof_2":0,"blemishes_1":0,"helmet_1":-1,"ears_2":0,"eyebrows_1":0,"ears_1":-1,"torso_2":0,"bags_2":0,"lipstick_1":0,"arms_2":0,"mask_2":0,"hair_color_2":0,"arms":20,"pants_1":10,"decals_2":0,"mask_1":0,"bproof_1":0,"blush_1":0,"complexion_2":0,"glasses_2":3,"beard_3":0,"shoes_1":21,"makeup_3":0,"eyebrows_3":0,"hair_2":0,"beard_1":11,"bracelets_2":0,"skin":0,"lipstick_2":0,"blush_2":0}'),
	(148, 'taxi', 1, 'novice', 'Novice', 500, '{"eyebrows_2":10,"bodyb_1":0,"eye_color":0,"sex":0,"sun_1":0,"face":0,"chest_2":0,"lipstick_4":0,"chest_3":0,"moles_1":0,"lipstick_3":0,"shoes":9,"beard_2":10,"shoes_2":0,"torso_1":4,"bodyb_2":0,"moles_2":0,"tshirt_2":0,"glasses_1":0,"helmet_2":0,"makeup_4":0,"complexion_1":0,"watches_1":3,"hair_color_1":1,"chain_1":12,"makeup_2":0,"beard_4":0,"age_1":0,"watches_2":0,"blush_3":0,"tshirt_1":11,"bracelets_1":-1,"eyebrows_4":0,"makeup_1":0,"hair_1":19,"blemishes_2":0,"bags_1":0,"sun_2":0,"pants_2":0,"chest_1":0,"chain_2":0,"age_2":0,"decals_1":0,"bproof_2":0,"blemishes_1":0,"helmet_1":-1,"ears_2":0,"eyebrows_1":0,"ears_1":-1,"torso_2":0,"bags_2":0,"lipstick_1":0,"arms_2":0,"mask_2":0,"hair_color_2":0,"arms":20,"pants_1":10,"decals_2":0,"mask_1":0,"bproof_1":0,"blush_1":0,"complexion_2":0,"glasses_2":3,"beard_3":0,"shoes_1":21,"makeup_3":0,"eyebrows_3":0,"hair_2":0,"beard_1":11,"bracelets_2":0,"skin":0,"lipstick_2":0,"blush_2":0}', '{"eyebrows_2":10,"bodyb_1":0,"eye_color":0,"sex":0,"sun_1":0,"face":0,"chest_2":0,"lipstick_4":0,"chest_3":0,"moles_1":0,"lipstick_3":0,"shoes":9,"beard_2":10,"shoes_2":0,"torso_1":4,"bodyb_2":0,"moles_2":0,"tshirt_2":0,"glasses_1":0,"helmet_2":0,"makeup_4":0,"complexion_1":0,"watches_1":3,"hair_color_1":1,"chain_1":12,"makeup_2":0,"beard_4":0,"age_1":0,"watches_2":0,"blush_3":0,"tshirt_1":11,"bracelets_1":-1,"eyebrows_4":0,"makeup_1":0,"hair_1":19,"blemishes_2":0,"bags_1":0,"sun_2":0,"pants_2":0,"chest_1":0,"chain_2":0,"age_2":0,"decals_1":0,"bproof_2":0,"blemishes_1":0,"helmet_1":-1,"ears_2":0,"eyebrows_1":0,"ears_1":-1,"torso_2":0,"bags_2":0,"lipstick_1":0,"arms_2":0,"mask_2":0,"hair_color_2":0,"arms":20,"pants_1":10,"decals_2":0,"mask_1":0,"bproof_1":0,"blush_1":0,"complexion_2":0,"glasses_2":3,"beard_3":0,"shoes_1":21,"makeup_3":0,"eyebrows_3":0,"hair_2":0,"beard_1":11,"bracelets_2":0,"skin":0,"lipstick_2":0,"blush_2":0}'),
	(149, 'taxi', 2, 'experimente', 'Experimente', 500, '{"eyebrows_2":10,"bodyb_1":0,"eye_color":0,"sex":0,"sun_1":0,"face":0,"chest_2":0,"lipstick_4":0,"chest_3":0,"moles_1":0,"lipstick_3":0,"shoes":9,"beard_2":10,"shoes_2":0,"torso_1":4,"bodyb_2":0,"moles_2":0,"tshirt_2":0,"glasses_1":0,"helmet_2":0,"makeup_4":0,"complexion_1":0,"watches_1":3,"hair_color_1":1,"chain_1":12,"makeup_2":0,"beard_4":0,"age_1":0,"watches_2":0,"blush_3":0,"tshirt_1":11,"bracelets_1":-1,"eyebrows_4":0,"makeup_1":0,"hair_1":19,"blemishes_2":0,"bags_1":0,"sun_2":0,"pants_2":0,"chest_1":0,"chain_2":0,"age_2":0,"decals_1":0,"bproof_2":0,"blemishes_1":0,"helmet_1":-1,"ears_2":0,"eyebrows_1":0,"ears_1":-1,"torso_2":0,"bags_2":0,"lipstick_1":0,"arms_2":0,"mask_2":0,"hair_color_2":0,"arms":20,"pants_1":10,"decals_2":0,"mask_1":0,"bproof_1":0,"blush_1":0,"complexion_2":0,"glasses_2":3,"beard_3":0,"shoes_1":21,"makeup_3":0,"eyebrows_3":0,"hair_2":0,"beard_1":11,"bracelets_2":0,"skin":0,"lipstick_2":0,"blush_2":0}', '{"eyebrows_2":10,"bodyb_1":0,"eye_color":0,"sex":0,"sun_1":0,"face":0,"chest_2":0,"lipstick_4":0,"chest_3":0,"moles_1":0,"lipstick_3":0,"shoes":9,"beard_2":10,"shoes_2":0,"torso_1":4,"bodyb_2":0,"moles_2":0,"tshirt_2":0,"glasses_1":0,"helmet_2":0,"makeup_4":0,"complexion_1":0,"watches_1":3,"hair_color_1":1,"chain_1":12,"makeup_2":0,"beard_4":0,"age_1":0,"watches_2":0,"blush_3":0,"tshirt_1":11,"bracelets_1":-1,"eyebrows_4":0,"makeup_1":0,"hair_1":19,"blemishes_2":0,"bags_1":0,"sun_2":0,"pants_2":0,"chest_1":0,"chain_2":0,"age_2":0,"decals_1":0,"bproof_2":0,"blemishes_1":0,"helmet_1":-1,"ears_2":0,"eyebrows_1":0,"ears_1":-1,"torso_2":0,"bags_2":0,"lipstick_1":0,"arms_2":0,"mask_2":0,"hair_color_2":0,"arms":20,"pants_1":10,"decals_2":0,"mask_1":0,"bproof_1":0,"blush_1":0,"complexion_2":0,"glasses_2":3,"beard_3":0,"shoes_1":21,"makeup_3":0,"eyebrows_3":0,"hair_2":0,"beard_1":11,"bracelets_2":0,"skin":0,"lipstick_2":0,"blush_2":0}'),
	(150, 'taxi', 3, 'uber', 'Uber', 800, '{"eyebrows_2":10,"bodyb_1":0,"eye_color":0,"sex":0,"sun_1":0,"face":0,"chest_2":0,"lipstick_4":0,"chest_3":0,"moles_1":0,"lipstick_3":0,"shoes":9,"beard_2":10,"shoes_2":0,"torso_1":4,"bodyb_2":0,"moles_2":0,"tshirt_2":0,"glasses_1":0,"helmet_2":0,"makeup_4":0,"complexion_1":0,"watches_1":3,"hair_color_1":1,"chain_1":12,"makeup_2":0,"beard_4":0,"age_1":0,"watches_2":0,"blush_3":0,"tshirt_1":11,"bracelets_1":-1,"eyebrows_4":0,"makeup_1":0,"hair_1":19,"blemishes_2":0,"bags_1":0,"sun_2":0,"pants_2":0,"chest_1":0,"chain_2":0,"age_2":0,"decals_1":0,"bproof_2":0,"blemishes_1":0,"helmet_1":-1,"ears_2":0,"eyebrows_1":0,"ears_1":-1,"torso_2":0,"bags_2":0,"lipstick_1":0,"arms_2":0,"mask_2":0,"hair_color_2":0,"arms":20,"pants_1":10,"decals_2":0,"mask_1":0,"bproof_1":0,"blush_1":0,"complexion_2":0,"glasses_2":3,"beard_3":0,"shoes_1":21,"makeup_3":0,"eyebrows_3":0,"hair_2":0,"beard_1":11,"bracelets_2":0,"skin":0,"lipstick_2":0,"blush_2":0}', '{"eyebrows_2":10,"bodyb_1":0,"eye_color":0,"sex":0,"sun_1":0,"face":0,"chest_2":0,"lipstick_4":0,"chest_3":0,"moles_1":0,"lipstick_3":0,"shoes":9,"beard_2":10,"shoes_2":0,"torso_1":4,"bodyb_2":0,"moles_2":0,"tshirt_2":0,"glasses_1":0,"helmet_2":0,"makeup_4":0,"complexion_1":0,"watches_1":3,"hair_color_1":1,"chain_1":12,"makeup_2":0,"beard_4":0,"age_1":0,"watches_2":0,"blush_3":0,"tshirt_1":11,"bracelets_1":-1,"eyebrows_4":0,"makeup_1":0,"hair_1":19,"blemishes_2":0,"bags_1":0,"sun_2":0,"pants_2":0,"chest_1":0,"chain_2":0,"age_2":0,"decals_1":0,"bproof_2":0,"blemishes_1":0,"helmet_1":-1,"ears_2":0,"eyebrows_1":0,"ears_1":-1,"torso_2":0,"bags_2":0,"lipstick_1":0,"arms_2":0,"mask_2":0,"hair_color_2":0,"arms":20,"pants_1":10,"decals_2":0,"mask_1":0,"bproof_1":0,"blush_1":0,"complexion_2":0,"glasses_2":3,"beard_3":0,"shoes_1":21,"makeup_3":0,"eyebrows_3":0,"hair_2":0,"beard_1":11,"bracelets_2":0,"skin":0,"lipstick_2":0,"blush_2":0}'),
	(151, 'taxi', 4, 'boss', 'Patron', 1000, '{"eyebrows_2":10,"bodyb_1":0,"eye_color":0,"sex":0,"sun_1":0,"face":0,"chest_2":0,"lipstick_4":0,"chest_3":0,"moles_1":0,"lipstick_3":0,"shoes":9,"beard_2":10,"shoes_2":0,"torso_1":4,"bodyb_2":0,"moles_2":0,"tshirt_2":0,"glasses_1":0,"helmet_2":0,"makeup_4":0,"complexion_1":0,"watches_1":3,"hair_color_1":1,"chain_1":12,"makeup_2":0,"beard_4":0,"age_1":0,"watches_2":0,"blush_3":0,"tshirt_1":11,"bracelets_1":-1,"eyebrows_4":0,"makeup_1":0,"hair_1":19,"blemishes_2":0,"bags_1":0,"sun_2":0,"pants_2":0,"chest_1":0,"chain_2":0,"age_2":0,"decals_1":0,"bproof_2":0,"blemishes_1":0,"helmet_1":-1,"ears_2":0,"eyebrows_1":0,"ears_1":-1,"torso_2":0,"bags_2":0,"lipstick_1":0,"arms_2":0,"mask_2":0,"hair_color_2":0,"arms":20,"pants_1":10,"decals_2":0,"mask_1":0,"bproof_1":0,"blush_1":0,"complexion_2":0,"glasses_2":3,"beard_3":0,"shoes_1":21,"makeup_3":0,"eyebrows_3":0,"hair_2":0,"beard_1":11,"bracelets_2":0,"skin":0,"lipstick_2":0,"blush_2":0}', '{"eyebrows_2":10,"bodyb_1":0,"eye_color":0,"sex":0,"sun_1":0,"face":0,"chest_2":0,"lipstick_4":0,"chest_3":0,"moles_1":0,"lipstick_3":0,"shoes":9,"beard_2":10,"shoes_2":0,"torso_1":4,"bodyb_2":0,"moles_2":0,"tshirt_2":0,"glasses_1":0,"helmet_2":0,"makeup_4":0,"complexion_1":0,"watches_1":3,"hair_color_1":1,"chain_1":12,"makeup_2":0,"beard_4":0,"age_1":0,"watches_2":0,"blush_3":0,"tshirt_1":11,"bracelets_1":-1,"eyebrows_4":0,"makeup_1":0,"hair_1":19,"blemishes_2":0,"bags_1":0,"sun_2":0,"pants_2":0,"chest_1":0,"chain_2":0,"age_2":0,"decals_1":0,"bproof_2":0,"blemishes_1":0,"helmet_1":-1,"ears_2":0,"eyebrows_1":0,"ears_1":-1,"torso_2":0,"bags_2":0,"lipstick_1":0,"arms_2":0,"mask_2":0,"hair_color_2":0,"arms":20,"pants_1":10,"decals_2":0,"mask_1":0,"bproof_1":0,"blush_1":0,"complexion_2":0,"glasses_2":3,"beard_3":0,"shoes_1":21,"makeup_3":0,"eyebrows_3":0,"hair_2":0,"beard_1":11,"bracelets_2":0,"skin":0,"lipstick_2":0,"blush_2":0}'),
	(155, 'mechanic', 0, 'recrue', 'Recruit', 600, '{"eye_color":0,"complexion_1":0,"eyebrows_1":0,"pants_1":39,"chain_2":0,"hair_color_2":0,"tshirt_2":1,"chain_1":121,"makeup_4":0,"age_1":0,"glasses_1":0,"watches_2":0,"hair_2":0,"blush_3":0,"makeup_2":0,"sun_1":0,"beard_4":0,"sun_2":0,"bags_1":0,"makeup_3":0,"moles_1":0,"hair_color_1":0,"lipstick_4":0,"sex":0,"bproof_2":0,"moles_2":0,"beard_1":11,"bproof_1":0,"blemishes_2":0,"skin":0,"complexion_2":0,"helmet_2":4,"blush_2":0,"lipstick_2":0,"beard_3":0,"torso_2":0,"eyebrows_2":10,"glasses_2":3,"decals_2":0,"shoes_2":2,"tshirt_1":90,"mask_2":0,"chest_1":0,"blemishes_1":0,"watches_1":-1,"arms":4,"ears_1":-1,"shoes":9,"beard_2":10,"arms_2":0,"lipstick_3":0,"age_2":0,"bracelets_1":-1,"bracelets_2":0,"helmet_1":56,"face":0,"bags_2":0,"mask_1":0,"makeup_1":0,"chest_2":0,"chest_3":0,"ears_2":0,"eyebrows_3":0,"blush_1":0,"bodyb_1":0,"eyebrows_4":0,"bodyb_2":0,"hair_1":18,"torso_1":66,"shoes_1":4,"lipstick_1":0,"pants_2":0,"decals_1":0}', '{}'),
	(156, 'mechanic', 1, 'novice', 'Novice', 800, '{"eye_color":0,"complexion_1":0,"eyebrows_1":0,"pants_1":39,"chain_2":0,"hair_color_2":0,"tshirt_2":1,"chain_1":121,"makeup_4":0,"age_1":0,"glasses_1":0,"watches_2":0,"hair_2":0,"blush_3":0,"makeup_2":0,"sun_1":0,"beard_4":0,"sun_2":0,"bags_1":0,"makeup_3":0,"moles_1":0,"hair_color_1":0,"lipstick_4":0,"sex":0,"bproof_2":0,"moles_2":0,"beard_1":11,"bproof_1":0,"blemishes_2":0,"skin":0,"complexion_2":0,"helmet_2":4,"blush_2":0,"lipstick_2":0,"beard_3":0,"torso_2":0,"eyebrows_2":10,"glasses_2":3,"decals_2":0,"shoes_2":2,"tshirt_1":90,"mask_2":0,"chest_1":0,"blemishes_1":0,"watches_1":-1,"arms":4,"ears_1":-1,"shoes":9,"beard_2":10,"arms_2":0,"lipstick_3":0,"age_2":0,"bracelets_1":-1,"bracelets_2":0,"helmet_1":56,"face":0,"bags_2":0,"mask_1":0,"makeup_1":0,"chest_2":0,"chest_3":0,"ears_2":0,"eyebrows_3":0,"blush_1":0,"bodyb_1":0,"eyebrows_4":0,"bodyb_2":0,"hair_1":18,"torso_1":66,"shoes_1":4,"lipstick_1":0,"pants_2":0,"decals_1":0}', '{}'),
	(157, 'mechanic', 2, 'experimente', 'Experienced', 1100, '{"eye_color":0,"complexion_1":0,"eyebrows_1":0,"pants_1":39,"chain_2":0,"hair_color_2":0,"tshirt_2":1,"chain_1":121,"makeup_4":0,"age_1":0,"glasses_1":0,"watches_2":0,"hair_2":0,"blush_3":0,"makeup_2":0,"sun_1":0,"beard_4":0,"sun_2":0,"bags_1":0,"makeup_3":0,"moles_1":0,"hair_color_1":0,"lipstick_4":0,"sex":0,"bproof_2":0,"moles_2":0,"beard_1":11,"bproof_1":0,"blemishes_2":0,"skin":0,"complexion_2":0,"helmet_2":4,"blush_2":0,"lipstick_2":0,"beard_3":0,"torso_2":0,"eyebrows_2":10,"glasses_2":3,"decals_2":0,"shoes_2":2,"tshirt_1":90,"mask_2":0,"chest_1":0,"blemishes_1":0,"watches_1":-1,"arms":4,"ears_1":-1,"shoes":9,"beard_2":10,"arms_2":0,"lipstick_3":0,"age_2":0,"bracelets_1":-1,"bracelets_2":0,"helmet_1":56,"face":0,"bags_2":0,"mask_1":0,"makeup_1":0,"chest_2":0,"chest_3":0,"ears_2":0,"eyebrows_3":0,"blush_1":0,"bodyb_1":0,"eyebrows_4":0,"bodyb_2":0,"hair_1":18,"torso_1":66,"shoes_1":4,"lipstick_1":0,"pants_2":0,"decals_1":0}', '{}'),
	(158, 'mechanic', 3, 'chief', 'Leader', 1500, '{"eye_color":0,"complexion_1":0,"eyebrows_1":0,"pants_1":39,"chain_2":0,"hair_color_2":0,"tshirt_2":1,"chain_1":121,"makeup_4":0,"age_1":0,"glasses_1":0,"watches_2":0,"hair_2":0,"blush_3":0,"makeup_2":0,"sun_1":0,"beard_4":0,"sun_2":0,"bags_1":0,"makeup_3":0,"moles_1":0,"hair_color_1":0,"lipstick_4":0,"sex":0,"bproof_2":0,"moles_2":0,"beard_1":11,"bproof_1":0,"blemishes_2":0,"skin":0,"complexion_2":0,"helmet_2":4,"blush_2":0,"lipstick_2":0,"beard_3":0,"torso_2":0,"eyebrows_2":10,"glasses_2":3,"decals_2":0,"shoes_2":2,"tshirt_1":90,"mask_2":0,"chest_1":0,"blemishes_1":0,"watches_1":-1,"arms":4,"ears_1":-1,"shoes":9,"beard_2":10,"arms_2":0,"lipstick_3":0,"age_2":0,"bracelets_1":-1,"bracelets_2":0,"helmet_1":56,"face":0,"bags_2":0,"mask_1":0,"makeup_1":0,"chest_2":0,"chest_3":0,"ears_2":0,"eyebrows_3":0,"blush_1":0,"bodyb_1":0,"eyebrows_4":0,"bodyb_2":0,"hair_1":18,"torso_1":66,"shoes_1":4,"lipstick_1":0,"pants_2":0,"decals_1":0}', '{}'),
	(159, 'mechanic', 4, 'boss', 'Boss', 3500, '{"eye_color":0,"complexion_1":0,"eyebrows_1":0,"pants_1":39,"chain_2":0,"hair_color_2":0,"tshirt_2":1,"chain_1":121,"makeup_4":0,"age_1":0,"glasses_1":0,"watches_2":0,"hair_2":0,"blush_3":0,"makeup_2":0,"sun_1":0,"beard_4":0,"sun_2":0,"bags_1":0,"makeup_3":0,"moles_1":0,"hair_color_1":0,"lipstick_4":0,"sex":0,"bproof_2":0,"moles_2":0,"beard_1":11,"bproof_1":0,"blemishes_2":0,"skin":0,"complexion_2":0,"helmet_2":4,"blush_2":0,"lipstick_2":0,"beard_3":0,"torso_2":0,"eyebrows_2":10,"glasses_2":3,"decals_2":0,"shoes_2":2,"tshirt_1":90,"mask_2":0,"chest_1":0,"blemishes_1":0,"watches_1":-1,"arms":4,"ears_1":-1,"shoes":9,"beard_2":10,"arms_2":0,"lipstick_3":0,"age_2":0,"bracelets_1":-1,"bracelets_2":0,"helmet_1":56,"face":0,"bags_2":0,"mask_1":0,"makeup_1":0,"chest_2":0,"chest_3":0,"ears_2":0,"eyebrows_3":0,"blush_1":0,"bodyb_1":0,"eyebrows_4":0,"bodyb_2":0,"hair_1":18,"torso_1":66,"shoes_1":4,"lipstick_1":0,"pants_2":0,"decals_1":0}', '{}'),
	(160, 'mafia', 0, 'soldato', 'Ptite-Frappe', 0, '{}', '{}'),
	(161, 'mafia', 1, 'capo', 'Capo', 0, '{}', '{}'),
	(162, 'mafia', 2, 'consigliere', 'Consigliere', 0, '{}', '{}'),
	(163, 'mafia', 3, 'boss', 'Don', 0, '{}', '{}'),
	(164, 'police', 2, 'sergeant', 'รองผู้กำกับการ', 1200, '{"makeup_3":25,"sex":0,"lipstick_3":19,"lipstick_1":0,"eyebrows_3":1,"hair_color_1":1,"decals_2":2,"helmet_1":65,"age_2":7,"eyebrows_4":0,"pants_1":28,"decals_1":8,"makeup_1":0,"tshirt_1":58,"hair_color_2":0,"shoes":1,"mask_1":0,"ears_2":0,"tshirt_2":0,"beard_3":0,"glasses":0,"beard_1":7,"beard_2":7,"skin":0,"lipstick_4":0,"lipstick_2":0,"beard_4":0,"makeup_4":0,"bags_2":0,"eyebrows_1":10,"torso_2":1,"pants_2":0,"mask_2":0,"ears_1":2,"hair_1":12,"bproof_2":1,"helmet_2":0,"face":0,"shoes_1":51,"chain_1":125,"bproof_1":0,"hair_2":0,"makeup_2":0,"glasses_2":2,"glasses_1":15,"torso_1":95,"shoes_2":0,"eyebrows_2":10,"arms":37,"bags_1":0,"chain_2":0,"age_1":5}', '{"mask_2":0,"complexion_1":0,"makeup_4":0,"helmet_2":0,"makeup_2":0,"age_2":0,"chain_2":0,"moles_1":0,"bodyb_2":0,"decals_1":0,"lipstick_4":0,"bracelets_2":0,"hair_color_2":0,"arms":19,"hair_1":18,"bproof_2":0,"torso_2":0,"makeup_3":0,"chain_1":125,"pants_1":35,"glasses_1":0,"lipstick_1":0,"makeup_1":0,"eyebrows_4":0,"blush_1":0,"chest_1":0,"blemishes_2":0,"face":0,"beard_3":0,"helmet_1":46,"bodyb_1":0,"eyebrows_1":9,"tshirt_2":0,"bags_2":0,"eyebrows_2":10,"chest_2":0,"bproof_1":0,"sex":0,"shoes_1":21,"glasses_2":1,"watches_2":0,"torso_1":55,"blush_3":0,"sun_1":0,"blemishes_1":0,"watches_1":-1,"hair_color_1":0,"mask_1":0,"ears_1":2,"beard_2":10,"shoes_2":0,"skin":0,"lipstick_2":0,"hair_2":1,"tshirt_1":129,"sun_2":0,"age_1":0,"lipstick_3":0,"eye_color":0,"complexion_2":0,"beard_1":11,"beard_4":0,"chest_3":0,"decals_2":0,"moles_2":0,"blush_2":0,"ears_2":0,"arms_2":0,"pants_2":0,"eyebrows_3":0,"bags_1":0,"bracelets_1":-1}'),
	(165, 'police', 1, 'officer', 'ตำรวจ', 1000, '{"makeup_3":25,"sex":0,"lipstick_3":19,"lipstick_1":0,"eyebrows_3":1,"hair_color_1":1,"decals_2":2,"helmet_1":65,"age_2":7,"eyebrows_4":0,"pants_1":28,"decals_1":8,"makeup_1":0,"tshirt_1":58,"hair_color_2":0,"shoes":1,"mask_1":0,"ears_2":0,"tshirt_2":0,"beard_3":0,"glasses":0,"beard_1":7,"beard_2":7,"skin":0,"lipstick_4":0,"lipstick_2":0,"beard_4":0,"makeup_4":0,"bags_2":0,"eyebrows_1":10,"torso_2":1,"pants_2":0,"mask_2":0,"ears_1":2,"hair_1":12,"bproof_2":1,"helmet_2":0,"face":0,"shoes_1":51,"chain_1":125,"bproof_1":0,"hair_2":0,"makeup_2":0,"glasses_2":2,"glasses_1":15,"torso_1":95,"shoes_2":0,"eyebrows_2":10,"arms":37,"bags_1":0,"chain_2":0,"age_1":5}', '{"mask_2":0,"complexion_1":0,"makeup_4":0,"helmet_2":0,"makeup_2":0,"age_2":0,"chain_2":0,"moles_1":0,"bodyb_2":0,"decals_1":0,"lipstick_4":0,"bracelets_2":0,"hair_color_2":0,"arms":19,"hair_1":18,"bproof_2":0,"torso_2":0,"makeup_3":0,"chain_1":125,"pants_1":35,"glasses_1":0,"lipstick_1":0,"makeup_1":0,"eyebrows_4":0,"blush_1":0,"chest_1":0,"blemishes_2":0,"face":0,"beard_3":0,"helmet_1":46,"bodyb_1":0,"eyebrows_1":9,"tshirt_2":0,"bags_2":0,"eyebrows_2":10,"chest_2":0,"bproof_1":0,"sex":0,"shoes_1":21,"glasses_2":1,"watches_2":0,"torso_1":55,"blush_3":0,"sun_1":0,"blemishes_1":0,"watches_1":-1,"hair_color_1":0,"mask_1":0,"ears_1":2,"beard_2":10,"shoes_2":0,"skin":0,"lipstick_2":0,"hair_2":1,"tshirt_1":129,"sun_2":0,"age_1":0,"lipstick_3":0,"eye_color":0,"complexion_2":0,"beard_1":11,"beard_4":0,"chest_3":0,"decals_2":0,"moles_2":0,"blush_2":0,"ears_2":0,"arms_2":0,"pants_2":0,"eyebrows_3":0,"bags_1":0,"bracelets_1":-1}'),
	(174, 'cardealer', 0, 'recruit', 'recruit', 0, '{}', '{}'),
	(175, 'offpolice', 0, 'recruit', 'Recruit', 0, '{}', '{}'),
	(176, 'offpolice', 1, 'officer', 'Officer', 0, '{}', '{}'),
	(177, 'offpolice', 2, 'sergeant', 'Sergeant', 0, '{}', '{}'),
	(178, 'offpolice', 3, 'lieutenant', 'Lieutenant', 0, '{}', '{}'),
	(179, 'offpolice', 4, 'boss', 'Boss', 0, '{}', '{}'),
	(180, 'offambulance', 0, 'ambulance', 'Ambulance', 0, '{}', '{}'),
	(181, 'offambulance', 1, 'doctor', 'Doctor', 0, '{}', '{}'),
	(182, 'offambulance', 2, 'chief_doctor', 'Chef', 0, '{}', '{}'),
	(183, 'offambulance', 3, 'boss', 'Boss', 0, '{}', '{}');
/*!40000 ALTER TABLE `job_grades` ENABLE KEYS */;

-- Dumping structure for table essentialmode.jsfour_cardetails
CREATE TABLE IF NOT EXISTS `jsfour_cardetails` (
  `pk` int(11) NOT NULL AUTO_INCREMENT,
  `plate` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `owner` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `incident` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '{}',
  `inspected` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `identifier` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`pk`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.jsfour_cardetails: ~0 rows (approximately)
/*!40000 ALTER TABLE `jsfour_cardetails` DISABLE KEYS */;
/*!40000 ALTER TABLE `jsfour_cardetails` ENABLE KEYS */;

-- Dumping structure for table essentialmode.jsfour_criminalrecord
CREATE TABLE IF NOT EXISTS `jsfour_criminalrecord` (
  `offense` varchar(160) COLLATE utf8mb4_bin NOT NULL,
  `date` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `institution` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `charge` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `term` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `classified` int(2) NOT NULL DEFAULT 0,
  `identifier` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `dob` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `warden` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`offense`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.jsfour_criminalrecord: ~0 rows (approximately)
/*!40000 ALTER TABLE `jsfour_criminalrecord` DISABLE KEYS */;
/*!40000 ALTER TABLE `jsfour_criminalrecord` ENABLE KEYS */;

-- Dumping structure for table essentialmode.jsfour_criminaluserinfo
CREATE TABLE IF NOT EXISTS `jsfour_criminaluserinfo` (
  `identifier` varchar(160) COLLATE utf8mb4_bin NOT NULL,
  `aliases` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `recordid` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `weight` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `eyecolor` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `haircolor` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `dob` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `sex` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `height` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.jsfour_criminaluserinfo: ~0 rows (approximately)
/*!40000 ALTER TABLE `jsfour_criminaluserinfo` DISABLE KEYS */;
/*!40000 ALTER TABLE `jsfour_criminaluserinfo` ENABLE KEYS */;

-- Dumping structure for table essentialmode.jsfour_dna
CREATE TABLE IF NOT EXISTS `jsfour_dna` (
  `pk` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `killer` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `dead` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `weapon` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_bin DEFAULT 'murder',
  `lastdigits` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `uploader` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `datum` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.jsfour_dna: ~0 rows (approximately)
/*!40000 ALTER TABLE `jsfour_dna` DISABLE KEYS */;
/*!40000 ALTER TABLE `jsfour_dna` ENABLE KEYS */;

-- Dumping structure for table essentialmode.jsfour_efterlysningar
CREATE TABLE IF NOT EXISTS `jsfour_efterlysningar` (
  `pk` int(11) NOT NULL AUTO_INCREMENT,
  `wanted` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `dob` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `crime` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `uploader` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `date` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `incident` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`pk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.jsfour_efterlysningar: ~0 rows (approximately)
/*!40000 ALTER TABLE `jsfour_efterlysningar` DISABLE KEYS */;
/*!40000 ALTER TABLE `jsfour_efterlysningar` ENABLE KEYS */;

-- Dumping structure for table essentialmode.jsfour_incidents
CREATE TABLE IF NOT EXISTS `jsfour_incidents` (
  `pk` int(11) NOT NULL AUTO_INCREMENT,
  `number` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `text` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `uploader` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `date` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`pk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.jsfour_incidents: ~0 rows (approximately)
/*!40000 ALTER TABLE `jsfour_incidents` DISABLE KEYS */;
/*!40000 ALTER TABLE `jsfour_incidents` ENABLE KEYS */;

-- Dumping structure for table essentialmode.jsfour_logs
CREATE TABLE IF NOT EXISTS `jsfour_logs` (
  `pk` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `remover` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `wanted` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`pk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.jsfour_logs: ~0 rows (approximately)
/*!40000 ALTER TABLE `jsfour_logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `jsfour_logs` ENABLE KEYS */;

-- Dumping structure for table essentialmode.licenses
CREATE TABLE IF NOT EXISTS `licenses` (
  `type` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.licenses: ~7 rows (approximately)
/*!40000 ALTER TABLE `licenses` DISABLE KEYS */;
INSERT INTO `licenses` (`type`, `label`) VALUES
	('boating', 'Boating License'),
	('dmv', 'ใบขับขี่'),
	('drive', 'ใบอณุญาติขับขี่รถยนต์'),
	('drive_bike', 'ใบอณุญาติขับขี่มอเตอร์ไซค์'),
	('drive_truck', 'ใบอณุญาติขับรถบรรทุก'),
	('weapon', 'ใบอณุญาติพกอาวุธ'),
	('weed_processing', 'ใบอนุญาติซื้อกัญชา');
/*!40000 ALTER TABLE `licenses` ENABLE KEYS */;

-- Dumping structure for table essentialmode.outfits
CREATE TABLE IF NOT EXISTS `outfits` (
  `idSteam` varchar(255) NOT NULL,
  `dad` int(11) NOT NULL DEFAULT 0,
  `mum` int(11) NOT NULL DEFAULT 0,
  `dadmumpercent` int(11) NOT NULL DEFAULT 0,
  `skinton` int(11) NOT NULL DEFAULT 0,
  `eyecolor` int(11) NOT NULL DEFAULT 0,
  `acne` int(11) NOT NULL DEFAULT 0,
  `skinproblem` int(11) NOT NULL DEFAULT 0,
  `freckle` int(11) NOT NULL DEFAULT 0,
  `wrinkle` int(11) NOT NULL DEFAULT 0,
  `wrinkleopacity` int(11) NOT NULL DEFAULT 0,
  `eyebrow` int(11) NOT NULL DEFAULT 0,
  `eyebrowopacity` int(11) NOT NULL DEFAULT 0,
  `beard` int(11) NOT NULL DEFAULT 0,
  `beardopacity` int(11) NOT NULL DEFAULT 0,
  `beardcolor` int(11) NOT NULL DEFAULT 0,
  `hair` int(11) NOT NULL DEFAULT 0,
  `hairtext` int(11) NOT NULL DEFAULT 0,
  `torso` int(11) NOT NULL DEFAULT 0,
  `torsotext` int(11) NOT NULL DEFAULT 0,
  `leg` int(11) NOT NULL DEFAULT 0,
  `legtext` int(11) NOT NULL DEFAULT 0,
  `shoes` int(11) NOT NULL DEFAULT 0,
  `shoestext` int(11) NOT NULL DEFAULT 0,
  `accessory` int(11) NOT NULL DEFAULT 0,
  `accessorytext` int(11) NOT NULL DEFAULT 0,
  `undershirt` int(11) NOT NULL DEFAULT 0,
  `undershirttext` int(11) NOT NULL DEFAULT 0,
  `torso2` int(11) NOT NULL DEFAULT 0,
  `torso2text` int(11) NOT NULL DEFAULT 0,
  `prop_hat` int(11) NOT NULL DEFAULT 0,
  `prop_hat_text` int(11) NOT NULL DEFAULT 0,
  `prop_glasses` int(11) NOT NULL DEFAULT 0,
  `prop_glasses_text` int(11) NOT NULL DEFAULT 0,
  `prop_earrings` int(11) NOT NULL DEFAULT 0,
  `prop_earrings_text` int(11) NOT NULL DEFAULT 0,
  `prop_watches` int(11) NOT NULL DEFAULT 0,
  `prop_watches_text` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.outfits: ~0 rows (approximately)
/*!40000 ALTER TABLE `outfits` DISABLE KEYS */;
/*!40000 ALTER TABLE `outfits` ENABLE KEYS */;

-- Dumping structure for table essentialmode.owned_properties
CREATE TABLE IF NOT EXISTS `owned_properties` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `price` double NOT NULL,
  `rented` int(11) NOT NULL,
  `owner` varchar(60) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.owned_properties: ~25 rows (approximately)
/*!40000 ALTER TABLE `owned_properties` DISABLE KEYS */;
/*!40000 ALTER TABLE `owned_properties` ENABLE KEYS */;

-- Dumping structure for table essentialmode.owned_vehicles
CREATE TABLE IF NOT EXISTS `owned_vehicles` (
  `owner` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `plate` varchar(20) COLLATE utf8mb4_bin NOT NULL,
  `vehicle` longtext COLLATE utf8mb4_bin NOT NULL,
  `type` varchar(20) COLLATE utf8mb4_bin NOT NULL DEFAULT 'car',
  `job` varchar(20) COLLATE utf8mb4_bin NOT NULL,
  `stored` tinyint(1) NOT NULL DEFAULT 0,
  `state` tinyint(1) NOT NULL DEFAULT 0,
  `fourrieremecano` tinyint(1) NOT NULL DEFAULT 0,
  `vehiclename` varchar(50) COLLATE utf8mb4_bin NOT NULL DEFAULT 'voiture',
  PRIMARY KEY (`plate`),
  KEY `owner` (`owner`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.owned_vehicles: ~0 rows (approximately)
/*!40000 ALTER TABLE `owned_vehicles` DISABLE KEYS */;
/*!40000 ALTER TABLE `owned_vehicles` ENABLE KEYS */;

-- Dumping structure for table essentialmode.phone_app_chat
CREATE TABLE IF NOT EXISTS `phone_app_chat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `channel` varchar(20) NOT NULL,
  `message` varchar(255) NOT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

-- Dumping data for table essentialmode.phone_app_chat: ~0 rows (approximately)
/*!40000 ALTER TABLE `phone_app_chat` DISABLE KEYS */;
/*!40000 ALTER TABLE `phone_app_chat` ENABLE KEYS */;

-- Dumping structure for table essentialmode.phone_calls
CREATE TABLE IF NOT EXISTS `phone_calls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner` varchar(10) NOT NULL COMMENT 'Num tel proprio',
  `num` varchar(10) NOT NULL COMMENT 'Num reférence du contact',
  `incoming` int(11) NOT NULL COMMENT 'Défini si on est à l''origine de l''appels',
  `time` timestamp NOT NULL DEFAULT current_timestamp(),
  `accepts` int(11) NOT NULL COMMENT 'Appels accepter ou pas',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=391 DEFAULT CHARSET=utf8;

-- Dumping data for table essentialmode.phone_calls: ~0 rows (approximately)
/*!40000 ALTER TABLE `phone_calls` DISABLE KEYS */;
/*!40000 ALTER TABLE `phone_calls` ENABLE KEYS */;

-- Dumping structure for table essentialmode.phone_messages
CREATE TABLE IF NOT EXISTS `phone_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transmitter` varchar(10) NOT NULL,
  `receiver` varchar(10) NOT NULL,
  `message` varchar(255) NOT NULL DEFAULT '0',
  `time` timestamp NOT NULL DEFAULT current_timestamp(),
  `isRead` int(11) NOT NULL DEFAULT 0,
  `owner` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1241 DEFAULT CHARSET=utf8;

-- Dumping data for table essentialmode.phone_messages: 0 rows
/*!40000 ALTER TABLE `phone_messages` DISABLE KEYS */;
/*!40000 ALTER TABLE `phone_messages` ENABLE KEYS */;

-- Dumping structure for table essentialmode.phone_users_contacts
CREATE TABLE IF NOT EXISTS `phone_users_contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(60) CHARACTER SET utf8mb4 DEFAULT NULL,
  `number` varchar(10) CHARACTER SET utf8mb4 DEFAULT NULL,
  `display` varchar(64) CHARACTER SET utf8mb4 NOT NULL DEFAULT '-1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=85 DEFAULT CHARSET=utf8;

-- Dumping data for table essentialmode.phone_users_contacts: 0 rows
/*!40000 ALTER TABLE `phone_users_contacts` DISABLE KEYS */;
/*!40000 ALTER TABLE `phone_users_contacts` ENABLE KEYS */;

-- Dumping structure for table essentialmode.playerstattoos
CREATE TABLE IF NOT EXISTS `playerstattoos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(255) NOT NULL,
  `tattoos` longtext NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.playerstattoos: ~0 rows (approximately)
/*!40000 ALTER TABLE `playerstattoos` DISABLE KEYS */;
/*!40000 ALTER TABLE `playerstattoos` ENABLE KEYS */;

-- Dumping structure for table essentialmode.properties
CREATE TABLE IF NOT EXISTS `properties` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  `entering` varchar(255) DEFAULT NULL,
  `exit` varchar(255) DEFAULT NULL,
  `inside` varchar(255) DEFAULT NULL,
  `outside` varchar(255) DEFAULT NULL,
  `ipls` varchar(255) DEFAULT '[]',
  `gateway` varchar(255) DEFAULT NULL,
  `is_single` int(11) DEFAULT NULL,
  `is_room` int(11) DEFAULT NULL,
  `is_gateway` int(11) DEFAULT NULL,
  `room_menu` varchar(255) DEFAULT NULL,
  `price` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.properties: ~72 rows (approximately)
/*!40000 ALTER TABLE `properties` DISABLE KEYS */;
INSERT INTO `properties` (`id`, `name`, `label`, `entering`, `exit`, `inside`, `outside`, `ipls`, `gateway`, `is_single`, `is_room`, `is_gateway`, `room_menu`, `price`) VALUES
	(1, 'WhispymoundDrive', '2677 Whispymound Drive', '{"y":564.89,"z":182.959,"x":119.384}', '{"x":117.347,"y":559.506,"z":183.304}', '{"y":557.032,"z":183.301,"x":118.037}', '{"y":567.798,"z":182.131,"x":119.249}', '[]', NULL, 1, 1, 0, '{"x":118.748,"y":566.573,"z":175.697}', 1500000),
	(2, 'NorthConkerAvenue2045', '2045 North Conker Avenue', '{"x":372.796,"y":428.327,"z":144.685}', '{"x":373.548,"y":422.982,"z":144.907},', '{"y":420.075,"z":145.904,"x":372.161}', '{"x":372.454,"y":432.886,"z":143.443}', '[]', NULL, 1, 1, 0, '{"x":377.349,"y":429.422,"z":137.3}', 1500000),
	(3, 'RichardMajesticApt2', 'Richard Majestic, Apt 2', '{"y":-379.165,"z":37.961,"x":-936.363}', '{"y":-365.476,"z":113.274,"x":-913.097}', '{"y":-367.637,"z":113.274,"x":-918.022}', '{"y":-382.023,"z":37.961,"x":-943.626}', '[]', NULL, 1, 1, 0, '{"x":-927.554,"y":-377.744,"z":112.674}', 1700000),
	(4, 'NorthConkerAvenue2044', '2044 North Conker Avenue', '{"y":440.8,"z":146.702,"x":346.964}', '{"y":437.456,"z":148.394,"x":341.683}', '{"y":435.626,"z":148.394,"x":339.595}', '{"x":350.535,"y":443.329,"z":145.764}', '[]', NULL, 1, 1, 0, '{"x":337.726,"y":436.985,"z":140.77}', 1500000),
	(5, 'WildOatsDrive', '3655 Wild Oats Drive', '{"y":502.696,"z":136.421,"x":-176.003}', '{"y":497.817,"z":136.653,"x":-174.349}', '{"y":495.069,"z":136.666,"x":-173.331}', '{"y":506.412,"z":135.0664,"x":-177.927}', '[]', NULL, 1, 1, 0, '{"x":-174.725,"y":493.095,"z":129.043}', 1500000),
	(6, 'HillcrestAvenue2862', '2862 Hillcrest Avenue', '{"y":596.58,"z":142.641,"x":-686.554}', '{"y":591.988,"z":144.392,"x":-681.728}', '{"y":590.608,"z":144.392,"x":-680.124}', '{"y":599.019,"z":142.059,"x":-689.492}', '[]', NULL, 1, 1, 0, '{"x":-680.46,"y":588.6,"z":136.769}', 1500000),
	(7, 'LowEndApartment', 'Appartement de base', '{"y":-1078.735,"z":28.4031,"x":292.528}', '{"y":-1007.152,"z":-102.002,"x":265.845}', '{"y":-1002.802,"z":-100.008,"x":265.307}', '{"y":-1078.669,"z":28.401,"x":296.738}', '[]', NULL, 1, 1, 0, '{"x":265.916,"y":-999.38,"z":-100.008}', 562500),
	(8, 'MadWayneThunder', '2113 Mad Wayne Thunder', '{"y":454.955,"z":96.462,"x":-1294.433}', '{"x":-1289.917,"y":449.541,"z":96.902}', '{"y":446.322,"z":96.899,"x":-1289.642}', '{"y":455.453,"z":96.517,"x":-1298.851}', '[]', NULL, 1, 1, 0, '{"x":-1287.306,"y":455.901,"z":89.294}', 1500000),
	(9, 'HillcrestAvenue2874', '2874 Hillcrest Avenue', '{"x":-853.346,"y":696.678,"z":147.782}', '{"y":690.875,"z":151.86,"x":-859.961}', '{"y":688.361,"z":151.857,"x":-859.395}', '{"y":701.628,"z":147.773,"x":-855.007}', '[]', NULL, 1, 1, 0, '{"x":-858.543,"y":697.514,"z":144.253}', 1500000),
	(10, 'HillcrestAvenue2868', '2868 Hillcrest Avenue', '{"y":620.494,"z":141.588,"x":-752.82}', '{"y":618.62,"z":143.153,"x":-759.317}', '{"y":617.629,"z":143.153,"x":-760.789}', '{"y":621.281,"z":141.254,"x":-750.919}', '[]', NULL, 1, 1, 0, '{"x":-762.504,"y":618.992,"z":135.53}', 1500000),
	(11, 'TinselTowersApt12', 'Tinsel Towers, Apt 42', '{"y":37.025,"z":42.58,"x":-618.299}', '{"y":58.898,"z":97.2,"x":-603.301}', '{"y":58.941,"z":97.2,"x":-608.741}', '{"y":30.603,"z":42.524,"x":-620.017}', '[]', NULL, 1, 1, 0, '{"x":-622.173,"y":54.585,"z":96.599}', 1700000),
	(12, 'MiltonDrive', 'Milton Drive', '{"x":-775.17,"y":312.01,"z":84.658}', NULL, NULL, '{"x":-775.346,"y":306.776,"z":84.7}', '[]', NULL, 0, 0, 1, NULL, 0),
	(13, 'Modern1Apartment', 'Appartement Moderne 1', NULL, '{"x":-784.194,"y":323.636,"z":210.997}', '{"x":-779.751,"y":323.385,"z":210.997}', NULL, '["apa_v_mp_h_01_a"]', 'MiltonDrive', 0, 1, 0, '{"x":-766.661,"y":327.672,"z":210.396}', 1300000),
	(14, 'Modern2Apartment', 'Appartement Moderne 2', NULL, '{"x":-786.8663,"y":315.764,"z":186.913}', '{"x":-781.808,"y":315.866,"z":186.913}', NULL, '["apa_v_mp_h_01_c"]', 'MiltonDrive', 0, 1, 0, '{"x":-795.735,"y":326.757,"z":186.313}', 1300000),
	(15, 'Modern3Apartment', 'Appartement Moderne 3', NULL, '{"x":-774.012,"y":342.042,"z":195.686}', '{"x":-779.057,"y":342.063,"z":195.686}', NULL, '["apa_v_mp_h_01_b"]', 'MiltonDrive', 0, 1, 0, '{"x":-765.386,"y":330.782,"z":195.08}', 1300000),
	(16, 'Mody1Apartment', 'Appartement Mode 1', NULL, '{"x":-784.194,"y":323.636,"z":210.997}', '{"x":-779.751,"y":323.385,"z":210.997}', NULL, '["apa_v_mp_h_02_a"]', 'MiltonDrive', 0, 1, 0, '{"x":-766.615,"y":327.878,"z":210.396}', 1300000),
	(17, 'Mody2Apartment', 'Appartement Mode 2', NULL, '{"x":-786.8663,"y":315.764,"z":186.913}', '{"x":-781.808,"y":315.866,"z":186.913}', NULL, '["apa_v_mp_h_02_c"]', 'MiltonDrive', 0, 1, 0, '{"x":-795.297,"y":327.092,"z":186.313}', 1300000),
	(18, 'Mody3Apartment', 'Appartement Mode 3', NULL, '{"x":-774.012,"y":342.042,"z":195.686}', '{"x":-779.057,"y":342.063,"z":195.686}', NULL, '["apa_v_mp_h_02_b"]', 'MiltonDrive', 0, 1, 0, '{"x":-765.303,"y":330.932,"z":195.085}', 1300000),
	(19, 'Vibrant1Apartment', 'Appartement Vibrant 1', NULL, '{"x":-784.194,"y":323.636,"z":210.997}', '{"x":-779.751,"y":323.385,"z":210.997}', NULL, '["apa_v_mp_h_03_a"]', 'MiltonDrive', 0, 1, 0, '{"x":-765.885,"y":327.641,"z":210.396}', 1300000),
	(20, 'Vibrant2Apartment', 'Appartement Vibrant 2', NULL, '{"x":-786.8663,"y":315.764,"z":186.913}', '{"x":-781.808,"y":315.866,"z":186.913}', NULL, '["apa_v_mp_h_03_c"]', 'MiltonDrive', 0, 1, 0, '{"x":-795.607,"y":327.344,"z":186.313}', 1300000),
	(21, 'Vibrant3Apartment', 'Appartement Vibrant 3', NULL, '{"x":-774.012,"y":342.042,"z":195.686}', '{"x":-779.057,"y":342.063,"z":195.686}', NULL, '["apa_v_mp_h_03_b"]', 'MiltonDrive', 0, 1, 0, '{"x":-765.525,"y":330.851,"z":195.085}', 1300000),
	(22, 'Sharp1Apartment', 'Appartement Persan 1', NULL, '{"x":-784.194,"y":323.636,"z":210.997}', '{"x":-779.751,"y":323.385,"z":210.997}', NULL, '["apa_v_mp_h_04_a"]', 'MiltonDrive', 0, 1, 0, '{"x":-766.527,"y":327.89,"z":210.396}', 1300000),
	(23, 'Sharp2Apartment', 'Appartement Persan 2', NULL, '{"x":-786.8663,"y":315.764,"z":186.913}', '{"x":-781.808,"y":315.866,"z":186.913}', NULL, '["apa_v_mp_h_04_c"]', 'MiltonDrive', 0, 1, 0, '{"x":-795.642,"y":326.497,"z":186.313}', 1300000),
	(24, 'Sharp3Apartment', 'Appartement Persan 3', NULL, '{"x":-774.012,"y":342.042,"z":195.686}', '{"x":-779.057,"y":342.063,"z":195.686}', NULL, '["apa_v_mp_h_04_b"]', 'MiltonDrive', 0, 1, 0, '{"x":-765.503,"y":331.318,"z":195.085}', 1300000),
	(25, 'Monochrome1Apartment', 'Appartement Monochrome 1', NULL, '{"x":-784.194,"y":323.636,"z":210.997}', '{"x":-779.751,"y":323.385,"z":210.997}', NULL, '["apa_v_mp_h_05_a"]', 'MiltonDrive', 0, 1, 0, '{"x":-766.289,"y":328.086,"z":210.396}', 1300000),
	(26, 'Monochrome2Apartment', 'Appartement Monochrome 2', NULL, '{"x":-786.8663,"y":315.764,"z":186.913}', '{"x":-781.808,"y":315.866,"z":186.913}', NULL, '["apa_v_mp_h_05_c"]', 'MiltonDrive', 0, 1, 0, '{"x":-795.692,"y":326.762,"z":186.313}', 1300000),
	(27, 'Monochrome3Apartment', 'Appartement Monochrome 3', NULL, '{"x":-774.012,"y":342.042,"z":195.686}', '{"x":-779.057,"y":342.063,"z":195.686}', NULL, '["apa_v_mp_h_05_b"]', 'MiltonDrive', 0, 1, 0, '{"x":-765.094,"y":330.976,"z":195.085}', 1300000),
	(28, 'Seductive1Apartment', 'Appartement Séduisant 1', NULL, '{"x":-784.194,"y":323.636,"z":210.997}', '{"x":-779.751,"y":323.385,"z":210.997}', NULL, '["apa_v_mp_h_06_a"]', 'MiltonDrive', 0, 1, 0, '{"x":-766.263,"y":328.104,"z":210.396}', 1300000),
	(29, 'Seductive2Apartment', 'Appartement Séduisant 2', NULL, '{"x":-786.8663,"y":315.764,"z":186.913}', '{"x":-781.808,"y":315.866,"z":186.913}', NULL, '["apa_v_mp_h_06_c"]', 'MiltonDrive', 0, 1, 0, '{"x":-795.655,"y":326.611,"z":186.313}', 1300000),
	(30, 'Seductive3Apartment', 'Appartement Séduisant 3', NULL, '{"x":-774.012,"y":342.042,"z":195.686}', '{"x":-779.057,"y":342.063,"z":195.686}', NULL, '["apa_v_mp_h_06_b"]', 'MiltonDrive', 0, 1, 0, '{"x":-765.3,"y":331.414,"z":195.085}', 1300000),
	(31, 'Regal1Apartment', 'Appartement Régal 1', NULL, '{"x":-784.194,"y":323.636,"z":210.997}', '{"x":-779.751,"y":323.385,"z":210.997}', NULL, '["apa_v_mp_h_07_a"]', 'MiltonDrive', 0, 1, 0, '{"x":-765.956,"y":328.257,"z":210.396}', 1300000),
	(32, 'Regal2Apartment', 'Appartement Régal 2', NULL, '{"x":-786.8663,"y":315.764,"z":186.913}', '{"x":-781.808,"y":315.866,"z":186.913}', NULL, '["apa_v_mp_h_07_c"]', 'MiltonDrive', 0, 1, 0, '{"x":-795.545,"y":326.659,"z":186.313}', 1300000),
	(33, 'Regal3Apartment', 'Appartement Régal 3', NULL, '{"x":-774.012,"y":342.042,"z":195.686}', '{"x":-779.057,"y":342.063,"z":195.686}', NULL, '["apa_v_mp_h_07_b"]', 'MiltonDrive', 0, 1, 0, '{"x":-765.087,"y":331.429,"z":195.123}', 1300000),
	(34, 'Aqua1Apartment', 'Appartement Aqua 1', NULL, '{"x":-784.194,"y":323.636,"z":210.997}', '{"x":-779.751,"y":323.385,"z":210.997}', NULL, '["apa_v_mp_h_08_a"]', 'MiltonDrive', 0, 1, 0, '{"x":-766.187,"y":328.47,"z":210.396}', 1300000),
	(35, 'Aqua2Apartment', 'Appartement Aqua 2', NULL, '{"x":-786.8663,"y":315.764,"z":186.913}', '{"x":-781.808,"y":315.866,"z":186.913}', NULL, '["apa_v_mp_h_08_c"]', 'MiltonDrive', 0, 1, 0, '{"x":-795.658,"y":326.563,"z":186.313}', 1300000),
	(36, 'Aqua3Apartment', 'Appartement Aqua 3', NULL, '{"x":-774.012,"y":342.042,"z":195.686}', '{"x":-779.057,"y":342.063,"z":195.686}', NULL, '["apa_v_mp_h_08_b"]', 'MiltonDrive', 0, 1, 0, '{"x":-765.287,"y":331.084,"z":195.086}', 1300000),
	(37, 'IntegrityWay', '4 Integrity Way', '{"x":-47.804,"y":-585.867,"z":36.956}', NULL, NULL, '{"x":-54.178,"y":-583.762,"z":35.798}', '[]', NULL, 0, 0, 1, NULL, 0),
	(38, 'IntegrityWay28', '4 Integrity Way - Apt 28', NULL, '{"x":-31.409,"y":-594.927,"z":79.03}', '{"x":-26.098,"y":-596.909,"z":79.03}', NULL, '[]', 'IntegrityWay', 0, 1, 0, '{"x":-11.923,"y":-597.083,"z":78.43}', 1700000),
	(39, 'IntegrityWay30', '4 Integrity Way - Apt 30', NULL, '{"x":-17.702,"y":-588.524,"z":89.114}', '{"x":-16.21,"y":-582.569,"z":89.114}', NULL, '[]', 'IntegrityWay', 0, 1, 0, '{"x":-26.327,"y":-588.384,"z":89.123}', 1700000),
	(40, 'DellPerroHeights', 'Dell Perro Heights', '{"x":-1447.06,"y":-538.28,"z":33.74}', NULL, NULL, '{"x":-1440.022,"y":-548.696,"z":33.74}', '[]', NULL, 0, 0, 1, NULL, 0),
	(41, 'DellPerroHeightst4', 'Dell Perro Heights - Apt 28', NULL, '{"x":-1452.125,"y":-540.591,"z":73.044}', '{"x":-1455.435,"y":-535.79,"z":73.044}', NULL, '[]', 'DellPerroHeights', 0, 1, 0, '{"x":-1467.058,"y":-527.571,"z":72.443}', 1700000),
	(42, 'DellPerroHeightst7', 'Dell Perro Heights - Apt 30', NULL, '{"x":-1451.562,"y":-523.535,"z":55.928}', '{"x":-1456.02,"y":-519.209,"z":55.929}', NULL, '[]', 'DellPerroHeights', 0, 1, 0, '{"x":-1457.026,"y":-530.219,"z":55.937}', 1700000),
	(43, 'MazeBankBuilding', 'Maze Bank Building', '{"x":-79.18,"y":-795.92,"z":43.35}', NULL, NULL, '{"x":-72.50,"y":-786.92,"z":43.40}', '[]', NULL, 0, 0, 1, NULL, 0),
	(44, 'OldSpiceWarm', 'Old Spice Warm', NULL, '{"x":-75.69,"y":-827.08,"z":242.43}', '{"x":-75.51,"y":-823.90,"z":242.43}', NULL, '["ex_dt1_11_office_01a"]', 'MazeBankBuilding', 0, 1, 0, '{"x":-71.81,"y":-814.34,"z":242.39}', 5000000),
	(45, 'OldSpiceClassical', 'Old Spice Classical', NULL, '{"x":-75.69,"y":-827.08,"z":242.43}', '{"x":-75.51,"y":-823.90,"z":242.43}', NULL, '["ex_dt1_11_office_01b"]', 'MazeBankBuilding', 0, 1, 0, '{"x":-71.81,"y":-814.34,"z":242.39}', 5000000),
	(46, 'OldSpiceVintage', 'Old Spice Vintage', NULL, '{"x":-75.69,"y":-827.08,"z":242.43}', '{"x":-75.51,"y":-823.90,"z":242.43}', NULL, '["ex_dt1_11_office_01c"]', 'MazeBankBuilding', 0, 1, 0, '{"x":-71.81,"y":-814.34,"z":242.39}', 5000000),
	(47, 'ExecutiveRich', 'Executive Rich', NULL, '{"x":-75.69,"y":-827.08,"z":242.43}', '{"x":-75.51,"y":-823.90,"z":242.43}', NULL, '["ex_dt1_11_office_02b"]', 'MazeBankBuilding', 0, 1, 0, '{"x":-71.81,"y":-814.34,"z":242.39}', 5000000),
	(48, 'ExecutiveCool', 'Executive Cool', NULL, '{"x":-75.69,"y":-827.08,"z":242.43}', '{"x":-75.51,"y":-823.90,"z":242.43}', NULL, '["ex_dt1_11_office_02c"]', 'MazeBankBuilding', 0, 1, 0, '{"x":-71.81,"y":-814.34,"z":242.39}', 5000000),
	(49, 'ExecutiveContrast', 'Executive Contrast', NULL, '{"x":-75.69,"y":-827.08,"z":242.43}', '{"x":-75.51,"y":-823.90,"z":242.43}', NULL, '["ex_dt1_11_office_02a"]', 'MazeBankBuilding', 0, 1, 0, '{"x":-71.81,"y":-814.34,"z":242.39}', 5000000),
	(50, 'PowerBrokerIce', 'Power Broker Ice', NULL, '{"x":-75.69,"y":-827.08,"z":242.43}', '{"x":-75.51,"y":-823.90,"z":242.43}', NULL, '["ex_dt1_11_office_03a"]', 'MazeBankBuilding', 0, 1, 0, '{"x":-71.81,"y":-814.34,"z":242.39}', 5000000),
	(51, 'PowerBrokerConservative', 'Power Broker Conservative', NULL, '{"x":-75.69,"y":-827.08,"z":242.43}', '{"x":-75.51,"y":-823.90,"z":242.43}', NULL, '["ex_dt1_11_office_03b"]', 'MazeBankBuilding', 0, 1, 0, '{"x":-71.81,"y":-814.34,"z":242.39}', 5000000),
	(52, 'PowerBrokerPolished', 'Power Broker Polished', NULL, '{"x":-75.69,"y":-827.08,"z":242.43}', '{"x":-75.51,"y":-823.90,"z":242.43}', NULL, '["ex_dt1_11_office_03c"]', 'MazeBankBuilding', 0, 1, 0, '{"x":-71.81,"y":-814.34,"z":242.39}', 5000000),
	(53, 'LomBank', 'Lom Bank', '{"x":-1581.36,"y":-558.23,"z":34.07}', NULL, NULL, '{"x":-1583.60,"y":-555.12,"z":34.07}', '[]', NULL, 0, 0, 1, NULL, 0),
	(54, 'LBOldSpiceWarm', 'LB Old Spice Warm', NULL, '{"x":-1579.53,"y":-564.89,"z":107.62}', '{"x":-1576.42,"y":-567.57,"z":107.62}', NULL, '["ex_sm_13_office_01a"]', 'LomBank', 0, 1, 0, '{"x":-1571.26,"y":-575.76,"z":107.52}', 3500000),
	(55, 'LBOldSpiceClassical', 'LB Old Spice Classical', NULL, '{"x":-1579.53,"y":-564.89,"z":107.62}', '{"x":-1576.42,"y":-567.57,"z":107.62}', NULL, '["ex_sm_13_office_01b"]', 'LomBank', 0, 1, 0, '{"x":-1571.26,"y":-575.76,"z":107.52}', 3500000),
	(56, 'LBOldSpiceVintage', 'LB Old Spice Vintage', NULL, '{"x":-1579.53,"y":-564.89,"z":107.62}', '{"x":-1576.42,"y":-567.57,"z":107.62}', NULL, '["ex_sm_13_office_01c"]', 'LomBank', 0, 1, 0, '{"x":-1571.26,"y":-575.76,"z":107.52}', 3500000),
	(57, 'LBExecutiveRich', 'LB Executive Rich', NULL, '{"x":-1579.53,"y":-564.89,"z":107.62}', '{"x":-1576.42,"y":-567.57,"z":107.62}', NULL, '["ex_sm_13_office_02b"]', 'LomBank', 0, 1, 0, '{"x":-1571.26,"y":-575.76,"z":107.52}', 3500000),
	(58, 'LBExecutiveCool', 'LB Executive Cool', NULL, '{"x":-1579.53,"y":-564.89,"z":107.62}', '{"x":-1576.42,"y":-567.57,"z":107.62}', NULL, '["ex_sm_13_office_02c"]', 'LomBank', 0, 1, 0, '{"x":-1571.26,"y":-575.76,"z":107.52}', 3500000),
	(59, 'LBExecutiveContrast', 'LB Executive Contrast', NULL, '{"x":-1579.53,"y":-564.89,"z":107.62}', '{"x":-1576.42,"y":-567.57,"z":107.62}', NULL, '["ex_sm_13_office_02a"]', 'LomBank', 0, 1, 0, '{"x":-1571.26,"y":-575.76,"z":107.52}', 3500000),
	(60, 'LBPowerBrokerIce', 'LB Power Broker Ice', NULL, '{"x":-1579.53,"y":-564.89,"z":107.62}', '{"x":-1576.42,"y":-567.57,"z":107.62}', NULL, '["ex_sm_13_office_03a"]', 'LomBank', 0, 1, 0, '{"x":-1571.26,"y":-575.76,"z":107.52}', 3500000),
	(61, 'LBPowerBrokerConservative', 'LB Power Broker Conservative', NULL, '{"x":-1579.53,"y":-564.89,"z":107.62}', '{"x":-1576.42,"y":-567.57,"z":107.62}', NULL, '["ex_sm_13_office_03b"]', 'LomBank', 0, 1, 0, '{"x":-1571.26,"y":-575.76,"z":107.52}', 3500000),
	(62, 'LBPowerBrokerPolished', 'LB Power Broker Polished', NULL, '{"x":-1579.53,"y":-564.89,"z":107.62}', '{"x":-1576.42,"y":-567.57,"z":107.62}', NULL, '["ex_sm_13_office_03c"]', 'LomBank', 0, 1, 0, '{"x":-1571.26,"y":-575.76,"z":107.52}', 3500000),
	(63, 'MazeBankWest', 'Maze Bank West', '{"x":-1379.58,"y":-499.63,"z":32.22}', NULL, NULL, '{"x":-1378.95,"y":-502.82,"z":32.22}', '[]', NULL, 0, 0, 1, NULL, 0),
	(64, 'MBWOldSpiceWarm', 'MBW Old Spice Warm', NULL, '{"x":-1392.74,"y":-480.18,"z":71.14}', '{"x":-1389.43,"y":-479.01,"z":71.14}', NULL, '["ex_sm_15_office_01a"]', 'MazeBankWest', 0, 1, 0, '{"x":-1390.76,"y":-479.22,"z":72.04}', 2700000),
	(65, 'MBWOldSpiceClassical', 'MBW Old Spice Classical', NULL, '{"x":-1392.74,"y":-480.18,"z":71.14}', '{"x":-1389.43,"y":-479.01,"z":71.14}', NULL, '["ex_sm_15_office_01b"]', 'MazeBankWest', 0, 1, 0, '{"x":-1390.76,"y":-479.22,"z":72.04}', 2700000),
	(66, 'MBWOldSpiceVintage', 'MBW Old Spice Vintage', NULL, '{"x":-1392.74,"y":-480.18,"z":71.14}', '{"x":-1389.43,"y":-479.01,"z":71.14}', NULL, '["ex_sm_15_office_01c"]', 'MazeBankWest', 0, 1, 0, '{"x":-1390.76,"y":-479.22,"z":72.04}', 2700000),
	(67, 'MBWExecutiveRich', 'MBW Executive Rich', NULL, '{"x":-1392.74,"y":-480.18,"z":71.14}', '{"x":-1389.43,"y":-479.01,"z":71.14}', NULL, '["ex_sm_15_office_02b"]', 'MazeBankWest', 0, 1, 0, '{"x":-1390.76,"y":-479.22,"z":72.04}', 2700000),
	(68, 'MBWExecutiveCool', 'MBW Executive Cool', NULL, '{"x":-1392.74,"y":-480.18,"z":71.14}', '{"x":-1389.43,"y":-479.01,"z":71.14}', NULL, '["ex_sm_15_office_02c"]', 'MazeBankWest', 0, 1, 0, '{"x":-1390.76,"y":-479.22,"z":72.04}', 2700000),
	(69, 'MBWExecutive Contrast', 'MBW Executive Contrast', NULL, '{"x":-1392.74,"y":-480.18,"z":71.14}', '{"x":-1389.43,"y":-479.01,"z":71.14}', NULL, '["ex_sm_15_office_02a"]', 'MazeBankWest', 0, 1, 0, '{"x":-1390.76,"y":-479.22,"z":72.04}', 2700000),
	(70, 'MBWPowerBrokerIce', 'MBW Power Broker Ice', NULL, '{"x":-1392.74,"y":-480.18,"z":71.14}', '{"x":-1389.43,"y":-479.01,"z":71.14}', NULL, '["ex_sm_15_office_03a"]', 'MazeBankWest', 0, 1, 0, '{"x":-1390.76,"y":-479.22,"z":72.04}', 2700000),
	(71, 'MBWPowerBrokerConvservative', 'MBW Power Broker Convservative', NULL, '{"x":-1392.74,"y":-480.18,"z":71.14}', '{"x":-1389.43,"y":-479.01,"z":71.14}', NULL, '["ex_sm_15_office_03b"]', 'MazeBankWest', 0, 1, 0, '{"x":-1390.76,"y":-479.22,"z":72.04}', 2700000),
	(72, 'MBWPowerBrokerPolished', 'MBW Power Broker Polished', NULL, '{"x":-1392.74,"y":-480.18,"z":71.14}', '{"x":-1389.43,"y":-479.01,"z":71.14}', NULL, '["ex_sm_15_office_03c"]', 'MazeBankWest', 0, 1, 0, '{"x":-1390.76,"y":-479.22,"z":72.04}', 2700000);
/*!40000 ALTER TABLE `properties` ENABLE KEYS */;

-- Dumping structure for table essentialmode.rented_vehicles
CREATE TABLE IF NOT EXISTS `rented_vehicles` (
  `vehicle` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `plate` varchar(12) COLLATE utf8mb4_bin NOT NULL,
  `player_name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `base_price` int(11) NOT NULL,
  `rent_price` int(11) NOT NULL,
  `owner` varchar(22) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`plate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.rented_vehicles: ~0 rows (approximately)
/*!40000 ALTER TABLE `rented_vehicles` DISABLE KEYS */;
/*!40000 ALTER TABLE `rented_vehicles` ENABLE KEYS */;

-- Dumping structure for table essentialmode.shops
CREATE TABLE IF NOT EXISTS `shops` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `store` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `item` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `price` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=158 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.shops: ~115 rows (approximately)
/*!40000 ALTER TABLE `shops` DISABLE KEYS */;
INSERT INTO `shops` (`id`, `store`, `item`, `price`) VALUES
	(1, 'TwentyFourSeven', 'bread', 70),
	(2, 'TwentyFourSeven', 'water', 30),
	(3, 'RobsLiquor', 'bread', 70),
	(4, 'RobsLiquor', 'water', 30),
	(5, 'LTDgasoline', 'bread', 70),
	(6, 'LTDgasoline', 'water', 30),
	(7, 'TwentyFourSeven', 'chocolate', 60),
	(8, 'RobsLiquor', 'chocolate', 60),
	(9, 'LTDgasoline', 'chocolate', 60),
	(10, 'TwentyFourSeven', 'sandwich', 60),
	(11, 'RobsLiquor', 'sandwich', 60),
	(12, 'LTDgasoline', 'sandwich', 60),
	(13, 'TwentyFourSeven', 'hamburger', 70),
	(14, 'RobsLiquor', 'hamburger', 70),
	(15, 'LTDgasoline', 'hamburger', 70),
	(16, 'TwentyFourSeven', 'cupcake', 50),
	(17, 'RobsLiquor', 'cupcake', 50),
	(18, 'LTDgasoline', 'cupcake', 50),
	(19, 'TwentyFourSeven', 'cocacola', 30),
	(20, 'RobsLiquor', 'cocacola', 30),
	(21, 'LTDgasoline', 'cocacola', 30),
	(22, 'TwentyFourSeven', 'icetea', 30),
	(23, 'RobsLiquor', 'icetea', 30),
	(24, 'LTDgasoline', 'icetea', 30),
	(25, 'TwentyFourSeven', 'coffe', 30),
	(26, 'RobsLiquor', 'coffe', 30),
	(27, 'LTDgasoline', 'coffe', 30),
	(31, 'RobsLiquor', 'cigarett', 150),
	(32, 'RobsLiquor', 'lighter', 70),
	(33, 'LTDgasoline', 'cigarett', 150),
	(34, 'LTDgasoline', 'lighter', 70),
	(35, 'TwentyFourSeven', 'cigarett', 150),
	(36, 'TwentyFourSeven', 'lighter', 70),
	(37, 'Bar', 'beer', 80),
	(38, 'Bar', 'wine', 150),
	(39, 'Bar', 'vodka', 130),
	(40, 'Bar', 'tequila', 130),
	(41, 'Bar', 'whisky', 100),
	(42, 'Bar', 'cigarett', 150),
	(43, 'Bar', 'lighter', 75),
	(44, 'Disco', 'beer', 80),
	(45, 'Disco', 'wine', 150),
	(46, 'Disco', 'vodka', 130),
	(47, 'Disco', 'tequila', 130),
	(48, 'Disco', 'whisky', 100),
	(49, 'Disco', 'cigarett', 150),
	(50, 'Disco', 'lighter', 75),
	(51, 'fishing_shop', 'fishingrod', 2500),
	(52, 'fishing_shop', 'fishbait', 50),
	(53, 'fishing_shop', 'turtlebait', 150),
	(55, 'TwentyFourSeven', 'fixkit', 3000),
	(56, 'RobsLiquor', 'fixkit', 3000),
	(59, 'weed_shop', 'marijuana_cigarette', 500),
	(60, 'weed_shop', 'bong', 1000),
	(61, 'weed_shop', 'lighter', 75),
	(63, 'pillbox', 'oxygen_mask', 2500),
	(64, 'weaponeacc', 'clip', 10000),
	(65, 'weaponeacc', 'weaponflashlight', 10000),
	(66, 'weaponeacc', 'weapongrip', 10000),
	(67, 'weaponeacc', 'weaponskin', 10000),
	(70, 'LTDgasoline', 'fixkit', 3000),
	(71, 'LTDgasoline', 'croquettes', 150),
	(72, 'TwentyFourSeven', 'croquettes', 150),
	(73, 'RobsLiquor', 'croquettes', 150),
	(74, 'LTDgasoline', 'jumelles', 300),
	(75, 'TwentyFourSeven', 'jumelles', 300),
	(76, 'TwentyFourSeven', 'sickle', 1000),
	(77, 'RobsLiquor', 'sickle', 1000),
	(78, 'LTDgasoline', 'sickle', 1000),
	(85, 'LTDgasoline', 'phone', 3500),
	(86, 'TwentyFourSeven', 'phone', 3500),
	(87, 'RobsLiquor', 'phone', 3500),
	(88, 'LTDgasoline', 'milk_engine', 2000),
	(89, 'TwentyFourSeven', 'milk_engine', 2000),
	(90, 'RobsLiquor', 'milk_engine', 2000),
	(91, 'LTDgasoline', 'drill', 2500),
	(92, 'TwentyFourSeven', 'drill', 2500),
	(93, 'RobsLiquor', 'drill', 2500),
	(94, 'RobsLiquor', 'knife_chicken', 1500),
	(95, 'TwentyFourSeven', 'knife_chicken', 1500),
	(96, 'LTDgasoline', 'knife_chicken', 1500),
	(97, 'pillbox', 'anti', 70),
	(98, 'RobsLiquor', 'boxmilk', 30),
	(99, 'TwentyFourSeven', 'boxmilk', 30),
	(100, 'LTDgasoline', 'boxmilk', 30),
	(101, 'TwentyFourSeven', 'hatchet_lj', 1500),
	(102, 'LTDgasoline', 'hatchet_lj', 1500),
	(103, 'RobsLiquor', 'hatchet_lj', 1500),
	(119, 'pillbox', 'water', 30),
	(120, 'pillbox', 'bread', 70),
	(133, 'RobsLiquor', 'shell_engine', 2000),
	(134, 'LTDgasoline', 'shell_engine', 2000),
	(135, 'TwentyFourSeven', 'shell_engine', 2000),
	(136, 'TwentyFourSeven', 'shovel', 2000),
	(137, 'LTDgasoline', 'shovel', 2000),
	(138, 'RobsLiquor', 'shovel', 2000),
	(139, 'Bar', 'zippybag', 20),
	(140, 'Disco', 'zippybag', 20),
	(141, 'Bar', 'doubleaction_blueprint', 5000),
	(142, 'Bar', 'switchblade_blueprint', 5000),
	(143, 'Disco', 'switchblade_blueprint', 5000),
	(144, 'Disco', 'doubleaction_blueprint', 5000),
	(145, 'Bar', 'wrench_blueprint', 5000),
	(146, 'Disco', 'wrench_blueprint', 5000),
	(147, 'Bar', 'snspistol_blueprint', 5000),
	(148, 'Disco', 'snspistol_blueprint', 5000),
	(149, 'TwentyFourSeven', 'craftkit', 1000),
	(150, 'RobsLiquor', 'craftkit', 1000),
	(151, 'LTDgasoline', 'craftkit', 1000),
	(152, 'TwentyFourSeven', 'beer', 80),
	(153, 'LTDgasoline', 'beer', 80),
	(154, 'RobsLiquor', 'beer', 80),
	(155, 'TwentyFourSeven', 'jumelles', 1000),
	(156, 'RobsLiquor', 'jumelles', 1000),
	(157, 'LTDgasoline', 'jumelles', 1000);
/*!40000 ALTER TABLE `shops` ENABLE KEYS */;

-- Dumping structure for table essentialmode.society_moneywash
CREATE TABLE IF NOT EXISTS `society_moneywash` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(60) NOT NULL,
  `society` varchar(60) NOT NULL,
  `amount` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.society_moneywash: ~0 rows (approximately)
/*!40000 ALTER TABLE `society_moneywash` DISABLE KEYS */;
/*!40000 ALTER TABLE `society_moneywash` ENABLE KEYS */;

-- Dumping structure for table essentialmode.trunk_inventory
CREATE TABLE IF NOT EXISTS `trunk_inventory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `plate` varchar(8) COLLATE utf8mb4_bin NOT NULL,
  `data` text COLLATE utf8mb4_bin NOT NULL,
  `owned` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `plate` (`plate`)
) ENGINE=InnoDB AUTO_INCREMENT=169 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.trunk_inventory: ~0 rows (approximately)
/*!40000 ALTER TABLE `trunk_inventory` DISABLE KEYS */;
/*!40000 ALTER TABLE `trunk_inventory` ENABLE KEYS */;

-- Dumping structure for table essentialmode.twitter_accounts
CREATE TABLE IF NOT EXISTS `twitter_accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT '0',
  `password` varchar(50) COLLATE utf8mb4_bin NOT NULL DEFAULT '0',
  `avatar_url` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=420 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.twitter_accounts: ~0 rows (approximately)
/*!40000 ALTER TABLE `twitter_accounts` DISABLE KEYS */;
/*!40000 ALTER TABLE `twitter_accounts` ENABLE KEYS */;

-- Dumping structure for table essentialmode.twitter_likes
CREATE TABLE IF NOT EXISTS `twitter_likes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `authorId` int(11) DEFAULT NULL,
  `tweetId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_twitter_likes_twitter_accounts` (`authorId`),
  KEY `FK_twitter_likes_twitter_tweets` (`tweetId`),
  CONSTRAINT `FK_twitter_likes_twitter_accounts` FOREIGN KEY (`authorId`) REFERENCES `twitter_accounts` (`id`),
  CONSTRAINT `FK_twitter_likes_twitter_tweets` FOREIGN KEY (`tweetId`) REFERENCES `twitter_tweets` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=277 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.twitter_likes: ~0 rows (approximately)
/*!40000 ALTER TABLE `twitter_likes` DISABLE KEYS */;
/*!40000 ALTER TABLE `twitter_likes` ENABLE KEYS */;

-- Dumping structure for table essentialmode.twitter_tweets
CREATE TABLE IF NOT EXISTS `twitter_tweets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `authorId` int(11) NOT NULL,
  `realUser` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp(),
  `likes` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `FK_twitter_tweets_twitter_accounts` (`authorId`),
  CONSTRAINT `FK_twitter_tweets_twitter_accounts` FOREIGN KEY (`authorId`) REFERENCES `twitter_accounts` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1385 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table essentialmode.twitter_tweets: ~0 rows (approximately)
/*!40000 ALTER TABLE `twitter_tweets` DISABLE KEYS */;
/*!40000 ALTER TABLE `twitter_tweets` ENABLE KEYS */;

-- Dumping structure for table essentialmode.users
CREATE TABLE IF NOT EXISTS `users` (
  `identifier` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `license` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `money` int(11) DEFAULT NULL,
  `name` varchar(50) COLLATE utf8mb4_bin DEFAULT '',
  `skin` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `job` varchar(50) COLLATE utf8mb4_bin DEFAULT 'unemployed',
  `job_grade` int(11) DEFAULT 0,
  `loadout` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `position` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `bank` int(11) DEFAULT NULL,
  `permission_level` int(11) DEFAULT NULL,
  `group` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `status` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `firstname` varchar(50) COLLATE utf8mb4_bin DEFAULT '',
  `lastname` varchar(50) COLLATE utf8mb4_bin DEFAULT '',
  `dateofbirth` varchar(25) COLLATE utf8mb4_bin DEFAULT '',
  `sex` varchar(10) COLLATE utf8mb4_bin DEFAULT '',
  `height` varchar(5) COLLATE utf8mb4_bin DEFAULT '',
  `lastdigits` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `tattoos` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `phone_number` varchar(10) COLLATE utf8mb4_bin DEFAULT NULL,
  `is_dead` tinyint(1) DEFAULT 0,
  `jail` int(11) NOT NULL DEFAULT 0,
  `last_property` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `pet` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `skills` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `gang` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `gang_grade` int(11) DEFAULT NULL,
  PRIMARY KEY (`identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.users: ~2 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`identifier`, `license`, `money`, `name`, `skin`, `job`, `job_grade`, `loadout`, `position`, `bank`, `permission_level`, `group`, `status`, `firstname`, `lastname`, `dateofbirth`, `sex`, `height`, `lastdigits`, `tattoos`, `phone_number`, `is_dead`, `jail`, `last_property`, `pet`, `skills`, `gang`, `gang_grade`) VALUES
	('steam:110000104a1abc7', 'license:1695737ba9de1c01bf92473f152b1df5ccb54e35', 5000, 'THE-ONE', '{"blush_3":0,"face":0,"helmet_2":0,"beard_3":0,"chest_2":0,"age_1":0,"decals_1":0,"watches_1":-1,"ears_1":-1,"beard_4":0,"tshirt_2":0,"arms_2":0,"torso_2":0,"hair_2":0,"eye_color":0,"hair_color_1":0,"blush_1":0,"lipstick_3":0,"eyebrows_2":0,"makeup_2":0,"blemishes_2":0,"pants_1":0,"eyebrows_4":0,"sun_1":0,"mask_2":0,"moles_2":0,"bracelets_2":0,"lipstick_2":0,"eyebrows_3":0,"bodyb_2":0,"makeup_4":0,"eyebrows_1":0,"chest_1":0,"decals_2":0,"chest_3":0,"glasses_2":0,"ears_2":0,"bproof_2":0,"hair_1":11,"pants_2":0,"arms":0,"bodyb_1":0,"glasses_1":0,"chain_2":0,"beard_2":0,"sun_2":0,"makeup_1":0,"bracelets_1":-1,"helmet_1":-1,"moles_1":0,"sex":0,"mask_1":0,"complexion_1":0,"blush_2":0,"chain_1":0,"blemishes_1":0,"watches_2":0,"bproof_1":0,"bags_1":0,"shoes_1":0,"lipstick_1":0,"tshirt_1":0,"hair_color_2":0,"complexion_2":0,"beard_1":0,"skin":0,"shoes_2":0,"makeup_3":0,"age_2":0,"lipstick_4":0,"torso_1":0,"bags_2":0}', 'unemployed', 0, '[]', '{"x":-10.3,"y":25.7,"z":71.4}', 10000, 0, 'superadmin', '[{"percent":73.65,"name":"hunger","val":736500},{"percent":80.2375,"name":"thirst","val":802375}]', 'Abx', 'Abx', '1999-09-09', 'm', '200', NULL, NULL, '0709494018', 0, 0, NULL, '', '{"Lung Capacity":{"RemoveAmount":-0.1,"Current":0,"Stat":"MP0_LUNG_CAPACITY"},"Wheelie":{"RemoveAmount":-0.2,"Current":0,"Stat":"MP0_WHEELIE_ABILITY"},"Driving":{"RemoveAmount":-0.5,"Current":0,"Stat":"MP0_DRIVING_ABILITY"},"Shooting":{"RemoveAmount":-0.1,"Current":0,"Stat":"MP0_SHOOTING_ABILITY"},"Stamina":{"RemoveAmount":-0.3,"Current":19.9,"Stat":"MP0_STAMINA"},"Strength":{"RemoveAmount":-0.3,"Current":9.7,"Stat":"MP0_STRENGTH"}}', NULL, NULL),
	('steam:11000013ac515f5', 'license:1695737ba9de1c01bf92473f152b1df5ccb54e35', 0, 'OMG', '{"shoes_2":0,"beard_4":0,"chain_1":0,"hair_2":0,"bags_1":0,"hair_color_1":0,"arms":0,"eyebrows_4":0,"shoes_1":0,"decals_2":0,"lipstick_2":0,"pants_2":0,"glasses_2":0,"sex":0,"beard_2":0,"torso_1":0,"helmet_2":0,"makeup_1":0,"pants_1":0,"tshirt_2":0,"hair_color_2":0,"lipstick_3":0,"age_1":0,"chain_2":0,"bproof_2":0,"eyebrows_3":0,"makeup_2":0,"eyebrows_1":0,"tshirt_1":0,"beard_3":0,"face":0,"decals_1":0,"mask_1":0,"lipstick_4":0,"ears_2":0,"helmet_1":-1,"age_2":0,"bproof_1":0,"makeup_3":0,"hair_1":0,"skin":0,"eyebrows_2":0,"ears_1":-1,"mask_2":0,"bags_2":0,"torso_2":0,"makeup_4":0,"lipstick_1":0,"beard_1":0,"glasses_1":0}', 'unemployed', 0, '[]', '{"x":-1.5,"y":19.3,"z":71.1}', 0, 0, 'user', '[{"percent":74.74,"name":"hunger","val":747400},{"percent":81.055,"name":"thirst","val":810550}]', 'VVvv', 'Vvv', '1999-09-09', 'm', '200', NULL, NULL, '0866101074', 0, 0, NULL, '', '{"Strength":{"Current":9.7,"RemoveAmount":-0.3,"Stat":"MP0_STRENGTH"},"Stamina":{"Current":19.7,"RemoveAmount":-0.3,"Stat":"MP0_STAMINA"},"Wheelie":{"Current":0,"RemoveAmount":-0.2,"Stat":"MP0_WHEELIE_ABILITY"},"Lung Capacity":{"Current":0,"RemoveAmount":-0.1,"Stat":"MP0_LUNG_CAPACITY"},"Driving":{"Current":0,"RemoveAmount":-0.5,"Stat":"MP0_DRIVING_ABILITY"},"Shooting":{"Current":0,"RemoveAmount":-0.1,"Stat":"MP0_SHOOTING_ABILITY"}}', NULL, NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table essentialmode.user_accounts
CREATE TABLE IF NOT EXISTS `user_accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(22) COLLATE utf8mb4_bin NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `money` double NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.user_accounts: ~2 rows (approximately)
/*!40000 ALTER TABLE `user_accounts` DISABLE KEYS */;
INSERT INTO `user_accounts` (`id`, `identifier`, `name`, `money`) VALUES
	(1, 'steam:11000013ac515f5', 'black_money', 0),
	(2, 'steam:110000104a1abc7', 'black_money', 0);
/*!40000 ALTER TABLE `user_accounts` ENABLE KEYS */;

-- Dumping structure for table essentialmode.user_inventory
CREATE TABLE IF NOT EXISTS `user_inventory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(22) COLLATE utf8mb4_bin NOT NULL,
  `item` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `count` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=441 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.user_inventory: ~440 rows (approximately)
/*!40000 ALTER TABLE `user_inventory` DISABLE KEYS */;
INSERT INTO `user_inventory` (`id`, `identifier`, `item`, `count`) VALUES
	(1, 'steam:11000013ac515f5', 'chest_a', 0),
	(2, 'steam:11000013ac515f5', 'carotool', 0),
	(3, 'steam:11000013ac515f5', 'meat', 0),
	(4, 'steam:11000013ac515f5', 'WEAPON_VINTAGEPISTOL', 0),
	(5, 'steam:11000013ac515f5', 'coke', 0),
	(6, 'steam:11000013ac515f5', 'tequila', 0),
	(7, 'steam:11000013ac515f5', 'blowtorch', 0),
	(8, 'steam:11000013ac515f5', 'WEAPON_HEAVYSHOTGUN', 0),
	(9, 'steam:11000013ac515f5', 'alive_chicken', 0),
	(10, 'steam:11000013ac515f5', 'WEAPON_BULLPUPSHOTGUN', 0),
	(11, 'steam:11000013ac515f5', 'bong', 0),
	(12, 'steam:11000013ac515f5', 'WEAPON_HAMMER', 0),
	(13, 'steam:11000013ac515f5', 'mushroom_p', 0),
	(14, 'steam:11000013ac515f5', 'rope', 0),
	(15, 'steam:11000013ac515f5', 'jumelles', 0),
	(16, 'steam:11000013ac515f5', 'squid', 0),
	(17, 'steam:11000013ac515f5', 'WEAPON_ASSAULTRIFLE', 0),
	(18, 'steam:11000013ac515f5', 'washed_stone', 0),
	(19, 'steam:11000013ac515f5', 'packaged_chicken', 0),
	(20, 'steam:11000013ac515f5', 'copper', 0),
	(21, 'steam:11000013ac515f5', 'WEAPON_POOLCUE', 0),
	(22, 'steam:11000013ac515f5', 'chickenwithoutfeather', 0),
	(23, 'steam:11000013ac515f5', 'petrol_raffin', 0),
	(24, 'steam:11000013ac515f5', 'lockpick', 0),
	(25, 'steam:11000013ac515f5', 'WEAPON_BULLPUPRIFLE', 0),
	(26, 'steam:11000013ac515f5', 'WEAPON_BATTLEAXE', 0),
	(27, 'steam:11000013ac515f5', 'bottle', 0),
	(28, 'steam:11000013ac515f5', 'snspistol_blueprint', 0),
	(29, 'steam:11000013ac515f5', 'wrench_blueprint', 0),
	(30, 'steam:11000013ac515f5', 'WEAPON_HEAVYSNIPER', 0),
	(31, 'steam:11000013ac515f5', 'chicken_package', 0),
	(32, 'steam:11000013ac515f5', 'WEAPON_GRENADE', 0),
	(33, 'steam:11000013ac515f5', 'WEAPON_FIREEXTINGUISHER', 0),
	(34, 'steam:11000013ac515f5', 'WEAPON_GRENADELAUNCHER', 0),
	(35, 'steam:11000013ac515f5', 'WEAPON_SAWNOFFSHOTGUN', 0),
	(36, 'steam:11000013ac515f5', 'WEAPON_KNUCKLE', 0),
	(37, 'steam:11000013ac515f5', 'rice_pro', 0),
	(38, 'steam:11000013ac515f5', 'WEAPON_GARBAGEBAG', 0),
	(39, 'steam:11000013ac515f5', 'WEAPON_SMOKEGRENADE', 0),
	(40, 'steam:11000013ac515f5', 'shell', 0),
	(41, 'steam:11000013ac515f5', 'iron', 0),
	(42, 'steam:11000013ac515f5', 'WEAPON_DIGISCANNER', 0),
	(43, 'steam:11000013ac515f5', 'gold', 0),
	(44, 'steam:11000013ac515f5', 'sickle', 0),
	(45, 'steam:11000013ac515f5', 'WEAPON_SWITCHBLADE', 0),
	(46, 'steam:11000013ac515f5', 'chocolate', 0),
	(47, 'steam:11000013ac515f5', 'clip', 0),
	(48, 'steam:11000013ac515f5', 'weaponskin', 0),
	(49, 'steam:11000013ac515f5', 'WEAPON_BALL', 0),
	(50, 'steam:11000013ac515f5', 'britadeira', 0),
	(51, 'steam:11000013ac515f5', 'WEAPON_REVOLVER', 0),
	(52, 'steam:11000013ac515f5', 'WEAPON_ADVANCEDRIFLE', 0),
	(53, 'steam:11000013ac515f5', 'WEAPON_BAT', 0),
	(54, 'steam:11000013ac515f5', 'slaughtered_chicken', 0),
	(55, 'steam:11000013ac515f5', 'WEAPON_COMPACTRIFLE', 0),
	(56, 'steam:11000013ac515f5', 'gym_membership', 0),
	(57, 'steam:11000013ac515f5', 'WEAPON_HEAVYPISTOL', 0),
	(58, 'steam:11000013ac515f5', 'essence', 0),
	(59, 'steam:11000013ac515f5', 'whisky', 0),
	(60, 'steam:11000013ac515f5', 'WEAPON_MINIGUN', 0),
	(61, 'steam:11000013ac515f5', 'WEAPON_SPECIALCARBINE', 0),
	(62, 'steam:11000013ac515f5', 'WEAPON_BZGAS', 0),
	(63, 'steam:11000013ac515f5', 'rice', 0),
	(64, 'steam:11000013ac515f5', 'water', 0),
	(65, 'steam:11000013ac515f5', 'zippybag', 0),
	(66, 'steam:11000013ac515f5', 'hydrocodone', 0),
	(67, 'steam:11000013ac515f5', 'worm', 0),
	(68, 'steam:11000013ac515f5', 'pork', 0),
	(69, 'steam:11000013ac515f5', 'WEAPON_SNSPISTOL', 0),
	(70, 'steam:11000013ac515f5', 'WEAPON_DOUBLEACTION', 0),
	(71, 'steam:11000013ac515f5', 'wood', 0),
	(72, 'steam:11000013ac515f5', 'WEAPON_PUMPSHOTGUN', 0),
	(73, 'steam:11000013ac515f5', 'wine', 0),
	(74, 'steam:11000013ac515f5', 'weed_pooch', 0),
	(75, 'steam:11000013ac515f5', 'cow_card', 0),
	(76, 'steam:11000013ac515f5', 'carokit', 0),
	(77, 'steam:11000013ac515f5', 'weapongrip', 0),
	(78, 'steam:11000013ac515f5', 'beer', 0),
	(79, 'steam:11000013ac515f5', 'honey_a', 0),
	(80, 'steam:11000013ac515f5', 'glasses', 0),
	(81, 'steam:11000013ac515f5', 'vicodin', 0),
	(82, 'steam:11000013ac515f5', 'chicken_meat', 0),
	(83, 'steam:11000013ac515f5', 'turtlebait', 0),
	(84, 'steam:11000013ac515f5', 'WEAPON_DAGGER', 0),
	(85, 'steam:11000013ac515f5', 'WEAPON_FIREWORK', 0),
	(86, 'steam:11000013ac515f5', 'WEAPON_NIGHTSTICK', 0),
	(87, 'steam:11000013ac515f5', 'turtle', 0),
	(88, 'steam:11000013ac515f5', 'WEAPON_HATCHET', 0),
	(89, 'steam:11000013ac515f5', 'switchblade_blueprint', 0),
	(90, 'steam:11000013ac515f5', 'shell_c', 0),
	(91, 'steam:11000013ac515f5', 'WEAPON_COMBATPISTOL', 0),
	(92, 'steam:11000013ac515f5', 'squidbait', 0),
	(93, 'steam:11000013ac515f5', 'sandwich', 0),
	(94, 'steam:11000013ac515f5', 'skate', 0),
	(95, 'steam:11000013ac515f5', 'shovel', 0),
	(96, 'steam:11000013ac515f5', 'drill', 0),
	(97, 'steam:11000013ac515f5', 'shell_package', 0),
	(98, 'steam:11000013ac515f5', 'shell_engine', 0),
	(99, 'steam:11000013ac515f5', 'stone', 0),
	(100, 'steam:11000013ac515f5', 'fishingrod', 0),
	(101, 'steam:11000013ac515f5', 'shell_b', 0),
	(102, 'steam:11000013ac515f5', 'shell_a', 0),
	(103, 'steam:11000013ac515f5', 'crazypersoncard', 0),
	(104, 'steam:11000013ac515f5', 'fixkit', 0),
	(105, 'steam:11000013ac515f5', 'bread', 0),
	(106, 'steam:11000013ac515f5', 'cannabis', 0),
	(107, 'steam:11000013ac515f5', 'sharkfin', 0),
	(108, 'steam:11000013ac515f5', 'WEAPON_STUNGUN', 0),
	(109, 'steam:11000013ac515f5', 'WEAPON_PROXMINE', 0),
	(110, 'steam:11000013ac515f5', 'shark', 0),
	(111, 'steam:11000013ac515f5', 'sportlunch', 0),
	(112, 'steam:11000013ac515f5', 'sand', 0),
	(113, 'steam:11000013ac515f5', 'sand_b', 0),
	(114, 'steam:11000013ac515f5', 'sand_a', 0),
	(115, 'steam:11000013ac515f5', 'radio', 0),
	(116, 'steam:11000013ac515f5', 'fishbait', 0),
	(117, 'steam:11000013ac515f5', 'protein_shake', 0),
	(118, 'steam:11000013ac515f5', 'bobbypin', 0),
	(119, 'steam:11000013ac515f5', 'clothe', 0),
	(120, 'steam:11000013ac515f5', 'WEAPON_COMPACTLAUNCHER', 0),
	(121, 'steam:11000013ac515f5', 'prawnbait', 0),
	(122, 'steam:11000013ac515f5', 'prawn', 0),
	(123, 'steam:11000013ac515f5', 'milk_package', 0),
	(124, 'steam:11000013ac515f5', 'powerade', 0),
	(125, 'steam:11000013ac515f5', 'wool', 0),
	(126, 'steam:11000013ac515f5', 'porkpackage', 0),
	(127, 'steam:11000013ac515f5', 'cupcake', 0),
	(128, 'steam:11000013ac515f5', 'phone', 0),
	(129, 'steam:11000013ac515f5', 'milk', 0),
	(130, 'steam:11000013ac515f5', 'petrol', 0),
	(131, 'steam:11000013ac515f5', 'pro_wood', 0),
	(132, 'steam:11000013ac515f5', 'WEAPON_KNIFE', 0),
	(133, 'steam:11000013ac515f5', 'oxygen_mask', 0),
	(134, 'steam:11000013ac515f5', 'mushroom_d', 0),
	(135, 'steam:11000013ac515f5', 'WEAPON_ASSAULTSMG', 0),
	(136, 'steam:11000013ac515f5', 'WEAPON_PISTOL', 0),
	(137, 'steam:11000013ac515f5', 'WEAPON_GUSENBERG', 0),
	(138, 'steam:11000013ac515f5', 'mushroom', 0),
	(139, 'steam:11000013ac515f5', 'morphine', 0),
	(140, 'steam:11000013ac515f5', 'meth', 0),
	(141, 'steam:11000013ac515f5', 'cutted_wood', 0),
	(142, 'steam:11000013ac515f5', 'milk_engine', 0),
	(143, 'steam:11000013ac515f5', 'gazbottle', 0),
	(144, 'steam:11000013ac515f5', 'WEAPON_MUSKET', 0),
	(145, 'steam:11000013ac515f5', 'lighter', 0),
	(146, 'steam:11000013ac515f5', 'cigarett', 0),
	(147, 'steam:11000013ac515f5', 'meatshark', 0),
	(148, 'steam:11000013ac515f5', 'bandage', 0),
	(149, 'steam:11000013ac515f5', 'medikit', 0),
	(150, 'steam:11000013ac515f5', 'chicken', 0),
	(151, 'steam:11000013ac515f5', 'marijuana', 0),
	(152, 'steam:11000013ac515f5', 'WEAPON_APPISTOL', 0),
	(153, 'steam:11000013ac515f5', 'marijuana_cigarette', 0),
	(154, 'steam:11000013ac515f5', 'WEAPON_PETROLCAN', 0),
	(155, 'steam:11000013ac515f5', 'fuel_b', 0),
	(156, 'steam:11000013ac515f5', 'WEAPON_RAILGUN', 0),
	(157, 'steam:11000013ac515f5', 'cocacola', 0),
	(158, 'steam:11000013ac515f5', 'WEAPON_MACHETE', 0),
	(159, 'steam:11000013ac515f5', 'coke_pooch', 0),
	(160, 'steam:11000013ac515f5', 'weaponflashlight', 0),
	(161, 'steam:11000013ac515f5', 'WEAPON_RPG', 0),
	(162, 'steam:11000013ac515f5', 'icetea', 0),
	(163, 'steam:11000013ac515f5', 'honey_b', 0),
	(164, 'steam:11000013ac515f5', 'WEAPON_AUTOSHOTGUN', 0),
	(165, 'steam:11000013ac515f5', 'WEAPON_HOMINGLAUNCHER', 0),
	(166, 'steam:11000013ac515f5', 'hatchet_lj', 0),
	(167, 'steam:11000013ac515f5', 'WEAPON_WRENCH', 0),
	(168, 'steam:11000013ac515f5', 'fabric', 0),
	(169, 'steam:11000013ac515f5', 'WEAPON_GOLFCLUB', 0),
	(170, 'steam:11000013ac515f5', 'handcuffs', 0),
	(171, 'steam:11000013ac515f5', 'WEAPON_STINGER', 0),
	(172, 'steam:11000013ac515f5', 'WEAPON_MACHINEPISTOL', 0),
	(173, 'steam:11000013ac515f5', 'hamburger', 0),
	(174, 'steam:11000013ac515f5', 'hamajifish', 0),
	(175, 'steam:11000013ac515f5', 'WEAPON_CROWBAR', 0),
	(176, 'steam:11000013ac515f5', 'Packaged_plank', 0),
	(177, 'steam:11000013ac515f5', 'WEAPON_COMBATPDW', 0),
	(178, 'steam:11000013ac515f5', 'anti', 0),
	(179, 'steam:11000013ac515f5', 'vodka', 0),
	(180, 'steam:11000013ac515f5', 'boxmilk', 0),
	(181, 'steam:11000013ac515f5', 'WEAPON_PIPEBOMB', 0),
	(182, 'steam:11000013ac515f5', 'meth_pooch', 0),
	(183, 'steam:11000013ac515f5', 'leather', 0),
	(184, 'steam:11000013ac515f5', 'fuel_a', 0),
	(185, 'steam:11000013ac515f5', 'blowpipe', 0),
	(186, 'steam:11000013ac515f5', 'fish', 0),
	(187, 'steam:11000013ac515f5', 'doubleaction_blueprint', 0),
	(188, 'steam:11000013ac515f5', 'AED', 0),
	(189, 'steam:11000013ac515f5', 'WEAPON_MARKSMANPISTOL', 0),
	(190, 'steam:11000013ac515f5', 'WEAPON_CARBINERIFLE', 0),
	(191, 'steam:11000013ac515f5', 'WEAPON_SNIPERRIFLE', 0),
	(192, 'steam:11000013ac515f5', 'WEAPON_FLASHLIGHT', 0),
	(193, 'steam:11000013ac515f5', 'WEAPON_SNOWBALL', 0),
	(194, 'steam:11000013ac515f5', 'WEAPON_PISTOL50', 0),
	(195, 'steam:11000013ac515f5', 'WEAPON_MG', 0),
	(196, 'steam:11000013ac515f5', 'WEAPON_HANDCUFFS', 0),
	(197, 'steam:11000013ac515f5', 'SteelScrap', 0),
	(198, 'steam:11000013ac515f5', 'WEAPON_STICKYBOMB', 0),
	(199, 'steam:11000013ac515f5', 'fixtool', 0),
	(200, 'steam:11000013ac515f5', 'diamond', 0),
	(201, 'steam:11000013ac515f5', 'WEAPON_MARKSMANRIFLE', 0),
	(202, 'steam:11000013ac515f5', 'diesel', 0),
	(203, 'steam:11000013ac515f5', 'WEAPON_ASSAULTSHOTGUN', 0),
	(204, 'steam:11000013ac515f5', 'croquettes', 0),
	(205, 'steam:11000013ac515f5', 'craftkit', 0),
	(206, 'steam:11000013ac515f5', 'WEAPON_DBSHOTGUN', 0),
	(207, 'steam:11000013ac515f5', 'knife_chicken', 0),
	(208, 'steam:11000013ac515f5', 'packaged_plank', 0),
	(209, 'steam:11000013ac515f5', 'WEAPON_MICROSMG', 0),
	(210, 'steam:11000013ac515f5', 'WEAPON_MINISMG', 0),
	(211, 'steam:11000013ac515f5', 'coffe', 0),
	(212, 'steam:11000013ac515f5', 'gunpowder', 0),
	(213, 'steam:11000013ac515f5', 'WEAPON_BOTTLE', 0),
	(214, 'steam:11000013ac515f5', 'bcabbage', 0),
	(215, 'steam:11000013ac515f5', 'WEAPON_FLAREGUN', 0),
	(216, 'steam:11000013ac515f5', 'WEAPON_FLARE', 0),
	(217, 'steam:11000013ac515f5', 'acabbage', 0),
	(218, 'steam:11000013ac515f5', 'WEAPON_SMG', 0),
	(219, 'steam:11000013ac515f5', 'WEAPON_MOLOTOV', 0),
	(220, 'steam:11000013ac515f5', 'WEAPON_COMBATMG', 0),
	(221, 'steam:110000104a1abc7', 'chest_a', 0),
	(222, 'steam:110000104a1abc7', 'carotool', 0),
	(223, 'steam:110000104a1abc7', 'coke', 0),
	(224, 'steam:110000104a1abc7', 'WEAPON_VINTAGEPISTOL', 0),
	(225, 'steam:110000104a1abc7', 'meat', 0),
	(226, 'steam:110000104a1abc7', 'alive_chicken', 0),
	(227, 'steam:110000104a1abc7', 'tequila', 0),
	(228, 'steam:110000104a1abc7', 'blowtorch', 0),
	(229, 'steam:110000104a1abc7', 'WEAPON_HEAVYSHOTGUN', 0),
	(230, 'steam:110000104a1abc7', 'WEAPON_BULLPUPSHOTGUN', 0),
	(231, 'steam:110000104a1abc7', 'bong', 0),
	(232, 'steam:110000104a1abc7', 'WEAPON_HAMMER', 0),
	(233, 'steam:110000104a1abc7', 'mushroom_p', 0),
	(234, 'steam:110000104a1abc7', 'rope', 0),
	(235, 'steam:110000104a1abc7', 'jumelles', 0),
	(236, 'steam:110000104a1abc7', 'WEAPON_ASSAULTRIFLE', 0),
	(237, 'steam:110000104a1abc7', 'squid', 0),
	(238, 'steam:110000104a1abc7', 'copper', 0),
	(239, 'steam:110000104a1abc7', 'washed_stone', 0),
	(240, 'steam:110000104a1abc7', 'packaged_chicken', 0),
	(241, 'steam:110000104a1abc7', 'WEAPON_POOLCUE', 0),
	(242, 'steam:110000104a1abc7', 'chickenwithoutfeather', 0),
	(243, 'steam:110000104a1abc7', 'petrol_raffin', 0),
	(244, 'steam:110000104a1abc7', 'WEAPON_BULLPUPRIFLE', 0),
	(245, 'steam:110000104a1abc7', 'lockpick', 0),
	(246, 'steam:110000104a1abc7', 'WEAPON_BATTLEAXE', 0),
	(247, 'steam:110000104a1abc7', 'bottle', 5),
	(248, 'steam:110000104a1abc7', 'snspistol_blueprint', 0),
	(249, 'steam:110000104a1abc7', 'wrench_blueprint', 0),
	(250, 'steam:110000104a1abc7', 'WEAPON_HEAVYSNIPER', 0),
	(251, 'steam:110000104a1abc7', 'chicken_package', 0),
	(252, 'steam:110000104a1abc7', 'WEAPON_GRENADE', 0),
	(253, 'steam:110000104a1abc7', 'WEAPON_FIREEXTINGUISHER', 0),
	(254, 'steam:110000104a1abc7', 'WEAPON_SAWNOFFSHOTGUN', 0),
	(255, 'steam:110000104a1abc7', 'WEAPON_GRENADELAUNCHER', 0),
	(256, 'steam:110000104a1abc7', 'rice_pro', 0),
	(257, 'steam:110000104a1abc7', 'WEAPON_KNUCKLE', 0),
	(258, 'steam:110000104a1abc7', 'WEAPON_GARBAGEBAG', 0),
	(259, 'steam:110000104a1abc7', 'shell', 0),
	(260, 'steam:110000104a1abc7', 'WEAPON_SMOKEGRENADE', 0),
	(261, 'steam:110000104a1abc7', 'iron', 0),
	(262, 'steam:110000104a1abc7', 'gold', 0),
	(263, 'steam:110000104a1abc7', 'WEAPON_SWITCHBLADE', 0),
	(264, 'steam:110000104a1abc7', 'sickle', 0),
	(265, 'steam:110000104a1abc7', 'WEAPON_DIGISCANNER', 0),
	(266, 'steam:110000104a1abc7', 'weaponskin', 0),
	(267, 'steam:110000104a1abc7', 'WEAPON_BALL', 0),
	(268, 'steam:110000104a1abc7', 'britadeira', 0),
	(269, 'steam:110000104a1abc7', 'chocolate', 0),
	(270, 'steam:110000104a1abc7', 'clip', 0),
	(271, 'steam:110000104a1abc7', 'WEAPON_REVOLVER', 0),
	(272, 'steam:110000104a1abc7', 'slaughtered_chicken', 0),
	(273, 'steam:110000104a1abc7', 'WEAPON_ADVANCEDRIFLE', 0),
	(274, 'steam:110000104a1abc7', 'WEAPON_BAT', 0),
	(275, 'steam:110000104a1abc7', 'WEAPON_COMPACTRIFLE', 0),
	(276, 'steam:110000104a1abc7', 'gym_membership', 0),
	(277, 'steam:110000104a1abc7', 'WEAPON_HEAVYPISTOL', 0),
	(278, 'steam:110000104a1abc7', 'essence', 0),
	(279, 'steam:110000104a1abc7', 'WEAPON_MINIGUN', 0),
	(280, 'steam:110000104a1abc7', 'whisky', 0),
	(281, 'steam:110000104a1abc7', 'WEAPON_SPECIALCARBINE', 0),
	(282, 'steam:110000104a1abc7', 'WEAPON_BZGAS', 0),
	(283, 'steam:110000104a1abc7', 'rice', 0),
	(284, 'steam:110000104a1abc7', 'water', 0),
	(285, 'steam:110000104a1abc7', 'zippybag', 0),
	(286, 'steam:110000104a1abc7', 'worm', 0),
	(287, 'steam:110000104a1abc7', 'WEAPON_SNSPISTOL', 0),
	(288, 'steam:110000104a1abc7', 'WEAPON_DOUBLEACTION', 0),
	(289, 'steam:110000104a1abc7', 'hydrocodone', 0),
	(290, 'steam:110000104a1abc7', 'pork', 0),
	(291, 'steam:110000104a1abc7', 'wood', 0),
	(292, 'steam:110000104a1abc7', 'WEAPON_PUMPSHOTGUN', 0),
	(293, 'steam:110000104a1abc7', 'weed_pooch', 0),
	(294, 'steam:110000104a1abc7', 'cow_card', 0),
	(295, 'steam:110000104a1abc7', 'wine', 0),
	(296, 'steam:110000104a1abc7', 'carokit', 0),
	(297, 'steam:110000104a1abc7', 'weapongrip', 0),
	(298, 'steam:110000104a1abc7', 'beer', 0),
	(299, 'steam:110000104a1abc7', 'honey_a', 0),
	(300, 'steam:110000104a1abc7', 'glasses', 0),
	(301, 'steam:110000104a1abc7', 'vicodin', 0),
	(302, 'steam:110000104a1abc7', 'chicken_meat', 0),
	(303, 'steam:110000104a1abc7', 'turtlebait', 0),
	(304, 'steam:110000104a1abc7', 'WEAPON_FIREWORK', 0),
	(305, 'steam:110000104a1abc7', 'WEAPON_DAGGER', 0),
	(306, 'steam:110000104a1abc7', 'WEAPON_NIGHTSTICK', 0),
	(307, 'steam:110000104a1abc7', 'turtle', 0),
	(308, 'steam:110000104a1abc7', 'switchblade_blueprint', 0),
	(309, 'steam:110000104a1abc7', 'shell_c', 0),
	(310, 'steam:110000104a1abc7', 'WEAPON_HATCHET', 0),
	(311, 'steam:110000104a1abc7', 'WEAPON_COMBATPISTOL', 0),
	(312, 'steam:110000104a1abc7', 'skate', 0),
	(313, 'steam:110000104a1abc7', 'squidbait', 0),
	(314, 'steam:110000104a1abc7', 'sandwich', 0),
	(315, 'steam:110000104a1abc7', 'shovel', 0),
	(316, 'steam:110000104a1abc7', 'drill', 0),
	(317, 'steam:110000104a1abc7', 'shell_package', 0),
	(318, 'steam:110000104a1abc7', 'shell_engine', 0),
	(319, 'steam:110000104a1abc7', 'stone', 0),
	(320, 'steam:110000104a1abc7', 'fishingrod', 0),
	(321, 'steam:110000104a1abc7', 'shell_b', 0),
	(322, 'steam:110000104a1abc7', 'shell_a', 0),
	(323, 'steam:110000104a1abc7', 'crazypersoncard', 0),
	(324, 'steam:110000104a1abc7', 'fixkit', 0),
	(325, 'steam:110000104a1abc7', 'bread', 0),
	(326, 'steam:110000104a1abc7', 'cannabis', 0),
	(327, 'steam:110000104a1abc7', 'sharkfin', 0),
	(328, 'steam:110000104a1abc7', 'WEAPON_STUNGUN', 0),
	(329, 'steam:110000104a1abc7', 'WEAPON_PROXMINE', 0),
	(330, 'steam:110000104a1abc7', 'shark', 0),
	(331, 'steam:110000104a1abc7', 'sportlunch', 0),
	(332, 'steam:110000104a1abc7', 'sand', 0),
	(333, 'steam:110000104a1abc7', 'sand_b', 0),
	(334, 'steam:110000104a1abc7', 'sand_a', 0),
	(335, 'steam:110000104a1abc7', 'radio', 0),
	(336, 'steam:110000104a1abc7', 'fishbait', 0),
	(337, 'steam:110000104a1abc7', 'protein_shake', 0),
	(338, 'steam:110000104a1abc7', 'bobbypin', 0),
	(339, 'steam:110000104a1abc7', 'clothe', 0),
	(340, 'steam:110000104a1abc7', 'WEAPON_COMPACTLAUNCHER', 0),
	(341, 'steam:110000104a1abc7', 'prawnbait', 0),
	(342, 'steam:110000104a1abc7', 'milk_package', 0),
	(343, 'steam:110000104a1abc7', 'powerade', 0),
	(344, 'steam:110000104a1abc7', 'prawn', 0),
	(345, 'steam:110000104a1abc7', 'porkpackage', 0),
	(346, 'steam:110000104a1abc7', 'cupcake', 0),
	(347, 'steam:110000104a1abc7', 'wool', 0),
	(348, 'steam:110000104a1abc7', 'phone', 0),
	(349, 'steam:110000104a1abc7', 'milk', 0),
	(350, 'steam:110000104a1abc7', 'petrol', 0),
	(351, 'steam:110000104a1abc7', 'pro_wood', 0),
	(352, 'steam:110000104a1abc7', 'WEAPON_KNIFE', 0),
	(353, 'steam:110000104a1abc7', 'oxygen_mask', 0),
	(354, 'steam:110000104a1abc7', 'mushroom_d', 0),
	(355, 'steam:110000104a1abc7', 'WEAPON_ASSAULTSMG', 0),
	(356, 'steam:110000104a1abc7', 'WEAPON_PISTOL', 0),
	(357, 'steam:110000104a1abc7', 'WEAPON_GUSENBERG', 0),
	(358, 'steam:110000104a1abc7', 'mushroom', 0),
	(359, 'steam:110000104a1abc7', 'morphine', 0),
	(360, 'steam:110000104a1abc7', 'meth', 0),
	(361, 'steam:110000104a1abc7', 'cutted_wood', 0),
	(362, 'steam:110000104a1abc7', 'milk_engine', 0),
	(363, 'steam:110000104a1abc7', 'gazbottle', 0),
	(364, 'steam:110000104a1abc7', 'WEAPON_MUSKET', 0),
	(365, 'steam:110000104a1abc7', 'lighter', 0),
	(366, 'steam:110000104a1abc7', 'cigarett', 0),
	(367, 'steam:110000104a1abc7', 'meatshark', 0),
	(368, 'steam:110000104a1abc7', 'bandage', 0),
	(369, 'steam:110000104a1abc7', 'chicken', 0),
	(370, 'steam:110000104a1abc7', 'medikit', 0),
	(371, 'steam:110000104a1abc7', 'marijuana_cigarette', 0),
	(372, 'steam:110000104a1abc7', 'marijuana', 0),
	(373, 'steam:110000104a1abc7', 'WEAPON_APPISTOL', 0),
	(374, 'steam:110000104a1abc7', 'WEAPON_PETROLCAN', 0),
	(375, 'steam:110000104a1abc7', 'fuel_b', 0),
	(376, 'steam:110000104a1abc7', 'WEAPON_RAILGUN', 0),
	(377, 'steam:110000104a1abc7', 'cocacola', 0),
	(378, 'steam:110000104a1abc7', 'WEAPON_MACHETE', 0),
	(379, 'steam:110000104a1abc7', 'icetea', 0),
	(380, 'steam:110000104a1abc7', 'honey_b', 0),
	(381, 'steam:110000104a1abc7', 'coke_pooch', 0),
	(382, 'steam:110000104a1abc7', 'weaponflashlight', 0),
	(383, 'steam:110000104a1abc7', 'WEAPON_RPG', 0),
	(384, 'steam:110000104a1abc7', 'WEAPON_HOMINGLAUNCHER', 0),
	(385, 'steam:110000104a1abc7', 'WEAPON_AUTOSHOTGUN', 0),
	(386, 'steam:110000104a1abc7', 'WEAPON_WRENCH', 0),
	(387, 'steam:110000104a1abc7', 'hatchet_lj', 0),
	(388, 'steam:110000104a1abc7', 'fabric', 0),
	(389, 'steam:110000104a1abc7', 'WEAPON_GOLFCLUB', 0),
	(390, 'steam:110000104a1abc7', 'handcuffs', 0),
	(391, 'steam:110000104a1abc7', 'WEAPON_STINGER', 0),
	(392, 'steam:110000104a1abc7', 'WEAPON_MACHINEPISTOL', 0),
	(393, 'steam:110000104a1abc7', 'hamburger', 0),
	(394, 'steam:110000104a1abc7', 'hamajifish', 0),
	(395, 'steam:110000104a1abc7', 'WEAPON_CROWBAR', 0),
	(396, 'steam:110000104a1abc7', 'Packaged_plank', 0),
	(397, 'steam:110000104a1abc7', 'WEAPON_COMBATPDW', 0),
	(398, 'steam:110000104a1abc7', 'anti', 0),
	(399, 'steam:110000104a1abc7', 'vodka', 0),
	(400, 'steam:110000104a1abc7', 'boxmilk', 0),
	(401, 'steam:110000104a1abc7', 'WEAPON_PIPEBOMB', 0),
	(402, 'steam:110000104a1abc7', 'meth_pooch', 0),
	(403, 'steam:110000104a1abc7', 'leather', 0),
	(404, 'steam:110000104a1abc7', 'fuel_a', 0),
	(405, 'steam:110000104a1abc7', 'blowpipe', 0),
	(406, 'steam:110000104a1abc7', 'fish', 0),
	(407, 'steam:110000104a1abc7', 'doubleaction_blueprint', 0),
	(408, 'steam:110000104a1abc7', 'AED', 0),
	(409, 'steam:110000104a1abc7', 'WEAPON_MARKSMANPISTOL', 0),
	(410, 'steam:110000104a1abc7', 'WEAPON_SNIPERRIFLE', 0),
	(411, 'steam:110000104a1abc7', 'WEAPON_FLASHLIGHT', 0),
	(412, 'steam:110000104a1abc7', 'WEAPON_CARBINERIFLE', 0),
	(413, 'steam:110000104a1abc7', 'WEAPON_SNOWBALL', 0),
	(414, 'steam:110000104a1abc7', 'WEAPON_PISTOL50', 0),
	(415, 'steam:110000104a1abc7', 'WEAPON_MG', 0),
	(416, 'steam:110000104a1abc7', 'WEAPON_HANDCUFFS', 0),
	(417, 'steam:110000104a1abc7', 'WEAPON_STICKYBOMB', 0),
	(418, 'steam:110000104a1abc7', 'SteelScrap', 0),
	(419, 'steam:110000104a1abc7', 'fixtool', 0),
	(420, 'steam:110000104a1abc7', 'diamond', 0),
	(421, 'steam:110000104a1abc7', 'WEAPON_ASSAULTSHOTGUN', 0),
	(422, 'steam:110000104a1abc7', 'WEAPON_MARKSMANRIFLE', 0),
	(423, 'steam:110000104a1abc7', 'diesel', 0),
	(424, 'steam:110000104a1abc7', 'croquettes', 0),
	(425, 'steam:110000104a1abc7', 'craftkit', 0),
	(426, 'steam:110000104a1abc7', 'WEAPON_DBSHOTGUN', 0),
	(427, 'steam:110000104a1abc7', 'knife_chicken', 0),
	(428, 'steam:110000104a1abc7', 'packaged_plank', 0),
	(429, 'steam:110000104a1abc7', 'WEAPON_MICROSMG', 0),
	(430, 'steam:110000104a1abc7', 'gunpowder', 0),
	(431, 'steam:110000104a1abc7', 'WEAPON_MINISMG', 0),
	(432, 'steam:110000104a1abc7', 'WEAPON_BOTTLE', 0),
	(433, 'steam:110000104a1abc7', 'coffe', 0),
	(434, 'steam:110000104a1abc7', 'bcabbage', 0),
	(435, 'steam:110000104a1abc7', 'WEAPON_FLAREGUN', 0),
	(436, 'steam:110000104a1abc7', 'WEAPON_FLARE', 0),
	(437, 'steam:110000104a1abc7', 'WEAPON_SMG', 0),
	(438, 'steam:110000104a1abc7', 'acabbage', 0),
	(439, 'steam:110000104a1abc7', 'WEAPON_MOLOTOV', 0),
	(440, 'steam:110000104a1abc7', 'WEAPON_COMBATMG', 0);
/*!40000 ALTER TABLE `user_inventory` ENABLE KEYS */;

-- Dumping structure for table essentialmode.user_licenses
CREATE TABLE IF NOT EXISTS `user_licenses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `owner` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=123 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.user_licenses: ~0 rows (approximately)
/*!40000 ALTER TABLE `user_licenses` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_licenses` ENABLE KEYS */;

-- Dumping structure for table essentialmode.user_parkings
CREATE TABLE IF NOT EXISTS `user_parkings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(60) DEFAULT NULL,
  `garage` varchar(60) DEFAULT NULL,
  `zone` int(11) NOT NULL,
  `vehicle` longtext DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.user_parkings: ~0 rows (approximately)
/*!40000 ALTER TABLE `user_parkings` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_parkings` ENABLE KEYS */;

-- Dumping structure for table essentialmode.user_whitelist
CREATE TABLE IF NOT EXISTS `user_whitelist` (
  `identifier` varchar(255) NOT NULL,
  PRIMARY KEY (`identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table essentialmode.user_whitelist: ~0 rows (approximately)
/*!40000 ALTER TABLE `user_whitelist` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_whitelist` ENABLE KEYS */;

-- Dumping structure for table essentialmode.vehicles
CREATE TABLE IF NOT EXISTS `vehicles` (
  `id` int(11) NOT NULL,
  `name` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `model` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `price` int(11) NOT NULL,
  `category` varchar(60) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.vehicles: ~84 rows (approximately)
/*!40000 ALTER TABLE `vehicles` DISABLE KEYS */;
INSERT INTO `vehicles` (`id`, `name`, `model`, `price`, `category`) VALUES
	(0, 'Adder   15kg.', 'adder ', 3200000, 'supercar'),
	(0, 'Akuma 5kg.', 'akuma', 16000, 'motorcycles'),
	(0, 'Asea  20kg.', 'asea', 48000, 'sedans'),
	(0, 'Asterope   20kg.', 'asterope ', 48000, 'sedans'),
	(0, 'Bagger 5kg.', 'bagger', 17000, 'motorcycles'),
	(0, 'Baller 40kg.', 'baller', 240000, 'suv'),
	(0, 'Baller2  40kg.', 'baller2', 320000, 'suv'),
	(0, 'Banshee  20kg.', 'banshee', 480000, 'sport'),
	(0, 'Bestiagts    20kg.', 'bestiagts', 480000, 'sport'),
	(0, 'Bf400  5kg.', 'bf400', 24000, 'motorcycles'),
	(0, 'Bison  40kg.', 'bison', 80000, 'offroad'),
	(0, 'BMX', 'bmx', 800, 'cycles'),
	(0, 'Bobcatxl  40kg.', 'bobcatxl', 80000, 'offroad'),
	(0, 'Bodhi2   40kg.', 'bodhi2', 80000, 'offroad'),
	(0, 'Brioso   20kg.', 'brioso ', 48000, 'sedans'),
	(0, 'Buffalo2  20kg.', 'buffalo2', 320000, 'muscle'),
	(0, 'Burrito2   40kg.', 'burrito2', 64000, 'vans'),
	(0, 'Burrito4  40kg.', 'burrito4', 64000, 'vans'),
	(0, 'Burrito5  40kg.', 'burrito5', 64000, 'vans'),
	(0, 'Cog55  20kg.', 'cog55 ', 56000, 'sedans'),
	(0, 'Comet3   20kg.', 'comet3', 290000, 'muscle'),
	(0, 'Coquette  20kg.', 'coquette', 320000, 'muscle'),
	(0, 'Cyclone 15kg.', 'cyclone', 4000000, 'supercar'),
	(0, 'Daemon  5kg.', 'daemon', 25000, 'motorcycles'),
	(0, 'Daemon2  5kg.', 'daemon2', 24000, 'motorcycles'),
	(0, 'Defiler   5kg.', 'defiler', 40000, 'motorcycles'),
	(0, 'Diablous  5kg.', 'diablous', 17000, 'motorcycles'),
	(0, 'Diablous2  5kg.', 'diablous2', 40000, 'motorcycles'),
	(0, 'Elegy   20kg.', 'elegy', 460000, 'muscle'),
	(0, 'Enduro 5kg.', 'enduro', 16000, 'motorcycles'),
	(0, 'Entityxf   15kg.', 'entityxf ', 3200000, 'supercar'),
	(0, 'Esskey 5kg.', 'esskey', 17000, 'motorcycles'),
	(0, 'Faggio  5kg.', 'faggio', 12000, 'motorcycles'),
	(0, 'Faggio3  5kg.', 'faggio3', 3000, 'รถเริ่มต้น'),
	(0, 'Fcr2   5kg.', 'fcr2', 17000, 'motorcycles'),
	(0, 'Fixter', 'fixter', 1200, 'cycles'),
	(0, 'Fq2   40kg.', 'fq2', 160000, 'suv'),
	(0, 'Fugitive  20kg.', 'fugitive ', 56000, 'sedans'),
	(0, 'Furoregt   20kg.', 'furoregt', 560000, 'sport'),
	(0, 'Gauntlet   20kg.', 'gauntlet', 240000, 'muscle'),
	(0, 'Gburrito2  40kg.', 'gburrito2', 64000, 'vans'),
	(0, 'Gresley  40kg.', 'gresley', 320000, 'suv'),
	(0, 'Intruder  20kg.', 'intruder ', 56000, 'sedans'),
	(0, 'Issi2   20kg.', 'issi2 ', 48000, 'sedans'),
	(0, 'Italigtb   15kg.', 'italigtb ', 4800000, 'supercar'),
	(0, 'Italigtb2   15kg.', 'italigtb2', 4800000, 'supercar'),
	(0, 'Jester   20kg.', 'jester', 720000, 'sport'),
	(0, 'Khamelion   20kg.', 'khamelion', 720000, 'sport'),
	(0, 'Massacro   20kg.', 'massacro', 560000, 'sport'),
	(0, 'Nemesis   5kg.', 'nemesis', 32000, 'motorcycles'),
	(0, 'Nero   15kg.', 'nero', 7200000, 'supercar'),
	(0, 'Nero2   15kg.', 'nero2', 7200000, 'supercar'),
	(0, 'Nightshade 20kg.', 'nightshade', 240000, 'muscle'),
	(0, 'Ninef   20kg.', 'ninef', 640000, 'sport'),
	(0, 'Panto   20kg.', 'panto ', 48000, 'sedans'),
	(0, 'Patriot  40kg.', 'patriot', 240000, 'suv'),
	(0, 'Pcj 5kg.', 'pcj', 16000, 'motorcycles'),
	(0, 'Pfister811  15kg.', 'pfister811', 4000000, 'supercar'),
	(0, 'Premier  20kg.', 'premier ', 48000, 'sedans'),
	(0, 'Primo2   20kg.', 'primo2 ', 56000, 'sedans'),
	(0, 'Raiden   20kg', 'raiden', 640000, 'sport'),
	(0, 'Ratloader   20kg.', 'ratloader', 16000, 'รถเริ่มต้น'),
	(0, 'Rebel  40kg.', 'rebel', 64000, 'offroad'),
	(0, 'Sabregt   20kg.', 'sabregt', 240000, 'muscle'),
	(0, 'Sanchez2  5kg.', 'sanchez2', 17000, 'motorcycles'),
	(0, 'Schlagen  20kg.', 'schlagen', 720000, 'sport'),
	(0, 'Scorcher', 'scorcher', 1200, 'cycles'),
	(0, 'sentinel3 20kg.', 'sentinel3', 240000, 'muscle'),
	(0, 'Seven70   20kg.', 'seven70', 640000, 'sport'),
	(0, 'Stanier   20kg.', 'stanier ', 40000, 'sedans'),
	(0, 'T20   15kg.', 't20', 6400000, 'supercar'),
	(0, 'Tornado3   20kg.', 'tornado3', 16000, 'รถเริ่มต้น'),
	(0, 'Tornado4   20kg.', 'tornado4', 16000, 'รถเริ่มต้น'),
	(0, 'Tribike2', 'tribike2', 1400, 'cycles'),
	(0, 'Turismor   15kg.', 'turismor', 7200000, 'supercar'),
	(0, 'Vacca     15kg.', 'vacca', 6400000, 'supercar'),
	(0, 'Vader 5kg.', 'vader', 32000, 'motorcycles'),
	(0, 'Vagner  15kg.', 'vagner', 5600000, 'supercar'),
	(0, 'Visione   15kg.', 'visione', 7200000, 'supercar'),
	(0, 'Voodoo2    20kg.', 'voodoo2', 16000, 'รถเริ่มต้น'),
	(0, 'Vortex   5kg.', 'vortex', 40000, 'motorcycles'),
	(0, 'Xa21  15kg.', 'xa21', 4000000, 'supercar'),
	(0, 'Xls2   40kg.', 'xls2 ', 400000, 'suv'),
	(0, 'Zentorno   15kg.', 'zentorno', 5600000, 'supercar');
/*!40000 ALTER TABLE `vehicles` ENABLE KEYS */;

-- Dumping structure for table essentialmode.vehicles_for_sale
CREATE TABLE IF NOT EXISTS `vehicles_for_sale` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seller` varchar(50) NOT NULL,
  `vehicleProps` longtext NOT NULL,
  `price` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=223 DEFAULT CHARSET=utf8;

-- Dumping data for table essentialmode.vehicles_for_sale: ~0 rows (approximately)
/*!40000 ALTER TABLE `vehicles_for_sale` DISABLE KEYS */;
/*!40000 ALTER TABLE `vehicles_for_sale` ENABLE KEYS */;

-- Dumping structure for table essentialmode.vehicle_categories
CREATE TABLE IF NOT EXISTS `vehicle_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.vehicle_categories: ~10 rows (approximately)
/*!40000 ALTER TABLE `vehicle_categories` DISABLE KEYS */;
INSERT INTO `vehicle_categories` (`id`, `name`, `label`) VALUES
	(0, 'cycles', 'จักรยาน'),
	(0, 'motorcycles', 'รถมอเตอร์ไซ'),
	(0, 'muscle', 'Muscle'),
	(0, 'offroad', 'รถกระบะ'),
	(0, 'sedans', 'รถเก๋ง'),
	(0, 'sport', 'Sports'),
	(0, 'supercar', 'Supercar'),
	(0, 'suv', 'SUV'),
	(0, 'vans', 'รถตู้'),
	(0, 'รถเริ่มต้น', 'รถเริ่มต้น');
/*!40000 ALTER TABLE `vehicle_categories` ENABLE KEYS */;

-- Dumping structure for table essentialmode.vehicle_sold
CREATE TABLE IF NOT EXISTS `vehicle_sold` (
  `client` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `model` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `plate` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `soldby` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `date` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`plate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.vehicle_sold: ~0 rows (approximately)
/*!40000 ALTER TABLE `vehicle_sold` DISABLE KEYS */;
/*!40000 ALTER TABLE `vehicle_sold` ENABLE KEYS */;

-- Dumping structure for table essentialmode.weashops
CREATE TABLE IF NOT EXISTS `weashops` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zone` varchar(255) NOT NULL,
  `item` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.weashops: ~40 rows (approximately)
/*!40000 ALTER TABLE `weashops` DISABLE KEYS */;
INSERT INTO `weashops` (`id`, `zone`, `item`, `price`) VALUES
	(1, 'GunShop', 'WEAPON_PISTOL', 300),
	(2, 'BlackWeashop', 'WEAPON_PISTOL', 500),
	(3, 'GunShop', 'WEAPON_FLASHLIGHT', 60),
	(4, 'BlackWeashop', 'WEAPON_FLASHLIGHT', 70),
	(5, 'GunShop', 'WEAPON_MACHETE', 90),
	(6, 'BlackWeashop', 'WEAPON_MACHETE', 110),
	(7, 'GunShop', 'WEAPON_NIGHTSTICK', 150),
	(8, 'BlackWeashop', 'WEAPON_NIGHTSTICK', 150),
	(9, 'GunShop', 'WEAPON_BAT', 100),
	(10, 'BlackWeashop', 'WEAPON_BAT', 100),
	(11, 'GunShop', 'WEAPON_STUNGUN', 50),
	(12, 'BlackWeashop', 'WEAPON_STUNGUN', 50),
	(13, 'GunShop', 'WEAPON_MICROSMG', 1400),
	(14, 'BlackWeashop', 'WEAPON_MICROSMG', 1700),
	(15, 'GunShop', 'WEAPON_PUMPSHOTGUN', 3400),
	(16, 'BlackWeashop', 'WEAPON_PUMPSHOTGUN', 3500),
	(17, 'GunShop', 'WEAPON_ASSAULTRIFLE', 10000),
	(18, 'BlackWeashop', 'WEAPON_ASSAULTRIFLE', 11000),
	(19, 'GunShop', 'WEAPON_SPECIALCARBINE', 15000),
	(20, 'BlackWeashop', 'WEAPON_SPECIALCARBINE', 16500),
	(21, 'GunShop', 'WEAPON_SNIPERRIFLE', 22000),
	(22, 'BlackWeashop', 'WEAPON_SNIPERRIFLE', 24000),
	(23, 'GunShop', 'WEAPON_FIREWORK', 18000),
	(24, 'BlackWeashop', 'WEAPON_FIREWORK', 20000),
	(25, 'GunShop', 'WEAPON_GRENADE', 500),
	(26, 'BlackWeashop', 'WEAPON_GRENADE', 650),
	(27, 'GunShop', 'WEAPON_BZGAS', 200),
	(28, 'BlackWeashop', 'WEAPON_BZGAS', 350),
	(29, 'GunShop', 'WEAPON_FIREEXTINGUISHER', 100),
	(30, 'BlackWeashop', 'WEAPON_FIREEXTINGUISHER', 100),
	(31, 'GunShop', 'WEAPON_BALL', 50),
	(32, 'BlackWeashop', 'WEAPON_BALL', 50),
	(33, 'GunShop', 'WEAPON_SMOKEGRENADE', 100),
	(34, 'BlackWeashop', 'WEAPON_SMOKEGRENADE', 100),
	(35, 'BlackWeashop', 'WEAPON_APPISTOL', 1100),
	(36, 'BlackWeashop', 'WEAPON_CARBINERIFLE', 12000),
	(37, 'BlackWeashop', 'WEAPON_HEAVYSNIPER', 30000),
	(38, 'BlackWeashop', 'WEAPON_MINIGUN', 45000),
	(39, 'BlackWeashop', 'WEAPON_RAILGUN', 50000),
	(40, 'BlackWeashop', 'WEAPON_STICKYBOMB', 500);
/*!40000 ALTER TABLE `weashops` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
