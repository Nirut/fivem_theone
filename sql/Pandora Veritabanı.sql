-- --------------------------------------------------------
-- Sunucu:                       127.0.0.1
-- Sunucu sürümü:                10.4.8-MariaDB - mariadb.org binary distribution
-- Sunucu İşletim Sistemi:       Win64
-- HeidiSQL Sürüm:               10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- essentialmode için veritabanı yapısı dökülüyor
CREATE DATABASE IF NOT EXISTS `essentialmode` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_turkish_ci */;
USE `essentialmode`;

-- tablo yapısı dökülüyor essentialmode.addon_account
CREATE TABLE IF NOT EXISTS `addon_account` (
  `name` varchar(60) COLLATE utf8_turkish_ci NOT NULL,
  `label` varchar(100) COLLATE utf8_turkish_ci NOT NULL,
  `shared` int(11) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

-- essentialmode.addon_account: ~9 rows (yaklaşık) tablosu için veriler indiriliyor
/*!40000 ALTER TABLE `addon_account` DISABLE KEYS */;
INSERT INTO `addon_account` (`name`, `label`, `shared`) VALUES
	('caution', 'Caution', 0),
	('society_ambulance', 'Ambulance', 1),
	('society_amekanik', 'A Mekanik', 1),
	('society_bmekanik', 'B Mekanik', 1),
	('society_cardealer', 'Concessionnaire', 1),
	('society_cete1', 'cete1', 1),
	('society_cete2', 'cete2', 1),
	('society_cete3', 'cete3', 1),
	('society_cete4', 'cete4', 1),
	('society_police', 'Police', 1),
	('society_taxi', 'Taxi', 1);
/*!40000 ALTER TABLE `addon_account` ENABLE KEYS */;

-- tablo yapısı dökülüyor essentialmode.addon_account_data
CREATE TABLE IF NOT EXISTS `addon_account_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_name` varchar(100) COLLATE utf8_turkish_ci DEFAULT NULL,
  `money` int(11) NOT NULL,
  `owner` varchar(100) COLLATE utf8_turkish_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_addon_account_data_account_name_owner` (`account_name`,`owner`),
  KEY `index_addon_account_data_account_name` (`account_name`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

-- essentialmode.addon_account_data: ~9 rows (yaklaşık) tablosu için veriler indiriliyor
/*!40000 ALTER TABLE `addon_account_data` DISABLE KEYS */;
INSERT INTO `addon_account_data` (`id`, `account_name`, `money`, `owner`) VALUES
	(1, 'society_amekanik', 0, NULL),
	(2, 'society_bmekanik', 0, NULL),
	(3, 'society_cardealer', 0, NULL),
	(4, 'society_cete1', 0, NULL),
	(5, 'society_cete2', 0, NULL),
	(6, 'society_cete3', 0, NULL),
	(7, 'society_cete4', 0, NULL),
	(8, 'society_police', 0, NULL),
	(9, 'society_taxi', 0, NULL),
	(10, 'caution', 0, 'steam:11000013dc8c976'),
	(11, 'society_ambulance', 0, NULL);
/*!40000 ALTER TABLE `addon_account_data` ENABLE KEYS */;

-- tablo yapısı dökülüyor essentialmode.addon_inventory
CREATE TABLE IF NOT EXISTS `addon_inventory` (
  `name` varchar(60) COLLATE utf8_turkish_ci NOT NULL,
  `label` varchar(100) COLLATE utf8_turkish_ci NOT NULL,
  `shared` int(11) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

-- essentialmode.addon_inventory: ~8 rows (yaklaşık) tablosu için veriler indiriliyor
/*!40000 ALTER TABLE `addon_inventory` DISABLE KEYS */;
INSERT INTO `addon_inventory` (`name`, `label`, `shared`) VALUES
	('society_ambulance', 'Ambulance', 1),
	('society_amekanik', 'A Mekanik', 1),
	('society_bmekanik', 'B Mekanik', 1),
	('society_cardealer', 'Concesionnaire', 1),
	('society_cete1', 'cete1', 1),
	('society_cete2', 'cete2', 1),
	('society_cete3', 'cete3', 1),
	('society_cete4', 'cete4', 1),
	('society_police', 'Police', 1),
	('society_taxi', 'Taxi', 1);
/*!40000 ALTER TABLE `addon_inventory` ENABLE KEYS */;

-- tablo yapısı dökülüyor essentialmode.addon_inventory_items
CREATE TABLE IF NOT EXISTS `addon_inventory_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inventory_name` varchar(100) COLLATE utf8_turkish_ci NOT NULL,
  `name` varchar(100) COLLATE utf8_turkish_ci NOT NULL,
  `count` int(11) NOT NULL,
  `owner` varchar(60) COLLATE utf8_turkish_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_addon_inventory_items_inventory_name_name` (`inventory_name`,`name`),
  KEY `index_addon_inventory_items_inventory_name_name_owner` (`inventory_name`,`name`,`owner`),
  KEY `index_addon_inventory_inventory_name` (`inventory_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

-- essentialmode.addon_inventory_items: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
/*!40000 ALTER TABLE `addon_inventory_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `addon_inventory_items` ENABLE KEYS */;

-- tablo yapısı dökülüyor essentialmode.billing
CREATE TABLE IF NOT EXISTS `billing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(255) COLLATE utf8_turkish_ci NOT NULL,
  `sender` varchar(255) COLLATE utf8_turkish_ci NOT NULL,
  `target_type` varchar(50) COLLATE utf8_turkish_ci NOT NULL,
  `target` varchar(255) COLLATE utf8_turkish_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_turkish_ci NOT NULL,
  `amount` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

-- essentialmode.billing: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
/*!40000 ALTER TABLE `billing` DISABLE KEYS */;
/*!40000 ALTER TABLE `billing` ENABLE KEYS */;

-- tablo yapısı dökülüyor essentialmode.cardealer_vehicles
CREATE TABLE IF NOT EXISTS `cardealer_vehicles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vehicle` varchar(255) COLLATE utf8_turkish_ci NOT NULL,
  `price` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

-- essentialmode.cardealer_vehicles: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
/*!40000 ALTER TABLE `cardealer_vehicles` DISABLE KEYS */;
/*!40000 ALTER TABLE `cardealer_vehicles` ENABLE KEYS */;

-- tablo yapısı dökülüyor essentialmode.characters
CREATE TABLE IF NOT EXISTS `characters` (
  `identifier` varchar(255) COLLATE utf8_turkish_ci NOT NULL,
  `firstname` varchar(255) COLLATE utf8_turkish_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8_turkish_ci NOT NULL,
  `dateofbirth` varchar(255) COLLATE utf8_turkish_ci NOT NULL,
  `sex` varchar(1) COLLATE utf8_turkish_ci NOT NULL DEFAULT 'f',
  `height` varchar(128) COLLATE utf8_turkish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

-- essentialmode.characters: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
/*!40000 ALTER TABLE `characters` DISABLE KEYS */;
INSERT INTO `characters` (`identifier`, `firstname`, `lastname`, `dateofbirth`, `sex`, `height`) VALUES
	('steam:11000013dc8c976', 'Bekir', 'Can', '17-07-1992', 'm', '67');
/*!40000 ALTER TABLE `characters` ENABLE KEYS */;

-- tablo yapısı dökülüyor essentialmode.datastore
CREATE TABLE IF NOT EXISTS `datastore` (
  `name` varchar(60) COLLATE utf8_turkish_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_turkish_ci NOT NULL,
  `shared` int(11) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

-- essentialmode.datastore: ~6 rows (yaklaşık) tablosu için veriler indiriliyor
/*!40000 ALTER TABLE `datastore` DISABLE KEYS */;
INSERT INTO `datastore` (`name`, `label`, `shared`) VALUES
	('society_ambulance', 'Ambulance', 1),
	('society_cete1', 'cete1', 1),
	('society_cete2', 'cete2', 1),
	('society_cete3', 'cete3', 1),
	('society_cete4', 'cete4', 1),
	('society_police', 'Police', 1),
	('society_taxi', 'Taxi', 1);
/*!40000 ALTER TABLE `datastore` ENABLE KEYS */;

-- tablo yapısı dökülüyor essentialmode.datastore_data
CREATE TABLE IF NOT EXISTS `datastore_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) COLLATE utf8_turkish_ci NOT NULL,
  `owner` varchar(60) COLLATE utf8_turkish_ci DEFAULT NULL,
  `data` longtext COLLATE utf8_turkish_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_datastore_owner_name` (`owner`,`name`),
  KEY `index_datastore_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

-- essentialmode.datastore_data: ~6 rows (yaklaşık) tablosu için veriler indiriliyor
/*!40000 ALTER TABLE `datastore_data` DISABLE KEYS */;
INSERT INTO `datastore_data` (`id`, `name`, `owner`, `data`) VALUES
	(1, 'society_cete1', NULL, '{}'),
	(2, 'society_cete2', NULL, '{}'),
	(3, 'society_cete3', NULL, '{}'),
	(4, 'society_cete4', NULL, '{}'),
	(5, 'society_police', NULL, '{}'),
	(6, 'society_taxi', NULL, '{}'),
	(7, 'society_ambulance', NULL, '{}');
/*!40000 ALTER TABLE `datastore_data` ENABLE KEYS */;

-- tablo yapısı dökülüyor essentialmode.fine_types
CREATE TABLE IF NOT EXISTS `fine_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) COLLATE utf8_turkish_ci DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

-- essentialmode.fine_types: ~52 rows (yaklaşık) tablosu için veriler indiriliyor
/*!40000 ALTER TABLE `fine_types` DISABLE KEYS */;
INSERT INTO `fine_types` (`id`, `label`, `amount`, `category`) VALUES
	(1, 'Usage abusif du klaxon', 30, 0),
	(2, 'Franchir une ligne continue', 40, 0),
	(3, 'Circulation à contresens', 250, 0),
	(4, 'Demi-tour non autorisé', 250, 0),
	(5, 'Circulation hors-route', 170, 0),
	(6, 'Non-respect des distances de sécurité', 30, 0),
	(7, 'Arrêt dangereux / interdit', 150, 0),
	(8, 'Stationnement gênant / interdit', 70, 0),
	(9, 'Non respect  de la priorité à droite', 70, 0),
	(10, 'Non-respect à un véhicule prioritaire', 90, 0),
	(11, 'Non-respect d\'un stop', 105, 0),
	(12, 'Non-respect d\'un feu rouge', 130, 0),
	(13, 'Dépassement dangereux', 100, 0),
	(14, 'Véhicule non en état', 100, 0),
	(15, 'Conduite sans permis', 1500, 0),
	(16, 'Délit de fuite', 800, 0),
	(17, 'Excès de vitesse < 5 kmh', 90, 0),
	(18, 'Excès de vitesse 5-15 kmh', 120, 0),
	(19, 'Excès de vitesse 15-30 kmh', 180, 0),
	(20, 'Excès de vitesse > 30 kmh', 300, 0),
	(21, 'Entrave de la circulation', 110, 1),
	(22, 'Dégradation de la voie publique', 90, 1),
	(23, 'Trouble à l\'ordre publique', 90, 1),
	(24, 'Entrave opération de police', 130, 1),
	(25, 'Insulte envers / entre civils', 75, 1),
	(26, 'Outrage à agent de police', 110, 1),
	(27, 'Menace verbale ou intimidation envers civil', 90, 1),
	(28, 'Menace verbale ou intimidation envers policier', 150, 1),
	(29, 'Manifestation illégale', 250, 1),
	(30, 'Tentative de corruption', 1500, 1),
	(31, 'Arme blanche sortie en ville', 120, 2),
	(32, 'Arme léthale sortie en ville', 300, 2),
	(33, 'Port d\'arme non autorisé (défaut de license)', 600, 2),
	(34, 'Port d\'arme illégal', 700, 2),
	(35, 'Pris en flag lockpick', 300, 2),
	(36, 'Vol de voiture', 1800, 2),
	(37, 'Vente de drogue', 1500, 2),
	(38, 'Fabriquation de drogue', 1500, 2),
	(39, 'Possession de drogue', 650, 2),
	(40, 'Prise d\'ôtage civil', 1500, 2),
	(41, 'Prise d\'ôtage agent de l\'état', 2000, 2),
	(42, 'Braquage particulier', 650, 2),
	(43, 'Braquage magasin', 650, 2),
	(44, 'Braquage de banque', 1500, 2),
	(45, 'Tir sur civil', 2000, 3),
	(46, 'Tir sur agent de l\'état', 2500, 3),
	(47, 'Tentative de meurtre sur civil', 3000, 3),
	(48, 'Tentative de meurtre sur agent de l\'état', 5000, 3),
	(49, 'Meurtre sur civil', 10000, 3),
	(50, 'Meurte sur agent de l\'état', 30000, 3),
	(51, 'Meurtre involontaire', 1800, 3),
	(52, 'Escroquerie à l\'entreprise', 2000, 2);
/*!40000 ALTER TABLE `fine_types` ENABLE KEYS */;

-- tablo yapısı dökülüyor essentialmode.fine_types_cete1
CREATE TABLE IF NOT EXISTS `fine_types_cete1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) COLLATE utf8_turkish_ci DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

-- essentialmode.fine_types_cete1: ~10 rows (yaklaşık) tablosu için veriler indiriliyor
/*!40000 ALTER TABLE `fine_types_cete1` DISABLE KEYS */;
INSERT INTO `fine_types_cete1` (`id`, `label`, `amount`, `category`) VALUES
	(1, 'Haraç', 10000, 0),
	(2, 'Haraç', 20000, 0),
	(3, 'Haraç', 30000, 0),
	(4, 'Haraç', 40000, 0),
	(5, 'Haraç', 50000, 0),
	(6, 'Haraç', 60000, 0),
	(7, 'Haraç', 70000, 0),
	(8, 'Haraç', 80000, 0),
	(9, 'Haraç', 90000, 0),
	(10, 'Haraç', 100000, 0);
/*!40000 ALTER TABLE `fine_types_cete1` ENABLE KEYS */;

-- tablo yapısı dökülüyor essentialmode.fine_types_cete2
CREATE TABLE IF NOT EXISTS `fine_types_cete2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) COLLATE utf8_turkish_ci DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

-- essentialmode.fine_types_cete2: ~10 rows (yaklaşık) tablosu için veriler indiriliyor
/*!40000 ALTER TABLE `fine_types_cete2` DISABLE KEYS */;
INSERT INTO `fine_types_cete2` (`id`, `label`, `amount`, `category`) VALUES
	(1, 'Haraç', 10000, 0),
	(2, 'Haraç', 20000, 0),
	(3, 'Haraç', 30000, 0),
	(4, 'Haraç', 40000, 0),
	(5, 'Haraç', 50000, 0),
	(6, 'Haraç', 60000, 0),
	(7, 'Haraç', 70000, 0),
	(8, 'Haraç', 80000, 0),
	(9, 'Haraç', 90000, 0),
	(10, 'Haraç', 100000, 0);
/*!40000 ALTER TABLE `fine_types_cete2` ENABLE KEYS */;

-- tablo yapısı dökülüyor essentialmode.fine_types_cete3
CREATE TABLE IF NOT EXISTS `fine_types_cete3` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) COLLATE utf8_turkish_ci DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

-- essentialmode.fine_types_cete3: ~10 rows (yaklaşık) tablosu için veriler indiriliyor
/*!40000 ALTER TABLE `fine_types_cete3` DISABLE KEYS */;
INSERT INTO `fine_types_cete3` (`id`, `label`, `amount`, `category`) VALUES
	(1, 'Haraç', 10000, 0),
	(2, 'Haraç', 20000, 0),
	(3, 'Haraç', 30000, 0),
	(4, 'Haraç', 40000, 0),
	(5, 'Haraç', 50000, 0),
	(6, 'Haraç', 60000, 0),
	(7, 'Haraç', 70000, 0),
	(8, 'Haraç', 80000, 0),
	(9, 'Haraç', 90000, 0),
	(10, 'Haraç', 100000, 0);
/*!40000 ALTER TABLE `fine_types_cete3` ENABLE KEYS */;

-- tablo yapısı dökülüyor essentialmode.fine_types_cete4
CREATE TABLE IF NOT EXISTS `fine_types_cete4` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) COLLATE utf8_turkish_ci DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

-- essentialmode.fine_types_cete4: ~10 rows (yaklaşık) tablosu için veriler indiriliyor
/*!40000 ALTER TABLE `fine_types_cete4` DISABLE KEYS */;
INSERT INTO `fine_types_cete4` (`id`, `label`, `amount`, `category`) VALUES
	(1, 'Haraç', 10000, 0),
	(2, 'Haraç', 20000, 0),
	(3, 'Haraç', 30000, 0),
	(4, 'Haraç', 40000, 0),
	(5, 'Haraç', 50000, 0),
	(6, 'Haraç', 60000, 0),
	(7, 'Haraç', 70000, 0),
	(8, 'Haraç', 80000, 0),
	(9, 'Haraç', 90000, 0),
	(10, 'Haraç', 100000, 0);
/*!40000 ALTER TABLE `fine_types_cete4` ENABLE KEYS */;

-- tablo yapısı dökülüyor essentialmode.items
CREATE TABLE IF NOT EXISTS `items` (
  `name` varchar(50) COLLATE utf8_turkish_ci NOT NULL,
  `label` varchar(50) COLLATE utf8_turkish_ci NOT NULL,
  `limit` int(11) NOT NULL DEFAULT -1,
  `rare` int(11) NOT NULL DEFAULT 0,
  `can_remove` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

-- essentialmode.items: ~21 rows (yaklaşık) tablosu için veriler indiriliyor
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
INSERT INTO `items` (`name`, `label`, `limit`, `rare`, `can_remove`) VALUES
	('alive_chicken', 'Poulet vivant', 20, 0, 1),
	('bandage', 'Bandaj', 20, 0, 1),
	('bread', 'Pain', 10, 0, 1),
	('clothe', 'Vêtement', 40, 0, 1),
	('copper', 'Cuivre', 56, 0, 1),
	('croquettes', 'Croquettes', -1, 0, 1),
	('cutted_wood', 'Bois coupé', 20, 0, 1),
	('diamond', 'Diamant', 50, 0, 1),
	('essence', 'Essence', 24, 0, 1),
	('fabric', 'Tissu', 80, 0, 1),
	('fish', 'Poisson', 100, 0, 1),
	('gold', 'Or', 21, 0, 1),
	('iron', 'Fer', 42, 0, 1),
	('medikit', 'Medkit', 5, 0, 1),
	('packaged_chicken', 'Poulet en barquette', 100, 0, 1),
	('packaged_plank', 'Paquet de planches', 100, 0, 1),
	('petrol', 'Pétrole', 24, 0, 1),
	('petrol_raffin', 'Pétrole Raffiné', 24, 0, 1),
	('phone', 'phone', 20, 0, 1),
	('slaughtered_chicken', 'Poulet abattu', 20, 0, 1),
	('stone', 'Pierre', 7, 0, 1),
	('washed_stone', 'Pierre Lavée', 7, 0, 1),
	('water', 'Eau', 5, 0, 1),
	('wood', 'Bois', 20, 0, 1),
	('wool', 'Laine', 40, 0, 1);
/*!40000 ALTER TABLE `items` ENABLE KEYS */;

-- tablo yapısı dökülüyor essentialmode.jobs
CREATE TABLE IF NOT EXISTS `jobs` (
  `name` varchar(50) COLLATE utf8_turkish_ci NOT NULL,
  `label` varchar(50) COLLATE utf8_turkish_ci DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

-- essentialmode.jobs: ~15 rows (yaklaşık) tablosu için veriler indiriliyor
/*!40000 ALTER TABLE `jobs` DISABLE KEYS */;
INSERT INTO `jobs` (`name`, `label`) VALUES
	('ambulance', 'Ambulance'),
	('amekanik', 'A Mekanik'),
	('bmekanik', 'B Mekanik'),
	('cardealer', 'Concessionnaire'),
	('cete1', 'Çete Adı Girin'),
	('cete2', 'Çete Adı Girin'),
	('cete3', 'Çete Adı Girin'),
	('cete4', 'Çete Adı Girin'),
	('fisherman', 'Pêcheur'),
	('fueler', 'Raffineur'),
	('lumberjack', 'Bûcheron'),
	('miner', 'Mineur'),
	('police', 'LSPD'),
	('reporter', 'Journaliste'),
	('slaughterer', 'Abatteur'),
	('tailor', 'Couturier'),
	('taxi', 'Taxi'),
	('unemployed', 'Unemployed');
/*!40000 ALTER TABLE `jobs` ENABLE KEYS */;

-- tablo yapısı dökülüyor essentialmode.job_grades
CREATE TABLE IF NOT EXISTS `job_grades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_name` varchar(50) COLLATE utf8_turkish_ci DEFAULT NULL,
  `grade` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_turkish_ci NOT NULL,
  `label` varchar(50) COLLATE utf8_turkish_ci NOT NULL,
  `salary` int(11) NOT NULL,
  `skin_male` longtext COLLATE utf8_turkish_ci NOT NULL,
  `skin_female` longtext COLLATE utf8_turkish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

-- essentialmode.job_grades: ~48 rows (yaklaşık) tablosu için veriler indiriliyor
/*!40000 ALTER TABLE `job_grades` DISABLE KEYS */;
INSERT INTO `job_grades` (`id`, `job_name`, `grade`, `name`, `label`, `salary`, `skin_male`, `skin_female`) VALUES
	(1, 'unemployed', 0, 'unemployed', 'Unemployed', 200, '{}', '{}'),
	(2, 'amekanik', 0, 'recrue', 'Çalışan', 2000, '{}', '{}'),
	(3, 'amekanik', 1, 'novice', 'Teknik Çalışan', 3000, '{}', '{}'),
	(4, 'amekanik', 2, 'experimente', 'Şef', 3500, '{}', '{}'),
	(5, 'amekanik', 3, 'chief', 'Patron Yard.', 4000, '{}', '{}'),
	(6, 'amekanik', 4, 'boss', 'Patron', 0, '{}', '{}'),
	(7, 'bmekanik', 0, 'recrue', 'Çalışan', 2000, '{}', '{}'),
	(8, 'bmekanik', 1, 'novice', 'Teknik Çalışan', 3000, '{}', '{}'),
	(9, 'bmekanik', 2, 'experimente', 'Şef', 3500, '{}', '{}'),
	(10, 'bmekanik', 3, 'chief', 'Patron Yard.', 4000, '{}', '{}'),
	(11, 'bmekanik', 4, 'boss', 'Patron', 0, '{}', '{}'),
	(12, 'cete1', 0, 'recruit', 'Yeni Üye', 1500, '{}', '{}'),
	(13, 'cete1', 1, 'officer', 'Üye', 2500, '{}', '{}'),
	(14, 'cete1', 2, 'sergeant', 'Yetkili Üye', 3000, '{}', '{}'),
	(15, 'cete1', 3, 'lieutenant', 'Lider Yard.', 3500, '{}', '{}'),
	(16, 'cete1', 4, 'boss', 'Çete Lideri', 4000, '{}', '{}'),
	(17, 'cete2', 0, 'recruit', 'Yeni Üye', 1500, '{}', '{}'),
	(18, 'cete2', 1, 'officer', 'Üye', 2500, '{}', '{}'),
	(19, 'cete2', 2, 'sergeant', 'Yetkili Üye', 3000, '{}', '{}'),
	(20, 'cete2', 3, 'lieutenant', 'Lider Yard.', 3500, '{}', '{}'),
	(21, 'cete2', 4, 'boss', 'Çete Lideri', 4000, '{}', '{}'),
	(22, 'cete3', 0, 'recruit', 'Yeni Üye', 1500, '{}', '{}'),
	(23, 'cete3', 1, 'officer', 'Üye', 2500, '{}', '{}'),
	(24, 'cete3', 2, 'sergeant', 'Yetkili Üye', 3000, '{}', '{}'),
	(25, 'cete3', 3, 'lieutenant', 'Lider Yard.', 3500, '{}', '{}'),
	(26, 'cete3', 4, 'boss', 'Çete Lideri', 4000, '{}', '{}'),
	(27, 'cete4', 0, 'recruit', 'Yeni Üye', 1500, '{}', '{}'),
	(28, 'cete4', 1, 'officer', 'Üye', 2500, '{}', '{}'),
	(29, 'cete4', 2, 'sergeant', 'Yetkili Üye', 3000, '{}', '{}'),
	(30, 'cete4', 3, 'lieutenant', 'Lider Yard.', 3500, '{}', '{}'),
	(31, 'cete4', 4, 'boss', 'Çete Lideri', 4000, '{}', '{}'),
	(32, 'lumberjack', 0, 'employee', 'Intérimaire', 0, '{}', '{}'),
	(33, 'fisherman', 0, 'employee', 'Intérimaire', 0, '{}', '{}'),
	(34, 'fueler', 0, 'employee', 'Intérimaire', 0, '{}', '{}'),
	(35, 'reporter', 0, 'employee', 'Intérimaire', 0, '{}', '{}'),
	(36, 'tailor', 0, 'employee', 'Intérimaire', 0, '{"mask_1":0,"arms":1,"glasses_1":0,"hair_color_2":4,"makeup_1":0,"face":19,"glasses":0,"mask_2":0,"makeup_3":0,"skin":29,"helmet_2":0,"lipstick_4":0,"sex":0,"torso_1":24,"makeup_2":0,"bags_2":0,"chain_2":0,"ears_1":-1,"bags_1":0,"bproof_1":0,"shoes_2":0,"lipstick_2":0,"chain_1":0,"tshirt_1":0,"eyebrows_3":0,"pants_2":0,"beard_4":0,"torso_2":0,"beard_2":6,"ears_2":0,"hair_2":0,"shoes_1":36,"tshirt_2":0,"beard_3":0,"hair_1":2,"hair_color_1":0,"pants_1":48,"helmet_1":-1,"bproof_2":0,"eyebrows_4":0,"eyebrows_2":0,"decals_1":0,"age_2":0,"beard_1":5,"shoes":10,"lipstick_1":0,"eyebrows_1":0,"glasses_2":0,"makeup_4":0,"decals_2":0,"lipstick_3":0,"age_1":0}', '{"mask_1":0,"arms":5,"glasses_1":5,"hair_color_2":4,"makeup_1":0,"face":19,"glasses":0,"mask_2":0,"makeup_3":0,"skin":29,"helmet_2":0,"lipstick_4":0,"sex":1,"torso_1":52,"makeup_2":0,"bags_2":0,"chain_2":0,"ears_1":-1,"bags_1":0,"bproof_1":0,"shoes_2":1,"lipstick_2":0,"chain_1":0,"tshirt_1":23,"eyebrows_3":0,"pants_2":0,"beard_4":0,"torso_2":0,"beard_2":6,"ears_2":0,"hair_2":0,"shoes_1":42,"tshirt_2":4,"beard_3":0,"hair_1":2,"hair_color_1":0,"pants_1":36,"helmet_1":-1,"bproof_2":0,"eyebrows_4":0,"eyebrows_2":0,"decals_1":0,"age_2":0,"beard_1":5,"shoes":10,"lipstick_1":0,"eyebrows_1":0,"glasses_2":0,"makeup_4":0,"decals_2":0,"lipstick_3":0,"age_1":0}'),
	(37, 'miner', 0, 'employee', 'Intérimaire', 0, '{"tshirt_2":1,"ears_1":8,"glasses_1":15,"torso_2":0,"ears_2":2,"glasses_2":3,"shoes_2":1,"pants_1":75,"shoes_1":51,"bags_1":0,"helmet_2":0,"pants_2":7,"torso_1":71,"tshirt_1":59,"arms":2,"bags_2":0,"helmet_1":0}', '{}'),
	(38, 'slaughterer', 0, 'employee', 'Intérimaire', 0, '{"age_1":0,"glasses_2":0,"beard_1":5,"decals_2":0,"beard_4":0,"shoes_2":0,"tshirt_2":0,"lipstick_2":0,"hair_2":0,"arms":67,"pants_1":36,"skin":29,"eyebrows_2":0,"shoes":10,"helmet_1":-1,"lipstick_1":0,"helmet_2":0,"hair_color_1":0,"glasses":0,"makeup_4":0,"makeup_1":0,"hair_1":2,"bproof_1":0,"bags_1":0,"mask_1":0,"lipstick_3":0,"chain_1":0,"eyebrows_4":0,"sex":0,"torso_1":56,"beard_2":6,"shoes_1":12,"decals_1":0,"face":19,"lipstick_4":0,"tshirt_1":15,"mask_2":0,"age_2":0,"eyebrows_3":0,"chain_2":0,"glasses_1":0,"ears_1":-1,"bags_2":0,"ears_2":0,"torso_2":0,"bproof_2":0,"makeup_2":0,"eyebrows_1":0,"makeup_3":0,"pants_2":0,"beard_3":0,"hair_color_2":4}', '{"age_1":0,"glasses_2":0,"beard_1":5,"decals_2":0,"beard_4":0,"shoes_2":0,"tshirt_2":0,"lipstick_2":0,"hair_2":0,"arms":72,"pants_1":45,"skin":29,"eyebrows_2":0,"shoes":10,"helmet_1":-1,"lipstick_1":0,"helmet_2":0,"hair_color_1":0,"glasses":0,"makeup_4":0,"makeup_1":0,"hair_1":2,"bproof_1":0,"bags_1":0,"mask_1":0,"lipstick_3":0,"chain_1":0,"eyebrows_4":0,"sex":1,"torso_1":49,"beard_2":6,"shoes_1":24,"decals_1":0,"face":19,"lipstick_4":0,"tshirt_1":9,"mask_2":0,"age_2":0,"eyebrows_3":0,"chain_2":0,"glasses_1":5,"ears_1":-1,"bags_2":0,"ears_2":0,"torso_2":0,"bproof_2":0,"makeup_2":0,"eyebrows_1":0,"makeup_3":0,"pants_2":0,"beard_3":0,"hair_color_2":4}'),
	(39, 'police', 0, 'recruit', 'Recrue', 20, '{}', '{}'),
	(40, 'police', 1, 'officer', 'Officier', 40, '{}', '{}'),
	(41, 'police', 2, 'sergeant', 'Sergent', 60, '{}', '{}'),
	(42, 'police', 3, 'lieutenant', 'Lieutenant', 85, '{}', '{}'),
	(43, 'police', 4, 'boss', 'Commandant', 100, '{}', '{}'),
	(44, 'taxi', 0, 'recrue', 'Recrue', 12, '{"hair_2":0,"hair_color_2":0,"torso_1":32,"bags_1":0,"helmet_2":0,"chain_2":0,"eyebrows_3":0,"makeup_3":0,"makeup_2":0,"tshirt_1":31,"makeup_1":0,"bags_2":0,"makeup_4":0,"eyebrows_4":0,"chain_1":0,"lipstick_4":0,"bproof_2":0,"hair_color_1":0,"decals_2":0,"pants_2":0,"age_2":0,"glasses_2":0,"ears_2":0,"arms":27,"lipstick_1":0,"ears_1":-1,"mask_2":0,"sex":0,"lipstick_3":0,"helmet_1":-1,"shoes_2":0,"beard_2":0,"beard_1":0,"lipstick_2":0,"beard_4":0,"glasses_1":0,"bproof_1":0,"mask_1":0,"decals_1":1,"hair_1":0,"eyebrows_2":0,"beard_3":0,"age_1":0,"tshirt_2":0,"skin":0,"torso_2":0,"eyebrows_1":0,"face":0,"shoes_1":10,"pants_1":24}', '{"hair_2":0,"hair_color_2":0,"torso_1":57,"bags_1":0,"helmet_2":0,"chain_2":0,"eyebrows_3":0,"makeup_3":0,"makeup_2":0,"tshirt_1":38,"makeup_1":0,"bags_2":0,"makeup_4":0,"eyebrows_4":0,"chain_1":0,"lipstick_4":0,"bproof_2":0,"hair_color_1":0,"decals_2":0,"pants_2":1,"age_2":0,"glasses_2":0,"ears_2":0,"arms":21,"lipstick_1":0,"ears_1":-1,"mask_2":0,"sex":1,"lipstick_3":0,"helmet_1":-1,"shoes_2":0,"beard_2":0,"beard_1":0,"lipstick_2":0,"beard_4":0,"glasses_1":5,"bproof_1":0,"mask_1":0,"decals_1":1,"hair_1":0,"eyebrows_2":0,"beard_3":0,"age_1":0,"tshirt_2":0,"skin":0,"torso_2":0,"eyebrows_1":0,"face":0,"shoes_1":49,"pants_1":11}'),
	(45, 'taxi', 1, 'novice', 'Novice', 24, '{"hair_2":0,"hair_color_2":0,"torso_1":32,"bags_1":0,"helmet_2":0,"chain_2":0,"eyebrows_3":0,"makeup_3":0,"makeup_2":0,"tshirt_1":31,"makeup_1":0,"bags_2":0,"makeup_4":0,"eyebrows_4":0,"chain_1":0,"lipstick_4":0,"bproof_2":0,"hair_color_1":0,"decals_2":0,"pants_2":0,"age_2":0,"glasses_2":0,"ears_2":0,"arms":27,"lipstick_1":0,"ears_1":-1,"mask_2":0,"sex":0,"lipstick_3":0,"helmet_1":-1,"shoes_2":0,"beard_2":0,"beard_1":0,"lipstick_2":0,"beard_4":0,"glasses_1":0,"bproof_1":0,"mask_1":0,"decals_1":1,"hair_1":0,"eyebrows_2":0,"beard_3":0,"age_1":0,"tshirt_2":0,"skin":0,"torso_2":0,"eyebrows_1":0,"face":0,"shoes_1":10,"pants_1":24}', '{"hair_2":0,"hair_color_2":0,"torso_1":57,"bags_1":0,"helmet_2":0,"chain_2":0,"eyebrows_3":0,"makeup_3":0,"makeup_2":0,"tshirt_1":38,"makeup_1":0,"bags_2":0,"makeup_4":0,"eyebrows_4":0,"chain_1":0,"lipstick_4":0,"bproof_2":0,"hair_color_1":0,"decals_2":0,"pants_2":1,"age_2":0,"glasses_2":0,"ears_2":0,"arms":21,"lipstick_1":0,"ears_1":-1,"mask_2":0,"sex":1,"lipstick_3":0,"helmet_1":-1,"shoes_2":0,"beard_2":0,"beard_1":0,"lipstick_2":0,"beard_4":0,"glasses_1":5,"bproof_1":0,"mask_1":0,"decals_1":1,"hair_1":0,"eyebrows_2":0,"beard_3":0,"age_1":0,"tshirt_2":0,"skin":0,"torso_2":0,"eyebrows_1":0,"face":0,"shoes_1":49,"pants_1":11}'),
	(46, 'taxi', 2, 'experimente', 'Experimente', 36, '{"hair_2":0,"hair_color_2":0,"torso_1":26,"bags_1":0,"helmet_2":0,"chain_2":0,"eyebrows_3":0,"makeup_3":0,"makeup_2":0,"tshirt_1":57,"makeup_1":0,"bags_2":0,"makeup_4":0,"eyebrows_4":0,"chain_1":0,"lipstick_4":0,"bproof_2":0,"hair_color_1":0,"decals_2":0,"pants_2":4,"age_2":0,"glasses_2":0,"ears_2":0,"arms":11,"lipstick_1":0,"ears_1":-1,"mask_2":0,"sex":0,"lipstick_3":0,"helmet_1":-1,"shoes_2":0,"beard_2":0,"beard_1":0,"lipstick_2":0,"beard_4":0,"glasses_1":0,"bproof_1":0,"mask_1":0,"decals_1":0,"hair_1":0,"eyebrows_2":0,"beard_3":0,"age_1":0,"tshirt_2":0,"skin":0,"torso_2":0,"eyebrows_1":0,"face":0,"shoes_1":10,"pants_1":24}', '{"hair_2":0,"hair_color_2":0,"torso_1":57,"bags_1":0,"helmet_2":0,"chain_2":0,"eyebrows_3":0,"makeup_3":0,"makeup_2":0,"tshirt_1":38,"makeup_1":0,"bags_2":0,"makeup_4":0,"eyebrows_4":0,"chain_1":0,"lipstick_4":0,"bproof_2":0,"hair_color_1":0,"decals_2":0,"pants_2":1,"age_2":0,"glasses_2":0,"ears_2":0,"arms":21,"lipstick_1":0,"ears_1":-1,"mask_2":0,"sex":1,"lipstick_3":0,"helmet_1":-1,"shoes_2":0,"beard_2":0,"beard_1":0,"lipstick_2":0,"beard_4":0,"glasses_1":5,"bproof_1":0,"mask_1":0,"decals_1":1,"hair_1":0,"eyebrows_2":0,"beard_3":0,"age_1":0,"tshirt_2":0,"skin":0,"torso_2":0,"eyebrows_1":0,"face":0,"shoes_1":49,"pants_1":11}'),
	(47, 'taxi', 3, 'uber', 'Uber', 48, '{"hair_2":0,"hair_color_2":0,"torso_1":26,"bags_1":0,"helmet_2":0,"chain_2":0,"eyebrows_3":0,"makeup_3":0,"makeup_2":0,"tshirt_1":57,"makeup_1":0,"bags_2":0,"makeup_4":0,"eyebrows_4":0,"chain_1":0,"lipstick_4":0,"bproof_2":0,"hair_color_1":0,"decals_2":0,"pants_2":4,"age_2":0,"glasses_2":0,"ears_2":0,"arms":11,"lipstick_1":0,"ears_1":-1,"mask_2":0,"sex":0,"lipstick_3":0,"helmet_1":-1,"shoes_2":0,"beard_2":0,"beard_1":0,"lipstick_2":0,"beard_4":0,"glasses_1":0,"bproof_1":0,"mask_1":0,"decals_1":0,"hair_1":0,"eyebrows_2":0,"beard_3":0,"age_1":0,"tshirt_2":0,"skin":0,"torso_2":0,"eyebrows_1":0,"face":0,"shoes_1":10,"pants_1":24}', '{"hair_2":0,"hair_color_2":0,"torso_1":57,"bags_1":0,"helmet_2":0,"chain_2":0,"eyebrows_3":0,"makeup_3":0,"makeup_2":0,"tshirt_1":38,"makeup_1":0,"bags_2":0,"makeup_4":0,"eyebrows_4":0,"chain_1":0,"lipstick_4":0,"bproof_2":0,"hair_color_1":0,"decals_2":0,"pants_2":1,"age_2":0,"glasses_2":0,"ears_2":0,"arms":21,"lipstick_1":0,"ears_1":-1,"mask_2":0,"sex":1,"lipstick_3":0,"helmet_1":-1,"shoes_2":0,"beard_2":0,"beard_1":0,"lipstick_2":0,"beard_4":0,"glasses_1":5,"bproof_1":0,"mask_1":0,"decals_1":1,"hair_1":0,"eyebrows_2":0,"beard_3":0,"age_1":0,"tshirt_2":0,"skin":0,"torso_2":0,"eyebrows_1":0,"face":0,"shoes_1":49,"pants_1":11}'),
	(48, 'taxi', 4, 'boss', 'Patron', 0, '{"hair_2":0,"hair_color_2":0,"torso_1":29,"bags_1":0,"helmet_2":0,"chain_2":0,"eyebrows_3":0,"makeup_3":0,"makeup_2":0,"tshirt_1":31,"makeup_1":0,"bags_2":0,"makeup_4":0,"eyebrows_4":0,"chain_1":0,"lipstick_4":0,"bproof_2":0,"hair_color_1":0,"decals_2":0,"pants_2":4,"age_2":0,"glasses_2":0,"ears_2":0,"arms":1,"lipstick_1":0,"ears_1":-1,"mask_2":0,"sex":0,"lipstick_3":0,"helmet_1":-1,"shoes_2":0,"beard_2":0,"beard_1":0,"lipstick_2":0,"beard_4":0,"glasses_1":0,"bproof_1":0,"mask_1":0,"decals_1":0,"hair_1":0,"eyebrows_2":0,"beard_3":0,"age_1":0,"tshirt_2":0,"skin":0,"torso_2":4,"eyebrows_1":0,"face":0,"shoes_1":10,"pants_1":24}', '{"hair_2":0,"hair_color_2":0,"torso_1":57,"bags_1":0,"helmet_2":0,"chain_2":0,"eyebrows_3":0,"makeup_3":0,"makeup_2":0,"tshirt_1":38,"makeup_1":0,"bags_2":0,"makeup_4":0,"eyebrows_4":0,"chain_1":0,"lipstick_4":0,"bproof_2":0,"hair_color_1":0,"decals_2":0,"pants_2":1,"age_2":0,"glasses_2":0,"ears_2":0,"arms":21,"lipstick_1":0,"ears_1":-1,"mask_2":0,"sex":1,"lipstick_3":0,"helmet_1":-1,"shoes_2":0,"beard_2":0,"beard_1":0,"lipstick_2":0,"beard_4":0,"glasses_1":5,"bproof_1":0,"mask_1":0,"decals_1":1,"hair_1":0,"eyebrows_2":0,"beard_3":0,"age_1":0,"tshirt_2":0,"skin":0,"torso_2":0,"eyebrows_1":0,"face":0,"shoes_1":49,"pants_1":11}'),
	(49, 'cardealer', 0, 'recruit', 'Recrue', 10, '{}', '{}'),
	(50, 'cardealer', 1, 'novice', 'Novice', 25, '{}', '{}'),
	(51, 'cardealer', 2, 'experienced', 'Experimente', 40, '{}', '{}'),
	(52, 'cardealer', 3, 'boss', 'Patron', 0, '{}', '{}'),
	(53, 'ambulance', 0, 'ambulance', 'Stj. Doktor', 20, '{"tshirt_2":0,"hair_color_1":5,"glasses_2":3,"shoes":9,"torso_2":3,"hair_color_2":0,"pants_1":24,"glasses_1":4,"hair_1":2,"sex":0,"decals_2":0,"tshirt_1":15,"helmet_1":8,"helmet_2":0,"arms":92,"face":19,"decals_1":60,"torso_1":13,"hair_2":0,"skin":34,"pants_2":5}', '{"tshirt_2":3,"decals_2":0,"glasses":0,"hair_1":2,"torso_1":73,"shoes":1,"hair_color_2":0,"glasses_1":19,"skin":13,"face":6,"pants_2":5,"tshirt_1":75,"pants_1":37,"helmet_1":57,"torso_2":0,"arms":14,"sex":1,"glasses_2":0,"decals_1":0,"hair_2":0,"helmet_2":0,"hair_color_1":0}'),
	(54, 'ambulance', 1, 'doctor', 'Doktor', 40, '{"tshirt_2":0,"hair_color_1":5,"glasses_2":3,"shoes":9,"torso_2":3,"hair_color_2":0,"pants_1":24,"glasses_1":4,"hair_1":2,"sex":0,"decals_2":0,"tshirt_1":15,"helmet_1":8,"helmet_2":0,"arms":92,"face":19,"decals_1":60,"torso_1":13,"hair_2":0,"skin":34,"pants_2":5}', '{"tshirt_2":3,"decals_2":0,"glasses":0,"hair_1":2,"torso_1":73,"shoes":1,"hair_color_2":0,"glasses_1":19,"skin":13,"face":6,"pants_2":5,"tshirt_1":75,"pants_1":37,"helmet_1":57,"torso_2":0,"arms":14,"sex":1,"glasses_2":0,"decals_1":0,"hair_2":0,"helmet_2":0,"hair_color_1":0}'),
	(55, 'ambulance', 2, 'chief_doctor', 'Prof. Doktor', 60, '{"tshirt_2":0,"hair_color_1":5,"glasses_2":3,"shoes":9,"torso_2":3,"hair_color_2":0,"pants_1":24,"glasses_1":4,"hair_1":2,"sex":0,"decals_2":0,"tshirt_1":15,"helmet_1":8,"helmet_2":0,"arms":92,"face":19,"decals_1":60,"torso_1":13,"hair_2":0,"skin":34,"pants_2":5}', '{"tshirt_2":3,"decals_2":0,"glasses":0,"hair_1":2,"torso_1":73,"shoes":1,"hair_color_2":0,"glasses_1":19,"skin":13,"face":6,"pants_2":5,"tshirt_1":75,"pants_1":37,"helmet_1":57,"torso_2":0,"arms":14,"sex":1,"glasses_2":0,"decals_1":0,"hair_2":0,"helmet_2":0,"hair_color_1":0}'),
	(56, 'ambulance', 3, 'boss', 'Baş Hekim', 80, '{"tshirt_2":0,"hair_color_1":5,"glasses_2":3,"shoes":9,"torso_2":3,"hair_color_2":0,"pants_1":24,"glasses_1":4,"hair_1":2,"sex":0,"decals_2":0,"tshirt_1":15,"helmet_1":8,"helmet_2":0,"arms":92,"face":19,"decals_1":60,"torso_1":13,"hair_2":0,"skin":34,"pants_2":5}', '{"tshirt_2":3,"decals_2":0,"glasses":0,"hair_1":2,"torso_1":73,"shoes":1,"hair_color_2":0,"glasses_1":19,"skin":13,"face":6,"pants_2":5,"tshirt_1":75,"pants_1":37,"helmet_1":57,"torso_2":0,"arms":14,"sex":1,"glasses_2":0,"decals_1":0,"hair_2":0,"helmet_2":0,"hair_color_1":0}');
/*!40000 ALTER TABLE `job_grades` ENABLE KEYS */;

-- tablo yapısı dökülüyor essentialmode.licenses
CREATE TABLE IF NOT EXISTS `licenses` (
  `type` varchar(60) COLLATE utf8_turkish_ci NOT NULL,
  `label` varchar(60) COLLATE utf8_turkish_ci NOT NULL,
  PRIMARY KEY (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

-- essentialmode.licenses: ~4 rows (yaklaşık) tablosu için veriler indiriliyor
/*!40000 ALTER TABLE `licenses` DISABLE KEYS */;
INSERT INTO `licenses` (`type`, `label`) VALUES
	('dmv', 'Code de la route'),
	('drive', 'Permis de conduire'),
	('drive_bike', 'Permis moto'),
	('drive_truck', 'Permis camion');
/*!40000 ALTER TABLE `licenses` ENABLE KEYS */;

-- tablo yapısı dökülüyor essentialmode.owned_cetearaclar
CREATE TABLE IF NOT EXISTS `owned_cetearaclar` (
  `owner` varchar(22) COLLATE utf8_turkish_ci NOT NULL,
  `plate` varchar(12) COLLATE utf8_turkish_ci NOT NULL,
  `vehicle` longtext COLLATE utf8_turkish_ci DEFAULT NULL,
  `type` varchar(20) COLLATE utf8_turkish_ci NOT NULL DEFAULT 'car',
  `job` varchar(20) COLLATE utf8_turkish_ci NOT NULL,
  `stored` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`plate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

-- essentialmode.owned_cetearaclar: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
/*!40000 ALTER TABLE `owned_cetearaclar` DISABLE KEYS */;
/*!40000 ALTER TABLE `owned_cetearaclar` ENABLE KEYS */;

-- tablo yapısı dökülüyor essentialmode.owned_properties
CREATE TABLE IF NOT EXISTS `owned_properties` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `price` double NOT NULL,
  `rented` int(11) NOT NULL,
  `owner` varchar(60) NOT NULL,
  `type` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- essentialmode.owned_properties: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
/*!40000 ALTER TABLE `owned_properties` DISABLE KEYS */;
INSERT INTO `owned_properties` (`id`, `name`, `price`, `rented`, `owner`, `type`) VALUES
	(7, 'MotelRoom26', 50, 1, 'steam:11000013dc8c976', 'motel');
/*!40000 ALTER TABLE `owned_properties` ENABLE KEYS */;

-- tablo yapısı dökülüyor essentialmode.owned_vehicles
CREATE TABLE IF NOT EXISTS `owned_vehicles` (
  `owner` varchar(22) COLLATE utf8_turkish_ci NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Etat de la voiture',
  `plate` varchar(12) COLLATE utf8_turkish_ci NOT NULL,
  `vehicle` longtext COLLATE utf8_turkish_ci DEFAULT NULL,
  `type` varchar(20) COLLATE utf8_turkish_ci NOT NULL DEFAULT 'car',
  `job` varchar(20) COLLATE utf8_turkish_ci DEFAULT NULL,
  `stored` tinyint(1) NOT NULL DEFAULT 0,
  `fourrieremecano` tinyint(1) NOT NULL DEFAULT 0,
  `vehiclename` varchar(50) COLLATE utf8_turkish_ci NOT NULL DEFAULT 'voiture',
  PRIMARY KEY (`plate`),
  KEY `vehsowned` (`owner`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

-- essentialmode.owned_vehicles: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
/*!40000 ALTER TABLE `owned_vehicles` DISABLE KEYS */;
/*!40000 ALTER TABLE `owned_vehicles` ENABLE KEYS */;

-- tablo yapısı dökülüyor essentialmode.phone_app_chat
CREATE TABLE IF NOT EXISTS `phone_app_chat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `channel` varchar(20) NOT NULL,
  `message` varchar(255) NOT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- essentialmode.phone_app_chat: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
/*!40000 ALTER TABLE `phone_app_chat` DISABLE KEYS */;
/*!40000 ALTER TABLE `phone_app_chat` ENABLE KEYS */;

-- tablo yapısı dökülüyor essentialmode.phone_calls
CREATE TABLE IF NOT EXISTS `phone_calls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner` varchar(10) NOT NULL COMMENT 'Num tel proprio',
  `num` varchar(10) NOT NULL COMMENT 'Num reférence du contact',
  `incoming` int(11) NOT NULL COMMENT 'Défini si on est à l''origine de l''appels',
  `time` timestamp NOT NULL DEFAULT current_timestamp(),
  `accepts` int(11) NOT NULL COMMENT 'Appels accepter ou pas',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- essentialmode.phone_calls: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
/*!40000 ALTER TABLE `phone_calls` DISABLE KEYS */;
/*!40000 ALTER TABLE `phone_calls` ENABLE KEYS */;

-- tablo yapısı dökülüyor essentialmode.phone_messages
CREATE TABLE IF NOT EXISTS `phone_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transmitter` varchar(10) NOT NULL,
  `receiver` varchar(10) NOT NULL,
  `message` varchar(255) NOT NULL DEFAULT '0',
  `time` timestamp NOT NULL DEFAULT current_timestamp(),
  `isRead` int(11) NOT NULL DEFAULT 0,
  `owner` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- essentialmode.phone_messages: 0 rows tablosu için veriler indiriliyor
/*!40000 ALTER TABLE `phone_messages` DISABLE KEYS */;
/*!40000 ALTER TABLE `phone_messages` ENABLE KEYS */;

-- tablo yapısı dökülüyor essentialmode.phone_users_contacts
CREATE TABLE IF NOT EXISTS `phone_users_contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(60) CHARACTER SET utf8mb4 DEFAULT NULL,
  `number` varchar(10) CHARACTER SET utf8mb4 DEFAULT NULL,
  `display` varchar(64) CHARACTER SET utf8mb4 NOT NULL DEFAULT '-1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- essentialmode.phone_users_contacts: 0 rows tablosu için veriler indiriliyor
/*!40000 ALTER TABLE `phone_users_contacts` DISABLE KEYS */;
/*!40000 ALTER TABLE `phone_users_contacts` ENABLE KEYS */;

-- tablo yapısı dökülüyor essentialmode.properties
CREATE TABLE IF NOT EXISTS `properties` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  `entering` varchar(255) DEFAULT NULL,
  `exit` varchar(255) DEFAULT NULL,
  `inside` varchar(255) DEFAULT NULL,
  `outside` varchar(255) DEFAULT NULL,
  `ipls` varchar(255) DEFAULT '[]',
  `gateway` varchar(255) DEFAULT NULL,
  `is_single` int(11) DEFAULT NULL,
  `is_room` int(11) DEFAULT NULL,
  `is_gateway` int(11) DEFAULT NULL,
  `room_menu` varchar(255) DEFAULT NULL,
  `price` int(11) NOT NULL,
  `type` varchar(255) NOT NULL DEFAULT 'property',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=264 DEFAULT CHARSET=utf8mb4;

-- essentialmode.properties: ~38 rows (yaklaşık) tablosu için veriler indiriliyor
/*!40000 ALTER TABLE `properties` DISABLE KEYS */;
INSERT INTO `properties` (`id`, `name`, `label`, `entering`, `exit`, `inside`, `outside`, `ipls`, `gateway`, `is_single`, `is_room`, `is_gateway`, `room_menu`, `price`, `type`) VALUES
	(1, 'MotelRoom1', 'Room 1', '{"y":-218.82,"z":54.22,"x":312.86}', '{"x":151.48,"y":-1007.59,"z":-99.00}', '{"y":-1007.59,"z":-99.00,"x":151.48}', '{"y":-218.82,"z":54.22,"x":312.86}', '[]', NULL, 1, 1, 0, '{"x":151.62,"y":-1003.27,"z":-99.00}', 10000, 'motel'),
	(2, 'MotelRoom2', 'Room 2', '{"y":-217.9,"z":54.22,"x":310.89}', '{"x":151.48,"y":-1007.59,"z":-99.00}', '{"y":-1007.59,"z":-99.00,"x":151.48}', '{"y":-217.9,"z":54.22,"x":310.89}', '[]', NULL, 1, 1, 0, '{"x":151.62,"y":-1003.27,"z":-99.00}', 10000, 'motel'),
	(3, 'MotelRoom3', 'Room 3', '{"y":-216.47,"z":54.22,"x":307.24}', '{"x":151.48,"y":-1007.59,"z":-99.00}', '{"y":-1007.59,"z":-99.00,"x":151.48}', '{"y":-216.47,"z":54.22,"x":307.24}', '[]', NULL, 1, 1, 0, '{"x":151.62,"y":-1003.27,"z":-99.00}', 10000, 'motel'),
	(4, 'MotelRoom4', 'Room 4', '{"y":-213.32,"z":54.22,"x":307.52}', '{"x":151.48,"y":-1007.59,"z":-99.00}', '{"y":-1007.59,"z":-99.00,"x":151.48}', '{"y":-213.32,"z":54.22,"x":307.52}', '[]', NULL, 1, 1, 0, '{"x":151.62,"y":-1003.27,"z":-99.00}', 10000, 'motel'),
	(5, 'MotelRoom5a', 'Room 5a', '{"y":-208.03,"z":54.22,"x":309.46}', '{"x":151.48,"y":-1007.59,"z":-99.00}', '{"y":-1007.59,"z":-99.00,"x":151.48}', '{"y":-208.03,"z":54.22,"x":309.46}', '[]', NULL, 1, 1, 0, '{"x":151.62,"y":-1003.27,"z":-99.00}', 10000, 'motel'),
	(6, 'MotelRoom5b', 'Room 5b', '{"y":-203.38,"z":54.22,"x":311.32}', '{"x":151.48,"y":-1007.59,"z":-99.00}', '{"y":-1007.59,"z":-99.00,"x":151.48}', '{"y":-203.38,"z":54.22,"x":311.32}', '[]', NULL, 1, 1, 0, '{"x":151.62,"y":-1003.27,"z":-99.00}', 10000, 'motel'),
	(7, 'MotelRoom6', 'Room 6', '{"y":-198.14,"z":54.22,"x":313.71}', '{"x":151.48,"y":-1007.59,"z":-99.00}', '{"y":-1007.59,"z":-99.00,"x":151.48}', '{"y":-198.14,"z":54.22,"x":313.71}', '[]', NULL, 1, 1, 0, '{"x":151.62,"y":-1003.27,"z":-99.00}', 10000, 'motel'),
	(8, 'MotelRoom7', 'Room 7', '{"y":-194.93,"z":54.22,"x":315.66}', '{"x":151.48,"y":-1007.59,"z":-99.00}', '{"y":-1007.59,"z":-99.00,"x":151.48}', '{"y":-194.93,"z":54.22,"x":315.66}', '[]', NULL, 1, 1, 0, '{"x":151.62,"y":-1003.27,"z":-99.00}', 10000, 'motel'),
	(9, 'MotelRoom8', 'Room 8', '{"y":-196.54,"z":54.22,"x":319.17}', '{"x":151.48,"y":-1007.59,"z":-99.00}', '{"y":-1007.59,"z":-99.00,"x":151.48}', '{"y":-196.54,"z":54.22,"x":319.17}', '[]', NULL, 1, 1, 0, '{"x":151.62,"y":-1003.27,"z":-99.00}', 10000, 'motel'),
	(10, 'MotelRoom9', 'Room 9', '{"y":-197.22,"z":54.22,"x":321.31}', '{"x":151.48,"y":-1007.59,"z":-99.00}', '{"y":-1007.59,"z":-99.00,"x":151.48}', '{"y":-197.22,"z":54.22,"x":321.31}', '[]', NULL, 1, 1, 0, '{"x":151.62,"y":-1003.27,"z":-99.00}', 10000, 'motel'),
	(11, 'MotelRoom12', 'Room 12', '{"y":-217.9,"z":58.02,"x":310.89}', '{"x":151.48,"y":-1007.59,"z":-99.00}', '{"y":-1007.59,"z":-99.00,"x":151.48}', '{"y":-217.9,"z":58.02,"x":310.89}', '[]', NULL, 1, 1, 0, '{"x":151.62,"y":-1003.27,"z":-99.00}', 10000, 'motel'),
	(12, 'MotelRoom13', 'Room 13', '{"y":-216.47,"z":58.02,"x":307.24}', '{"x":151.48,"y":-1007.59,"z":-99.00}', '{"y":-1007.59,"z":-99.00,"x":151.48}', '{"y":-216.47,"z":58.02,"x":307.24}', '[]', NULL, 1, 1, 0, '{"x":151.62,"y":-1003.27,"z":-99.00}', 10000, 'motel'),
	(13, 'MotelRoom14', 'Room 14', '{"y":-213.32,"z":58.02,"x":307.52}', '{"x":151.48,"y":-1007.59,"z":-99.00}', '{"y":-1007.59,"z":-99.00,"x":151.48}', '{"y":-213.32,"z":58.02,"x":307.52}', '[]', NULL, 1, 1, 0, '{"x":151.62,"y":-1003.27,"z":-99.00}', 10000, 'motel'),
	(14, 'MotelRoom15', 'Room 15', '{"y":-208.03,"z":58.02,"x":309.46}', '{"x":151.48,"y":-1007.59,"z":-99.00}', '{"y":-1007.59,"z":-99.00,"x":151.48}', '{"y":-208.03,"z":58.02,"x":309.46}', '[]', NULL, 1, 1, 0, '{"x":151.62,"y":-1003.27,"z":-99.00}', 10000, 'motel'),
	(15, 'MotelRoom16', 'Room 16', '{"y":-203.38,"z":58.02,"x":311.32}', '{"x":151.48,"y":-1007.59,"z":-99.00}', '{"y":-1007.59,"z":-99.00,"x":151.48}', '{"y":-203.38,"z":58.02,"x":311.32}', '[]', NULL, 1, 1, 0, '{"x":151.62,"y":-1003.27,"z":-99.00}', 10000, 'motel'),
	(16, 'MotelRoom17', 'Room 17', '{"y":-198.14,"z":58.02,"x":313.71}', '{"x":151.48,"y":-1007.59,"z":-99.00}', '{"y":-1007.59,"z":-99.00,"x":151.48}', '{"y":-198.14,"z":58.02,"x":313.71}', '[]', NULL, 1, 1, 0, '{"x":151.62,"y":-1003.27,"z":-99.00}', 10000, 'motel'),
	(17, 'MotelRoom18', 'Room 18', '{"y":-194.93,"z":58.02,"x":315.66}', '{"x":151.48,"y":-1007.59,"z":-99.00}', '{"y":-1007.59,"z":-99.00,"x":151.48}', '{"y":-194.93,"z":58.02,"x":315.66}', '[]', NULL, 1, 1, 0, '{"x":151.62,"y":-1003.27,"z":-99.00}', 10000, 'motel'),
	(18, 'MotelRoom19', 'Room 19', '{"y":-196.54,"z":58.02,"x":319.17}', '{"x":151.48,"y":-1007.59,"z":-99.00}', '{"y":-1007.59,"z":-99.00,"x":151.48}', '{"y":-196.54,"z":58.02,"x":319.17}', '[]', NULL, 1, 1, 0, '{"x":151.62,"y":-1003.27,"z":-99.00}', 10000, 'motel'),
	(19, 'MotelRoom20', 'Room 20', '{"y":-197.22,"z":58.02,"x":321.31}', '{"x":151.48,"y":-1007.59,"z":-99.00}', '{"y":-1007.59,"z":-99.00,"x":151.48}', '{"y":-197.22,"z":58.02,"x":321.31}', '[]', NULL, 1, 1, 0, '{"x":151.62,"y":-1003.27,"z":-99.00}', 10000, 'motel'),
	(20, 'MotelRoom21', 'Room 21', '{"y":-225.17,"z":54.22,"x":329.3}', '{"x":151.48,"y":-1007.59,"z":-99.00}', '{"y":-1007.59,"z":-99.00,"x":151.48}', '{"y":-225.17,"z":54.22,"x":329.3}', '[]', NULL, 1, 1, 0, '{"x":151.62,"y":-1003.27,"z":-99.00}', 10000, 'motel'),
	(21, 'MotelRoom22', 'Room 22', '{"y":-226.0,"z":54.22,"x":331.34}', '{"x":151.48,"y":-1007.59,"z":-99.00}', '{"y":-1007.59,"z":-99.00,"x":151.48}', '{"y":-226.0,"z":54.22,"x":331.34}', '[]', NULL, 1, 1, 0, '{"x":151.62,"y":-1003.27,"z":-99.00}', 10000, 'motel'),
	(22, 'MotelRoom23', 'Room 23', '{"y":-227.39,"z":54.22,"x":334.92}', '{"x":151.48,"y":-1007.59,"z":-99.00}', '{"y":-1007.59,"z":-99.00,"x":151.48}', '{"y":-227.39,"z":54.22,"x":334.92}', '[]', NULL, 1, 1, 0, '{"x":151.62,"y":-1003.27,"z":-99.00}', 10000, 'motel'),
	(23, 'MotelRoom24', 'Room 24', '{"y":-224.83,"z":54.22,"x":337.03}', '{"x":151.48,"y":-1007.59,"z":-99.00}', '{"y":-1007.59,"z":-99.00,"x":151.48}', '{"y":-224.83,"z":54.22,"x":337.03}', '[]', NULL, 1, 1, 0, '{"x":151.62,"y":-1003.27,"z":-99.00}', 10000, 'motel'),
	(24, 'MotelRoom25', 'Room 25', '{"y":-219.54,"z":54.22,"x":339.16}', '{"x":151.48,"y":-1007.59,"z":-99.00}', '{"y":-1007.59,"z":-99.00,"x":151.48}', '{"y":-219.54,"z":54.22,"x":339.16}', '[]', NULL, 1, 1, 0, '{"x":151.62,"y":-1003.27,"z":-99.00}', 10000, 'motel'),
	(25, 'MotelRoom26', 'Room 26', '{"y":-214.96,"z":54.22,"x":340.94}', '{"x":151.48,"y":-1007.59,"z":-99.00}', '{"y":-1007.59,"z":-99.00,"x":151.48}', '{"y":-214.96,"z":54.22,"x":340.94}', '[]', NULL, 1, 1, 0, '{"x":151.62,"y":-1003.27,"z":-99.00}', 10000, 'motel'),
	(26, 'MotelRoom27', 'Room 27', '{"y":-209.61,"z":54.22,"x":343.02}', '{"x":151.48,"y":-1007.59,"z":-99.00}', '{"y":-1007.59,"z":-99.00,"x":151.48}', '{"y":-209.61,"z":54.22,"x":343.02}', '[]', NULL, 1, 1, 0, '{"x":151.62,"y":-1003.27,"z":-99.00}', 10000, 'motel'),
	(27, 'MotelRoom28', 'Room 28', '{"y":-204.98,"z":54.22,"x":344.75}', '{"x":151.48,"y":-1007.59,"z":-99.00}', '{"y":-1007.59,"z":-99.00,"x":151.48}', '{"y":-204.98,"z":54.22,"x":344.75}', '[]', NULL, 1, 1, 0, '{"x":151.62,"y":-1003.27,"z":-99.00}', 10000, 'motel'),
	(28, 'MotelRoom29', 'Room 29', '{"y":-199.74,"z":54.22,"x":346.81}', '{"x":151.48,"y":-1007.59,"z":-99.00}', '{"y":-1007.59,"z":-99.00,"x":151.48}', '{"y":-199.74,"z":54.22,"x":346.81}', '[]', NULL, 1, 1, 0, '{"x":151.62,"y":-1003.27,"z":-99.00}', 10000, 'motel'),
	(29, 'MotelRoom30', 'Room 30', '{"y":-225.17,"z":58.02,"x":329.3}', '{"x":151.48,"y":-1007.59,"z":-99.00}', '{"y":-1007.59,"z":-99.00,"x":151.48}', '{"y":-225.17,"z":58.02,"x":329.3}', '[]', NULL, 1, 1, 0, '{"x":151.62,"y":-1003.27,"z":-99.00}', 10000, 'motel'),
	(30, 'MotelRoom31', 'Room 31', '{"y":-226.0,"z":58.02,"x":331.34}', '{"x":151.48,"y":-1007.59,"z":-99.00}', '{"y":-1007.59,"z":-99.00,"x":151.48}', '{"y":-226.0,"z":58.02,"x":331.34}', '[]', NULL, 1, 1, 0, '{"x":151.62,"y":-1003.27,"z":-99.00}', 10000, 'motel'),
	(31, 'MotelRoom32', 'Room 32', '{"y":-227.39,"z":58.02,"x":334.92}', '{"x":151.48,"y":-1007.59,"z":-99.00}', '{"y":-1007.59,"z":-99.00,"x":151.48}', '{"y":-227.39,"z":58.02,"x":334.92}', '[]', NULL, 1, 1, 0, '{"x":151.62,"y":-1003.27,"z":-99.00}', 10000, 'motel'),
	(32, 'MotelRoom33', 'Room 33', '{"y":-224.83,"z":58.02,"x":337.03}', '{"x":151.48,"y":-1007.59,"z":-99.00}', '{"y":-1007.59,"z":-99.00,"x":151.48}', '{"y":-224.83,"z":58.02,"x":337.03}', '[]', NULL, 1, 1, 0, '{"x":151.62,"y":-1003.27,"z":-99.00}', 10000, 'motel'),
	(33, 'MotelRoom34', 'Room 34', '{"y":-219.54,"z":58.02,"x":339.16}', '{"x":151.48,"y":-1007.59,"z":-99.00}', '{"y":-1007.59,"z":-99.00,"x":151.48}', '{"y":-219.54,"z":58.02,"x":339.16}', '[]', NULL, 1, 1, 0, '{"x":151.62,"y":-1003.27,"z":-99.00}', 10000, 'motel'),
	(34, 'MotelRoom35', 'Room 35', '{"y":-214.96,"z":58.02,"x":340.94}', '{"x":151.48,"y":-1007.59,"z":-99.00}', '{"y":-1007.59,"z":-99.00,"x":151.48}', '{"y":-214.96,"z":58.02,"x":340.94}', '[]', NULL, 1, 1, 0, '{"x":151.62,"y":-1003.27,"z":-99.00}', 10000, 'motel'),
	(35, 'MotelRoom36', 'Room 36', '{"y":-209.61,"z":58.02,"x":343.02}', '{"x":151.48,"y":-1007.59,"z":-99.00}', '{"y":-1007.59,"z":-99.00,"x":151.48}', '{"y":-209.61,"z":58.02,"x":343.02}', '[]', NULL, 1, 1, 0, '{"x":151.62,"y":-1003.27,"z":-99.00}', 10000, 'motel'),
	(36, 'MotelRoom37', 'Room 37', '{"y":-204.98,"z":58.02,"x":344.75}', '{"x":151.48,"y":-1007.59,"z":-99.00}', '{"y":-1007.59,"z":-99.00,"x":151.48}', '{"y":-204.98,"z":58.02,"x":344.75}', '[]', NULL, 1, 1, 0, '{"x":151.62,"y":-1003.27,"z":-99.00}', 10000, 'motel'),
	(37, 'MotelRoom38', 'Room 38', '{"y":-199.74,"z":58.02,"x":346.81}', '{"x":151.48,"y":-1007.59,"z":-99.00}', '{"y":-1007.59,"z":-99.00,"x":151.48}', '{"y":-199.74,"z":58.02,"x":346.81}', '[]', NULL, 1, 1, 0, '{"x":151.62,"y":-1003.27,"z":-99.00}', 10000, 'motel'),
	(261, 'MotelRoom351', 'Room 351', '{"y":-1623.01,"z":31.22,"x":-83.54}', '{"y":-1007.24,"z":-101.01,"x":266.03}', '{"y":-1007.24,"z":-101.01,"x":266.03}', '{"y":-1623.01,"z":31.22,"x":-83.54}', '[]', NULL, 1, 1, 0, '{"y":-1003.77,"z":-99.01,"x":259.82}', 10000, 'motel');
/*!40000 ALTER TABLE `properties` ENABLE KEYS */;

-- tablo yapısı dökülüyor essentialmode.rented_vehicles
CREATE TABLE IF NOT EXISTS `rented_vehicles` (
  `vehicle` varchar(60) COLLATE utf8_turkish_ci NOT NULL,
  `plate` varchar(12) COLLATE utf8_turkish_ci NOT NULL,
  `player_name` varchar(255) COLLATE utf8_turkish_ci NOT NULL,
  `base_price` int(11) NOT NULL,
  `rent_price` int(11) NOT NULL,
  `owner` varchar(22) COLLATE utf8_turkish_ci NOT NULL,
  PRIMARY KEY (`plate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

-- essentialmode.rented_vehicles: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
/*!40000 ALTER TABLE `rented_vehicles` DISABLE KEYS */;
/*!40000 ALTER TABLE `rented_vehicles` ENABLE KEYS */;

-- tablo yapısı dökülüyor essentialmode.shops
CREATE TABLE IF NOT EXISTS `shops` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_turkish_ci NOT NULL DEFAULT '0',
  `store` varchar(100) COLLATE utf8_turkish_ci NOT NULL,
  `item` varchar(100) COLLATE utf8_turkish_ci NOT NULL,
  `price` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

-- essentialmode.shops: ~6 rows (yaklaşık) tablosu için veriler indiriliyor
/*!40000 ALTER TABLE `shops` DISABLE KEYS */;
INSERT INTO `shops` (`id`, `name`, `store`, `item`, `price`) VALUES
	(1, '0', 'TwentyFourSeven', 'bread', 30),
	(2, '0', 'TwentyFourSeven', 'water', 15),
	(3, '0', 'RobsLiquor', 'bread', 30),
	(4, '0', 'RobsLiquor', 'water', 15),
	(5, '0', 'LTDgasoline', 'bread', 30),
	(6, '0', 'LTDgasoline', 'water', 15),
	(7, 'LTDgasoline', '', 'croquettes', 1000);
/*!40000 ALTER TABLE `shops` ENABLE KEYS */;

-- tablo yapısı dökülüyor essentialmode.society_moneywash
CREATE TABLE IF NOT EXISTS `society_moneywash` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(60) COLLATE utf8_turkish_ci NOT NULL,
  `society` varchar(60) COLLATE utf8_turkish_ci NOT NULL,
  `amount` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

-- essentialmode.society_moneywash: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
/*!40000 ALTER TABLE `society_moneywash` DISABLE KEYS */;
/*!40000 ALTER TABLE `society_moneywash` ENABLE KEYS */;

-- tablo yapısı dökülüyor essentialmode.twitter_accounts
CREATE TABLE IF NOT EXISTS `twitter_accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT '0',
  `password` varchar(50) COLLATE utf8mb4_bin NOT NULL DEFAULT '0',
  `avatar_url` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- essentialmode.twitter_accounts: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
/*!40000 ALTER TABLE `twitter_accounts` DISABLE KEYS */;
INSERT INTO `twitter_accounts` (`id`, `username`, `password`, `avatar_url`) VALUES
	(1, 'ozan01123', 'kartal01123', NULL);
/*!40000 ALTER TABLE `twitter_accounts` ENABLE KEYS */;

-- tablo yapısı dökülüyor essentialmode.twitter_likes
CREATE TABLE IF NOT EXISTS `twitter_likes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `authorId` int(11) DEFAULT NULL,
  `tweetId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_twitter_likes_twitter_accounts` (`authorId`),
  KEY `FK_twitter_likes_twitter_tweets` (`tweetId`),
  CONSTRAINT `FK_twitter_likes_twitter_accounts` FOREIGN KEY (`authorId`) REFERENCES `twitter_accounts` (`id`),
  CONSTRAINT `FK_twitter_likes_twitter_tweets` FOREIGN KEY (`tweetId`) REFERENCES `twitter_tweets` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- essentialmode.twitter_likes: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
/*!40000 ALTER TABLE `twitter_likes` DISABLE KEYS */;
/*!40000 ALTER TABLE `twitter_likes` ENABLE KEYS */;

-- tablo yapısı dökülüyor essentialmode.twitter_tweets
CREATE TABLE IF NOT EXISTS `twitter_tweets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `authorId` int(11) NOT NULL,
  `realUser` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp(),
  `likes` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `FK_twitter_tweets_twitter_accounts` (`authorId`),
  CONSTRAINT `FK_twitter_tweets_twitter_accounts` FOREIGN KEY (`authorId`) REFERENCES `twitter_accounts` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- essentialmode.twitter_tweets: ~3 rows (yaklaşık) tablosu için veriler indiriliyor
/*!40000 ALTER TABLE `twitter_tweets` DISABLE KEYS */;
INSERT INTO `twitter_tweets` (`id`, `authorId`, `realUser`, `message`, `time`, `likes`) VALUES
	(1, 1, 'steam:11000010ce74ba9', 'Herkese selamlar', '2019-05-18 10:08:01', 0),
	(2, 1, 'steam:11000010ce74ba9', 'Selam millet', '2019-05-18 10:08:08', 0),
	(3, 1, 'steam:11000010ce74ba9', 'selam', '2019-05-18 10:08:31', 0);
/*!40000 ALTER TABLE `twitter_tweets` ENABLE KEYS */;

-- tablo yapısı dökülüyor essentialmode.users
CREATE TABLE IF NOT EXISTS `users` (
  `identifier` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `license` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `money` int(11) DEFAULT NULL,
  `name` varchar(50) COLLATE utf8mb4_bin DEFAULT '',
  `skin` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `job` varchar(50) COLLATE utf8mb4_bin DEFAULT 'unemployed',
  `job_grade` int(11) DEFAULT 0,
  `loadout` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `position` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `bank` int(11) DEFAULT NULL,
  `permission_level` int(11) DEFAULT NULL,
  `group` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `firstname` varchar(50) COLLATE utf8mb4_bin DEFAULT '',
  `lastname` varchar(50) COLLATE utf8mb4_bin DEFAULT '',
  `dateofbirth` varchar(25) COLLATE utf8mb4_bin DEFAULT '',
  `sex` varchar(10) COLLATE utf8mb4_bin DEFAULT '',
  `height` varchar(5) COLLATE utf8mb4_bin DEFAULT '',
  `animal` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `status` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `tattoos` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `last_property` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `is_dead` tinyint(1) DEFAULT 0,
  `phone_number` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- essentialmode.users: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`identifier`, `license`, `money`, `name`, `skin`, `job`, `job_grade`, `loadout`, `position`, `bank`, `permission_level`, `group`, `firstname`, `lastname`, `dateofbirth`, `sex`, `height`, `animal`, `status`, `tattoos`, `last_property`, `is_dead`, `phone_number`) VALUES
	('steam:11000013dc8c976', 'license:ee1777a7400cfa853844153c695fb08087d42a2e', 199250, '*BRB', NULL, 'unemployed', 0, '[]', '{"y":-1596.2,"z":31.6,"x":-105.0}', 0, 0, 'superadmin', 'Bekir', 'Can', '17-07-1992', 'm', '67', NULL, '[{"percent":95.31,"val":953100,"name":"hunger"},{"percent":96.4825,"val":964825,"name":"thirst"}]', NULL, NULL, 0, '');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- tablo yapısı dökülüyor essentialmode.user_accounts
CREATE TABLE IF NOT EXISTS `user_accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(22) COLLATE utf8_turkish_ci NOT NULL,
  `name` varchar(50) COLLATE utf8_turkish_ci NOT NULL,
  `money` double NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

-- essentialmode.user_accounts: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
/*!40000 ALTER TABLE `user_accounts` DISABLE KEYS */;
INSERT INTO `user_accounts` (`id`, `identifier`, `name`, `money`) VALUES
	(1, 'steam:11000013dc8c976', 'black_money', 0);
/*!40000 ALTER TABLE `user_accounts` ENABLE KEYS */;

-- tablo yapısı dökülüyor essentialmode.user_inventory
CREATE TABLE IF NOT EXISTS `user_inventory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(22) COLLATE utf8_turkish_ci NOT NULL,
  `item` varchar(50) COLLATE utf8_turkish_ci NOT NULL,
  `count` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

-- essentialmode.user_inventory: ~22 rows (yaklaşık) tablosu için veriler indiriliyor
/*!40000 ALTER TABLE `user_inventory` DISABLE KEYS */;
INSERT INTO `user_inventory` (`id`, `identifier`, `item`, `count`) VALUES
	(1, 'steam:11000013dc8c976', 'alive_chicken', 0),
	(2, 'steam:11000013dc8c976', 'petrol_raffin', 0),
	(3, 'steam:11000013dc8c976', 'iron', 0),
	(4, 'steam:11000013dc8c976', 'clothe', 0),
	(5, 'steam:11000013dc8c976', 'washed_stone', 0),
	(6, 'steam:11000013dc8c976', 'croquettes', 0),
	(7, 'steam:11000013dc8c976', 'wool', 0),
	(8, 'steam:11000013dc8c976', 'copper', 0),
	(9, 'steam:11000013dc8c976', 'packaged_plank', 0),
	(10, 'steam:11000013dc8c976', 'packaged_chicken', 0),
	(11, 'steam:11000013dc8c976', 'petrol', 0),
	(12, 'steam:11000013dc8c976', 'fish', 0),
	(13, 'steam:11000013dc8c976', 'water', 0),
	(14, 'steam:11000013dc8c976', 'stone', 0),
	(15, 'steam:11000013dc8c976', 'slaughtered_chicken', 0),
	(16, 'steam:11000013dc8c976', 'cutted_wood', 0),
	(17, 'steam:11000013dc8c976', 'fabric', 0),
	(18, 'steam:11000013dc8c976', 'gold', 0),
	(19, 'steam:11000013dc8c976', 'wood', 0),
	(20, 'steam:11000013dc8c976', 'essence', 0),
	(21, 'steam:11000013dc8c976', 'bread', 0),
	(22, 'steam:11000013dc8c976', 'diamond', 0),
	(23, 'steam:11000013dc8c976', 'bandage', 0),
	(24, 'steam:11000013dc8c976', 'medikit', 0),
	(25, 'steam:11000013dc8c976', 'phone', 0);
/*!40000 ALTER TABLE `user_inventory` ENABLE KEYS */;

-- tablo yapısı dökülüyor essentialmode.user_licenses
CREATE TABLE IF NOT EXISTS `user_licenses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(60) COLLATE utf8_turkish_ci NOT NULL,
  `owner` varchar(60) COLLATE utf8_turkish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

-- essentialmode.user_licenses: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
/*!40000 ALTER TABLE `user_licenses` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_licenses` ENABLE KEYS */;

-- tablo yapısı dökülüyor essentialmode.vehicles
CREATE TABLE IF NOT EXISTS `vehicles` (
  `name` varchar(60) COLLATE utf8_turkish_ci NOT NULL,
  `model` varchar(60) COLLATE utf8_turkish_ci NOT NULL,
  `price` int(11) NOT NULL,
  `category` varchar(60) COLLATE utf8_turkish_ci DEFAULT NULL,
  PRIMARY KEY (`model`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

-- essentialmode.vehicles: ~240 rows (yaklaşık) tablosu için veriler indiriliyor
/*!40000 ALTER TABLE `vehicles` DISABLE KEYS */;
INSERT INTO `vehicles` (`name`, `model`, `price`, `category`) VALUES
	('Adder', 'adder', 900000, 'super'),
	('Akuma', 'AKUMA', 7500, 'motorcycles'),
	('Alpha', 'alpha', 60000, 'sports'),
	('Ardent', 'ardent', 1150000, 'sportsclassics'),
	('Asea', 'asea', 5500, 'sedans'),
	('Autarch', 'autarch', 1955000, 'super'),
	('Avarus', 'avarus', 18000, 'motorcycles'),
	('Bagger', 'bagger', 13500, 'motorcycles'),
	('Baller', 'baller2', 40000, 'suvs'),
	('Baller Sport', 'baller3', 60000, 'suvs'),
	('Banshee', 'banshee', 70000, 'sports'),
	('Banshee 900R', 'banshee2', 255000, 'super'),
	('Bati 801', 'bati', 12000, 'motorcycles'),
	('Bati 801RR', 'bati2', 19000, 'motorcycles'),
	('Bestia GTS', 'bestiagts', 55000, 'sports'),
	('BF400', 'bf400', 6500, 'motorcycles'),
	('Bf Injection', 'bfinjection', 16000, 'offroad'),
	('Bifta', 'bifta', 12000, 'offroad'),
	('Bison', 'bison', 45000, 'vans'),
	('Blade', 'blade', 15000, 'muscle'),
	('Blazer', 'blazer', 6500, 'offroad'),
	('Blazer Sport', 'blazer4', 8500, 'offroad'),
	('blazer5', 'blazer5', 1755600, 'offroad'),
	('Blista', 'blista', 8000, 'compacts'),
	('BMX (velo)', 'bmx', 160, 'motorcycles'),
	('Bobcat XL', 'bobcatxl', 32000, 'vans'),
	('Brawler', 'brawler', 45000, 'offroad'),
	('Brioso R/A', 'brioso', 18000, 'compacts'),
	('Btype', 'btype', 62000, 'sportsclassics'),
	('Btype Hotroad', 'btype2', 155000, 'sportsclassics'),
	('Btype Luxe', 'btype3', 85000, 'sportsclassics'),
	('Buccaneer', 'buccaneer', 18000, 'muscle'),
	('Buccaneer Rider', 'buccaneer2', 24000, 'muscle'),
	('Buffalo', 'buffalo', 12000, 'sports'),
	('Buffalo S', 'buffalo2', 20000, 'sports'),
	('Bullet', 'bullet', 90000, 'super'),
	('Burrito', 'burrito3', 19000, 'vans'),
	('Camper', 'camper', 42000, 'vans'),
	('Carbonizzare', 'carbonizzare', 75000, 'sports'),
	('Carbon RS', 'carbonrs', 18000, 'motorcycles'),
	('Casco', 'casco', 30000, 'sportsclassics'),
	('Cavalcade', 'cavalcade2', 55000, 'suvs'),
	('Cheetah', 'cheetah', 375000, 'super'),
	('Chimera', 'chimera', 38000, 'motorcycles'),
	('Chino', 'chino', 15000, 'muscle'),
	('Chino Luxe', 'chino2', 19000, 'muscle'),
	('Cliffhanger', 'cliffhanger', 9500, 'motorcycles'),
	('Cognoscenti Cabrio', 'cogcabrio', 55000, 'coupes'),
	('Cognoscenti', 'cognoscenti', 55000, 'sedans'),
	('Comet', 'comet2', 65000, 'sports'),
	('Comet 5', 'comet5', 1145000, 'sports'),
	('Contender', 'contender', 70000, 'suvs'),
	('Coquette', 'coquette', 65000, 'sports'),
	('Coquette Classic', 'coquette2', 40000, 'sportsclassics'),
	('Coquette BlackFin', 'coquette3', 55000, 'muscle'),
	('Cruiser (velo)', 'cruiser', 510, 'motorcycles'),
	('Cyclone', 'cyclone', 1890000, 'super'),
	('Daemon', 'daemon', 11500, 'motorcycles'),
	('Daemon High', 'daemon2', 13500, 'motorcycles'),
	('Defiler', 'defiler', 9800, 'motorcycles'),
	('Deluxo', 'deluxo', 4721500, 'sportsclassics'),
	('Dominator', 'dominator', 35000, 'muscle'),
	('Double T', 'double', 28000, 'motorcycles'),
	('Dubsta', 'dubsta', 45000, 'suvs'),
	('Dubsta Luxuary', 'dubsta2', 60000, 'suvs'),
	('Bubsta 6x6', 'dubsta3', 120000, 'offroad'),
	('Dukes', 'dukes', 28000, 'muscle'),
	('Dune Buggy', 'dune', 8000, 'offroad'),
	('Elegy', 'elegy2', 38500, 'sports'),
	('Emperor', 'emperor', 8500, 'sedans'),
	('Enduro', 'enduro', 5500, 'motorcycles'),
	('Entity XF', 'entityxf', 425000, 'super'),
	('Esskey', 'esskey', 4200, 'motorcycles'),
	('Exemplar', 'exemplar', 32000, 'coupes'),
	('F620', 'f620', 40000, 'coupes'),
	('Faction', 'faction', 20000, 'muscle'),
	('Faction Rider', 'faction2', 30000, 'muscle'),
	('Faction XL', 'faction3', 40000, 'muscle'),
	('Faggio', 'faggio', 1900, 'motorcycles'),
	('Vespa', 'faggio2', 2800, 'motorcycles'),
	('Felon', 'felon', 42000, 'coupes'),
	('Felon GT', 'felon2', 55000, 'coupes'),
	('Feltzer', 'feltzer2', 55000, 'sports'),
	('Stirling GT', 'feltzer3', 65000, 'sportsclassics'),
	('Fixter (velo)', 'fixter', 225, 'motorcycles'),
	('FMJ', 'fmj', 185000, 'super'),
	('Fhantom', 'fq2', 17000, 'suvs'),
	('Fugitive', 'fugitive', 12000, 'sedans'),
	('Furore GT', 'furoregt', 45000, 'sports'),
	('Fusilade', 'fusilade', 40000, 'sports'),
	('Gargoyle', 'gargoyle', 16500, 'motorcycles'),
	('Gauntlet', 'gauntlet', 30000, 'muscle'),
	('Gang Burrito', 'gburrito', 45000, 'vans'),
	('Burrito', 'gburrito2', 29000, 'vans'),
	('Glendale', 'glendale', 6500, 'sedans'),
	('Grabger', 'granger', 50000, 'suvs'),
	('Gresley', 'gresley', 47500, 'suvs'),
	('GT 500', 'gt500', 785000, 'sportsclassics'),
	('Guardian', 'guardian', 45000, 'offroad'),
	('Hakuchou', 'hakuchou', 31000, 'motorcycles'),
	('Hakuchou Sport', 'hakuchou2', 55000, 'motorcycles'),
	('Hermes', 'hermes', 535000, 'muscle'),
	('Hexer', 'hexer', 12000, 'motorcycles'),
	('Hotknife', 'hotknife', 125000, 'muscle'),
	('Huntley S', 'huntley', 40000, 'suvs'),
	('Hustler', 'hustler', 625000, 'muscle'),
	('Infernus', 'infernus', 180000, 'super'),
	('Innovation', 'innovation', 23500, 'motorcycles'),
	('Intruder', 'intruder', 7500, 'sedans'),
	('Issi', 'issi2', 10000, 'compacts'),
	('Jackal', 'jackal', 38000, 'coupes'),
	('Jester', 'jester', 65000, 'sports'),
	('Jester(Racecar)', 'jester2', 135000, 'sports'),
	('Journey', 'journey', 6500, 'vans'),
	('Kamacho', 'kamacho', 345000, 'offroad'),
	('Khamelion', 'khamelion', 38000, 'sports'),
	('Kuruma', 'kuruma', 30000, 'sports'),
	('Landstalker', 'landstalker', 35000, 'suvs'),
	('RE-7B', 'le7b', 325000, 'super'),
	('Lynx', 'lynx', 40000, 'sports'),
	('Mamba', 'mamba', 70000, 'sports'),
	('Manana', 'manana', 12800, 'sportsclassics'),
	('Manchez', 'manchez', 5300, 'motorcycles'),
	('Massacro', 'massacro', 65000, 'sports'),
	('Massacro(Racecar)', 'massacro2', 130000, 'sports'),
	('Mesa', 'mesa', 16000, 'suvs'),
	('Mesa Trail', 'mesa3', 40000, 'suvs'),
	('Minivan', 'minivan', 13000, 'vans'),
	('Monroe', 'monroe', 55000, 'sportsclassics'),
	('The Liberator', 'monster', 210000, 'offroad'),
	('Moonbeam', 'moonbeam', 18000, 'vans'),
	('Moonbeam Rider', 'moonbeam2', 35000, 'vans'),
	('Nemesis', 'nemesis', 5800, 'motorcycles'),
	('Neon', 'neon', 1500000, 'sports'),
	('Nightblade', 'nightblade', 35000, 'motorcycles'),
	('Nightshade', 'nightshade', 65000, 'muscle'),
	('9F', 'ninef', 65000, 'sports'),
	('9F Cabrio', 'ninef2', 80000, 'sports'),
	('Omnis', 'omnis', 35000, 'sports'),
	('Oppressor', 'oppressor', 3524500, 'super'),
	('Oracle XS', 'oracle2', 35000, 'coupes'),
	('Osiris', 'osiris', 160000, 'super'),
	('Panto', 'panto', 10000, 'compacts'),
	('Paradise', 'paradise', 19000, 'vans'),
	('Pariah', 'pariah', 1420000, 'sports'),
	('Patriot', 'patriot', 55000, 'suvs'),
	('PCJ-600', 'pcj', 6200, 'motorcycles'),
	('Penumbra', 'penumbra', 28000, 'sports'),
	('Pfister', 'pfister811', 85000, 'super'),
	('Phoenix', 'phoenix', 12500, 'muscle'),
	('Picador', 'picador', 18000, 'muscle'),
	('Pigalle', 'pigalle', 20000, 'sportsclassics'),
	('Prairie', 'prairie', 12000, 'compacts'),
	('Premier', 'premier', 8000, 'sedans'),
	('Primo Custom', 'primo2', 14000, 'sedans'),
	('X80 Proto', 'prototipo', 2500000, 'super'),
	('Radius', 'radi', 29000, 'suvs'),
	('raiden', 'raiden', 1375000, 'sports'),
	('Rapid GT', 'rapidgt', 35000, 'sports'),
	('Rapid GT Convertible', 'rapidgt2', 45000, 'sports'),
	('Rapid GT3', 'rapidgt3', 885000, 'sportsclassics'),
	('Reaper', 'reaper', 150000, 'super'),
	('Rebel', 'rebel2', 35000, 'offroad'),
	('Regina', 'regina', 5000, 'sedans'),
	('Retinue', 'retinue', 615000, 'sportsclassics'),
	('Revolter', 'revolter', 1610000, 'sports'),
	('riata', 'riata', 380000, 'offroad'),
	('Rocoto', 'rocoto', 45000, 'suvs'),
	('Ruffian', 'ruffian', 6800, 'motorcycles'),
	('Ruiner 2', 'ruiner2', 5745600, 'muscle'),
	('Rumpo', 'rumpo', 15000, 'vans'),
	('Rumpo Trail', 'rumpo3', 19500, 'vans'),
	('Sabre Turbo', 'sabregt', 20000, 'muscle'),
	('Sabre GT', 'sabregt2', 25000, 'muscle'),
	('Sanchez', 'sanchez', 5300, 'motorcycles'),
	('Sanchez Sport', 'sanchez2', 5300, 'motorcycles'),
	('Sanctus', 'sanctus', 25000, 'motorcycles'),
	('Sandking', 'sandking', 55000, 'offroad'),
	('Savestra', 'savestra', 990000, 'sportsclassics'),
	('SC 1', 'sc1', 1603000, 'super'),
	('Schafter', 'schafter2', 25000, 'sedans'),
	('Schafter V12', 'schafter3', 50000, 'sports'),
	('Scorcher (velo)', 'scorcher', 280, 'motorcycles'),
	('Seminole', 'seminole', 25000, 'suvs'),
	('Sentinel', 'sentinel', 32000, 'coupes'),
	('Sentinel XS', 'sentinel2', 40000, 'coupes'),
	('Sentinel3', 'sentinel3', 650000, 'sports'),
	('Seven 70', 'seven70', 39500, 'sports'),
	('ETR1', 'sheava', 220000, 'super'),
	('Shotaro Concept', 'shotaro', 320000, 'motorcycles'),
	('Slam Van', 'slamvan3', 11500, 'muscle'),
	('Sovereign', 'sovereign', 22000, 'motorcycles'),
	('Stinger', 'stinger', 80000, 'sportsclassics'),
	('Stinger GT', 'stingergt', 75000, 'sportsclassics'),
	('Streiter', 'streiter', 500000, 'sports'),
	('Stretch', 'stretch', 90000, 'sedans'),
	('Stromberg', 'stromberg', 3185350, 'sports'),
	('Sultan', 'sultan', 15000, 'sports'),
	('Sultan RS', 'sultanrs', 65000, 'super'),
	('Super Diamond', 'superd', 130000, 'sedans'),
	('Surano', 'surano', 50000, 'sports'),
	('Surfer', 'surfer', 12000, 'vans'),
	('T20', 't20', 300000, 'super'),
	('Tailgater', 'tailgater', 30000, 'sedans'),
	('Tampa', 'tampa', 16000, 'muscle'),
	('Drift Tampa', 'tampa2', 80000, 'sports'),
	('Thrust', 'thrust', 24000, 'motorcycles'),
	('Tri bike (velo)', 'tribike3', 520, 'motorcycles'),
	('Trophy Truck', 'trophytruck', 60000, 'offroad'),
	('Trophy Truck Limited', 'trophytruck2', 80000, 'offroad'),
	('Tropos', 'tropos', 40000, 'sports'),
	('Turismo R', 'turismor', 350000, 'super'),
	('Tyrus', 'tyrus', 600000, 'super'),
	('Vacca', 'vacca', 120000, 'super'),
	('Vader', 'vader', 7200, 'motorcycles'),
	('Verlierer', 'verlierer2', 70000, 'sports'),
	('Vigero', 'vigero', 12500, 'muscle'),
	('Virgo', 'virgo', 14000, 'muscle'),
	('Viseris', 'viseris', 875000, 'sportsclassics'),
	('Visione', 'visione', 2250000, 'super'),
	('Voltic', 'voltic', 90000, 'super'),
	('Voltic 2', 'voltic2', 3830400, 'super'),
	('Voodoo', 'voodoo', 7200, 'muscle'),
	('Vortex', 'vortex', 9800, 'motorcycles'),
	('Warrener', 'warrener', 4000, 'sedans'),
	('Washington', 'washington', 9000, 'sedans'),
	('Windsor', 'windsor', 95000, 'coupes'),
	('Windsor Drop', 'windsor2', 125000, 'coupes'),
	('Woflsbane', 'wolfsbane', 9000, 'motorcycles'),
	('XLS', 'xls', 32000, 'suvs'),
	('Yosemite', 'yosemite', 485000, 'muscle'),
	('Youga', 'youga', 10800, 'vans'),
	('Youga Luxuary', 'youga2', 14500, 'vans'),
	('Z190', 'z190', 900000, 'sportsclassics'),
	('Zentorno', 'zentorno', 1500000, 'super'),
	('Zion', 'zion', 36000, 'coupes'),
	('Zion Cabrio', 'zion2', 45000, 'coupes'),
	('Zombie', 'zombiea', 9500, 'motorcycles'),
	('Zombie Luxuary', 'zombieb', 12000, 'motorcycles'),
	('Z-Type', 'ztype', 220000, 'sportsclassics');
/*!40000 ALTER TABLE `vehicles` ENABLE KEYS */;

-- tablo yapısı dökülüyor essentialmode.vehicle_categories
CREATE TABLE IF NOT EXISTS `vehicle_categories` (
  `name` varchar(60) COLLATE utf8_turkish_ci NOT NULL,
  `label` varchar(60) COLLATE utf8_turkish_ci NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

-- essentialmode.vehicle_categories: ~11 rows (yaklaşık) tablosu için veriler indiriliyor
/*!40000 ALTER TABLE `vehicle_categories` DISABLE KEYS */;
INSERT INTO `vehicle_categories` (`name`, `label`) VALUES
	('compacts', 'Compacts'),
	('coupes', 'Coupés'),
	('motorcycles', 'Motos'),
	('muscle', 'Muscle'),
	('offroad', 'Off Road'),
	('sedans', 'Sedans'),
	('sports', 'Sports'),
	('sportsclassics', 'Sports Classics'),
	('super', 'Super'),
	('suvs', 'SUVs'),
	('vans', 'Vans');
/*!40000 ALTER TABLE `vehicle_categories` ENABLE KEYS */;

-- tablo yapısı dökülüyor essentialmode.vehicle_sold
CREATE TABLE IF NOT EXISTS `vehicle_sold` (
  `client` varchar(50) COLLATE utf8_turkish_ci NOT NULL,
  `model` varchar(50) COLLATE utf8_turkish_ci NOT NULL,
  `plate` varchar(50) COLLATE utf8_turkish_ci NOT NULL,
  `soldby` varchar(50) COLLATE utf8_turkish_ci NOT NULL,
  `date` varchar(50) COLLATE utf8_turkish_ci NOT NULL,
  PRIMARY KEY (`plate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

-- essentialmode.vehicle_sold: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
/*!40000 ALTER TABLE `vehicle_sold` DISABLE KEYS */;
/*!40000 ALTER TABLE `vehicle_sold` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
