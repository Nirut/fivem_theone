-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.8-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for essentialmode
CREATE DATABASE IF NOT EXISTS `essentialmode` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `essentialmode`;

-- Dumping structure for table essentialmode.addon_account
CREATE TABLE IF NOT EXISTS `addon_account` (
  `name` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `shared` int(11) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.addon_account: ~11 rows (approximately)
/*!40000 ALTER TABLE `addon_account` DISABLE KEYS */;
INSERT INTO `addon_account` (`name`, `label`, `shared`) VALUES
	('caution', 'caution', 0),
	('property_black_money', 'Money Sale Property', 0),
	('society_ambulance', 'หน่วยแพทย์', 1),
	('society_cardealer', 'Concessionnaire', 1),
	('society_foodtruck', 'Foodtruck', 1),
	('society_mecano', 'Mécano', 1),
	('society_mechanic', 'Mechanic', 1),
	('society_police', 'เจ้าหน้าที่ตำรวจ', 1),
	('society_realestateagent', 'Agent immobilier', 1),
	('society_taxi', 'Taxi', 1),
	('society_vipz', 'vipz', 1);
/*!40000 ALTER TABLE `addon_account` ENABLE KEYS */;

-- Dumping structure for table essentialmode.addon_account_data
CREATE TABLE IF NOT EXISTS `addon_account_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `money` double NOT NULL,
  `owner` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5890 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.addon_account_data: ~21 rows (approximately)
/*!40000 ALTER TABLE `addon_account_data` DISABLE KEYS */;
INSERT INTO `addon_account_data` (`id`, `account_name`, `money`, `owner`) VALUES
	(5869, 'society_ambulance', 0, NULL),
	(5870, 'society_cardealer', 0, NULL),
	(5871, 'society_foodtruck', 0, NULL),
	(5872, 'society_mechanic', 800110000, NULL),
	(5873, 'society_police', 303800, NULL),
	(5874, 'society_taxi', 0, NULL),
	(5875, 'caution', 0, 'steam:11000013ac515f5'),
	(5876, 'property_black_money', 0, 'steam:11000013ac515f5'),
	(5877, 'property_black_money', 0, 'steam:110000104a1abc7'),
	(5878, 'caution', 0, 'steam:110000104a1abc7'),
	(5879, 'caution', 0, 'steam:110000115e0024c'),
	(5880, 'property_black_money', 0, 'steam:110000115e0024c'),
	(5881, 'society_vipz', 0, NULL),
	(5882, 'society_realestateagent', 32500, NULL),
	(5883, 'society_mecano', 0, NULL),
	(5884, 'caution', 0, 'steam:11000013dce3ca4'),
	(5885, 'property_black_money', 0, 'steam:11000013dce3ca4'),
	(5886, 'caution', 0, 'steam:11000013ac37e2b'),
	(5887, 'property_black_money', 0, 'steam:11000013ac37e2b'),
	(5888, 'property_black_money', 0, 'steam:110000136cd1665'),
	(5889, 'caution', 0, 'steam:110000136cd1665');
/*!40000 ALTER TABLE `addon_account_data` ENABLE KEYS */;

-- Dumping structure for table essentialmode.addon_inventory
CREATE TABLE IF NOT EXISTS `addon_inventory` (
  `name` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `shared` int(11) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.addon_inventory: ~9 rows (approximately)
/*!40000 ALTER TABLE `addon_inventory` DISABLE KEYS */;
INSERT INTO `addon_inventory` (`name`, `label`, `shared`) VALUES
	('property', 'Property', 0),
	('society_ambulance', 'หน่วยแพทย์', 1),
	('society_cardealer', 'Concesionnaire', 1),
	('society_mafia', 'Mafia', 1),
	('society_mecano', 'Mécano', 1),
	('society_mechanic', 'Mechanic', 1),
	('society_police', 'เจ้าหน้าที่ตำรวจ', 1),
	('society_taxi', 'Taxi', 1),
	('society_vipz', 'vipz', 1);
/*!40000 ALTER TABLE `addon_inventory` ENABLE KEYS */;

-- Dumping structure for table essentialmode.addon_inventory_items
CREATE TABLE IF NOT EXISTS `addon_inventory_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inventory_name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `count` int(11) NOT NULL,
  `owner` varchar(60) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=301 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.addon_inventory_items: ~117 rows (approximately)
/*!40000 ALTER TABLE `addon_inventory_items` DISABLE KEYS */;
INSERT INTO `addon_inventory_items` (`id`, `inventory_name`, `name`, `count`, `owner`) VALUES
	(57, 'society_police', 'shovel', 0, NULL),
	(58, 'society_police', 'weed_pooch', 389, NULL),
	(59, 'society_police', 'repairkit', 0, NULL),
	(60, 'society_police', 'opium', 409, NULL),
	(61, 'society_police', 'bread', 1, NULL),
	(62, 'society_police', 'clip', 443, NULL),
	(63, 'society_police', 'cannabis', 2492, NULL),
	(64, 'society_police', 'opium_pooch', 24, NULL),
	(65, 'society_police', 'weed', 7, NULL),
	(66, 'society_police', 'medikit', 0, NULL),
	(67, 'society_police', 'bulletproof', 189, NULL),
	(68, 'society_police', 'bottle', 5, NULL),
	(69, 'society_police', 'lrod', 1, NULL),
	(70, 'society_police', 'lbait', 79, NULL),
	(72, 'society_police', 'leather', 0, NULL),
	(73, 'society_taxi', 'cannabis', 0, NULL),
	(74, 'society_police', 'laranja', 0, NULL),
	(75, 'society_taxi', 'weed_pooch', 0, NULL),
	(83, 'society_mechanic', 'gazbottle', 0, NULL),
	(84, 'society_mechanic', 'carotool', 0, NULL),
	(85, 'society_mechanic', 'fixtool', 0, NULL),
	(86, 'society_police', 'acabbage', 0, NULL),
	(100, 'society_police', 'meth_pooch', 74, NULL),
	(101, 'society_police', 'meth', 611, NULL),
	(104, 'society_police', 'scissor', 1, NULL),
	(105, 'society_police', 'oil_a', 0, NULL),
	(106, 'society_police', 'sand', 0, NULL),
	(107, 'society_police', 'turtlebait', 0, NULL),
	(108, 'society_police', 'hatchet_lj', 1, NULL),
	(112, 'society_police', 'wood', 0, NULL),
	(153, 'society_police', 'lfish', 14, NULL),
	(154, 'society_police', 'chicken', 28, NULL),
	(155, 'society_police', 'knife_chicken', 0, NULL),
	(156, 'society_mecano', 'shovel', 0, NULL),
	(157, 'society_mecano', 'drill', 0, NULL),
	(160, 'society_mecano', 'knife_chicken', 1, NULL),
	(161, 'society_mecano', 'lrod', 1, NULL),
	(210, 'society_mechanic', 'mushroom', 0, NULL),
	(211, 'society_mechanic', 'milk_engine', 1, NULL),
	(212, 'society_mechanic', 'drill', 0, NULL),
	(213, 'society_mechanic', 'sickle', 1, NULL),
	(214, 'society_mechanic', 'hatchet_lj', 1, NULL),
	(226, 'property', 'stone', 1, 'steam:11000010bcd9aa5'),
	(227, 'property', 'marijuana_cigarette', 1, 'steam:11000010bcd9aa5'),
	(228, 'property', 'clip', 1, 'steam:11000010bcd9aa5'),
	(229, 'property', 'turtlebait', 1, 'steam:11000010bcd9aa5'),
	(230, 'property', 'oxygen_mask', 1, 'steam:11000010bcd9aa5'),
	(231, 'property', 'weed_pooch', 0, 'steam:11000010bcd9aa5'),
	(232, 'property', 'weed_pooch', 165, 'steam:110000115151cb4'),
	(233, 'property', 'oxygen_mask', 10, 'steam:110000115151cb4'),
	(234, 'property', 'cupcake', 0, 'steam:110000114b5f1de'),
	(235, 'property', 'fixkit', 37, 'steam:110000136506bc2'),
	(236, 'property', 'wrench_blueprint', 1, 'steam:110000136506bc2'),
	(237, 'property', 'snspistol_blueprint', 1, 'steam:110000136506bc2'),
	(238, 'property', 'SteelScrap', 60, 'steam:110000136506bc2'),
	(239, 'property', 'shovel', 1, 'steam:110000136506bc2'),
	(240, 'property', 'knife_chicken', 1, 'steam:110000136506bc2'),
	(241, 'property', 'cocacola', 0, 'steam:110000114b5f1de'),
	(242, 'property', 'iron', 60, 'steam:110000114b5f1de'),
	(243, 'property', 'chest_a', 10, 'steam:110000114b5f1de'),
	(244, 'property', 'diamond', 6, 'steam:110000114b5f1de'),
	(245, 'property', 'lighter', 0, 'steam:1100001328007ca'),
	(246, 'property', 'SteelScrap', 10, 'steam:1100001328007ca'),
	(247, 'property', 'SteelScrap', 18, 'steam:11000013ba816d4'),
	(248, 'property', 'SteelScrap', 5, 'steam:11000013b9dd243'),
	(249, 'property', 'stone', 5, 'steam:11000013b9dd243'),
	(250, 'property', 'gold', 79, 'steam:11000010bcd9aa5'),
	(251, 'property', 'SteelScrap', 2264, 'steam:11000010bcd9aa5'),
	(252, 'property', 'diamond', 30, 'steam:11000010bcd9aa5'),
	(253, 'property', 'chest_a', 304, 'steam:11000010bcd9aa5'),
	(254, 'society_police', 'coffe', 3, NULL),
	(255, 'property', 'cannabis', 1000, 'steam:11000011223dd27'),
	(256, 'property', 'weed_pooch', 92, 'steam:11000011223dd27'),
	(257, 'property', 'fixkit', 10, 'steam:11000011223dd27'),
	(258, 'property', 'rice_pro', 200, 'steam:11000011223dd27'),
	(259, 'property', 'clip', 5, 'steam:11000011223dd27'),
	(260, 'property', 'petrol', 1, 'steam:11000011b14adbc'),
	(261, 'property', 'copper', 2, 'steam:11000011b14adbc'),
	(262, 'property', 'gold', 1, 'steam:11000011b14adbc'),
	(263, 'property', 'fish', 19, 'steam:11000011b14adbc'),
	(264, 'property', 'gold', 0, 'steam:11000013cf1acff'),
	(265, 'property', 'stone', 1, 'steam:11000013cf1acff'),
	(266, 'society_mechanic', 'sandwich', 0, NULL),
	(267, 'property', 'jumelles', 1, 'steam:11000010bc9b236'),
	(268, 'property', 'sportlunch', 13, 'steam:11000010bc9b236'),
	(269, 'property', 'protein_shake', 20, 'steam:11000010bc9b236'),
	(270, 'property', 'powerade', 22, 'steam:11000010bc9b236'),
	(271, 'property', 'copper', 2, 'steam:1100001321288c8'),
	(272, 'property', 'sickle', 1, 'steam:110000136506bc2'),
	(273, 'property', 'drill', 1, 'steam:110000136506bc2'),
	(274, 'property', 'bong', 1, 'steam:110000136506bc2'),
	(275, 'property', 'diamond', 18, 'steam:11000011223dd27'),
	(276, 'property', 'iron', 249, 'steam:11000011223dd27'),
	(277, 'property', 'gold', 73, 'steam:11000011223dd27'),
	(278, 'society_mechanic', 'cannabis', 0, NULL),
	(279, 'society_mechanic', 'weed_pooch', 0, NULL),
	(280, 'property', 'copper', 100, 'steam:11000011223dd27'),
	(281, 'property', 'SteelScrap', 250, 'steam:11000011223dd27'),
	(282, 'property', 'phone', 0, 'steam:1100001335deef6'),
	(283, 'society_mechanic', 'pork', 9, NULL),
	(284, 'society_mechanic', 'drugs', 0, NULL),
	(285, 'property', 'anti', 30, 'steam:1100001139ef872'),
	(286, 'property', 'iron', 1000, 'steam:1100001139ef872'),
	(287, 'property', 'diamond', 500, 'steam:1100001139ef872'),
	(288, 'property', 'copper', 1000, 'steam:1100001139ef872'),
	(289, 'property', 'marijuana', 500, 'steam:1100001139ef872'),
	(290, 'property', 'SteelScrap', 607, 'steam:1100001139ef872'),
	(291, 'property', 'doubleaction_blueprint', 1, 'steam:1100001139ef872'),
	(292, 'property', 'snspistol_blueprint', 1, 'steam:1100001139ef872'),
	(293, 'property', 'switchblade_blueprint', 1, 'steam:1100001139ef872'),
	(294, 'property', 'wrench_blueprint', 1, 'steam:1100001139ef872'),
	(295, 'property', 'snspistol_blueprint', 1, 'steam:110000115dde3a7'),
	(296, 'property', 'cannabis', 2, 'steam:110000115c53a68'),
	(297, 'property', 'marijuana', 47, 'steam:110000115c53a68'),
	(298, 'property', 'weed_pooch', 13, 'steam:110000115c53a68'),
	(299, 'property', 'cannabis', 100, 'steam:11000010e09d44f'),
	(300, 'property', 'bcabbage', 0, 'steam:110000104a1abc7');
/*!40000 ALTER TABLE `addon_inventory_items` ENABLE KEYS */;

-- Dumping structure for table essentialmode.baninfo
CREATE TABLE IF NOT EXISTS `baninfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(25) COLLATE utf8mb4_bin DEFAULT 'no info',
  `license` varchar(50) COLLATE utf8mb4_bin DEFAULT 'no info',
  `liveid` varchar(21) COLLATE utf8mb4_bin DEFAULT 'no info',
  `xblid` varchar(21) COLLATE utf8mb4_bin DEFAULT 'no info',
  `discord` varchar(30) COLLATE utf8mb4_bin DEFAULT 'no info',
  `playerip` varchar(25) COLLATE utf8mb4_bin DEFAULT '0.0.0.0',
  `playername` varchar(32) COLLATE utf8mb4_bin DEFAULT 'no info',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.baninfo: ~0 rows (approximately)
/*!40000 ALTER TABLE `baninfo` DISABLE KEYS */;
/*!40000 ALTER TABLE `baninfo` ENABLE KEYS */;

-- Dumping structure for table essentialmode.banlist
CREATE TABLE IF NOT EXISTS `banlist` (
  `identifier` varchar(25) COLLATE utf8mb4_bin NOT NULL,
  `license` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `liveid` varchar(21) COLLATE utf8mb4_bin DEFAULT NULL,
  `xblid` varchar(21) COLLATE utf8mb4_bin DEFAULT NULL,
  `discord` varchar(30) COLLATE utf8mb4_bin DEFAULT NULL,
  `playerip` varchar(25) COLLATE utf8mb4_bin DEFAULT NULL,
  `targetplayername` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `sourceplayername` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `reason` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `timeat` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `expiration` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `permanent` int(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.banlist: ~0 rows (approximately)
/*!40000 ALTER TABLE `banlist` DISABLE KEYS */;
/*!40000 ALTER TABLE `banlist` ENABLE KEYS */;

-- Dumping structure for table essentialmode.banlisthistory
CREATE TABLE IF NOT EXISTS `banlisthistory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(25) COLLATE utf8mb4_bin NOT NULL,
  `license` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `liveid` varchar(21) COLLATE utf8mb4_bin DEFAULT NULL,
  `xblid` varchar(21) COLLATE utf8mb4_bin DEFAULT NULL,
  `discord` varchar(30) COLLATE utf8mb4_bin DEFAULT NULL,
  `playerip` varchar(25) COLLATE utf8mb4_bin DEFAULT NULL,
  `targetplayername` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `sourceplayername` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `reason` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `timeat` int(11) NOT NULL,
  `added` varchar(40) COLLATE utf8mb4_bin NOT NULL,
  `expiration` int(11) NOT NULL,
  `permanent` int(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.banlisthistory: ~0 rows (approximately)
/*!40000 ALTER TABLE `banlisthistory` DISABLE KEYS */;
/*!40000 ALTER TABLE `banlisthistory` ENABLE KEYS */;

-- Dumping structure for table essentialmode.bans
CREATE TABLE IF NOT EXISTS `bans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.bans: ~0 rows (approximately)
/*!40000 ALTER TABLE `bans` DISABLE KEYS */;
/*!40000 ALTER TABLE `bans` ENABLE KEYS */;

-- Dumping structure for table essentialmode.bansip
CREATE TABLE IF NOT EXISTS `bansip` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.bansip: ~0 rows (approximately)
/*!40000 ALTER TABLE `bansip` DISABLE KEYS */;
/*!40000 ALTER TABLE `bansip` ENABLE KEYS */;

-- Dumping structure for table essentialmode.banslicense
CREATE TABLE IF NOT EXISTS `banslicense` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `license` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.banslicense: ~0 rows (approximately)
/*!40000 ALTER TABLE `banslicense` DISABLE KEYS */;
/*!40000 ALTER TABLE `banslicense` ENABLE KEYS */;

-- Dumping structure for table essentialmode.billing
CREATE TABLE IF NOT EXISTS `billing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `sender` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `target_type` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `amount` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=481 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.billing: ~4 rows (approximately)
/*!40000 ALTER TABLE `billing` DISABLE KEYS */;
INSERT INTO `billing` (`id`, `identifier`, `sender`, `target_type`, `target`, `label`, `amount`) VALUES
	(462, 'steam:110000115e0024c', 'steam:110000104a1abc7', 'society', 'society_police', 'ปรับ: ทำลายทรัพย์สินของโรงพยาบาล', 50000),
	(464, 'steam:110000115e0024c', 'steam:110000104a1abc7', 'society', 'society_police', 'ปรับ: ปั่นป่วน ก่อจราจล ทำให้โรงพยาบาลวุ่นวาย', 30000),
	(465, 'steam:110000115e0024c', 'steam:110000104a1abc7', 'society', 'society_police', 'ปรับ: ปั่นป่วน ก่อจราจล ทำให้โรงพยาบาลวุ่นวาย', 30000),
	(467, 'steam:110000115e0024c', 'steam:110000104a1abc7', 'society', 'society_police', 'ปรับ: ตั้งใจใส่หน้ากากภายในโรงพยาบาล', 10000);
/*!40000 ALTER TABLE `billing` ENABLE KEYS */;

-- Dumping structure for table essentialmode.boats
CREATE TABLE IF NOT EXISTS `boats` (
  `name` varchar(60) NOT NULL,
  `model` varchar(60) NOT NULL,
  `price` int(11) NOT NULL,
  `category` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`model`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.boats: ~16 rows (approximately)
/*!40000 ALTER TABLE `boats` DISABLE KEYS */;
INSERT INTO `boats` (`name`, `model`, `price`, `category`) VALUES
	('Dinghy 4Seat', 'dinghy', 25000, 'boat'),
	('Dinghy 2Seat', 'dinghy2', 20000, 'boat'),
	('Dinghy Yacht', 'dinghy4', 25000, 'boat'),
	('Jetmax', 'jetmax', 30000, 'boat'),
	('Marquis', 'marquis', 45000, 'boat'),
	('Seashark', 'seashark', 10000, 'boat'),
	('Seashark Yacht', 'seashark3', 10000, 'boat'),
	('Speeder', 'speeder', 40000, 'boat'),
	('Squalo', 'squalo', 32000, 'boat'),
	('Submarine', 'submersible', 29000, 'subs'),
	('Kraken', 'submersible2', 31000, 'subs'),
	('Suntrap', 'suntrap', 34000, 'boat'),
	('Toro', 'toro', 38000, 'boat'),
	('Toro Yacht', 'toro2', 38000, 'boat'),
	('Tropic', 'tropic', 27000, 'boat'),
	('Tropic Yacht', 'tropic2', 27000, 'boat');
/*!40000 ALTER TABLE `boats` ENABLE KEYS */;

-- Dumping structure for table essentialmode.boat_categories
CREATE TABLE IF NOT EXISTS `boat_categories` (
  `name` varchar(60) NOT NULL,
  `label` varchar(60) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.boat_categories: ~2 rows (approximately)
/*!40000 ALTER TABLE `boat_categories` DISABLE KEYS */;
INSERT INTO `boat_categories` (`name`, `label`) VALUES
	('boat', 'Boats'),
	('subs', 'Submersibles');
/*!40000 ALTER TABLE `boat_categories` ENABLE KEYS */;

-- Dumping structure for table essentialmode.cardealer_vehicles
CREATE TABLE IF NOT EXISTS `cardealer_vehicles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vehicle` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `price` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.cardealer_vehicles: ~7 rows (approximately)
/*!40000 ALTER TABLE `cardealer_vehicles` DISABLE KEYS */;
INSERT INTO `cardealer_vehicles` (`id`, `vehicle`, `price`) VALUES
	(1, 'bison', 80000),
	(2, 'ek9', 800000),
	(3, 'asea', 60000),
	(4, 'AKUMA', 20000),
	(5, 'blista', 50000),
	(6, 'Tractor', 1500),
	(7, 'fnflan', 500000);
/*!40000 ALTER TABLE `cardealer_vehicles` ENABLE KEYS */;

-- Dumping structure for table essentialmode.characters
CREATE TABLE IF NOT EXISTS `characters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `dateofbirth` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `sex` varchar(1) COLLATE utf8mb4_bin NOT NULL DEFAULT 'M',
  `height` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  `lastdigits` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=214 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.characters: ~3 rows (approximately)
/*!40000 ALTER TABLE `characters` DISABLE KEYS */;
INSERT INTO `characters` (`id`, `identifier`, `firstname`, `lastname`, `dateofbirth`, `sex`, `height`, `lastdigits`) VALUES
	(197, 'steam:110000115e0024c', 'Tiffy', 'Miffy', '1994-16-02', 'f', '160', NULL),
	(199, 'steam:11000013dce3ca4', 'NaTasha', 'Romanov', '1999-01-17', 'f', '168', NULL),
	(211, 'steam:110000136cd1665', 'Dsadasdsa', 'Sgsdfdsf', '01/01/1111', 'f', '150', NULL),
	(212, 'steam:110000136cd1665', 'หฟกหฟกหฟ', 'ดเกเดกเกดเกดเ', '10/09/1999', 'm', '200', NULL),
	(213, 'steam:110000104a1abc7', 'TTTTT', 'TTTTT', '1999-09-09', 'f', '200', NULL);
/*!40000 ALTER TABLE `characters` ENABLE KEYS */;

-- Dumping structure for table essentialmode.communityservice
CREATE TABLE IF NOT EXISTS `communityservice` (
  `identifier` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `actions_remaining` int(10) NOT NULL,
  PRIMARY KEY (`identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.communityservice: ~0 rows (approximately)
/*!40000 ALTER TABLE `communityservice` DISABLE KEYS */;
/*!40000 ALTER TABLE `communityservice` ENABLE KEYS */;

-- Dumping structure for table essentialmode.datastore
CREATE TABLE IF NOT EXISTS `datastore` (
  `name` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `shared` int(11) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.datastore: ~9 rows (approximately)
/*!40000 ALTER TABLE `datastore` DISABLE KEYS */;
INSERT INTO `datastore` (`name`, `label`, `shared`) VALUES
	('property', 'Property', 0),
	('society_ambulance', 'หน่วยแพทย์', 1),
	('society_fbi', 'FBI', 1),
	('society_mafia', 'มาเฟีย', 1),
	('society_police', 'เจ้าหน้าที่ตำรวจ', 1),
	('society_vipz', 'vipz', 1),
	('user_ears', 'Ears', 0),
	('user_glasses', 'Glasses', 0),
	('user_helmet', 'Helmet', 0),
	('user_mask', 'Mask', 0);
/*!40000 ALTER TABLE `datastore` ENABLE KEYS */;

-- Dumping structure for table essentialmode.datastore_data
CREATE TABLE IF NOT EXISTS `datastore_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `owner` varchar(60) COLLATE utf8mb4_bin DEFAULT NULL,
  `data` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_datastore_owner_name` (`owner`,`name`),
  KEY `index_datastore_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=10486 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.datastore_data: ~35 rows (approximately)
/*!40000 ALTER TABLE `datastore_data` DISABLE KEYS */;
INSERT INTO `datastore_data` (`id`, `name`, `owner`, `data`) VALUES
	(10451, 'society_ambulance', NULL, '{}'),
	(10452, 'society_fbi', NULL, '{}'),
	(10453, 'society_mafia', NULL, '{}'),
	(10454, 'society_police', NULL, '{}'),
	(10455, 'user_glasses', 'steam:11000013ac515f5', '{}'),
	(10456, 'user_mask', 'steam:11000013ac515f5', '{}'),
	(10457, 'property', 'steam:11000013ac515f5', '{}'),
	(10458, 'user_helmet', 'steam:11000013ac515f5', '{}'),
	(10459, 'user_ears', 'steam:11000013ac515f5', '{}'),
	(10460, 'property', 'steam:110000104a1abc7', '{"dressing":[{"skin":{"glasses_2":0,"hair_1":0,"face":0.2,"lipstick_3":0,"decals_1":0,"watches_1":-1,"mom":21,"mask_2":0,"ears_1":-1,"age_1":0,"makeup_4":0,"age_2":0,"bodyb_1":0,"beard_3":0,"bags_1":0,"skin":0.5,"bproof_2":0,"arms":0,"helmet_2":0,"torso_2":0,"eyebrows_2":0,"bracelets_2":0,"blush_1":0,"arms_2":0,"helmet_1":-1,"beard_2":0,"eyebrows_4":0,"tshirt_1":0,"chest_2":0,"blush_3":0,"complexion_1":0,"eyebrows_3":0,"hair_color_2":0,"pants_2":0,"watches_2":0,"decals_2":0,"shoes_2":0,"chest_1":0,"beard_1":0,"tshirt_2":0,"bproof_1":2,"chain_1":0,"blemishes_1":0,"lipstick_2":0,"eye_color":0,"bodyb_2":0,"sun_1":0,"eyebrows_1":0,"moles_1":0,"glasses_1":0,"makeup_2":0,"bracelets_1":-1,"mask_1":0,"makeup_1":0,"chest_3":0,"hair_color_1":0,"complexion_2":0,"chain_2":0,"bags_2":0,"makeup_3":0,"sun_2":0,"torso_1":22,"moles_2":0,"ears_2":0,"lipstick_1":0,"sex":0,"lipstick_4":0,"hair_2":0,"blemishes_2":0,"pants_1":0,"beard_4":0,"blush_2":0,"shoes_1":0},"label":"ddsa"},{"skin":{"bracelets_1":-1,"hair_1":0,"moles_1":0,"lipstick_3":0,"decals_1":0,"watches_1":-1,"mom":21,"mask_2":0,"ears_1":-1,"age_1":0,"makeup_4":0,"age_2":0,"bodyb_1":0,"beard_3":0,"makeup_3":0,"skin":0.5,"bproof_2":0,"arms":0,"helmet_2":0,"torso_2":0,"eyebrows_2":0,"bracelets_2":0,"blush_1":0,"arms_2":0,"makeup_2":0,"beard_2":0,"eyebrows_4":0,"tshirt_1":0,"blemishes_1":0,"blush_3":0,"complexion_1":0,"eyebrows_3":0,"hair_color_2":0,"pants_2":0,"watches_2":0,"glasses_2":0,"shoes_2":0,"chest_1":0,"beard_1":0,"sex":0,"bproof_1":2,"tshirt_2":0,"beard_4":0,"lipstick_2":0,"eye_color":0,"bodyb_2":0,"sun_1":0,"eyebrows_1":0,"bags_2":0,"glasses_1":0,"mask_1":0,"makeup_1":0,"chest_2":0,"hair_color_1":0,"face":0.2,"chain_2":0,"complexion_2":0,"moles_2":0,"helmet_1":-1,"ears_2":0,"sun_2":0,"torso_1":22,"chain_1":0,"lipstick_4":0,"lipstick_1":0,"decals_2":0,"bags_1":0,"blush_2":0,"blemishes_2":0,"pants_1":0,"hair_2":0,"chest_3":0,"shoes_1":0},"label":"ทดสอบ"}],"weapons":[{"name":"WEAPON_KNIFE","ammo":0},{"name":"WEAPON_NIGHTSTICK","ammo":0},{"name":"WEAPON_HAMMER","ammo":0},{"name":"WEAPON_HAMMER","ammo":0},{"name":"WEAPON_BAT","ammo":0},{"name":"WEAPON_GOLFCLUB","ammo":0},{"name":"WEAPON_CROWBAR","ammo":0},{"name":"WEAPON_APPISTOL","ammo":18},{"name":"WEAPON_PISTOL50","ammo":18},{"name":"WEAPON_PISTOL50","ammo":18},{"name":"WEAPON_REVOLVER","ammo":18},{"name":"WEAPON_REVOLVER","ammo":18},{"name":"WEAPON_SNSPISTOL","ammo":18},{"name":"WEAPON_HEAVYPISTOL","ammo":18},{"name":"WEAPON_HEAVYPISTOL","ammo":18},{"name":"WEAPON_VINTAGEPISTOL","ammo":18},{"name":"WEAPON_VINTAGEPISTOL","ammo":18},{"name":"WEAPON_MICROSMG","ammo":30},{"name":"WEAPON_MICROSMG","ammo":30},{"name":"WEAPON_SMG","ammo":30},{"name":"WEAPON_SMG","ammo":30},{"name":"WEAPON_ASSAULTSMG","ammo":30},{"name":"WEAPON_ASSAULTSMG","ammo":30},{"name":"WEAPON_MACHINEPISTOL","ammo":30},{"name":"WEAPON_COMBATPDW","ammo":30},{"name":"WEAPON_PUMPSHOTGUN","ammo":36},{"name":"WEAPON_PUMPSHOTGUN","ammo":36},{"name":"WEAPON_SAWNOFFSHOTGUN","ammo":36},{"name":"WEAPON_SAWNOFFSHOTGUN","ammo":36},{"name":"WEAPON_BULLPUPSHOTGUN","ammo":36},{"name":"WEAPON_HEAVYSHOTGUN","ammo":36},{"name":"WEAPON_HEAVYSHOTGUN","ammo":36},{"name":"WEAPON_ADVANCEDRIFLE","ammo":90},{"name":"WEAPON_SPECIALCARBINE","ammo":90},{"name":"WEAPON_SPECIALCARBINE","ammo":90},{"name":"WEAPON_BULLPUPRIFLE","ammo":90},{"name":"WEAPON_COMPACTRIFLE","ammo":90},{"name":"WEAPON_COMPACTRIFLE","ammo":90},{"name":"WEAPON_MG","ammo":200},{"name":"WEAPON_MG","ammo":200},{"name":"WEAPON_GUSENBERG","ammo":200},{"name":"WEAPON_SNIPERRIFLE","ammo":10},{"name":"WEAPON_HEAVYSNIPER","ammo":10},{"name":"WEAPON_MARKSMANRIFLE","ammo":10},{"name":"WEAPON_GRENADELAUNCHER","ammo":10},{"name":"WEAPON_GRENADELAUNCHER","ammo":10},{"name":"WEAPON_RPG","ammo":1},{"name":"WEAPON_STINGER","ammo":1},{"name":"WEAPON_STINGER","ammo":1},{"name":"WEAPON_MINIGUN","ammo":250},{"name":"WEAPON_GRENADE","ammo":1},{"name":"WEAPON_STICKYBOMB","ammo":1},{"name":"WEAPON_STICKYBOMB","ammo":1},{"name":"WEAPON_SMOKEGRENADE","ammo":1},{"name":"WEAPON_BZGAS","ammo":1},{"name":"WEAPON_MOLOTOV","ammo":1},{"name":"WEAPON_MOLOTOV","ammo":1},{"name":"WEAPON_FIREEXTINGUISHER","ammo":2000},{"name":"WEAPON_FIREEXTINGUISHER","ammo":2000},{"name":"WEAPON_PETROLCAN","ammo":4500},{"name":"WEAPON_BALL","ammo":1},{"name":"WEAPON_BOTTLE","ammo":0},{"name":"WEAPON_DAGGER","ammo":0},{"name":"WEAPON_FIREWORK","ammo":1},{"name":"WEAPON_MUSKET","ammo":36},{"name":"WEAPON_HOMINGLAUNCHER","ammo":1},{"name":"WEAPON_PROXMINE","ammo":1},{"name":"WEAPON_SNOWBALL","ammo":1},{"name":"WEAPON_FLAREGUN","ammo":1},{"name":"WEAPON_MARKSMANPISTOL","ammo":18},{"name":"WEAPON_KNUCKLE","ammo":0},{"name":"WEAPON_HATCHET","ammo":0},{"name":"WEAPON_RAILGUN","ammo":1},{"name":"WEAPON_RAILGUN","ammo":1},{"name":"WEAPON_MACHETE","ammo":0},{"name":"WEAPON_MACHETE","ammo":0},{"name":"WEAPON_SWITCHBLADE","ammo":0},{"name":"WEAPON_FLASHLIGHT","ammo":0},{"name":"WEAPON_FLARE","ammo":1}]}'),
	(10461, 'user_glasses', 'steam:110000104a1abc7', '{}'),
	(10462, 'user_helmet', 'steam:110000104a1abc7', '{}'),
	(10463, 'user_ears', 'steam:110000104a1abc7', '{}'),
	(10464, 'user_mask', 'steam:110000104a1abc7', '{"hasMask":true,"skin":{"mask_1":134,"mask_2":8}}'),
	(10465, 'user_mask', 'steam:110000115e0024c', '{}'),
	(10466, 'property', 'steam:110000115e0024c', '{}'),
	(10467, 'user_helmet', 'steam:110000115e0024c', '{}'),
	(10468, 'user_ears', 'steam:110000115e0024c', '{}'),
	(10469, 'user_glasses', 'steam:110000115e0024c', '{}'),
	(10470, 'society_vipz', NULL, '{}'),
	(10471, 'user_ears', 'steam:11000013dce3ca4', '{}'),
	(10472, 'user_mask', 'steam:11000013dce3ca4', '{}'),
	(10473, 'property', 'steam:11000013dce3ca4', '{}'),
	(10474, 'user_helmet', 'steam:11000013dce3ca4', '{}'),
	(10475, 'user_glasses', 'steam:11000013dce3ca4', '{}'),
	(10476, 'property', 'steam:11000013ac37e2b', '{}'),
	(10477, 'user_helmet', 'steam:11000013ac37e2b', '{}'),
	(10478, 'user_ears', 'steam:11000013ac37e2b', '{}'),
	(10479, 'user_glasses', 'steam:11000013ac37e2b', '{}'),
	(10480, 'user_mask', 'steam:11000013ac37e2b', '{}'),
	(10481, 'property', 'steam:110000136cd1665', '{}'),
	(10482, 'user_helmet', 'steam:110000136cd1665', '{}'),
	(10483, 'user_ears', 'steam:110000136cd1665', '{}'),
	(10484, 'user_glasses', 'steam:110000136cd1665', '{}'),
	(10485, 'user_mask', 'steam:110000136cd1665', '{}');
/*!40000 ALTER TABLE `datastore_data` ENABLE KEYS */;

-- Dumping structure for table essentialmode.disc_inventory
CREATE TABLE IF NOT EXISTS `disc_inventory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner` text NOT NULL,
  `type` text DEFAULT NULL,
  `data` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.disc_inventory: ~0 rows (approximately)
/*!40000 ALTER TABLE `disc_inventory` DISABLE KEYS */;
/*!40000 ALTER TABLE `disc_inventory` ENABLE KEYS */;

-- Dumping structure for table essentialmode.disc_inventory_itemdata
CREATE TABLE IF NOT EXISTS `disc_inventory_itemdata` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `description` text DEFAULT NULL,
  `weight` int(11) NOT NULL DEFAULT 0,
  `closeonuse` tinyint(1) NOT NULL DEFAULT 0,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.disc_inventory_itemdata: ~0 rows (approximately)
/*!40000 ALTER TABLE `disc_inventory_itemdata` DISABLE KEYS */;
/*!40000 ALTER TABLE `disc_inventory_itemdata` ENABLE KEYS */;

-- Dumping structure for table essentialmode.economy
CREATE TABLE IF NOT EXISTS `economy` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `item` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `count` int(11) NOT NULL DEFAULT 0,
  `price` int(11) NOT NULL DEFAULT 0,
  `difference` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.economy: ~13 rows (approximately)
/*!40000 ALTER TABLE `economy` DISABLE KEYS */;
INSERT INTO `economy` (`ID`, `item`, `label`, `count`, `price`, `difference`) VALUES
	(1, 'shell_package', 'หอยผัดเผ็ด', 0, 312, 246),
	(2, 'glasses', 'กระจก', 0, 368, 4),
	(3, 'milk_package', 'นมกล่อง', 0, 379, 190),
	(4, 'rice_pro', 'ข้าวสาร', 0, 237, 223),
	(5, 'Packaged_plank', 'ไม้แปรรูป', 0, 360, 452),
	(6, 'petrol_raffin', 'น้ำมัน', 0, 253, 376),
	(7, 'packaged_chicken', 'ไก่แพ็ค', 0, 302, 117),
	(8, 'bcabbage', 'กระหล่ำแปลรูป', 0, 247, 379),
	(9, 'honey_b', 'น้ำผึ้ง', 0, 273, 319),
	(10, 'fish', 'ปลา', 0, 249, 290),
	(11, 'clothe', 'เสื้อผ้า', 0, 376, 410),
	(12, 'chest_a', 'เหรียญทองคำ', 0, 286, 18),
	(13, 'porkpackage', 'เนื้อหมูแพ็ค', 0, 391, 375);
/*!40000 ALTER TABLE `economy` ENABLE KEYS */;

-- Dumping structure for table essentialmode.fine_types
CREATE TABLE IF NOT EXISTS `fine_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=217 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.fine_types: ~169 rows (approximately)
/*!40000 ALTER TABLE `fine_types` DISABLE KEYS */;
INSERT INTO `fine_types` (`id`, `label`, `amount`, `category`) VALUES
	(1, 'ข้บรถชนแล้วหนี', 1000, 0),
	(2, 'ขับรถชนรถคนอื่นพัง', 1000, 0),
	(3, 'ฝ่าไฟแดง', 1000, 0),
	(4, 'ขับรถย้อนศร', 800, 0),
	(5, 'ไม่มีใบขับขี่ทุกชนิด', 500, 0),
	(6, 'ตำรวจเรียกแล้วไม่จอด', 3000, 0),
	(7, 'ขโมยรถ NPC', 1000, 0),
	(8, 'ขโมยรถผู้เล่น', 5000, 0),
	(9, 'ขโมยรถ (หมอ,ตำรวจ,ช่าง)', 5000, 0),
	(10, 'ถืออาวุธในที่สาธารณะ', 500, 0),
	(11, 'จับตัวประกันผู้เล่น', 1000, 1),
	(12, 'จับตัวประกันหมอ', 5000, 1),
	(13, 'จับตัวประกันตำรวจ', 5000, 1),
	(14, 'จับตัวประกันช่าง', 5000, 1),
	(15, 'ยิงปืนในที่สาธารณะ', 1500, 1),
	(16, 'ฆ่าหมอหัวละ', 30000, 1),
	(17, 'ฆ่าตำรวจหัวละ', 30000, 1),
	(18, 'ฆ่าช่าง', 30000, 1),
	(19, 'ฆ่าผู้เล่น', 10000, 1),
	(20, 'หนีการตรวจค้น', 5000, 1),
	(21, 'ปล้นผู้เล่น', 5000, 2),
	(22, 'พูดจาหมิ่นประมาทเจ้าหน้าที่', 500, 2),
	(23, 'ทำร้ายร่างกายผู้เล่น', 1500, 2),
	(24, 'มีสัตว์ผิดกฏหมายในครองครอง[เต่าหรือฉลาม]', 8000, 3),
	(25, 'ยึด สน.', 0, 2),
	(26, 'ชุมนุมใน สน.', 0, 2),
	(27, 'ถ่วงเวลาเจ้าหน้าที่', 500, 2),
	(28, 'ทำลายหลักฐาน', 5000, 2),
	(29, 'สมรู้ร่วมคิดก่อเหตุ', 500, 2),
	(30, 'แจ้งความเท็จ', 2000, 2),
	(31, 'ปลอมตัวเป็นเจ้าหน้าที่', 30000, 2),
	(35, 'ก่อเหตุใน สน.', 5000, 2),
	(36, 'มี Weed ครอบครองไว้เกิน 50 ชิ้น', 20000, 3),
	(37, 'มี Weed ครอบครองไว้น้อยกว่า 50 ชิ้น', 10000, 3),
	(42, 'มี Opium ครอบครอวไว้เกิน 50 ชิ้น', 20000, 3),
	(43, 'มี Opium ครอบครอวไว้น้อยกว่า 50 ชิ้น', 15000, 3),
	(44, 'มีเงินแดงในครอบครองน้อยกว่า 1 หมื่น', 8000, 3),
	(45, 'มีเงินแดงในครอบครองมากกว่า 1 หมื่น', 15000, 3),
	(46, 'มีเงินแดงในครอบครองมากกว่า 5 หมื่น', 50000, 3),
	(47, 'มีเงินแดงในครอบครองมากกว่า 1 แสน', 150000, 3),
	(48, 'ชุบหน่วยงาน', 2000, 4),
	(49, 'ชุบนอกเมือง', 4000, 4),
	(50, 'ชุบในเมือง', 3000, 4),
	(51, 'ชุบในสถานที่ยากจะเข้าถึง', 4600, 4),
	(52, 'ชุบในที่ผิดกฎหมาย', 5000, 4),
	(54, 'ข้ับรถชนแล้วหนี', 5000, 5),
	(55, 'ขับรถชนรถคนอื่นพัง', 7500, 5),
	(56, 'ฝ่าไฟแดง', 2000, 5),
	(57, 'ขับรถย้อนศร', 1500, 5),
	(58, 'ไม่มีใบขับขี่ทุกชนิด', 2500, 5),
	(59, 'ตำรวจเรียกแล้วไม่จอด', 5000, 5),
	(60, 'ขโมยรถ NPC', 5000, 5),
	(61, 'ขโมยรถผู้เล่น', 10000, 5),
	(62, 'ขโมยรถ (หมอ,ตำรวจ,ช่าง)', 27500, 5),
	(63, 'ถืออาวุธในที่สาธารณะ', 7500, 5),
	(64, 'จับตัวประกันผู้เล่น', 15000, 5),
	(65, 'จับตัวประกันหมอ', 100000, 5),
	(66, 'จับตัวประกันตำรวจ', 50000, 5),
	(67, 'จับตัวประกันช่าง', 35000, 5),
	(68, 'ยิงปืนในที่สาธารณะ', 15000, 5),
	(69, 'ฆ่าหมอหัวละ', 125000, 5),
	(70, 'ฆ่าตำรวจหัวละ', 50000, 5),
	(71, 'ฆ่าช่าง', 40000, 5),
	(72, 'ฆ่าผู้เล่น', 22500, 5),
	(73, 'หนีการตรวจค้น', 10000, 5),
	(74, 'ปล้นผู้เล่น', 17500, 5),
	(75, 'พูดจาหมิ่นประมาทเจ้าหน้าที่', 10000, 5),
	(76, 'ทำร้ายร่างกายผู้เล่น', 5000, 5),
	(77, 'นำรถเข้า สน.', 1000, 5),
	(78, 'ยึด สน.', 100000, 5),
	(79, 'ชุมนุมใน สน.', 17500, 5),
	(80, 'ถ่วงเวลาเจ้าหน้าที่', 2500, 5),
	(81, 'ทำลายหลักฐาน', 1000, 5),
	(82, 'สมรู้ร่วมคิดก่อเหตุ', 2500, 5),
	(83, 'แจ้งความเท็จ', 1000, 5),
	(84, 'ปลอมตัวเป็นเจ้าหน้าที่', 50000, 5),
	(85, 'ปล้นร้านค้า', 30000, 2),
	(86, 'ปล้นธนาคารเล็ก', 100000, 5),
	(87, 'ปล้นธนาคารใหญ่', 150000, 5),
	(88, 'ก่อเหตุใน สน', 5000, 5),
	(89, 'ซ่อมรถหน่วยงาน', 500, 6),
	(90, 'ซ่อมรถประชาชนทั่วไป', 1250, 6),
	(91, 'ซ่อมรถประชาชนสปอร์ต', 2000, 6),
	(92, 'ซ่อมรถประชาชนมอไซ', 1000, 6),
	(98, 'ส่งในเมือง', 1000, 8),
	(99, 'ส่งนอกเมือง', 3000, 8),
	(100, 'ส่งในที่ผิดกฎหมาย', 5000, 8),
	(101, 'มีอาวุธผิดกฎหมายไว้ในครอบครอง', 7500, 0),
	(102, 'ไม่มีใบอนุญาติพกปืน', 5000, 0),
	(106, 'การละเมิดของแตร', 1000, 0),
	(107, 'หลอกลวงทางธุรกิจ', 10000, 2),
	(108, 'มีเงินแดงในครอบครองมากกว่า 500K', 500000, 3),
	(109, 'ปิดบังหน้าตา', 800, 0),
	(110, 'ความเร็วเกินกำหนด', 800, 0),
	(111, 'จอดในที่ห้ามจอด', 800, 0),
	(112, 'พกพาอาวุธล่าสัตว์ในที่สาธารณะ', 10000, 2),
	(113, 'ก่อกวนหน้า สน.', 3000, 2),
	(130, 'ขับรถโดยประมาท', 5000, 0),
	(131, 'ขับรถโดยประมาททางอาญา', 10000, 0),
	(132, 'ฝ่าฝืนกฏจราจร ผ่าไฟแดง', 500, 0),
	(133, 'จอดรถผิดที่', 500, 0),
	(134, 'ขับรถโดยไม่มีใบขับขี่', 1500, 0),
	(135, 'ขับรถสภาพไม่เต็ม100%', 2500, 0),
	(136, 'เมาแล้วขับ', 2500, 0),
	(137, 'การแข่งรถโดยไม่ได้รับอนุญาติ', 10000, 0),
	(138, 'ก่อให้เกิตความไม่สงบ', 10000, 3),
	(139, 'รบกวนโดยใช้เสียง', 3500, 3),
	(140, 'ล่วงละเมิด ทางร่างกาย', 20000, 3),
	(141, 'ข้ามถนนโดยประมาท', 1500, 0),
	(142, 'ปฐิเสธการจับกุม', 10000, 0),
	(143, 'เมาในที่สาธารณะ', 3000, 0),
	(144, 'ขัดขวางการทำงานของเจ้าหน้าที่รัฐ', 50000, 3),
	(145, 'ตามสอดส่งเจ้าหน้าที่รัฐมากเกินไป', 5000, 3),
	(146, 'ปฎิเสธข้อกล่าวหา', 3000, 0),
	(147, 'แจ้งข้อมูลเท็จ', 5000, 0),
	(148, 'การละเมิดทางอาญา', 1000, 3),
	(149, 'ทำลายทรัพย์สิน น้อยกว่า $ 1,000', 1000, 3),
	(150, 'ชนแล้วหนีโดยไม่บาดเจ็บ', 800, 3),
	(151, 'การพยายามปล้นสิ่งของมีมูลค่าน้อยกว่า $950', 1000, 1),
	(152, 'มีกัญชา ครอบครอง/1ใบ', 2000, 2),
	(155, 'การปล้นสิ่งของที่มีมูลมากกว่า $950', 1000, 1),
	(156, 'ทำลายทรัพย์สิน มากกว่า $ 1,000', 3000, 3),
	(157, 'แจ้งเหตุ , แจ้งความหรือข้อมูลเท็จ', 3000, 0),
	(158, 'หลบหนีและมีความผิดทางอาญา', 15000, 0),
	(159, 'การแอบอ้างบุคคลว่าอื่นเป็นข้าราชการ', 8000, 3),
	(160, 'ปลอมแปลงหลักฐาน', 5000, 3),
	(161, 'ฟอกเงิน', 50000, 2),
	(162, 'กระทำให้ผู้อื่น รู้สึกถึงอันตราย', 3000, 3),
	(163, 'ชนแล้วหนี บาดเจ็บ', 3000, 3),
	(165, 'ทำร้ายร่างกาย', 20000, 3),
	(166, 'ทำร้ายเจ้าพนักงาน', 3000, 3),
	(167, 'ปล้นรถยนต์', 3000, 1),
	(168, 'การปล้นปืน', 25000, 1),
	(169, 'Weed กัญชา : 50อันขึ้นไป', 30000, 2),
	(170, 'Weed กัญชาแปรรูป : 20อันขึ้นไป', 50000, 2),
	(173, 'แอบซุกซ่อนสิ่งเสพติดไว้ในพาหนะ', 5000, 2),
	(174, 'ครอบครอง สิ่งของสำหรับลักพาตัว', 5000, 1),
	(175, 'ครอบของอาวุธสงครามไปก่อเหตุ', 30000, 2),
	(176, 'เงินผิดกฎหมาย/1000บาท', 1500, 2),
	(177, 'หลบหนีการจับกุม', 50000, 3),
	(178, 'ปฏิเสธที่จะจ่ายค่าปรับ', 500, 0),
	(182, 'พกปืนโดยไม่มีใบอนุญาติ', 5000, 2),
	(183, 'ก่อเหตุจลาจล', 1000, 3),
	(185, 'ปล้นธนาคารโดยใช้อาวุธ', 85000, 1),
	(186, 'ปล้นใช้อาวุธ', 35000, 1),
	(187, 'พยายามปล้นหรือขู่กรรโชก', 30000, 1),
	(188, 'พยานเท็จ', 6000, 0),
	(189, 'ติดสินบนเจ้าพนักงาน', 10000, 3),
	(190, 'ฆาตกรรมเจ้าหน้าที่รัฐ', 100000, 3),
	(191, 'สมรู้ร่วมคิดในการฆาตกรรมเจ้าหน้าที่รัฐ', 75000, 3),
	(192, 'ฆาตกรรมประชาชน', 30000, 3),
	(193, 'สมรู้ร่วมคิดในการฆาตกรรม', 10000, 3),
	(194, 'ช่วยเหลือผู้กระทำผิด', 5000, 3),
	(195, 'ฆ่าโดยไม่ไตร่ตรอง', 15000, 3),
	(196, 'ฆ่าคนตายโดยประมาท', 30000, 3),
	(198, 'ทรมาน', 35000, 3),
	(199, 'กักขังหน่วงเหนี่ยว', 8000, 3),
	(200, 'ลักพาตัว', 150000, 1),
	(201, 'ขโมยสิ่งของเจ้าหน้าที่รัฐ', 10000, 3),
	(206, 'ปิดบังใบหน้า', 10000, 0),
	(207, 'ดูหมิ่นเจ้าหน้าที่ตำรวจ', 10000, 3),
	(208, 'มีกัญชา ครอบครอง/1แพ็ค', 6000, 2),
	(209, 'ชกต่อยบริเวณโรงพยาบาล', 30000, 3),
	(210, 'ทำให้สลบบริเวณโรงพยาบาล', 50000, 3),
	(211, 'ถืออาวุธบริเวณโรงพยาบาล', 30000, 3),
	(212, 'ตั้งใจใส่หน้ากากภายในโรงพยาบาล', 10000, 3),
	(213, 'ไม่จ่ายค่ารักษาพยาบาล', 2500, 3),
	(214, 'ปั่นป่วน ก่อจราจล ทำให้โรงพยาบาลวุ่นวาย', 30000, 3),
	(215, 'ทำลายทรัพย์สินของโรงพยาบาล', 50000, 3),
	(216, 'วีไอพีทดสอบ', 0, 7);
/*!40000 ALTER TABLE `fine_types` ENABLE KEYS */;

-- Dumping structure for table essentialmode.gangs
CREATE TABLE IF NOT EXISTS `gangs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  `color` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.gangs: ~0 rows (approximately)
/*!40000 ALTER TABLE `gangs` DISABLE KEYS */;
/*!40000 ALTER TABLE `gangs` ENABLE KEYS */;

-- Dumping structure for table essentialmode.gang_grades
CREATE TABLE IF NOT EXISTS `gang_grades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gang_name` varchar(255) NOT NULL,
  `grade` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.gang_grades: ~0 rows (approximately)
/*!40000 ALTER TABLE `gang_grades` DISABLE KEYS */;
/*!40000 ALTER TABLE `gang_grades` ENABLE KEYS */;

-- Dumping structure for table essentialmode.hungerthirst
CREATE TABLE IF NOT EXISTS `hungerthirst` (
  `idSteam` varchar(255) NOT NULL,
  `hunger` int(11) NOT NULL DEFAULT 100,
  `thirst` int(11) NOT NULL DEFAULT 100
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.hungerthirst: ~0 rows (approximately)
/*!40000 ALTER TABLE `hungerthirst` DISABLE KEYS */;
/*!40000 ALTER TABLE `hungerthirst` ENABLE KEYS */;

-- Dumping structure for table essentialmode.items
CREATE TABLE IF NOT EXISTS `items` (
  `name` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `limit` int(11) NOT NULL DEFAULT -1,
  `rare` int(11) NOT NULL DEFAULT 0,
  `can_remove` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.items: ~220 rows (approximately)
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
INSERT INTO `items` (`name`, `label`, `limit`, `rare`, `can_remove`) VALUES
	('AED', 'เครื่องaed', 1, 0, 1),
	('Packaged_plank', 'ไม้แปรรูป', 100, 0, 1),
	('SteelScrap', 'เศษเหล็ก', 1000, 0, 1),
	('WEAPON_ADVANCEDRIFLE', 'Advanced Rifle', 1, 0, 1),
	('WEAPON_APPISTOL', 'AP Pistol', 1, 0, 1),
	('WEAPON_ASSAULTRIFLE', 'Assault Rifle', 1, 0, 1),
	('WEAPON_ASSAULTSHOTGUN', 'Assault Shotgun', 1, 0, 1),
	('WEAPON_ASSAULTSMG', 'Assault SMG', 1, 0, 1),
	('WEAPON_AUTOSHOTGUN', 'Auto Shotgun', 1, 0, 1),
	('WEAPON_BALL', 'Ball', 1, 0, 1),
	('WEAPON_BAT', 'Bat', 1, 0, 1),
	('WEAPON_BATTLEAXE', 'Battle Axe', 1, 0, 1),
	('WEAPON_BOTTLE', 'Bottle', 1, 0, 1),
	('WEAPON_BULLPUPRIFLE', 'Bullpup Rifle', 1, 0, 1),
	('WEAPON_BULLPUPSHOTGUN', 'Bullpup Shotgun', 1, 0, 1),
	('WEAPON_BZGAS', 'BZ Gas', 1, 0, 1),
	('WEAPON_CARBINERIFLE', 'Carbine Rifle', 1, 0, 1),
	('WEAPON_COMBATMG', 'Combat MG', 1, 0, 1),
	('WEAPON_COMBATPDW', 'Combat PDW', 1, 0, 1),
	('WEAPON_COMBATPISTOL', 'Combat Pistol', 1, 0, 1),
	('WEAPON_COMPACTLAUNCHER', 'Compact Launcher', 1, 0, 1),
	('WEAPON_COMPACTRIFLE', 'Compact Rifle', 1, 0, 1),
	('WEAPON_CROWBAR', 'Crowbar', 1, 0, 1),
	('WEAPON_DAGGER', 'Dagger', 1, 0, 1),
	('WEAPON_DBSHOTGUN', 'Double Barrel Shotgun', 1, 0, 1),
	('WEAPON_DIGISCANNER', 'Digiscanner', 1, 0, 1),
	('WEAPON_DOUBLEACTION', 'Double Action Revolver', 1, 0, 1),
	('WEAPON_FIREEXTINGUISHER', 'Fire Extinguisher', 1, 0, 1),
	('WEAPON_FIREWORK', 'Firework Launcher', 1, 0, 1),
	('WEAPON_FLARE', 'Flare', 1, 0, 1),
	('WEAPON_FLAREGUN', 'Flare Gun', 1, 0, 1),
	('WEAPON_FLASHLIGHT', 'Flashlight', 1, 0, 1),
	('WEAPON_GARBAGEBAG', 'Garbage Bag', 1, 0, 1),
	('WEAPON_GOLFCLUB', 'Golf Club', 1, 0, 1),
	('WEAPON_GRENADE', 'Gernade', 1, 0, 1),
	('WEAPON_GRENADELAUNCHER', 'Gernade Launcher', 1, 0, 1),
	('WEAPON_GUSENBERG', 'Gusenberg', 1, 0, 1),
	('WEAPON_HAMMER', 'Hammer', 1, 0, 1),
	('WEAPON_HANDCUFFS', 'Handcuffs', 1, 0, 1),
	('WEAPON_HATCHET', 'Hatchet', 1, 0, 1),
	('WEAPON_HEAVYPISTOL', 'Heavy Pistol', 1, 0, 1),
	('WEAPON_HEAVYSHOTGUN', 'Heavy Shotgun', 1, 0, 1),
	('WEAPON_HEAVYSNIPER', 'Heavy Sniper', 1, 0, 1),
	('WEAPON_HOMINGLAUNCHER', 'Homing Launcher', 1, 0, 1),
	('WEAPON_KNIFE', 'Knife', 1, 0, 1),
	('WEAPON_KNUCKLE', 'Knuckle Dusters ', 1, 0, 1),
	('WEAPON_MACHETE', 'Machete', 1, 0, 1),
	('WEAPON_MACHINEPISTOL', 'Machine Pistol', 1, 0, 1),
	('WEAPON_MARKSMANPISTOL', 'Marksman Pistol', 1, 0, 1),
	('WEAPON_MARKSMANRIFLE', 'Marksman Rifle', 1, 0, 1),
	('WEAPON_MG', 'MG', 1, 0, 1),
	('WEAPON_MICROSMG', 'Micro SMG', 1, 0, 1),
	('WEAPON_MINIGUN', 'Minigun', 1, 0, 1),
	('WEAPON_MINISMG', 'Mini SMG', 1, 0, 1),
	('WEAPON_MOLOTOV', 'Molotov', 1, 0, 1),
	('WEAPON_MUSKET', 'Musket', 1, 0, 1),
	('WEAPON_NIGHTSTICK', 'Police Baton', 1, 0, 1),
	('WEAPON_PETROLCAN', 'Petrol Can', 1, 0, 1),
	('WEAPON_PIPEBOMB', 'Pipe Bomb', 1, 0, 1),
	('WEAPON_PISTOL', 'Pistol', 1, 0, 1),
	('WEAPON_PISTOL50', 'Police .50', 1, 0, 1),
	('WEAPON_POOLCUE', 'Pool Cue', 1, 0, 1),
	('WEAPON_PROXMINE', 'Proximity Mine', 1, 0, 1),
	('WEAPON_PUMPSHOTGUN', 'Pump Shotgun', 1, 0, 1),
	('WEAPON_RAILGUN', 'Rail Gun', 1, 0, 1),
	('WEAPON_REVOLVER', 'Revolver', 1, 0, 1),
	('WEAPON_RPG', 'RPG', 1, 0, 1),
	('WEAPON_SAWNOFFSHOTGUN', 'Sawn Off Shotgun', 1, 0, 1),
	('WEAPON_SMG', 'SMG', 1, 0, 1),
	('WEAPON_SMOKEGRENADE', 'Smoke Gernade', 1, 0, 1),
	('WEAPON_SNIPERRIFLE', 'Sniper Rifle', 1, 0, 1),
	('WEAPON_SNOWBALL', 'Snow Ball', 1, 0, 1),
	('WEAPON_SNSPISTOL', 'SNS Pistol', 1, 0, 1),
	('WEAPON_SPECIALCARBINE', 'Special Rifle', 1, 0, 1),
	('WEAPON_STICKYBOMB', 'Sticky Bombs', 1, 0, 1),
	('WEAPON_STINGER', 'Stinger', 1, 0, 1),
	('WEAPON_STUNGUN', 'Police Taser', 1, 0, 1),
	('WEAPON_SWITCHBLADE', 'Switch Blade', 1, 0, 1),
	('WEAPON_VINTAGEPISTOL', 'Vintage Pistol', 1, 0, 1),
	('WEAPON_WRENCH', 'Wrench', 1, 0, 1),
	('acabbage', 'กระหล่ำสด', 100, 0, 1),
	('alive_chicken', 'เนื้อไก่สด', 70, 0, 1),
	('anti', 'ยาแก้ไอ', 30, 0, 1),
	('bandage', 'ผ้าพันแผล', 20, 0, 1),
	('bcabbage', 'กระหล่ำถูกล้าง', 100, 0, 1),
	('beer', 'เบียร์', 15, 0, 1),
	('blowpipe', 'คบเพลิง', 10, 0, 1),
	('blowtorch', 'Blowtorch', 1, 0, 1),
	('bobbypin', 'Bobbypin', 3, 0, 1),
	('bong', 'บ้องกัญชา', 1, 0, 1),
	('bottle', 'ขวด', 100, 0, 1),
	('boxmilk', 'นม', 20, 0, 1),
	('bread', 'ขนมปัง', 15, 0, 1),
	('britadeira', 'บดหิน', 1, 0, 1),
	('cannabis', 'กัญชา', 100, 0, 1),
	('carokit', 'Body Kit', 1, 0, 1),
	('carotool', 'Tools', 4, 0, 1),
	('chest_a', 'เหรียญทองคำ', -1, 0, 1),
	('chicken', 'ไก่สด', 20, 0, 1),
	('chicken_meat', 'เนื้อไก่สด', 100, 0, 1),
	('chicken_package', 'เนื้อไก่แช่เข็ง', 100, 0, 1),
	('chickenwithoutfeather', 'ไก่ที่ถูกดึงขนออก', 20, 0, 1),
	('chocolate', 'ช็อกโกแลต', 10, 0, 1),
	('cigarett', 'บุหรี่', 20, 0, 1),
	('clip', 'Weapon Clip', 5, 0, 1),
	('clothe', 'ผ้าหนังสัตว์', 40, 0, 1),
	('cocacola', 'แป๊ปซี่', 15, 0, 1),
	('coffe', 'กาแฟ', 15, 0, 1),
	('coke', 'โคเคน', 40, 0, 1),
	('coke_pooch', 'โคเคนสำเร็จ', 20, 0, 1),
	('copper', 'ทองแดง', 100, 0, 1),
	('cow_card', 'บัตรคราฟพิเศษ', 20, 0, 1),
	('craftkit', 'เครื่องมือคราฟ', 1, 0, 1),
	('crazypersoncard', 'บัตรคนบ้า', 1, 0, 1),
	('croquettes', 'อาหารสัตว์', 20, 0, 1),
	('cupcake', 'เค้ก', 15, 0, 1),
	('cutted_wood', '\r\nไม้ที่ถูกตัด', 100, 0, 1),
	('diamond', 'เพชร', 10, 0, 1),
	('diesel', 'ถังน้ำมัน', 100, 0, 1),
	('doubleaction_blueprint', 'Doubleaction Blueprint', 1, 0, 1),
	('drill', 'เครื่องเจาะหิน', 1, 0, 1),
	('essence', 'แก๊ส', 20, 0, 1),
	('fabric', '\r\nผ้า', 80, 0, 1),
	('fish', 'ปลา', 1000, 0, 1),
	('fishbait', 'เหยื่อตกปลา', 30, 0, 1),
	('fishingrod', 'เบ็ดตกปลา', 1, 0, 1),
	('fixkit', 'ชุดซ่อม', 5, 0, 1),
	('fixtool', '\r\nเครื่องมือซ่อม', 6, 0, 1),
	('fuel_a', 'น้ำมัน', 100, 0, 1),
	('fuel_b', '\r\nน้ำมันแปรรูป', 100, 0, 1),
	('gazbottle', '\r\nขวดแก๊ส', 11, 0, 1),
	('glasses', 'กระจก', 10, 0, 1),
	('gold', 'ทอง', 50, 0, 1),
	('gunpowder', 'ดินปืน', -1, 0, 1),
	('gym_membership', 'บัตรสมาชิกยิม', 1, 0, 1),
	('hamajifish', 'ปลาฮามาจิ', 100, 0, 1),
	('hamburger', 'แฮมเบอร์เกอร์', 15, 0, 1),
	('handcuffs', 'กุญแจมือ', 1, 0, 1),
	('hatchet_lj', 'ขวานตัดไม้', 1, 0, 1),
	('honey_a', 'รังผึ้ง', 100, 0, 1),
	('honey_b', 'แพ๊คน้ำผึ้ง', 100, 0, 1),
	('hydrocodone', 'Hydrocodone', 5, 0, 1),
	('icetea', 'ชา', 15, 0, 1),
	('iron', 'เหล็ก', 100, 0, 1),
	('jumelles', 'กล้องส่องทางไกล', 1, 0, 1),
	('knife_chicken', 'มีดฆ่าไก่', 1, 0, 1),
	('leather', 'Leather', 100, 0, 1),
	('lighter', 'ไฟแช๊ค', 1, 0, 1),
	('lockpick', 'Lock pick', 10, 0, 1),
	('marijuana', 'กัญชาแปรรูป', 50, 0, 1),
	('marijuana_cigarette', 'กัญชามวน', 15, 0, 1),
	('meat', 'Meat', 100, 0, 1),
	('meatshark', 'เนื้อฉลาม', 100, 0, 1),
	('medikit', 'ยาชุบชีวิต', 10, 0, 1),
	('meth', 'แอมเฟตามีน', 40, 0, 1),
	('meth_pooch', 'แพ็คแอมเฟตามีน', 20, 0, 1),
	('milk', 'นมวัวสด', 20, 0, 1),
	('milk_engine', 'เครื่องรีดนมวัว', 1, 0, 1),
	('milk_package', 'นมวัวกล่อง', 50, 0, 1),
	('morphine', 'Morphine', 5, 0, 1),
	('mushroom', 'Dirty Mushroom', 100, 0, 1),
	('mushroom_d', 'Mushroom', 100, 0, 1),
	('mushroom_p', 'Mushroom Pack', 100, 0, 1),
	('oxygen_mask', 'หน้ากากออกซิเจน', 5, 0, 1),
	('packaged_chicken', 'แพ๊คไก่', 70, 0, 1),
	('packaged_plank', 'packaged_plank', 100, 0, 1),
	('petrol', 'น้ำมันดิบ', 100, 0, 1),
	('petrol_raffin', 'น้ำมันแปรรูป', 100, 0, 1),
	('phone', 'มือถือ', 1, 0, 1),
	('pork', 'เนื้อหมู', 120, 0, 1),
	('porkpackage', 'เนื้อหมูแพ็ค', 40, 0, 1),
	('powerade', 'Powerade', -1, 0, 1),
	('prawn', 'กุ้งสด', 100, 0, 1),
	('prawnbait', 'กุ้งสำเร็จรูป', 100, 0, 1),
	('pro_wood', 'ไม้แปรรูป', 100, 0, 1),
	('protein_shake', 'Protein Shake', 15, 0, 1),
	('radio', 'radio', 1, 0, 1),
	('rice', 'ข้าวเปลือก', 100, 0, 1),
	('rice_pro', 'ข้าวสาร', 100, 0, 1),
	('rope', 'rope', -1, 0, 1),
	('sand', 'กองทราย', 100, 0, 1),
	('sand_a', 'กองทราย', 100, 0, 1),
	('sand_b', 'แพ็คทราย', 50, 0, 1),
	('sandwich', 'แซนด์วิช', 15, 0, 1),
	('shark', 'ปลาฉลาม', 20, 0, 1),
	('sharkfin', 'หูฉลาม', -1, 0, 1),
	('shell', 'หอยสด', 100, 0, 1),
	('shell_a', 'หอยสด', 100, 0, 1),
	('shell_b', 'หอยสะอาด', 100, 0, 1),
	('shell_c', 'หอยลวก', 100, 0, 1),
	('shell_engine', 'เครื่องมือขุดหอย', 1, 0, 1),
	('shell_package', 'หอยผัดเผ็ด', 50, 0, 1),
	('shovel', 'พลั่วขุดทราย', 1, 0, 1),
	('sickle', 'เคียวเกี่ยวข้าว', 1, 0, 1),
	('skate', 'skate', 1, 0, 1),
	('slaughtered_chicken', 'ไก่สำเร็จรูป', 70, 0, 1),
	('snspistol_blueprint', 'Snspistol Blueprint', 1, 0, 1),
	('sportlunch', 'Sportlunch', 15, 0, 1),
	('squid', 'ปลาหมึก', 100, 0, 1),
	('squidbait', '\r\nเหยื่อปลาหมึก', 100, 0, 1),
	('stone', 'หิน', 50, 0, 1),
	('switchblade_blueprint', 'Switchblade Blueprint', 1, 0, 1),
	('tequila', 'เทคิวล่า', 15, 0, 1),
	('turtle', 'เต่าทะเล', 20, 0, 1),
	('turtlebait', 'เหยื่อตกเต่าทะเล', 30, 0, 1),
	('vicodin', 'Vicodin', 5, 0, 1),
	('vodka', 'วอดก้า', 15, 0, 1),
	('washed_stone', 'หินล้าง', 50, 0, 1),
	('water', 'น้ำ', 10, 0, 1),
	('weaponflashlight', 'Weapon Flashlight', 1, 0, 1),
	('weapongrip', 'Weapon Grip', 1, 0, 1),
	('weaponskin', 'Weapon Skin', 1, 0, 1),
	('weed_pooch', 'กัญชาแพ๊ค', 25, 0, 1),
	('whisky', 'วิสกี้', 15, 0, 1),
	('wine', 'ไวน์', 15, 0, 1),
	('wood', 'ไม้', 100, 0, 1),
	('wool', 'ขนสัตว์', 40, 0, 1),
	('worm', '\r\nหนอน', 100, 0, 1),
	('wrench_blueprint', 'Wrench Blueprint', 1, 0, 1),
	('zippybag', 'ถุงซิบใส', 100, 0, 1);
/*!40000 ALTER TABLE `items` ENABLE KEYS */;

-- Dumping structure for table essentialmode.jail
CREATE TABLE IF NOT EXISTS `jail` (
  `identifier` varchar(100) NOT NULL,
  `isjailed` tinyint(1) DEFAULT NULL,
  `J_Time` datetime NOT NULL,
  `J_Cell` varchar(20) NOT NULL,
  `Jailer` varchar(100) NOT NULL,
  `Jailer_ID` varchar(100) NOT NULL,
  PRIMARY KEY (`identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.jail: ~0 rows (approximately)
/*!40000 ALTER TABLE `jail` DISABLE KEYS */;
/*!40000 ALTER TABLE `jail` ENABLE KEYS */;

-- Dumping structure for table essentialmode.jobs
CREATE TABLE IF NOT EXISTS `jobs` (
  `name` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `whitelisted` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.jobs: ~15 rows (approximately)
/*!40000 ALTER TABLE `jobs` DISABLE KEYS */;
INSERT INTO `jobs` (`name`, `label`, `whitelisted`) VALUES
	('ambulance', 'แพทย์', 1),
	('cardealer', 'Concessionnaire', 1),
	('deliverer', 'Deliverer', 0),
	('fueler', 'ขนส่งน้ำมัน', 1),
	('garbage', '♻พนักงานเก็บขยะ', 0),
	('mafia', 'มาเฟีย', 1),
	('mecano', 'Mécano', 0),
	('mechanic', 'ช่างซ่อม', 1),
	('offambulance', 'Off-Duty', 1),
	('offpolice', 'Off-Duty', 1),
	('police', 'ตำรวจ', 1),
	('realestateagent', 'Agent immobilier', 0),
	('taxi', 'Taxi', 1),
	('unemployed', 'ว่างงาน', 0),
	('vipz', '👑 สมาชิกวีไอพี', 0);
/*!40000 ALTER TABLE `jobs` ENABLE KEYS */;

-- Dumping structure for table essentialmode.job_grades
CREATE TABLE IF NOT EXISTS `job_grades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_name` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `grade` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `salary` int(11) NOT NULL,
  `skin_male` longtext COLLATE utf8mb4_bin NOT NULL,
  `skin_female` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=197 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.job_grades: ~45 rows (approximately)
/*!40000 ALTER TABLE `job_grades` DISABLE KEYS */;
INSERT INTO `job_grades` (`id`, `job_name`, `grade`, `name`, `label`, `salary`, `skin_male`, `skin_female`) VALUES
	(1, 'unemployed', 0, 'unemployed', 'Unemployed', 0, '{}', '{}'),
	(9, 'ambulance', 0, 'ambulance', 'นักเรียนแพทย์', 500, '{"torso_2":3,"makeup_3":0,"moles_2":0,"sun_2":0,"makeup_2":0,"arms":88,"bproof_2":0,"makeup_4":0,"decals_1":57,"hair_color_1":0,"complexion_2":0,"tshirt_1":11,"beard_4":0,"helmet_2":0,"makeup_1":0,"eyebrows_2":10,"complexion_1":0,"skin":0,"pants_1":20,"mask_1":0,"beard_2":10,"age_2":0,"decals_2":0,"blush_2":0,"watches_2":0,"ears_1":-1,"chain_2":0,"hair_color_2":0,"blush_1":0,"hair_1":18,"bproof_1":0,"chain_1":126,"torso_1":23,"blemishes_2":0,"arms_2":0,"mask_2":0,"ears_2":0,"watches_1":-1,"chest_1":0,"beard_3":0,"eyebrows_1":9,"eyebrows_3":0,"pants_2":0,"bags_1":0,"tshirt_2":0,"bracelets_1":-1,"chest_2":0,"sex":0,"glasses_1":0,"bags_2":0,"glasses_2":3,"eyebrows_4":0,"chest_3":0,"age_1":0,"face":0,"helmet_1":122,"bodyb_1":0,"shoes":9,"eye_color":0,"blush_3":0,"shoes_1":4,"shoes_2":2,"bracelets_2":0,"sun_1":0,"blemishes_1":0,"beard_1":10,"lipstick_4":0,"lipstick_3":0,"lipstick_2":0,"lipstick_1":0,"hair_2":0,"moles_1":0,"bodyb_2":0}', '{"mask_2":0,"complexion_1":0,"makeup_4":0,"helmet_2":1,"lipstick_3":0,"age_2":0,"chain_2":0,"moles_1":0,"beard_1":11,"decals_1":66,"lipstick_4":0,"bracelets_2":0,"ears_2":0,"arms":88,"hair_1":18,"bproof_2":0,"torso_2":2,"makeup_3":0,"chain_1":0,"pants_1":12,"glasses_1":4,"lipstick_1":0,"makeup_1":0,"eyebrows_4":0,"beard_4":0,"age_1":0,"blemishes_2":0,"face":0,"beard_3":0,"helmet_1":7,"bodyb_1":0,"eyebrows_1":0,"tshirt_2":0,"bags_2":0,"eyebrows_2":0,"chest_2":0,"bproof_1":0,"lipstick_2":0,"shoes":9,"shoes_1":10,"skin":4,"watches_2":0,"torso_1":10,"blush_3":0,"sun_1":0,"blemishes_1":0,"watches_1":3,"hair_color_1":0,"mask_1":0,"ears_1":-1,"beard_2":10,"shoes_2":1,"bags_1":0,"sun_2":0,"hair_2":1,"arms_2":0,"chest_3":0,"blush_1":0,"makeup_2":0,"eye_color":0,"complexion_2":0,"bracelets_1":-1,"sex":1,"eyebrows_3":0,"decals_2":0,"moles_2":0,"blush_2":0,"glasses_2":3,"hair_color_2":0,"pants_2":8,"bodyb_2":0,"chest_1":0,"tshirt_1":57}'),
	(10, 'ambulance', 1, 'doctor', 'แพทย์ชำนาญ', 800, '{"torso_2":3,"makeup_3":0,"moles_2":0,"sun_2":0,"makeup_2":0,"arms":88,"bproof_2":0,"makeup_4":0,"decals_1":57,"hair_color_1":0,"complexion_2":0,"tshirt_1":11,"beard_4":0,"helmet_2":0,"makeup_1":0,"eyebrows_2":10,"complexion_1":0,"skin":0,"pants_1":20,"mask_1":0,"beard_2":10,"age_2":0,"decals_2":0,"blush_2":0,"watches_2":0,"ears_1":-1,"chain_2":0,"hair_color_2":0,"blush_1":0,"hair_1":18,"bproof_1":0,"chain_1":126,"torso_1":23,"blemishes_2":0,"arms_2":0,"mask_2":0,"ears_2":0,"watches_1":-1,"chest_1":0,"beard_3":0,"eyebrows_1":9,"eyebrows_3":0,"pants_2":0,"bags_1":0,"tshirt_2":0,"bracelets_1":-1,"chest_2":0,"sex":0,"glasses_1":0,"bags_2":0,"glasses_2":3,"eyebrows_4":0,"chest_3":0,"age_1":0,"face":0,"helmet_1":122,"bodyb_1":0,"shoes":9,"eye_color":0,"blush_3":0,"shoes_1":4,"shoes_2":2,"bracelets_2":0,"sun_1":0,"blemishes_1":0,"beard_1":10,"lipstick_4":0,"lipstick_3":0,"lipstick_2":0,"lipstick_1":0,"hair_2":0,"moles_1":0,"bodyb_2":0}', '{"mask_2":0,"complexion_1":0,"makeup_4":0,"helmet_2":1,"lipstick_3":0,"age_2":0,"chain_2":0,"moles_1":0,"beard_1":11,"decals_1":66,"lipstick_4":0,"bracelets_2":0,"ears_2":0,"arms":88,"hair_1":18,"bproof_2":0,"torso_2":2,"makeup_3":0,"chain_1":0,"pants_1":12,"glasses_1":4,"lipstick_1":0,"makeup_1":0,"eyebrows_4":0,"beard_4":0,"age_1":0,"blemishes_2":0,"face":0,"beard_3":0,"helmet_1":7,"bodyb_1":0,"eyebrows_1":0,"tshirt_2":0,"bags_2":0,"eyebrows_2":0,"chest_2":0,"bproof_1":0,"lipstick_2":0,"shoes":9,"shoes_1":10,"skin":4,"watches_2":0,"torso_1":10,"blush_3":0,"sun_1":0,"blemishes_1":0,"watches_1":3,"hair_color_1":0,"mask_1":0,"ears_1":-1,"beard_2":10,"shoes_2":1,"bags_1":0,"sun_2":0,"hair_2":1,"arms_2":0,"chest_3":0,"blush_1":0,"makeup_2":0,"eye_color":0,"complexion_2":0,"bracelets_1":-1,"sex":1,"eyebrows_3":0,"decals_2":0,"moles_2":0,"blush_2":0,"glasses_2":3,"hair_color_2":0,"pants_2":8,"bodyb_2":0,"chest_1":0,"tshirt_1":57}'),
	(11, 'ambulance', 2, 'chief_doctor', 'รองผู้อำนวยการแพทย์', 1000, '{"torso_2":3,"makeup_3":0,"moles_2":0,"sun_2":0,"makeup_2":0,"arms":88,"bproof_2":0,"makeup_4":0,"decals_1":57,"hair_color_1":0,"complexion_2":0,"tshirt_1":11,"beard_4":0,"helmet_2":0,"makeup_1":0,"eyebrows_2":10,"complexion_1":0,"skin":0,"pants_1":20,"mask_1":0,"beard_2":10,"age_2":0,"decals_2":0,"blush_2":0,"watches_2":0,"ears_1":-1,"chain_2":0,"hair_color_2":0,"blush_1":0,"hair_1":18,"bproof_1":0,"chain_1":126,"torso_1":23,"blemishes_2":0,"arms_2":0,"mask_2":0,"ears_2":0,"watches_1":-1,"chest_1":0,"beard_3":0,"eyebrows_1":9,"eyebrows_3":0,"pants_2":0,"bags_1":0,"tshirt_2":0,"bracelets_1":-1,"chest_2":0,"sex":0,"glasses_1":0,"bags_2":0,"glasses_2":3,"eyebrows_4":0,"chest_3":0,"age_1":0,"face":0,"helmet_1":122,"bodyb_1":0,"shoes":9,"eye_color":0,"blush_3":0,"shoes_1":4,"shoes_2":2,"bracelets_2":0,"sun_1":0,"blemishes_1":0,"beard_1":10,"lipstick_4":0,"lipstick_3":0,"lipstick_2":0,"lipstick_1":0,"hair_2":0,"moles_1":0,"bodyb_2":0}', '{"mask_2":0,"complexion_1":0,"makeup_4":0,"helmet_2":1,"lipstick_3":0,"age_2":0,"chain_2":0,"moles_1":0,"beard_1":11,"decals_1":66,"lipstick_4":0,"bracelets_2":0,"ears_2":0,"arms":88,"hair_1":18,"bproof_2":0,"torso_2":2,"makeup_3":0,"chain_1":0,"pants_1":12,"glasses_1":4,"lipstick_1":0,"makeup_1":0,"eyebrows_4":0,"beard_4":0,"age_1":0,"blemishes_2":0,"face":0,"beard_3":0,"helmet_1":7,"bodyb_1":0,"eyebrows_1":0,"tshirt_2":0,"bags_2":0,"eyebrows_2":0,"chest_2":0,"bproof_1":0,"lipstick_2":0,"shoes":9,"shoes_1":10,"skin":4,"watches_2":0,"torso_1":10,"blush_3":0,"sun_1":0,"blemishes_1":0,"watches_1":3,"hair_color_1":0,"mask_1":0,"ears_1":-1,"beard_2":10,"shoes_2":1,"bags_1":0,"sun_2":0,"hair_2":1,"arms_2":0,"chest_3":0,"blush_1":0,"makeup_2":0,"eye_color":0,"complexion_2":0,"bracelets_1":-1,"sex":1,"eyebrows_3":0,"decals_2":0,"moles_2":0,"blush_2":0,"glasses_2":3,"hair_color_2":0,"pants_2":8,"bodyb_2":0,"chest_1":0,"tshirt_1":57}'),
	(12, 'ambulance', 3, 'boss', 'ผู้อำนวยการแพทย์', 2000, '{"torso_2":3,"makeup_3":0,"moles_2":0,"sun_2":0,"makeup_2":0,"arms":88,"bproof_2":0,"makeup_4":0,"decals_1":57,"hair_color_1":0,"complexion_2":0,"tshirt_1":11,"beard_4":0,"helmet_2":0,"makeup_1":0,"eyebrows_2":10,"complexion_1":0,"skin":0,"pants_1":20,"mask_1":0,"beard_2":10,"age_2":0,"decals_2":0,"blush_2":0,"watches_2":0,"ears_1":-1,"chain_2":0,"hair_color_2":0,"blush_1":0,"hair_1":18,"bproof_1":0,"chain_1":126,"torso_1":23,"blemishes_2":0,"arms_2":0,"mask_2":0,"ears_2":0,"watches_1":-1,"chest_1":0,"beard_3":0,"eyebrows_1":9,"eyebrows_3":0,"pants_2":0,"bags_1":0,"tshirt_2":0,"bracelets_1":-1,"chest_2":0,"sex":0,"glasses_1":0,"bags_2":0,"glasses_2":3,"eyebrows_4":0,"chest_3":0,"age_1":0,"face":0,"helmet_1":122,"bodyb_1":0,"shoes":9,"eye_color":0,"blush_3":0,"shoes_1":4,"shoes_2":2,"bracelets_2":0,"sun_1":0,"blemishes_1":0,"beard_1":10,"lipstick_4":0,"lipstick_3":0,"lipstick_2":0,"lipstick_1":0,"hair_2":0,"moles_1":0,"bodyb_2":0}', '{"mask_2":0,"complexion_1":0,"makeup_4":0,"helmet_2":1,"lipstick_3":0,"age_2":0,"chain_2":0,"moles_1":0,"beard_1":11,"decals_1":66,"lipstick_4":0,"bracelets_2":0,"ears_2":0,"arms":88,"hair_1":18,"bproof_2":0,"torso_2":2,"makeup_3":0,"chain_1":0,"pants_1":12,"glasses_1":4,"lipstick_1":0,"makeup_1":0,"eyebrows_4":0,"beard_4":0,"age_1":0,"blemishes_2":0,"face":0,"beard_3":0,"helmet_1":7,"bodyb_1":0,"eyebrows_1":0,"tshirt_2":0,"bags_2":0,"eyebrows_2":0,"chest_2":0,"bproof_1":0,"lipstick_2":0,"shoes":9,"shoes_1":10,"skin":4,"watches_2":0,"torso_1":10,"blush_3":0,"sun_1":0,"blemishes_1":0,"watches_1":3,"hair_color_1":0,"mask_1":0,"ears_1":-1,"beard_2":10,"shoes_2":1,"bags_1":0,"sun_2":0,"hair_2":1,"arms_2":0,"chest_3":0,"blush_1":0,"makeup_2":0,"eye_color":0,"complexion_2":0,"bracelets_1":-1,"sex":1,"eyebrows_3":0,"decals_2":0,"moles_2":0,"blush_2":0,"glasses_2":3,"hair_color_2":0,"pants_2":8,"bodyb_2":0,"chest_1":0,"tshirt_1":57}'),
	(13, 'police', 0, 'recruit', 'นักเรียนตำรวจ', 800, '{"makeup_3":25,"sex":0,"lipstick_3":19,"lipstick_1":0,"eyebrows_3":1,"hair_color_1":1,"decals_2":2,"helmet_1":65,"age_2":7,"eyebrows_4":0,"pants_1":28,"decals_1":8,"makeup_1":0,"tshirt_1":58,"hair_color_2":0,"shoes":1,"mask_1":0,"ears_2":0,"tshirt_2":0,"beard_3":0,"glasses":0,"beard_1":7,"beard_2":7,"skin":0,"lipstick_4":0,"lipstick_2":0,"beard_4":0,"makeup_4":0,"bags_2":0,"eyebrows_1":10,"torso_2":1,"pants_2":0,"mask_2":0,"ears_1":2,"hair_1":12,"bproof_2":1,"helmet_2":0,"face":0,"shoes_1":51,"chain_1":125,"bproof_1":0,"hair_2":0,"makeup_2":0,"glasses_2":2,"glasses_1":15,"torso_1":95,"shoes_2":0,"eyebrows_2":10,"arms":37,"bags_1":0,"chain_2":0,"age_1":5}', '{"mask_2":0,"complexion_1":0,"makeup_4":0,"helmet_2":0,"makeup_2":0,"age_2":0,"chain_2":0,"moles_1":0,"bodyb_2":0,"decals_1":0,"lipstick_4":0,"bracelets_2":0,"hair_color_2":0,"arms":19,"hair_1":18,"bproof_2":0,"torso_2":0,"makeup_3":0,"chain_1":125,"pants_1":35,"glasses_1":0,"lipstick_1":0,"makeup_1":0,"eyebrows_4":0,"blush_1":0,"chest_1":0,"blemishes_2":0,"face":0,"beard_3":0,"helmet_1":46,"bodyb_1":0,"eyebrows_1":9,"tshirt_2":0,"bags_2":0,"eyebrows_2":10,"chest_2":0,"bproof_1":0,"sex":0,"shoes_1":21,"glasses_2":1,"watches_2":0,"torso_1":55,"blush_3":0,"sun_1":0,"blemishes_1":0,"watches_1":-1,"hair_color_1":0,"mask_1":0,"ears_1":2,"beard_2":10,"shoes_2":0,"skin":0,"lipstick_2":0,"hair_2":1,"tshirt_1":129,"sun_2":0,"age_1":0,"lipstick_3":0,"eye_color":0,"complexion_2":0,"beard_1":11,"beard_4":0,"chest_3":0,"decals_2":0,"moles_2":0,"blush_2":0,"ears_2":0,"arms_2":0,"pants_2":0,"eyebrows_3":0,"bags_1":0,"bracelets_1":-1}'),
	(14, 'police', 3, 'lieutenant', 'ผู้กำกับ', 1300, '{"tshirt_2":2,"eyebrows_4":0,"makeup_3":0,"blush_2":0,"arms_2":0,"torso_1":64,"makeup_2":0,"lipstick_1":0,"glasses_2":0,"bags_1":43,"chain_2":0,"watches_1":3,"mask_2":0,"sun_2":0,"beard_3":0,"beard_4":0,"bproof_1":0,"makeup_4":0,"eyebrows_1":0,"chain_1":0,"face":0,"eye_color":0,"beard_1":18,"helmet_2":0,"eyebrows_3":0,"moles_2":0,"sun_1":0,"beard_2":10,"blush_3":0,"chest_2":0,"ears_2":0,"lipstick_4":0,"age_2":0,"hair_1":52,"blemishes_2":0,"chest_1":0,"glasses_1":0,"tshirt_1":2,"age_1":0,"eyebrows_2":10,"ears_1":-1,"sex":0,"hair_2":0,"bags_2":0,"bracelets_2":0,"hair_color_1":0,"torso_2":0,"skin":0,"helmet_1":-1,"chest_3":0,"bracelets_1":-1,"shoes_2":0,"pants_2":4,"complexion_1":0,"arms":6,"watches_2":0,"pants_1":0,"moles_1":0,"bproof_2":0,"decals_2":0,"complexion_2":0,"decals_1":0,"mask_1":0,"blush_1":0,"bodyb_1":0,"blemishes_1":0,"bodyb_2":0,"shoes_1":48,"lipstick_3":0,"lipstick_2":0,"hair_color_2":0,"makeup_1":0}', '{"mask_2":0,"complexion_1":0,"makeup_4":0,"helmet_2":0,"makeup_2":0,"age_2":0,"chain_2":0,"moles_1":0,"bodyb_2":0,"decals_1":0,"lipstick_4":0,"bracelets_2":0,"hair_color_2":0,"arms":19,"hair_1":18,"bproof_2":0,"torso_2":0,"makeup_3":0,"chain_1":125,"pants_1":35,"glasses_1":0,"lipstick_1":0,"makeup_1":0,"eyebrows_4":0,"blush_1":0,"chest_1":0,"blemishes_2":0,"face":0,"beard_3":0,"helmet_1":46,"bodyb_1":0,"eyebrows_1":9,"tshirt_2":0,"bags_2":0,"eyebrows_2":10,"chest_2":0,"bproof_1":0,"sex":0,"shoes_1":21,"glasses_2":1,"watches_2":0,"torso_1":55,"blush_3":0,"sun_1":0,"blemishes_1":0,"watches_1":-1,"hair_color_1":0,"mask_1":0,"ears_1":2,"beard_2":10,"shoes_2":0,"skin":0,"lipstick_2":0,"hair_2":1,"tshirt_1":129,"sun_2":0,"age_1":0,"lipstick_3":0,"eye_color":0,"complexion_2":0,"beard_1":11,"beard_4":0,"chest_3":0,"decals_2":0,"moles_2":0,"blush_2":0,"ears_2":0,"arms_2":0,"pants_2":0,"eyebrows_3":0,"bags_1":0,"bracelets_1":-1}'),
	(17, 'police', 4, 'boss', 'ผู้บังคับบัญชาตำรวจ', 3000, '{"tshirt_2":2,"eyebrows_4":0,"makeup_3":0,"blush_2":0,"arms_2":0,"torso_1":64,"makeup_2":0,"lipstick_1":0,"glasses_2":0,"bags_1":43,"chain_2":0,"watches_1":3,"mask_2":0,"sun_2":0,"beard_3":0,"beard_4":0,"bproof_1":0,"makeup_4":0,"eyebrows_1":0,"chain_1":0,"face":0,"eye_color":0,"beard_1":18,"helmet_2":0,"eyebrows_3":0,"moles_2":0,"sun_1":0,"beard_2":10,"blush_3":0,"chest_2":0,"ears_2":0,"lipstick_4":0,"age_2":0,"hair_1":52,"blemishes_2":0,"chest_1":0,"glasses_1":0,"tshirt_1":2,"age_1":0,"eyebrows_2":10,"ears_1":-1,"sex":0,"hair_2":0,"bags_2":0,"bracelets_2":0,"hair_color_1":0,"torso_2":0,"skin":0,"helmet_1":-1,"chest_3":0,"bracelets_1":-1,"shoes_2":0,"pants_2":4,"complexion_1":0,"arms":6,"watches_2":0,"pants_1":0,"moles_1":0,"bproof_2":0,"decals_2":0,"complexion_2":0,"decals_1":0,"mask_1":0,"blush_1":0,"bodyb_1":0,"blemishes_1":0,"bodyb_2":0,"shoes_1":48,"lipstick_3":0,"lipstick_2":0,"hair_color_2":0,"makeup_1":0}', '{"mask_2":0,"complexion_1":0,"makeup_4":0,"helmet_2":0,"makeup_2":0,"age_2":0,"chain_2":0,"moles_1":0,"bodyb_2":0,"decals_1":0,"lipstick_4":0,"bracelets_2":0,"hair_color_2":0,"arms":19,"hair_1":18,"bproof_2":0,"torso_2":0,"makeup_3":0,"chain_1":125,"pants_1":35,"glasses_1":0,"lipstick_1":0,"makeup_1":0,"eyebrows_4":0,"blush_1":0,"chest_1":0,"blemishes_2":0,"face":0,"beard_3":0,"helmet_1":46,"bodyb_1":0,"eyebrows_1":9,"tshirt_2":0,"bags_2":0,"eyebrows_2":10,"chest_2":0,"bproof_1":0,"sex":0,"shoes_1":21,"glasses_2":1,"watches_2":0,"torso_1":55,"blush_3":0,"sun_1":0,"blemishes_1":0,"watches_1":-1,"hair_color_1":0,"mask_1":0,"ears_1":2,"beard_2":10,"shoes_2":0,"skin":0,"lipstick_2":0,"hair_2":1,"tshirt_1":129,"sun_2":0,"age_1":0,"lipstick_3":0,"eye_color":0,"complexion_2":0,"beard_1":11,"beard_4":0,"chest_3":0,"decals_2":0,"moles_2":0,"blush_2":0,"ears_2":0,"arms_2":0,"pants_2":0,"eyebrows_3":0,"bags_1":0,"bracelets_1":-1}'),
	(31, 'deliverer', 0, 'employee', 'Employee', 0, '{"ears_1":2,"eyebrows_2":10,"chest_1":0,"moles_2":0,"helmet_2":2,"lipstick_3":0,"decals_1":0,"tshirt_1":15,"glasses_1":19,"arms":0,"glasses_2":0,"face":0,"mask_1":0,"pants_1":0,"blemishes_1":0,"blush_3":0,"makeup_1":0,"moles_1":0,"shoes_1":48,"complexion_1":0,"eyebrows_1":9,"beard_3":0,"beard_1":10,"bproof_1":0,"eye_color":0,"hair_2":0,"ears_2":0,"blemishes_2":0,"eyebrows_3":0,"hair_color_1":0,"decals_2":0,"hair_1":18,"shoes_2":0,"chain_2":0,"watches_1":-1,"makeup_2":0,"mask_2":0,"hair_color_2":0,"shoes":35,"arms_2":0,"bracelets_1":-1,"lipstick_4":0,"chest_2":0,"torso_1":22,"bags_2":0,"pants_2":1,"eyebrows_4":0,"chain_1":4,"tshirt_2":0,"age_2":0,"complexion_2":0,"bproof_2":0,"sex":0,"bracelets_2":0,"beard_2":10,"blush_2":0,"torso_2":2,"lipstick_2":0,"sun_2":0,"bags_1":0,"lipstick_1":0,"bodyb_1":0,"age_1":0,"skin":0,"blush_1":0,"watches_2":0,"beard_4":0,"makeup_4":0,"chest_3":0,"bodyb_2":0,"makeup_3":0,"helmet_1":56,"sun_1":0}', '{"ears_1":2,"eyebrows_2":10,"chest_1":0,"moles_2":0,"helmet_2":2,"lipstick_3":0,"decals_1":0,"tshirt_1":15,"glasses_1":19,"arms":0,"glasses_2":0,"face":0,"mask_1":0,"pants_1":0,"blemishes_1":0,"blush_3":0,"makeup_1":0,"moles_1":0,"shoes_1":48,"complexion_1":0,"eyebrows_1":9,"beard_3":0,"beard_1":10,"bproof_1":0,"eye_color":0,"hair_2":0,"ears_2":0,"blemishes_2":0,"eyebrows_3":0,"hair_color_1":0,"decals_2":0,"hair_1":18,"shoes_2":0,"chain_2":0,"watches_1":-1,"makeup_2":0,"mask_2":0,"hair_color_2":0,"shoes":35,"arms_2":0,"bracelets_1":-1,"lipstick_4":0,"chest_2":0,"torso_1":22,"bags_2":0,"pants_2":1,"eyebrows_4":0,"chain_1":4,"tshirt_2":0,"age_2":0,"complexion_2":0,"bproof_2":0,"sex":0,"bracelets_2":0,"beard_2":10,"blush_2":0,"torso_2":2,"lipstick_2":0,"sun_2":0,"bags_1":0,"lipstick_1":0,"bodyb_1":0,"age_1":0,"skin":0,"blush_1":0,"watches_2":0,"beard_4":0,"makeup_4":0,"chest_3":0,"bodyb_2":0,"makeup_3":0,"helmet_1":56,"sun_1":0}'),
	(108, 'garbage', 0, 'employee', 'Employee', 0, '{"tshirt_1":59,"torso_1":89,"arms":31,"pants_1":36,"glasses_1":19,"decals_2":0,"hair_color_2":0,"helmet_2":0,"hair_color_1":0,"face":2,"glasses_2":0,"torso_2":1,"shoes":35,"hair_1":0,"skin":0,"sex":0,"glasses_1":19,"pants_2":0,"hair_2":0,"decals_1":0,"tshirt_2":0,"helmet_1":5}', '{"tshirt_1":36,"torso_1":0,"arms":68,"pants_1":30,"glasses_1":15,"decals_2":0,"hair_color_2":0,"helmet_2":0,"hair_color_1":0,"face":27,"glasses_2":0,"torso_2":11,"shoes":26,"hair_1":5,"skin":0,"sex":1,"glasses_1":15,"pants_2":2,"hair_2":0,"decals_1":0,"tshirt_2":0,"helmet_1":19}'),
	(135, 'fueler', 0, 'employee', 'employee', 0, '{}', '{}'),
	(147, 'taxi', 0, 'recrue', 'Recrue', 300, '{"eyebrows_2":10,"bodyb_1":0,"eye_color":0,"sex":0,"sun_1":0,"face":0,"chest_2":0,"lipstick_4":0,"chest_3":0,"moles_1":0,"lipstick_3":0,"shoes":9,"beard_2":10,"shoes_2":0,"torso_1":4,"bodyb_2":0,"moles_2":0,"tshirt_2":0,"glasses_1":0,"helmet_2":0,"makeup_4":0,"complexion_1":0,"watches_1":3,"hair_color_1":1,"chain_1":12,"makeup_2":0,"beard_4":0,"age_1":0,"watches_2":0,"blush_3":0,"tshirt_1":11,"bracelets_1":-1,"eyebrows_4":0,"makeup_1":0,"hair_1":19,"blemishes_2":0,"bags_1":0,"sun_2":0,"pants_2":0,"chest_1":0,"chain_2":0,"age_2":0,"decals_1":0,"bproof_2":0,"blemishes_1":0,"helmet_1":-1,"ears_2":0,"eyebrows_1":0,"ears_1":-1,"torso_2":0,"bags_2":0,"lipstick_1":0,"arms_2":0,"mask_2":0,"hair_color_2":0,"arms":20,"pants_1":10,"decals_2":0,"mask_1":0,"bproof_1":0,"blush_1":0,"complexion_2":0,"glasses_2":3,"beard_3":0,"shoes_1":21,"makeup_3":0,"eyebrows_3":0,"hair_2":0,"beard_1":11,"bracelets_2":0,"skin":0,"lipstick_2":0,"blush_2":0}', '{"eyebrows_2":10,"bodyb_1":0,"eye_color":0,"sex":0,"sun_1":0,"face":0,"chest_2":0,"lipstick_4":0,"chest_3":0,"moles_1":0,"lipstick_3":0,"shoes":9,"beard_2":10,"shoes_2":0,"torso_1":4,"bodyb_2":0,"moles_2":0,"tshirt_2":0,"glasses_1":0,"helmet_2":0,"makeup_4":0,"complexion_1":0,"watches_1":3,"hair_color_1":1,"chain_1":12,"makeup_2":0,"beard_4":0,"age_1":0,"watches_2":0,"blush_3":0,"tshirt_1":11,"bracelets_1":-1,"eyebrows_4":0,"makeup_1":0,"hair_1":19,"blemishes_2":0,"bags_1":0,"sun_2":0,"pants_2":0,"chest_1":0,"chain_2":0,"age_2":0,"decals_1":0,"bproof_2":0,"blemishes_1":0,"helmet_1":-1,"ears_2":0,"eyebrows_1":0,"ears_1":-1,"torso_2":0,"bags_2":0,"lipstick_1":0,"arms_2":0,"mask_2":0,"hair_color_2":0,"arms":20,"pants_1":10,"decals_2":0,"mask_1":0,"bproof_1":0,"blush_1":0,"complexion_2":0,"glasses_2":3,"beard_3":0,"shoes_1":21,"makeup_3":0,"eyebrows_3":0,"hair_2":0,"beard_1":11,"bracelets_2":0,"skin":0,"lipstick_2":0,"blush_2":0}'),
	(148, 'taxi', 1, 'novice', 'Novice', 500, '{"eyebrows_2":10,"bodyb_1":0,"eye_color":0,"sex":0,"sun_1":0,"face":0,"chest_2":0,"lipstick_4":0,"chest_3":0,"moles_1":0,"lipstick_3":0,"shoes":9,"beard_2":10,"shoes_2":0,"torso_1":4,"bodyb_2":0,"moles_2":0,"tshirt_2":0,"glasses_1":0,"helmet_2":0,"makeup_4":0,"complexion_1":0,"watches_1":3,"hair_color_1":1,"chain_1":12,"makeup_2":0,"beard_4":0,"age_1":0,"watches_2":0,"blush_3":0,"tshirt_1":11,"bracelets_1":-1,"eyebrows_4":0,"makeup_1":0,"hair_1":19,"blemishes_2":0,"bags_1":0,"sun_2":0,"pants_2":0,"chest_1":0,"chain_2":0,"age_2":0,"decals_1":0,"bproof_2":0,"blemishes_1":0,"helmet_1":-1,"ears_2":0,"eyebrows_1":0,"ears_1":-1,"torso_2":0,"bags_2":0,"lipstick_1":0,"arms_2":0,"mask_2":0,"hair_color_2":0,"arms":20,"pants_1":10,"decals_2":0,"mask_1":0,"bproof_1":0,"blush_1":0,"complexion_2":0,"glasses_2":3,"beard_3":0,"shoes_1":21,"makeup_3":0,"eyebrows_3":0,"hair_2":0,"beard_1":11,"bracelets_2":0,"skin":0,"lipstick_2":0,"blush_2":0}', '{"eyebrows_2":10,"bodyb_1":0,"eye_color":0,"sex":0,"sun_1":0,"face":0,"chest_2":0,"lipstick_4":0,"chest_3":0,"moles_1":0,"lipstick_3":0,"shoes":9,"beard_2":10,"shoes_2":0,"torso_1":4,"bodyb_2":0,"moles_2":0,"tshirt_2":0,"glasses_1":0,"helmet_2":0,"makeup_4":0,"complexion_1":0,"watches_1":3,"hair_color_1":1,"chain_1":12,"makeup_2":0,"beard_4":0,"age_1":0,"watches_2":0,"blush_3":0,"tshirt_1":11,"bracelets_1":-1,"eyebrows_4":0,"makeup_1":0,"hair_1":19,"blemishes_2":0,"bags_1":0,"sun_2":0,"pants_2":0,"chest_1":0,"chain_2":0,"age_2":0,"decals_1":0,"bproof_2":0,"blemishes_1":0,"helmet_1":-1,"ears_2":0,"eyebrows_1":0,"ears_1":-1,"torso_2":0,"bags_2":0,"lipstick_1":0,"arms_2":0,"mask_2":0,"hair_color_2":0,"arms":20,"pants_1":10,"decals_2":0,"mask_1":0,"bproof_1":0,"blush_1":0,"complexion_2":0,"glasses_2":3,"beard_3":0,"shoes_1":21,"makeup_3":0,"eyebrows_3":0,"hair_2":0,"beard_1":11,"bracelets_2":0,"skin":0,"lipstick_2":0,"blush_2":0}'),
	(149, 'taxi', 2, 'experimente', 'Experimente', 500, '{"eyebrows_2":10,"bodyb_1":0,"eye_color":0,"sex":0,"sun_1":0,"face":0,"chest_2":0,"lipstick_4":0,"chest_3":0,"moles_1":0,"lipstick_3":0,"shoes":9,"beard_2":10,"shoes_2":0,"torso_1":4,"bodyb_2":0,"moles_2":0,"tshirt_2":0,"glasses_1":0,"helmet_2":0,"makeup_4":0,"complexion_1":0,"watches_1":3,"hair_color_1":1,"chain_1":12,"makeup_2":0,"beard_4":0,"age_1":0,"watches_2":0,"blush_3":0,"tshirt_1":11,"bracelets_1":-1,"eyebrows_4":0,"makeup_1":0,"hair_1":19,"blemishes_2":0,"bags_1":0,"sun_2":0,"pants_2":0,"chest_1":0,"chain_2":0,"age_2":0,"decals_1":0,"bproof_2":0,"blemishes_1":0,"helmet_1":-1,"ears_2":0,"eyebrows_1":0,"ears_1":-1,"torso_2":0,"bags_2":0,"lipstick_1":0,"arms_2":0,"mask_2":0,"hair_color_2":0,"arms":20,"pants_1":10,"decals_2":0,"mask_1":0,"bproof_1":0,"blush_1":0,"complexion_2":0,"glasses_2":3,"beard_3":0,"shoes_1":21,"makeup_3":0,"eyebrows_3":0,"hair_2":0,"beard_1":11,"bracelets_2":0,"skin":0,"lipstick_2":0,"blush_2":0}', '{"eyebrows_2":10,"bodyb_1":0,"eye_color":0,"sex":0,"sun_1":0,"face":0,"chest_2":0,"lipstick_4":0,"chest_3":0,"moles_1":0,"lipstick_3":0,"shoes":9,"beard_2":10,"shoes_2":0,"torso_1":4,"bodyb_2":0,"moles_2":0,"tshirt_2":0,"glasses_1":0,"helmet_2":0,"makeup_4":0,"complexion_1":0,"watches_1":3,"hair_color_1":1,"chain_1":12,"makeup_2":0,"beard_4":0,"age_1":0,"watches_2":0,"blush_3":0,"tshirt_1":11,"bracelets_1":-1,"eyebrows_4":0,"makeup_1":0,"hair_1":19,"blemishes_2":0,"bags_1":0,"sun_2":0,"pants_2":0,"chest_1":0,"chain_2":0,"age_2":0,"decals_1":0,"bproof_2":0,"blemishes_1":0,"helmet_1":-1,"ears_2":0,"eyebrows_1":0,"ears_1":-1,"torso_2":0,"bags_2":0,"lipstick_1":0,"arms_2":0,"mask_2":0,"hair_color_2":0,"arms":20,"pants_1":10,"decals_2":0,"mask_1":0,"bproof_1":0,"blush_1":0,"complexion_2":0,"glasses_2":3,"beard_3":0,"shoes_1":21,"makeup_3":0,"eyebrows_3":0,"hair_2":0,"beard_1":11,"bracelets_2":0,"skin":0,"lipstick_2":0,"blush_2":0}'),
	(150, 'taxi', 3, 'uber', 'Uber', 800, '{"eyebrows_2":10,"bodyb_1":0,"eye_color":0,"sex":0,"sun_1":0,"face":0,"chest_2":0,"lipstick_4":0,"chest_3":0,"moles_1":0,"lipstick_3":0,"shoes":9,"beard_2":10,"shoes_2":0,"torso_1":4,"bodyb_2":0,"moles_2":0,"tshirt_2":0,"glasses_1":0,"helmet_2":0,"makeup_4":0,"complexion_1":0,"watches_1":3,"hair_color_1":1,"chain_1":12,"makeup_2":0,"beard_4":0,"age_1":0,"watches_2":0,"blush_3":0,"tshirt_1":11,"bracelets_1":-1,"eyebrows_4":0,"makeup_1":0,"hair_1":19,"blemishes_2":0,"bags_1":0,"sun_2":0,"pants_2":0,"chest_1":0,"chain_2":0,"age_2":0,"decals_1":0,"bproof_2":0,"blemishes_1":0,"helmet_1":-1,"ears_2":0,"eyebrows_1":0,"ears_1":-1,"torso_2":0,"bags_2":0,"lipstick_1":0,"arms_2":0,"mask_2":0,"hair_color_2":0,"arms":20,"pants_1":10,"decals_2":0,"mask_1":0,"bproof_1":0,"blush_1":0,"complexion_2":0,"glasses_2":3,"beard_3":0,"shoes_1":21,"makeup_3":0,"eyebrows_3":0,"hair_2":0,"beard_1":11,"bracelets_2":0,"skin":0,"lipstick_2":0,"blush_2":0}', '{"eyebrows_2":10,"bodyb_1":0,"eye_color":0,"sex":0,"sun_1":0,"face":0,"chest_2":0,"lipstick_4":0,"chest_3":0,"moles_1":0,"lipstick_3":0,"shoes":9,"beard_2":10,"shoes_2":0,"torso_1":4,"bodyb_2":0,"moles_2":0,"tshirt_2":0,"glasses_1":0,"helmet_2":0,"makeup_4":0,"complexion_1":0,"watches_1":3,"hair_color_1":1,"chain_1":12,"makeup_2":0,"beard_4":0,"age_1":0,"watches_2":0,"blush_3":0,"tshirt_1":11,"bracelets_1":-1,"eyebrows_4":0,"makeup_1":0,"hair_1":19,"blemishes_2":0,"bags_1":0,"sun_2":0,"pants_2":0,"chest_1":0,"chain_2":0,"age_2":0,"decals_1":0,"bproof_2":0,"blemishes_1":0,"helmet_1":-1,"ears_2":0,"eyebrows_1":0,"ears_1":-1,"torso_2":0,"bags_2":0,"lipstick_1":0,"arms_2":0,"mask_2":0,"hair_color_2":0,"arms":20,"pants_1":10,"decals_2":0,"mask_1":0,"bproof_1":0,"blush_1":0,"complexion_2":0,"glasses_2":3,"beard_3":0,"shoes_1":21,"makeup_3":0,"eyebrows_3":0,"hair_2":0,"beard_1":11,"bracelets_2":0,"skin":0,"lipstick_2":0,"blush_2":0}'),
	(151, 'taxi', 4, 'boss', 'Patron', 1000, '{"eyebrows_2":10,"bodyb_1":0,"eye_color":0,"sex":0,"sun_1":0,"face":0,"chest_2":0,"lipstick_4":0,"chest_3":0,"moles_1":0,"lipstick_3":0,"shoes":9,"beard_2":10,"shoes_2":0,"torso_1":4,"bodyb_2":0,"moles_2":0,"tshirt_2":0,"glasses_1":0,"helmet_2":0,"makeup_4":0,"complexion_1":0,"watches_1":3,"hair_color_1":1,"chain_1":12,"makeup_2":0,"beard_4":0,"age_1":0,"watches_2":0,"blush_3":0,"tshirt_1":11,"bracelets_1":-1,"eyebrows_4":0,"makeup_1":0,"hair_1":19,"blemishes_2":0,"bags_1":0,"sun_2":0,"pants_2":0,"chest_1":0,"chain_2":0,"age_2":0,"decals_1":0,"bproof_2":0,"blemishes_1":0,"helmet_1":-1,"ears_2":0,"eyebrows_1":0,"ears_1":-1,"torso_2":0,"bags_2":0,"lipstick_1":0,"arms_2":0,"mask_2":0,"hair_color_2":0,"arms":20,"pants_1":10,"decals_2":0,"mask_1":0,"bproof_1":0,"blush_1":0,"complexion_2":0,"glasses_2":3,"beard_3":0,"shoes_1":21,"makeup_3":0,"eyebrows_3":0,"hair_2":0,"beard_1":11,"bracelets_2":0,"skin":0,"lipstick_2":0,"blush_2":0}', '{"eyebrows_2":10,"bodyb_1":0,"eye_color":0,"sex":0,"sun_1":0,"face":0,"chest_2":0,"lipstick_4":0,"chest_3":0,"moles_1":0,"lipstick_3":0,"shoes":9,"beard_2":10,"shoes_2":0,"torso_1":4,"bodyb_2":0,"moles_2":0,"tshirt_2":0,"glasses_1":0,"helmet_2":0,"makeup_4":0,"complexion_1":0,"watches_1":3,"hair_color_1":1,"chain_1":12,"makeup_2":0,"beard_4":0,"age_1":0,"watches_2":0,"blush_3":0,"tshirt_1":11,"bracelets_1":-1,"eyebrows_4":0,"makeup_1":0,"hair_1":19,"blemishes_2":0,"bags_1":0,"sun_2":0,"pants_2":0,"chest_1":0,"chain_2":0,"age_2":0,"decals_1":0,"bproof_2":0,"blemishes_1":0,"helmet_1":-1,"ears_2":0,"eyebrows_1":0,"ears_1":-1,"torso_2":0,"bags_2":0,"lipstick_1":0,"arms_2":0,"mask_2":0,"hair_color_2":0,"arms":20,"pants_1":10,"decals_2":0,"mask_1":0,"bproof_1":0,"blush_1":0,"complexion_2":0,"glasses_2":3,"beard_3":0,"shoes_1":21,"makeup_3":0,"eyebrows_3":0,"hair_2":0,"beard_1":11,"bracelets_2":0,"skin":0,"lipstick_2":0,"blush_2":0}'),
	(155, 'mechanic', 0, 'recrue', 'Recruit', 600, '{"eye_color":0,"complexion_1":0,"eyebrows_1":0,"pants_1":39,"chain_2":0,"hair_color_2":0,"tshirt_2":1,"chain_1":121,"makeup_4":0,"age_1":0,"glasses_1":0,"watches_2":0,"hair_2":0,"blush_3":0,"makeup_2":0,"sun_1":0,"beard_4":0,"sun_2":0,"bags_1":0,"makeup_3":0,"moles_1":0,"hair_color_1":0,"lipstick_4":0,"sex":0,"bproof_2":0,"moles_2":0,"beard_1":11,"bproof_1":0,"blemishes_2":0,"skin":0,"complexion_2":0,"helmet_2":4,"blush_2":0,"lipstick_2":0,"beard_3":0,"torso_2":0,"eyebrows_2":10,"glasses_2":3,"decals_2":0,"shoes_2":2,"tshirt_1":90,"mask_2":0,"chest_1":0,"blemishes_1":0,"watches_1":-1,"arms":4,"ears_1":-1,"shoes":9,"beard_2":10,"arms_2":0,"lipstick_3":0,"age_2":0,"bracelets_1":-1,"bracelets_2":0,"helmet_1":56,"face":0,"bags_2":0,"mask_1":0,"makeup_1":0,"chest_2":0,"chest_3":0,"ears_2":0,"eyebrows_3":0,"blush_1":0,"bodyb_1":0,"eyebrows_4":0,"bodyb_2":0,"hair_1":18,"torso_1":66,"shoes_1":4,"lipstick_1":0,"pants_2":0,"decals_1":0}', '{}'),
	(156, 'mechanic', 1, 'novice', 'Novice', 800, '{"eye_color":0,"complexion_1":0,"eyebrows_1":0,"pants_1":39,"chain_2":0,"hair_color_2":0,"tshirt_2":1,"chain_1":121,"makeup_4":0,"age_1":0,"glasses_1":0,"watches_2":0,"hair_2":0,"blush_3":0,"makeup_2":0,"sun_1":0,"beard_4":0,"sun_2":0,"bags_1":0,"makeup_3":0,"moles_1":0,"hair_color_1":0,"lipstick_4":0,"sex":0,"bproof_2":0,"moles_2":0,"beard_1":11,"bproof_1":0,"blemishes_2":0,"skin":0,"complexion_2":0,"helmet_2":4,"blush_2":0,"lipstick_2":0,"beard_3":0,"torso_2":0,"eyebrows_2":10,"glasses_2":3,"decals_2":0,"shoes_2":2,"tshirt_1":90,"mask_2":0,"chest_1":0,"blemishes_1":0,"watches_1":-1,"arms":4,"ears_1":-1,"shoes":9,"beard_2":10,"arms_2":0,"lipstick_3":0,"age_2":0,"bracelets_1":-1,"bracelets_2":0,"helmet_1":56,"face":0,"bags_2":0,"mask_1":0,"makeup_1":0,"chest_2":0,"chest_3":0,"ears_2":0,"eyebrows_3":0,"blush_1":0,"bodyb_1":0,"eyebrows_4":0,"bodyb_2":0,"hair_1":18,"torso_1":66,"shoes_1":4,"lipstick_1":0,"pants_2":0,"decals_1":0}', '{}'),
	(157, 'mechanic', 2, 'experimente', 'Experienced', 1100, '{"eye_color":0,"complexion_1":0,"eyebrows_1":0,"pants_1":39,"chain_2":0,"hair_color_2":0,"tshirt_2":1,"chain_1":121,"makeup_4":0,"age_1":0,"glasses_1":0,"watches_2":0,"hair_2":0,"blush_3":0,"makeup_2":0,"sun_1":0,"beard_4":0,"sun_2":0,"bags_1":0,"makeup_3":0,"moles_1":0,"hair_color_1":0,"lipstick_4":0,"sex":0,"bproof_2":0,"moles_2":0,"beard_1":11,"bproof_1":0,"blemishes_2":0,"skin":0,"complexion_2":0,"helmet_2":4,"blush_2":0,"lipstick_2":0,"beard_3":0,"torso_2":0,"eyebrows_2":10,"glasses_2":3,"decals_2":0,"shoes_2":2,"tshirt_1":90,"mask_2":0,"chest_1":0,"blemishes_1":0,"watches_1":-1,"arms":4,"ears_1":-1,"shoes":9,"beard_2":10,"arms_2":0,"lipstick_3":0,"age_2":0,"bracelets_1":-1,"bracelets_2":0,"helmet_1":56,"face":0,"bags_2":0,"mask_1":0,"makeup_1":0,"chest_2":0,"chest_3":0,"ears_2":0,"eyebrows_3":0,"blush_1":0,"bodyb_1":0,"eyebrows_4":0,"bodyb_2":0,"hair_1":18,"torso_1":66,"shoes_1":4,"lipstick_1":0,"pants_2":0,"decals_1":0}', '{}'),
	(158, 'mechanic', 3, 'chief', 'Leader', 1500, '{"eye_color":0,"complexion_1":0,"eyebrows_1":0,"pants_1":39,"chain_2":0,"hair_color_2":0,"tshirt_2":1,"chain_1":121,"makeup_4":0,"age_1":0,"glasses_1":0,"watches_2":0,"hair_2":0,"blush_3":0,"makeup_2":0,"sun_1":0,"beard_4":0,"sun_2":0,"bags_1":0,"makeup_3":0,"moles_1":0,"hair_color_1":0,"lipstick_4":0,"sex":0,"bproof_2":0,"moles_2":0,"beard_1":11,"bproof_1":0,"blemishes_2":0,"skin":0,"complexion_2":0,"helmet_2":4,"blush_2":0,"lipstick_2":0,"beard_3":0,"torso_2":0,"eyebrows_2":10,"glasses_2":3,"decals_2":0,"shoes_2":2,"tshirt_1":90,"mask_2":0,"chest_1":0,"blemishes_1":0,"watches_1":-1,"arms":4,"ears_1":-1,"shoes":9,"beard_2":10,"arms_2":0,"lipstick_3":0,"age_2":0,"bracelets_1":-1,"bracelets_2":0,"helmet_1":56,"face":0,"bags_2":0,"mask_1":0,"makeup_1":0,"chest_2":0,"chest_3":0,"ears_2":0,"eyebrows_3":0,"blush_1":0,"bodyb_1":0,"eyebrows_4":0,"bodyb_2":0,"hair_1":18,"torso_1":66,"shoes_1":4,"lipstick_1":0,"pants_2":0,"decals_1":0}', '{}'),
	(159, 'mechanic', 4, 'boss', 'Boss', 3500, '{"eye_color":0,"complexion_1":0,"eyebrows_1":0,"pants_1":39,"chain_2":0,"hair_color_2":0,"tshirt_2":1,"chain_1":121,"makeup_4":0,"age_1":0,"glasses_1":0,"watches_2":0,"hair_2":0,"blush_3":0,"makeup_2":0,"sun_1":0,"beard_4":0,"sun_2":0,"bags_1":0,"makeup_3":0,"moles_1":0,"hair_color_1":0,"lipstick_4":0,"sex":0,"bproof_2":0,"moles_2":0,"beard_1":11,"bproof_1":0,"blemishes_2":0,"skin":0,"complexion_2":0,"helmet_2":4,"blush_2":0,"lipstick_2":0,"beard_3":0,"torso_2":0,"eyebrows_2":10,"glasses_2":3,"decals_2":0,"shoes_2":2,"tshirt_1":90,"mask_2":0,"chest_1":0,"blemishes_1":0,"watches_1":-1,"arms":4,"ears_1":-1,"shoes":9,"beard_2":10,"arms_2":0,"lipstick_3":0,"age_2":0,"bracelets_1":-1,"bracelets_2":0,"helmet_1":56,"face":0,"bags_2":0,"mask_1":0,"makeup_1":0,"chest_2":0,"chest_3":0,"ears_2":0,"eyebrows_3":0,"blush_1":0,"bodyb_1":0,"eyebrows_4":0,"bodyb_2":0,"hair_1":18,"torso_1":66,"shoes_1":4,"lipstick_1":0,"pants_2":0,"decals_1":0}', '{}'),
	(160, 'mafia', 0, 'soldato', 'Ptite-Frappe', 0, '{}', '{}'),
	(161, 'mafia', 1, 'capo', 'Capo', 0, '{}', '{}'),
	(162, 'mafia', 2, 'consigliere', 'Consigliere', 0, '{}', '{}'),
	(163, 'mafia', 3, 'boss', 'Don', 0, '{}', '{}'),
	(164, 'police', 2, 'sergeant', 'รองผู้กำกับการ', 1200, '{"makeup_3":25,"sex":0,"lipstick_3":19,"lipstick_1":0,"eyebrows_3":1,"hair_color_1":1,"decals_2":2,"helmet_1":65,"age_2":7,"eyebrows_4":0,"pants_1":28,"decals_1":8,"makeup_1":0,"tshirt_1":58,"hair_color_2":0,"shoes":1,"mask_1":0,"ears_2":0,"tshirt_2":0,"beard_3":0,"glasses":0,"beard_1":7,"beard_2":7,"skin":0,"lipstick_4":0,"lipstick_2":0,"beard_4":0,"makeup_4":0,"bags_2":0,"eyebrows_1":10,"torso_2":1,"pants_2":0,"mask_2":0,"ears_1":2,"hair_1":12,"bproof_2":1,"helmet_2":0,"face":0,"shoes_1":51,"chain_1":125,"bproof_1":0,"hair_2":0,"makeup_2":0,"glasses_2":2,"glasses_1":15,"torso_1":95,"shoes_2":0,"eyebrows_2":10,"arms":37,"bags_1":0,"chain_2":0,"age_1":5}', '{"mask_2":0,"complexion_1":0,"makeup_4":0,"helmet_2":0,"makeup_2":0,"age_2":0,"chain_2":0,"moles_1":0,"bodyb_2":0,"decals_1":0,"lipstick_4":0,"bracelets_2":0,"hair_color_2":0,"arms":19,"hair_1":18,"bproof_2":0,"torso_2":0,"makeup_3":0,"chain_1":125,"pants_1":35,"glasses_1":0,"lipstick_1":0,"makeup_1":0,"eyebrows_4":0,"blush_1":0,"chest_1":0,"blemishes_2":0,"face":0,"beard_3":0,"helmet_1":46,"bodyb_1":0,"eyebrows_1":9,"tshirt_2":0,"bags_2":0,"eyebrows_2":10,"chest_2":0,"bproof_1":0,"sex":0,"shoes_1":21,"glasses_2":1,"watches_2":0,"torso_1":55,"blush_3":0,"sun_1":0,"blemishes_1":0,"watches_1":-1,"hair_color_1":0,"mask_1":0,"ears_1":2,"beard_2":10,"shoes_2":0,"skin":0,"lipstick_2":0,"hair_2":1,"tshirt_1":129,"sun_2":0,"age_1":0,"lipstick_3":0,"eye_color":0,"complexion_2":0,"beard_1":11,"beard_4":0,"chest_3":0,"decals_2":0,"moles_2":0,"blush_2":0,"ears_2":0,"arms_2":0,"pants_2":0,"eyebrows_3":0,"bags_1":0,"bracelets_1":-1}'),
	(165, 'police', 1, 'officer', 'ตำรวจ', 1000, '{"makeup_3":25,"sex":0,"lipstick_3":19,"lipstick_1":0,"eyebrows_3":1,"hair_color_1":1,"decals_2":2,"helmet_1":65,"age_2":7,"eyebrows_4":0,"pants_1":28,"decals_1":8,"makeup_1":0,"tshirt_1":58,"hair_color_2":0,"shoes":1,"mask_1":0,"ears_2":0,"tshirt_2":0,"beard_3":0,"glasses":0,"beard_1":7,"beard_2":7,"skin":0,"lipstick_4":0,"lipstick_2":0,"beard_4":0,"makeup_4":0,"bags_2":0,"eyebrows_1":10,"torso_2":1,"pants_2":0,"mask_2":0,"ears_1":2,"hair_1":12,"bproof_2":1,"helmet_2":0,"face":0,"shoes_1":51,"chain_1":125,"bproof_1":0,"hair_2":0,"makeup_2":0,"glasses_2":2,"glasses_1":15,"torso_1":95,"shoes_2":0,"eyebrows_2":10,"arms":37,"bags_1":0,"chain_2":0,"age_1":5}', '{"mask_2":0,"complexion_1":0,"makeup_4":0,"helmet_2":0,"makeup_2":0,"age_2":0,"chain_2":0,"moles_1":0,"bodyb_2":0,"decals_1":0,"lipstick_4":0,"bracelets_2":0,"hair_color_2":0,"arms":19,"hair_1":18,"bproof_2":0,"torso_2":0,"makeup_3":0,"chain_1":125,"pants_1":35,"glasses_1":0,"lipstick_1":0,"makeup_1":0,"eyebrows_4":0,"blush_1":0,"chest_1":0,"blemishes_2":0,"face":0,"beard_3":0,"helmet_1":46,"bodyb_1":0,"eyebrows_1":9,"tshirt_2":0,"bags_2":0,"eyebrows_2":10,"chest_2":0,"bproof_1":0,"sex":0,"shoes_1":21,"glasses_2":1,"watches_2":0,"torso_1":55,"blush_3":0,"sun_1":0,"blemishes_1":0,"watches_1":-1,"hair_color_1":0,"mask_1":0,"ears_1":2,"beard_2":10,"shoes_2":0,"skin":0,"lipstick_2":0,"hair_2":1,"tshirt_1":129,"sun_2":0,"age_1":0,"lipstick_3":0,"eye_color":0,"complexion_2":0,"beard_1":11,"beard_4":0,"chest_3":0,"decals_2":0,"moles_2":0,"blush_2":0,"ears_2":0,"arms_2":0,"pants_2":0,"eyebrows_3":0,"bags_1":0,"bracelets_1":-1}'),
	(174, 'cardealer', 0, 'recruit', 'recruit', 0, '{}', '{}'),
	(175, 'offpolice', 0, 'recruit', 'Recruit', 0, '{}', '{}'),
	(176, 'offpolice', 1, 'officer', 'Officer', 0, '{}', '{}'),
	(177, 'offpolice', 2, 'sergeant', 'Sergeant', 0, '{}', '{}'),
	(178, 'offpolice', 3, 'lieutenant', 'Lieutenant', 0, '{}', '{}'),
	(179, 'offpolice', 4, 'boss', 'Boss', 0, '{}', '{}'),
	(180, 'offambulance', 0, 'ambulance', 'Ambulance', 0, '{}', '{}'),
	(181, 'offambulance', 1, 'doctor', 'Doctor', 0, '{}', '{}'),
	(182, 'offambulance', 2, 'chief_doctor', 'Chef', 0, '{}', '{}'),
	(183, 'offambulance', 3, 'boss', 'Boss', 0, '{}', '{}'),
	(184, 'vipz', 0, 'vipa', 'ระยะเวลา 15 วัน', 99, '{}', '{}'),
	(185, 'vipz', 1, 'vipb', 'ระยะเวลา 30 วัน', 99, '{}', '{}'),
	(186, 'vipz', 2, 'vipc', 'ระยะเวลา ถาวร', 99, '{}', '{}'),
	(187, 'vipz', 3, 'vipd', 'ซูปเปอร์ วีไอพี', 99, '{}', '{}'),
	(188, 'realestateagent', 0, 'location', 'Location', 10, '{}', '{}'),
	(189, 'realestateagent', 1, 'vendeur', 'Vendeur', 25, '{}', '{}'),
	(190, 'realestateagent', 2, 'gestion', 'Gestion', 40, '{}', '{}'),
	(191, 'realestateagent', 3, 'boss', 'Patron', 0, '{}', '{}'),
	(192, 'mecano', 0, 'recrue', 'Recrue', 12, '{}', '{}'),
	(193, 'mecano', 1, 'novice', 'Novice', 24, '{}', '{}'),
	(194, 'mecano', 2, 'experimente', 'Experimente', 36, '{}', '{}'),
	(195, 'mecano', 3, 'chief', 'Chef d\'équipe', 48, '{}', '{}'),
	(196, 'mecano', 4, 'boss', 'Patron', 0, '{}', '{}');
/*!40000 ALTER TABLE `job_grades` ENABLE KEYS */;

-- Dumping structure for table essentialmode.jsfour_cardetails
CREATE TABLE IF NOT EXISTS `jsfour_cardetails` (
  `pk` int(11) NOT NULL AUTO_INCREMENT,
  `plate` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `owner` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `incident` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '{}',
  `inspected` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `identifier` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`pk`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.jsfour_cardetails: ~0 rows (approximately)
/*!40000 ALTER TABLE `jsfour_cardetails` DISABLE KEYS */;
/*!40000 ALTER TABLE `jsfour_cardetails` ENABLE KEYS */;

-- Dumping structure for table essentialmode.jsfour_criminalrecord
CREATE TABLE IF NOT EXISTS `jsfour_criminalrecord` (
  `offense` varchar(160) COLLATE utf8mb4_bin NOT NULL,
  `date` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `institution` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `charge` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `term` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `classified` int(2) NOT NULL DEFAULT 0,
  `identifier` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `dob` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `warden` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`offense`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.jsfour_criminalrecord: ~0 rows (approximately)
/*!40000 ALTER TABLE `jsfour_criminalrecord` DISABLE KEYS */;
/*!40000 ALTER TABLE `jsfour_criminalrecord` ENABLE KEYS */;

-- Dumping structure for table essentialmode.jsfour_criminaluserinfo
CREATE TABLE IF NOT EXISTS `jsfour_criminaluserinfo` (
  `identifier` varchar(160) COLLATE utf8mb4_bin NOT NULL,
  `aliases` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `recordid` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `weight` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `eyecolor` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `haircolor` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `dob` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `sex` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `height` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.jsfour_criminaluserinfo: ~0 rows (approximately)
/*!40000 ALTER TABLE `jsfour_criminaluserinfo` DISABLE KEYS */;
/*!40000 ALTER TABLE `jsfour_criminaluserinfo` ENABLE KEYS */;

-- Dumping structure for table essentialmode.jsfour_dna
CREATE TABLE IF NOT EXISTS `jsfour_dna` (
  `pk` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `killer` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `dead` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `weapon` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_bin DEFAULT 'murder',
  `lastdigits` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `uploader` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `datum` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.jsfour_dna: ~0 rows (approximately)
/*!40000 ALTER TABLE `jsfour_dna` DISABLE KEYS */;
/*!40000 ALTER TABLE `jsfour_dna` ENABLE KEYS */;

-- Dumping structure for table essentialmode.jsfour_efterlysningar
CREATE TABLE IF NOT EXISTS `jsfour_efterlysningar` (
  `pk` int(11) NOT NULL AUTO_INCREMENT,
  `wanted` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `dob` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `crime` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `uploader` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `date` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `incident` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`pk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.jsfour_efterlysningar: ~0 rows (approximately)
/*!40000 ALTER TABLE `jsfour_efterlysningar` DISABLE KEYS */;
/*!40000 ALTER TABLE `jsfour_efterlysningar` ENABLE KEYS */;

-- Dumping structure for table essentialmode.jsfour_incidents
CREATE TABLE IF NOT EXISTS `jsfour_incidents` (
  `pk` int(11) NOT NULL AUTO_INCREMENT,
  `number` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `text` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `uploader` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `date` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`pk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.jsfour_incidents: ~0 rows (approximately)
/*!40000 ALTER TABLE `jsfour_incidents` DISABLE KEYS */;
/*!40000 ALTER TABLE `jsfour_incidents` ENABLE KEYS */;

-- Dumping structure for table essentialmode.jsfour_logs
CREATE TABLE IF NOT EXISTS `jsfour_logs` (
  `pk` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `remover` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `wanted` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`pk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.jsfour_logs: ~0 rows (approximately)
/*!40000 ALTER TABLE `jsfour_logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `jsfour_logs` ENABLE KEYS */;

-- Dumping structure for table essentialmode.licenses
CREATE TABLE IF NOT EXISTS `licenses` (
  `type` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.licenses: ~7 rows (approximately)
/*!40000 ALTER TABLE `licenses` DISABLE KEYS */;
INSERT INTO `licenses` (`type`, `label`) VALUES
	('boating', 'Boating License'),
	('dmv', 'ใบขับขี่'),
	('drive', 'ใบอณุญาติขับขี่รถยนต์'),
	('drive_bike', 'ใบอณุญาติขับขี่มอเตอร์ไซค์'),
	('drive_truck', 'ใบอณุญาติขับรถบรรทุก'),
	('weapon', 'ใบอณุญาติพกอาวุธ'),
	('weed_processing', 'ใบอนุญาติซื้อกัญชา');
/*!40000 ALTER TABLE `licenses` ENABLE KEYS */;

-- Dumping structure for table essentialmode.outfits
CREATE TABLE IF NOT EXISTS `outfits` (
  `identifier` varchar(30) NOT NULL,
  `skin` varchar(30) CHARACTER SET utf8 NOT NULL DEFAULT 'mp_m_freemode_01',
  `face` int(11) NOT NULL DEFAULT 0,
  `face_text` int(11) NOT NULL DEFAULT 0,
  `hair` int(11) NOT NULL DEFAULT 0,
  `hair_text` int(11) NOT NULL DEFAULT 0,
  `pants` int(11) NOT NULL DEFAULT 0,
  `pants_text` int(11) NOT NULL DEFAULT 0,
  `shoes` int(11) NOT NULL DEFAULT 0,
  `shoes_text` int(11) NOT NULL DEFAULT 0,
  `torso` int(11) NOT NULL DEFAULT 0,
  `torso_text` int(11) NOT NULL DEFAULT 0,
  `shirt` int(11) NOT NULL DEFAULT 0,
  `shirt_text` int(11) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.outfits: 0 rows
/*!40000 ALTER TABLE `outfits` DISABLE KEYS */;
/*!40000 ALTER TABLE `outfits` ENABLE KEYS */;

-- Dumping structure for table essentialmode.owned_properties
CREATE TABLE IF NOT EXISTS `owned_properties` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `price` double NOT NULL,
  `rented` int(11) NOT NULL,
  `owner` varchar(60) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.owned_properties: ~10 rows (approximately)
/*!40000 ALTER TABLE `owned_properties` DISABLE KEYS */;
INSERT INTO `owned_properties` (`id`, `name`, `price`, `rented`, `owner`) VALUES
	(42, 'MadWayneThunder', 7500, 1, 'steam:110000104a1abc7'),
	(43, 'OldSpiceWarm', 5000000, 0, 'steam:110000104a1abc7'),
	(44, 'OldSpiceClassical', 25000, 1, 'steam:110000104a1abc7'),
	(45, 'OldSpiceVintage', 5000000, 0, 'steam:110000104a1abc7'),
	(46, 'ExecutiveRich', 5000000, 0, 'steam:110000104a1abc7'),
	(47, 'ExecutiveCool', 5000000, 0, 'steam:110000104a1abc7'),
	(48, 'ExecutiveContrast', 5000000, 0, 'steam:110000104a1abc7'),
	(49, 'PowerBrokerIce', 5000000, 0, 'steam:110000104a1abc7'),
	(50, 'PowerBrokerConservative', 5000000, 0, 'steam:110000104a1abc7'),
	(51, 'PowerBrokerPolished', 5000000, 0, 'steam:110000104a1abc7'),
	(52, 'OldSpiceWarm', 5000000, 0, 'steam:11000013dce3ca4');
/*!40000 ALTER TABLE `owned_properties` ENABLE KEYS */;

-- Dumping structure for table essentialmode.owned_vehicles
CREATE TABLE IF NOT EXISTS `owned_vehicles` (
  `owner` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `plate` varchar(20) COLLATE utf8mb4_bin NOT NULL,
  `vehicle` longtext COLLATE utf8mb4_bin NOT NULL,
  `type` varchar(20) COLLATE utf8mb4_bin NOT NULL DEFAULT 'car',
  `job` varchar(20) COLLATE utf8mb4_bin NOT NULL,
  `stored` tinyint(1) NOT NULL DEFAULT 0,
  `state` tinyint(1) NOT NULL DEFAULT 0,
  `fourrieremecano` tinyint(1) NOT NULL DEFAULT 0,
  `vehiclename` varchar(50) COLLATE utf8mb4_bin NOT NULL DEFAULT 'voiture',
  PRIMARY KEY (`plate`),
  KEY `owner` (`owner`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.owned_vehicles: ~6 rows (approximately)
/*!40000 ALTER TABLE `owned_vehicles` DISABLE KEYS */;
INSERT INTO `owned_vehicles` (`owner`, `plate`, `vehicle`, `type`, `job`, `stored`, `state`, `fourrieremecano`, `vehiclename`) VALUES
	('steam:110000104a1abc7', 'ENE 750', '{"modLivery":-1,"modTrimA":-1,"modEngine":3,"modSideSkirt":-1,"modFrontWheels":-1,"tyreSmokeColor":[255,255,255],"modSteeringWheel":-1,"modRearBumper":-1,"modTrunk":-1,"modShifterLeavers":-1,"fuelLevel":64.6,"modHydrolic":-1,"modDoorSpeaker":-1,"plateIndex":2,"modAPlate":-1,"modSeats":-1,"modRightFender":-1,"plate":"ENE 750","modStruts":-1,"neonEnabled":[false,false,false,false],"modAirFilter":-1,"model":917809321,"color2":112,"modRoof":-1,"modAerials":-1,"modEngineBlock":-1,"modTrimB":-1,"modFender":-1,"modXenon":false,"modFrontBumper":-1,"neonColor":[255,0,255],"modHorns":33,"modDial":-1,"modSpoilers":-1,"modPlateHolder":-1,"modVanityPlate":-1,"modBrakes":2,"extras":[],"modTurbo":1,"color1":55,"modArmor":4,"bodyHealth":1000.0,"modTransmission":0,"wheelColor":55,"modOrnaments":-1,"pearlescentColor":55,"windowTint":-1,"modExhaust":-1,"modBackWheels":-1,"modTank":-1,"modArchCover":-1,"modSpeakers":-1,"modHood":-1,"modSmokeEnabled":1,"modGrille":-1,"modDashboard":-1,"modFrame":-1,"modWindows":-1,"modSuspension":3,"engineHealth":1000.0,"wheels":7,"dirtLevel":0.0}', 'car', '', 0, 0, 0, 'voiture'),
	('steam:11000013dce3ca4', 'GIB 567', '{"modLivery":-1,"modTrimA":-1,"modEngine":0,"modSideSkirt":-1,"modFrontWheels":-1,"tyreSmokeColor":[255,255,255],"modSteeringWheel":-1,"modRearBumper":-1,"modTrunk":-1,"modShifterLeavers":-1,"fuelLevel":63.4,"modHydrolic":-1,"modDoorSpeaker":-1,"plateIndex":0,"modAPlate":-1,"modSeats":-1,"modRightFender":-1,"plate":"GIB 567","modStruts":-1,"neonEnabled":[1,1,1,1],"modAirFilter":-1,"model":1939284556,"color2":160,"modRoof":-1,"modAerials":-1,"modEngineBlock":-1,"modTrimB":-1,"modFender":-1,"modXenon":1,"modFrontBumper":-1,"neonColor":[245,222,179],"modHorns":2,"modDial":-1,"modSpoilers":-1,"modPlateHolder":-1,"modVanityPlate":-1,"modBrakes":2,"extras":[],"modTurbo":1,"color1":160,"modArmor":0,"bodyHealth":999.5,"modTransmission":-1,"wheelColor":0,"modOrnaments":-1,"pearlescentColor":5,"windowTint":-1,"modExhaust":-1,"modBackWheels":-1,"modTank":-1,"modArchCover":-1,"modSpeakers":-1,"modHood":-1,"modSmokeEnabled":1,"modGrille":-1,"modDashboard":-1,"modFrame":-1,"modWindows":-1,"modSuspension":3,"engineHealth":998.0,"wheels":7,"dirtLevel":6.2}', 'car', '', 0, 0, 0, 'voiture'),
	('steam:110000104a1abc7', 'TYM 172', '{"pearlescentColor":88,"model":1672195559,"modTrunk":-1,"modTurbo":false,"windowTint":-1,"modSeats":-1,"modRoof":-1,"modWindows":-1,"modEngine":-1,"dirtLevel":8.0,"modSuspension":-1,"modFrontBumper":-1,"modSpoilers":-1,"modDial":-1,"modFender":-1,"color1":88,"modBackWheels":-1,"modHorns":-1,"modAerials":-1,"modFrontWheels":-1,"neonColor":[255,0,255],"modTrimA":-1,"modGrille":-1,"modOrnaments":-1,"wheels":6,"fuelLevel":50.8,"modArchCover":-1,"modSpeakers":-1,"modSteeringWheel":-1,"color2":88,"modEngineBlock":-1,"modAPlate":-1,"wheelColor":156,"modTransmission":-1,"modTrimB":-1,"modShifterLeavers":-1,"neonEnabled":[false,false,false,false],"modArmor":-1,"tyreSmokeColor":[255,255,255],"modRightFender":-1,"modDashboard":-1,"bodyHealth":1000.0,"modDoorSpeaker":-1,"modPlateHolder":-1,"plate":"TYM 172","modSideSkirt":-1,"modExhaust":-1,"plateIndex":0,"modTank":-1,"modStruts":-1,"engineHealth":1000.0,"modHydrolic":-1,"modBrakes":-1,"modFrame":-1,"modXenon":false,"modHood":-1,"modVanityPlate":-1,"modAirFilter":-1,"modSmokeEnabled":false,"modRearBumper":-1,"extras":{"4":true,"1":true,"9":true},"modLivery":-1}', 'car', '', 0, 0, 0, 'voiture'),
	('steam:11000013dce3ca4', 'WKH 267', '{"modFrontBumper":-1,"modBackWheels":-1,"modHood":-1,"modLivery":-1,"modTank":-1,"modTrimA":-1,"modGrille":-1,"modVanityPlate":-1,"neonEnabled":[false,false,false,false],"modSeats":-1,"modExhaust":-1,"modSuspension":-1,"modDoorSpeaker":-1,"modAirFilter":-1,"color1":0,"modDial":-1,"modHydrolic":-1,"modDashboard":-1,"modHorns":-1,"modShifterLeavers":-1,"modEngineBlock":-1,"modSideSkirt":-1,"extras":[],"windowTint":-1,"wheels":7,"modFrontWheels":-1,"modTurbo":false,"modSmokeEnabled":false,"modBrakes":-1,"modPlateHolder":-1,"plateIndex":0,"modRearBumper":-1,"fuelLevel":65.0,"wheelColor":156,"modEngine":-1,"modFender":-1,"modTrunk":-1,"modStruts":-1,"modAerials":-1,"bodyHealth":1000.0,"plate":"WKH 267","tyreSmokeColor":[255,255,255],"modFrame":-1,"modRightFender":-1,"neonColor":[255,0,255],"modArmor":-1,"model":-1403128555,"color2":41,"modOrnaments":-1,"modSpoilers":-1,"modSpeakers":-1,"modTransmission":-1,"modWindows":-1,"engineHealth":1000.0,"dirtLevel":6.0,"modArchCover":-1,"pearlescentColor":3,"modRoof":-1,"modTrimB":-1,"modXenon":false,"modSteeringWheel":-1,"modAPlate":-1}', 'car', '', 0, 0, 0, 'voiture'),
	('steam:110000104a1abc7', 'YYR 747', '{"pearlescentColor":78,"model":-1543762099,"modTrunk":-1,"modTurbo":false,"windowTint":-1,"modSeats":-1,"modRoof":-1,"modWindows":-1,"modEngine":-1,"dirtLevel":6.0,"modSuspension":-1,"modFrontBumper":-1,"modSpoilers":-1,"modDial":-1,"modFender":-1,"color1":69,"modBackWheels":-1,"modHorns":-1,"modAerials":-1,"modFrontWheels":-1,"neonColor":[255,0,255],"modTrimA":-1,"modGrille":-1,"modOrnaments":-1,"wheels":3,"fuelLevel":65.0,"modArchCover":-1,"modSpeakers":-1,"modSteeringWheel":-1,"color2":0,"modEngineBlock":-1,"modAPlate":-1,"wheelColor":156,"modTransmission":-1,"modTrimB":-1,"modShifterLeavers":-1,"neonEnabled":[false,false,false,false],"modArmor":-1,"tyreSmokeColor":[255,255,255],"modRightFender":-1,"modDashboard":-1,"bodyHealth":1000.0,"modDoorSpeaker":-1,"modPlateHolder":-1,"plate":"YYR 747","modSideSkirt":-1,"modExhaust":-1,"plateIndex":0,"modTank":-1,"modStruts":-1,"engineHealth":1000.0,"modHydrolic":-1,"modBrakes":-1,"modFrame":-1,"modXenon":false,"modHood":-1,"modVanityPlate":-1,"modAirFilter":-1,"modSmokeEnabled":false,"modRearBumper":-1,"extras":{"12":true,"11":false,"10":false},"modLivery":-1}', 'car', '', 0, 0, 0, 'voiture'),
	('steam:110000104a1abc7', 'ZLR 019', '{"modOrnaments":-1,"modBackWheels":-1,"model":1034187331,"modHorns":-1,"modSpeakers":-1,"modTrimA":-1,"modGrille":-1,"modVanityPlate":-1,"modDoorSpeaker":-1,"modSeats":-1,"neonEnabled":[false,false,false,false],"modStruts":-1,"modArchCover":-1,"modHydrolic":-1,"color1":112,"modDial":-1,"modFrame":-1,"modDashboard":-1,"modLivery":-1,"modSuspension":-1,"modFrontBumper":-1,"modSideSkirt":-1,"modFrontWheels":-1,"windowTint":-1,"wheelColor":112,"modRearBumper":-1,"modAPlate":-1,"modSmokeEnabled":false,"modTrimB":-1,"modAerials":-1,"plateIndex":0,"dirtLevel":9.0,"fuelLevel":43.2,"bodyHealth":1000.0,"modEngine":-1,"modExhaust":-1,"modTrunk":-1,"modTank":-1,"modXenon":false,"modTransmission":-1,"plate":"ZLR 019","tyreSmokeColor":[255,255,255],"modSpoilers":-1,"modRightFender":-1,"neonColor":[255,0,255],"wheels":7,"modBrakes":-1,"color2":70,"modWindows":-1,"modTurbo":false,"modEngineBlock":-1,"extras":[],"modHood":-1,"engineHealth":1000.0,"modAirFilter":-1,"modPlateHolder":-1,"pearlescentColor":18,"modRoof":-1,"modFender":-1,"modArmor":-1,"modSteeringWheel":-1,"modShifterLeavers":-1}', 'car', '', 0, 0, 0, 'voiture');
/*!40000 ALTER TABLE `owned_vehicles` ENABLE KEYS */;

-- Dumping structure for table essentialmode.phone_app_chat
CREATE TABLE IF NOT EXISTS `phone_app_chat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `channel` varchar(20) NOT NULL,
  `message` varchar(255) NOT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

-- Dumping data for table essentialmode.phone_app_chat: ~0 rows (approximately)
/*!40000 ALTER TABLE `phone_app_chat` DISABLE KEYS */;
/*!40000 ALTER TABLE `phone_app_chat` ENABLE KEYS */;

-- Dumping structure for table essentialmode.phone_calls
CREATE TABLE IF NOT EXISTS `phone_calls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner` varchar(10) NOT NULL COMMENT 'Num tel proprio',
  `num` varchar(10) NOT NULL COMMENT 'Num reférence du contact',
  `incoming` int(11) NOT NULL COMMENT 'Défini si on est à l''origine de l''appels',
  `time` timestamp NOT NULL DEFAULT current_timestamp(),
  `accepts` int(11) NOT NULL COMMENT 'Appels accepter ou pas',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=401 DEFAULT CHARSET=utf8;

-- Dumping data for table essentialmode.phone_calls: ~8 rows (approximately)
/*!40000 ALTER TABLE `phone_calls` DISABLE KEYS */;
INSERT INTO `phone_calls` (`id`, `owner`, `num`, `incoming`, `time`, `accepts`) VALUES
	(391, '0300875854', '0700030517', 1, '2019-11-23 15:38:53', 1),
	(392, '0700030517', '0300875854', 0, '2019-11-23 15:38:53', 1),
	(393, '0700030517', '0300875854', 1, '2019-11-23 19:01:20', 1),
	(394, '0300875854', '0700030517', 0, '2019-11-23 19:01:20', 1),
	(395, '0700030517', '0300875854', 1, '2019-11-23 20:44:51', 0),
	(396, '0300875854', '0700030517', 0, '2019-11-23 20:44:51', 0),
	(397, '0300875854', '0700030517', 0, '2019-11-24 13:30:14', 1),
	(398, '0700030517', '0300875854', 1, '2019-11-24 13:30:14', 1),
	(399, '0700030517', '0300875854', 1, '2019-11-24 18:53:34', 0),
	(400, '0300875854', '0700030517', 0, '2019-11-24 18:53:34', 0);
/*!40000 ALTER TABLE `phone_calls` ENABLE KEYS */;

-- Dumping structure for table essentialmode.phone_messages
CREATE TABLE IF NOT EXISTS `phone_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transmitter` varchar(10) NOT NULL,
  `receiver` varchar(10) NOT NULL,
  `message` varchar(255) NOT NULL DEFAULT '0',
  `time` timestamp NOT NULL DEFAULT current_timestamp(),
  `isRead` int(11) NOT NULL DEFAULT 0,
  `owner` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1247 DEFAULT CHARSET=utf8;

-- Dumping data for table essentialmode.phone_messages: 6 rows
/*!40000 ALTER TABLE `phone_messages` DISABLE KEYS */;
INSERT INTO `phone_messages` (`id`, `transmitter`, `receiver`, `message`, `time`, `isRead`, `owner`) VALUES
	(1241, 'ambulance', '0700030517', 'De #0300875854 : ผู้เล่นหมดสติ ขอความช่วยเหลือพิกัด :  -385.04483032227, -391.6018371582', '2019-11-23 20:26:56', 1, 0),
	(1242, 'ambulance', '0700030517', 'De #0300875854 : ผู้เล่นหมดสติ ขอความช่วยเหลือพิกัด :  2.6577405929565, 19.746793746948', '2019-11-24 01:03:13', 1, 0),
	(1243, 'ambulance', '0700030517', 'De #0300875854 : ผู้เล่นหมดสติ ขอความช่วยเหลือพิกัด :  6.0762696266174, 10.490721702576', '2019-11-24 01:04:23', 1, 0),
	(1244, 'ambulance', '0700030517', 'De #0300875854 : ผู้เล่นหมดสติ ขอความช่วยเหลือพิกัด :  45.417469024658, 288.12243652344', '2019-11-24 01:09:19', 1, 0),
	(1245, 'ambulance', '0700030517', 'De #0300875854 : ผู้เล่นหมดสติ ขอความช่วยเหลือพิกัด :  45.417469024658, 288.12243652344', '2019-11-24 01:09:26', 1, 0),
	(1246, 'ambulance', '0700030517', 'De #0300875854 : ผู้เล่นหมดสติ ขอความช่วยเหลือพิกัด :  16.182949066162, 274.37014770508', '2019-11-24 01:21:39', 1, 0);
/*!40000 ALTER TABLE `phone_messages` ENABLE KEYS */;

-- Dumping structure for table essentialmode.phone_users_contacts
CREATE TABLE IF NOT EXISTS `phone_users_contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(60) CHARACTER SET utf8mb4 DEFAULT NULL,
  `number` varchar(10) CHARACTER SET utf8mb4 DEFAULT NULL,
  `display` varchar(64) CHARACTER SET utf8mb4 NOT NULL DEFAULT '-1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=86 DEFAULT CHARSET=utf8;

-- Dumping data for table essentialmode.phone_users_contacts: 1 rows
/*!40000 ALTER TABLE `phone_users_contacts` DISABLE KEYS */;
INSERT INTO `phone_users_contacts` (`id`, `identifier`, `number`, `display`) VALUES
	(85, 'steam:110000104a1abc7', '0700030517', 'ข้าวปั้น');
/*!40000 ALTER TABLE `phone_users_contacts` ENABLE KEYS */;

-- Dumping structure for table essentialmode.playerstattoos
CREATE TABLE IF NOT EXISTS `playerstattoos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(255) NOT NULL,
  `tattoos` longtext NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.playerstattoos: ~0 rows (approximately)
/*!40000 ALTER TABLE `playerstattoos` DISABLE KEYS */;
/*!40000 ALTER TABLE `playerstattoos` ENABLE KEYS */;

-- Dumping structure for table essentialmode.properties
CREATE TABLE IF NOT EXISTS `properties` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  `entering` varchar(255) DEFAULT NULL,
  `exit` varchar(255) DEFAULT NULL,
  `inside` varchar(255) DEFAULT NULL,
  `outside` varchar(255) DEFAULT NULL,
  `ipls` varchar(255) DEFAULT '[]',
  `gateway` varchar(255) DEFAULT NULL,
  `is_single` int(11) DEFAULT NULL,
  `is_room` int(11) DEFAULT NULL,
  `is_gateway` int(11) DEFAULT NULL,
  `room_menu` varchar(255) DEFAULT NULL,
  `price` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.properties: ~72 rows (approximately)
/*!40000 ALTER TABLE `properties` DISABLE KEYS */;
INSERT INTO `properties` (`id`, `name`, `label`, `entering`, `exit`, `inside`, `outside`, `ipls`, `gateway`, `is_single`, `is_room`, `is_gateway`, `room_menu`, `price`) VALUES
	(1, 'WhispymoundDrive', '2677 Whispymound Drive', '{"y":564.89,"z":182.959,"x":119.384}', '{"x":117.347,"y":559.506,"z":183.304}', '{"y":557.032,"z":183.301,"x":118.037}', '{"y":567.798,"z":182.131,"x":119.249}', '[]', NULL, 1, 1, 0, '{"x":118.748,"y":566.573,"z":175.697}', 1500000),
	(2, 'NorthConkerAvenue2045', '2045 North Conker Avenue', '{"x":372.796,"y":428.327,"z":144.685}', '{"x":373.548,"y":422.982,"z":144.907},', '{"y":420.075,"z":145.904,"x":372.161}', '{"x":372.454,"y":432.886,"z":143.443}', '[]', NULL, 1, 1, 0, '{"x":377.349,"y":429.422,"z":137.3}', 1500000),
	(3, 'RichardMajesticApt2', 'Richard Majestic, Apt 2', '{"y":-379.165,"z":37.961,"x":-936.363}', '{"y":-365.476,"z":113.274,"x":-913.097}', '{"y":-367.637,"z":113.274,"x":-918.022}', '{"y":-382.023,"z":37.961,"x":-943.626}', '[]', NULL, 1, 1, 0, '{"x":-927.554,"y":-377.744,"z":112.674}', 1700000),
	(4, 'NorthConkerAvenue2044', '2044 North Conker Avenue', '{"y":440.8,"z":146.702,"x":346.964}', '{"y":437.456,"z":148.394,"x":341.683}', '{"y":435.626,"z":148.394,"x":339.595}', '{"x":350.535,"y":443.329,"z":145.764}', '[]', NULL, 1, 1, 0, '{"x":337.726,"y":436.985,"z":140.77}', 1500000),
	(5, 'WildOatsDrive', '3655 Wild Oats Drive', '{"y":502.696,"z":136.421,"x":-176.003}', '{"y":497.817,"z":136.653,"x":-174.349}', '{"y":495.069,"z":136.666,"x":-173.331}', '{"y":506.412,"z":135.0664,"x":-177.927}', '[]', NULL, 1, 1, 0, '{"x":-174.725,"y":493.095,"z":129.043}', 1500000),
	(6, 'HillcrestAvenue2862', '2862 Hillcrest Avenue', '{"y":596.58,"z":142.641,"x":-686.554}', '{"y":591.988,"z":144.392,"x":-681.728}', '{"y":590.608,"z":144.392,"x":-680.124}', '{"y":599.019,"z":142.059,"x":-689.492}', '[]', NULL, 1, 1, 0, '{"x":-680.46,"y":588.6,"z":136.769}', 1500000),
	(7, 'LowEndApartment', 'Appartement de base', '{"y":-1078.735,"z":28.4031,"x":292.528}', '{"y":-1007.152,"z":-102.002,"x":265.845}', '{"y":-1002.802,"z":-100.008,"x":265.307}', '{"y":-1078.669,"z":28.401,"x":296.738}', '[]', NULL, 1, 1, 0, '{"x":265.916,"y":-999.38,"z":-100.008}', 562500),
	(8, 'MadWayneThunder', '2113 Mad Wayne Thunder', '{"y":454.955,"z":96.462,"x":-1294.433}', '{"x":-1289.917,"y":449.541,"z":96.902}', '{"y":446.322,"z":96.899,"x":-1289.642}', '{"y":455.453,"z":96.517,"x":-1298.851}', '[]', NULL, 1, 1, 0, '{"x":-1287.306,"y":455.901,"z":89.294}', 1500000),
	(9, 'HillcrestAvenue2874', '2874 Hillcrest Avenue', '{"x":-853.346,"y":696.678,"z":147.782}', '{"y":690.875,"z":151.86,"x":-859.961}', '{"y":688.361,"z":151.857,"x":-859.395}', '{"y":701.628,"z":147.773,"x":-855.007}', '[]', NULL, 1, 1, 0, '{"x":-858.543,"y":697.514,"z":144.253}', 1500000),
	(10, 'HillcrestAvenue2868', '2868 Hillcrest Avenue', '{"y":620.494,"z":141.588,"x":-752.82}', '{"y":618.62,"z":143.153,"x":-759.317}', '{"y":617.629,"z":143.153,"x":-760.789}', '{"y":621.281,"z":141.254,"x":-750.919}', '[]', NULL, 1, 1, 0, '{"x":-762.504,"y":618.992,"z":135.53}', 1500000),
	(11, 'TinselTowersApt12', 'Tinsel Towers, Apt 42', '{"y":37.025,"z":42.58,"x":-618.299}', '{"y":58.898,"z":97.2,"x":-603.301}', '{"y":58.941,"z":97.2,"x":-608.741}', '{"y":30.603,"z":42.524,"x":-620.017}', '[]', NULL, 1, 1, 0, '{"x":-622.173,"y":54.585,"z":96.599}', 1700000),
	(12, 'MiltonDrive', 'Milton Drive', '{"x":-775.17,"y":312.01,"z":84.658}', NULL, NULL, '{"x":-775.346,"y":306.776,"z":84.7}', '[]', NULL, 0, 0, 1, NULL, 0),
	(13, 'Modern1Apartment', 'Appartement Moderne 1', NULL, '{"x":-784.194,"y":323.636,"z":210.997}', '{"x":-779.751,"y":323.385,"z":210.997}', NULL, '["apa_v_mp_h_01_a"]', 'MiltonDrive', 0, 1, 0, '{"x":-766.661,"y":327.672,"z":210.396}', 1300000),
	(14, 'Modern2Apartment', 'Appartement Moderne 2', NULL, '{"x":-786.8663,"y":315.764,"z":186.913}', '{"x":-781.808,"y":315.866,"z":186.913}', NULL, '["apa_v_mp_h_01_c"]', 'MiltonDrive', 0, 1, 0, '{"x":-795.735,"y":326.757,"z":186.313}', 1300000),
	(15, 'Modern3Apartment', 'Appartement Moderne 3', NULL, '{"x":-774.012,"y":342.042,"z":195.686}', '{"x":-779.057,"y":342.063,"z":195.686}', NULL, '["apa_v_mp_h_01_b"]', 'MiltonDrive', 0, 1, 0, '{"x":-765.386,"y":330.782,"z":195.08}', 1300000),
	(16, 'Mody1Apartment', 'Appartement Mode 1', NULL, '{"x":-784.194,"y":323.636,"z":210.997}', '{"x":-779.751,"y":323.385,"z":210.997}', NULL, '["apa_v_mp_h_02_a"]', 'MiltonDrive', 0, 1, 0, '{"x":-766.615,"y":327.878,"z":210.396}', 1300000),
	(17, 'Mody2Apartment', 'Appartement Mode 2', NULL, '{"x":-786.8663,"y":315.764,"z":186.913}', '{"x":-781.808,"y":315.866,"z":186.913}', NULL, '["apa_v_mp_h_02_c"]', 'MiltonDrive', 0, 1, 0, '{"x":-795.297,"y":327.092,"z":186.313}', 1300000),
	(18, 'Mody3Apartment', 'Appartement Mode 3', NULL, '{"x":-774.012,"y":342.042,"z":195.686}', '{"x":-779.057,"y":342.063,"z":195.686}', NULL, '["apa_v_mp_h_02_b"]', 'MiltonDrive', 0, 1, 0, '{"x":-765.303,"y":330.932,"z":195.085}', 1300000),
	(19, 'Vibrant1Apartment', 'Appartement Vibrant 1', NULL, '{"x":-784.194,"y":323.636,"z":210.997}', '{"x":-779.751,"y":323.385,"z":210.997}', NULL, '["apa_v_mp_h_03_a"]', 'MiltonDrive', 0, 1, 0, '{"x":-765.885,"y":327.641,"z":210.396}', 1300000),
	(20, 'Vibrant2Apartment', 'Appartement Vibrant 2', NULL, '{"x":-786.8663,"y":315.764,"z":186.913}', '{"x":-781.808,"y":315.866,"z":186.913}', NULL, '["apa_v_mp_h_03_c"]', 'MiltonDrive', 0, 1, 0, '{"x":-795.607,"y":327.344,"z":186.313}', 1300000),
	(21, 'Vibrant3Apartment', 'Appartement Vibrant 3', NULL, '{"x":-774.012,"y":342.042,"z":195.686}', '{"x":-779.057,"y":342.063,"z":195.686}', NULL, '["apa_v_mp_h_03_b"]', 'MiltonDrive', 0, 1, 0, '{"x":-765.525,"y":330.851,"z":195.085}', 1300000),
	(22, 'Sharp1Apartment', 'Appartement Persan 1', NULL, '{"x":-784.194,"y":323.636,"z":210.997}', '{"x":-779.751,"y":323.385,"z":210.997}', NULL, '["apa_v_mp_h_04_a"]', 'MiltonDrive', 0, 1, 0, '{"x":-766.527,"y":327.89,"z":210.396}', 1300000),
	(23, 'Sharp2Apartment', 'Appartement Persan 2', NULL, '{"x":-786.8663,"y":315.764,"z":186.913}', '{"x":-781.808,"y":315.866,"z":186.913}', NULL, '["apa_v_mp_h_04_c"]', 'MiltonDrive', 0, 1, 0, '{"x":-795.642,"y":326.497,"z":186.313}', 1300000),
	(24, 'Sharp3Apartment', 'Appartement Persan 3', NULL, '{"x":-774.012,"y":342.042,"z":195.686}', '{"x":-779.057,"y":342.063,"z":195.686}', NULL, '["apa_v_mp_h_04_b"]', 'MiltonDrive', 0, 1, 0, '{"x":-765.503,"y":331.318,"z":195.085}', 1300000),
	(25, 'Monochrome1Apartment', 'Appartement Monochrome 1', NULL, '{"x":-784.194,"y":323.636,"z":210.997}', '{"x":-779.751,"y":323.385,"z":210.997}', NULL, '["apa_v_mp_h_05_a"]', 'MiltonDrive', 0, 1, 0, '{"x":-766.289,"y":328.086,"z":210.396}', 1300000),
	(26, 'Monochrome2Apartment', 'Appartement Monochrome 2', NULL, '{"x":-786.8663,"y":315.764,"z":186.913}', '{"x":-781.808,"y":315.866,"z":186.913}', NULL, '["apa_v_mp_h_05_c"]', 'MiltonDrive', 0, 1, 0, '{"x":-795.692,"y":326.762,"z":186.313}', 1300000),
	(27, 'Monochrome3Apartment', 'Appartement Monochrome 3', NULL, '{"x":-774.012,"y":342.042,"z":195.686}', '{"x":-779.057,"y":342.063,"z":195.686}', NULL, '["apa_v_mp_h_05_b"]', 'MiltonDrive', 0, 1, 0, '{"x":-765.094,"y":330.976,"z":195.085}', 1300000),
	(28, 'Seductive1Apartment', 'Appartement Séduisant 1', NULL, '{"x":-784.194,"y":323.636,"z":210.997}', '{"x":-779.751,"y":323.385,"z":210.997}', NULL, '["apa_v_mp_h_06_a"]', 'MiltonDrive', 0, 1, 0, '{"x":-766.263,"y":328.104,"z":210.396}', 1300000),
	(29, 'Seductive2Apartment', 'Appartement Séduisant 2', NULL, '{"x":-786.8663,"y":315.764,"z":186.913}', '{"x":-781.808,"y":315.866,"z":186.913}', NULL, '["apa_v_mp_h_06_c"]', 'MiltonDrive', 0, 1, 0, '{"x":-795.655,"y":326.611,"z":186.313}', 1300000),
	(30, 'Seductive3Apartment', 'Appartement Séduisant 3', NULL, '{"x":-774.012,"y":342.042,"z":195.686}', '{"x":-779.057,"y":342.063,"z":195.686}', NULL, '["apa_v_mp_h_06_b"]', 'MiltonDrive', 0, 1, 0, '{"x":-765.3,"y":331.414,"z":195.085}', 1300000),
	(31, 'Regal1Apartment', 'Appartement Régal 1', NULL, '{"x":-784.194,"y":323.636,"z":210.997}', '{"x":-779.751,"y":323.385,"z":210.997}', NULL, '["apa_v_mp_h_07_a"]', 'MiltonDrive', 0, 1, 0, '{"x":-765.956,"y":328.257,"z":210.396}', 1300000),
	(32, 'Regal2Apartment', 'Appartement Régal 2', NULL, '{"x":-786.8663,"y":315.764,"z":186.913}', '{"x":-781.808,"y":315.866,"z":186.913}', NULL, '["apa_v_mp_h_07_c"]', 'MiltonDrive', 0, 1, 0, '{"x":-795.545,"y":326.659,"z":186.313}', 1300000),
	(33, 'Regal3Apartment', 'Appartement Régal 3', NULL, '{"x":-774.012,"y":342.042,"z":195.686}', '{"x":-779.057,"y":342.063,"z":195.686}', NULL, '["apa_v_mp_h_07_b"]', 'MiltonDrive', 0, 1, 0, '{"x":-765.087,"y":331.429,"z":195.123}', 1300000),
	(34, 'Aqua1Apartment', 'Appartement Aqua 1', NULL, '{"x":-784.194,"y":323.636,"z":210.997}', '{"x":-779.751,"y":323.385,"z":210.997}', NULL, '["apa_v_mp_h_08_a"]', 'MiltonDrive', 0, 1, 0, '{"x":-766.187,"y":328.47,"z":210.396}', 1300000),
	(35, 'Aqua2Apartment', 'Appartement Aqua 2', NULL, '{"x":-786.8663,"y":315.764,"z":186.913}', '{"x":-781.808,"y":315.866,"z":186.913}', NULL, '["apa_v_mp_h_08_c"]', 'MiltonDrive', 0, 1, 0, '{"x":-795.658,"y":326.563,"z":186.313}', 1300000),
	(36, 'Aqua3Apartment', 'Appartement Aqua 3', NULL, '{"x":-774.012,"y":342.042,"z":195.686}', '{"x":-779.057,"y":342.063,"z":195.686}', NULL, '["apa_v_mp_h_08_b"]', 'MiltonDrive', 0, 1, 0, '{"x":-765.287,"y":331.084,"z":195.086}', 1300000),
	(37, 'IntegrityWay', '4 Integrity Way', '{"x":-47.804,"y":-585.867,"z":36.956}', NULL, NULL, '{"x":-54.178,"y":-583.762,"z":35.798}', '[]', NULL, 0, 0, 1, NULL, 0),
	(38, 'IntegrityWay28', '4 Integrity Way - Apt 28', NULL, '{"x":-31.409,"y":-594.927,"z":79.03}', '{"x":-26.098,"y":-596.909,"z":79.03}', NULL, '[]', 'IntegrityWay', 0, 1, 0, '{"x":-11.923,"y":-597.083,"z":78.43}', 1700000),
	(39, 'IntegrityWay30', '4 Integrity Way - Apt 30', NULL, '{"x":-17.702,"y":-588.524,"z":89.114}', '{"x":-16.21,"y":-582.569,"z":89.114}', NULL, '[]', 'IntegrityWay', 0, 1, 0, '{"x":-26.327,"y":-588.384,"z":89.123}', 1700000),
	(40, 'DellPerroHeights', 'Dell Perro Heights', '{"x":-1447.06,"y":-538.28,"z":33.74}', NULL, NULL, '{"x":-1440.022,"y":-548.696,"z":33.74}', '[]', NULL, 0, 0, 1, NULL, 0),
	(41, 'DellPerroHeightst4', 'Dell Perro Heights - Apt 28', NULL, '{"x":-1452.125,"y":-540.591,"z":73.044}', '{"x":-1455.435,"y":-535.79,"z":73.044}', NULL, '[]', 'DellPerroHeights', 0, 1, 0, '{"x":-1467.058,"y":-527.571,"z":72.443}', 1700000),
	(42, 'DellPerroHeightst7', 'Dell Perro Heights - Apt 30', NULL, '{"x":-1451.562,"y":-523.535,"z":55.928}', '{"x":-1456.02,"y":-519.209,"z":55.929}', NULL, '[]', 'DellPerroHeights', 0, 1, 0, '{"x":-1457.026,"y":-530.219,"z":55.937}', 1700000),
	(43, 'MazeBankBuilding', 'Maze Bank Building', '{"x":-79.18,"y":-795.92,"z":43.35}', NULL, NULL, '{"x":-72.50,"y":-786.92,"z":43.40}', '[]', NULL, 0, 0, 1, NULL, 0),
	(44, 'OldSpiceWarm', 'Old Spice Warm', NULL, '{"x":-75.69,"y":-827.08,"z":242.43}', '{"x":-75.51,"y":-823.90,"z":242.43}', NULL, '["ex_dt1_11_office_01a"]', 'MazeBankBuilding', 0, 1, 0, '{"x":-71.81,"y":-814.34,"z":242.39}', 5000000),
	(45, 'OldSpiceClassical', 'Old Spice Classical', NULL, '{"x":-75.69,"y":-827.08,"z":242.43}', '{"x":-75.51,"y":-823.90,"z":242.43}', NULL, '["ex_dt1_11_office_01b"]', 'MazeBankBuilding', 0, 1, 0, '{"x":-71.81,"y":-814.34,"z":242.39}', 5000000),
	(46, 'OldSpiceVintage', 'Old Spice Vintage', NULL, '{"x":-75.69,"y":-827.08,"z":242.43}', '{"x":-75.51,"y":-823.90,"z":242.43}', NULL, '["ex_dt1_11_office_01c"]', 'MazeBankBuilding', 0, 1, 0, '{"x":-71.81,"y":-814.34,"z":242.39}', 5000000),
	(47, 'ExecutiveRich', 'Executive Rich', NULL, '{"x":-75.69,"y":-827.08,"z":242.43}', '{"x":-75.51,"y":-823.90,"z":242.43}', NULL, '["ex_dt1_11_office_02b"]', 'MazeBankBuilding', 0, 1, 0, '{"x":-71.81,"y":-814.34,"z":242.39}', 5000000),
	(48, 'ExecutiveCool', 'Executive Cool', NULL, '{"x":-75.69,"y":-827.08,"z":242.43}', '{"x":-75.51,"y":-823.90,"z":242.43}', NULL, '["ex_dt1_11_office_02c"]', 'MazeBankBuilding', 0, 1, 0, '{"x":-71.81,"y":-814.34,"z":242.39}', 5000000),
	(49, 'ExecutiveContrast', 'Executive Contrast', NULL, '{"x":-75.69,"y":-827.08,"z":242.43}', '{"x":-75.51,"y":-823.90,"z":242.43}', NULL, '["ex_dt1_11_office_02a"]', 'MazeBankBuilding', 0, 1, 0, '{"x":-71.81,"y":-814.34,"z":242.39}', 5000000),
	(50, 'PowerBrokerIce', 'Power Broker Ice', NULL, '{"x":-75.69,"y":-827.08,"z":242.43}', '{"x":-75.51,"y":-823.90,"z":242.43}', NULL, '["ex_dt1_11_office_03a"]', 'MazeBankBuilding', 0, 1, 0, '{"x":-71.81,"y":-814.34,"z":242.39}', 5000000),
	(51, 'PowerBrokerConservative', 'Power Broker Conservative', NULL, '{"x":-75.69,"y":-827.08,"z":242.43}', '{"x":-75.51,"y":-823.90,"z":242.43}', NULL, '["ex_dt1_11_office_03b"]', 'MazeBankBuilding', 0, 1, 0, '{"x":-71.81,"y":-814.34,"z":242.39}', 5000000),
	(52, 'PowerBrokerPolished', 'Power Broker Polished', NULL, '{"x":-75.69,"y":-827.08,"z":242.43}', '{"x":-75.51,"y":-823.90,"z":242.43}', NULL, '["ex_dt1_11_office_03c"]', 'MazeBankBuilding', 0, 1, 0, '{"x":-71.81,"y":-814.34,"z":242.39}', 5000000),
	(53, 'LomBank', 'Lom Bank', '{"x":-1581.36,"y":-558.23,"z":34.07}', NULL, NULL, '{"x":-1583.60,"y":-555.12,"z":34.07}', '[]', NULL, 0, 0, 1, NULL, 0),
	(54, 'LBOldSpiceWarm', 'LB Old Spice Warm', NULL, '{"x":-1579.53,"y":-564.89,"z":107.62}', '{"x":-1576.42,"y":-567.57,"z":107.62}', NULL, '["ex_sm_13_office_01a"]', 'LomBank', 0, 1, 0, '{"x":-1571.26,"y":-575.76,"z":107.52}', 3500000),
	(55, 'LBOldSpiceClassical', 'LB Old Spice Classical', NULL, '{"x":-1579.53,"y":-564.89,"z":107.62}', '{"x":-1576.42,"y":-567.57,"z":107.62}', NULL, '["ex_sm_13_office_01b"]', 'LomBank', 0, 1, 0, '{"x":-1571.26,"y":-575.76,"z":107.52}', 3500000),
	(56, 'LBOldSpiceVintage', 'LB Old Spice Vintage', NULL, '{"x":-1579.53,"y":-564.89,"z":107.62}', '{"x":-1576.42,"y":-567.57,"z":107.62}', NULL, '["ex_sm_13_office_01c"]', 'LomBank', 0, 1, 0, '{"x":-1571.26,"y":-575.76,"z":107.52}', 3500000),
	(57, 'LBExecutiveRich', 'LB Executive Rich', NULL, '{"x":-1579.53,"y":-564.89,"z":107.62}', '{"x":-1576.42,"y":-567.57,"z":107.62}', NULL, '["ex_sm_13_office_02b"]', 'LomBank', 0, 1, 0, '{"x":-1571.26,"y":-575.76,"z":107.52}', 3500000),
	(58, 'LBExecutiveCool', 'LB Executive Cool', NULL, '{"x":-1579.53,"y":-564.89,"z":107.62}', '{"x":-1576.42,"y":-567.57,"z":107.62}', NULL, '["ex_sm_13_office_02c"]', 'LomBank', 0, 1, 0, '{"x":-1571.26,"y":-575.76,"z":107.52}', 3500000),
	(59, 'LBExecutiveContrast', 'LB Executive Contrast', NULL, '{"x":-1579.53,"y":-564.89,"z":107.62}', '{"x":-1576.42,"y":-567.57,"z":107.62}', NULL, '["ex_sm_13_office_02a"]', 'LomBank', 0, 1, 0, '{"x":-1571.26,"y":-575.76,"z":107.52}', 3500000),
	(60, 'LBPowerBrokerIce', 'LB Power Broker Ice', NULL, '{"x":-1579.53,"y":-564.89,"z":107.62}', '{"x":-1576.42,"y":-567.57,"z":107.62}', NULL, '["ex_sm_13_office_03a"]', 'LomBank', 0, 1, 0, '{"x":-1571.26,"y":-575.76,"z":107.52}', 3500000),
	(61, 'LBPowerBrokerConservative', 'LB Power Broker Conservative', NULL, '{"x":-1579.53,"y":-564.89,"z":107.62}', '{"x":-1576.42,"y":-567.57,"z":107.62}', NULL, '["ex_sm_13_office_03b"]', 'LomBank', 0, 1, 0, '{"x":-1571.26,"y":-575.76,"z":107.52}', 3500000),
	(62, 'LBPowerBrokerPolished', 'LB Power Broker Polished', NULL, '{"x":-1579.53,"y":-564.89,"z":107.62}', '{"x":-1576.42,"y":-567.57,"z":107.62}', NULL, '["ex_sm_13_office_03c"]', 'LomBank', 0, 1, 0, '{"x":-1571.26,"y":-575.76,"z":107.52}', 3500000),
	(63, 'MazeBankWest', 'Maze Bank West', '{"x":-1379.58,"y":-499.63,"z":32.22}', NULL, NULL, '{"x":-1378.95,"y":-502.82,"z":32.22}', '[]', NULL, 0, 0, 1, NULL, 0),
	(64, 'MBWOldSpiceWarm', 'MBW Old Spice Warm', NULL, '{"x":-1392.74,"y":-480.18,"z":71.14}', '{"x":-1389.43,"y":-479.01,"z":71.14}', NULL, '["ex_sm_15_office_01a"]', 'MazeBankWest', 0, 1, 0, '{"x":-1390.76,"y":-479.22,"z":72.04}', 2700000),
	(65, 'MBWOldSpiceClassical', 'MBW Old Spice Classical', NULL, '{"x":-1392.74,"y":-480.18,"z":71.14}', '{"x":-1389.43,"y":-479.01,"z":71.14}', NULL, '["ex_sm_15_office_01b"]', 'MazeBankWest', 0, 1, 0, '{"x":-1390.76,"y":-479.22,"z":72.04}', 2700000),
	(66, 'MBWOldSpiceVintage', 'MBW Old Spice Vintage', NULL, '{"x":-1392.74,"y":-480.18,"z":71.14}', '{"x":-1389.43,"y":-479.01,"z":71.14}', NULL, '["ex_sm_15_office_01c"]', 'MazeBankWest', 0, 1, 0, '{"x":-1390.76,"y":-479.22,"z":72.04}', 2700000),
	(67, 'MBWExecutiveRich', 'MBW Executive Rich', NULL, '{"x":-1392.74,"y":-480.18,"z":71.14}', '{"x":-1389.43,"y":-479.01,"z":71.14}', NULL, '["ex_sm_15_office_02b"]', 'MazeBankWest', 0, 1, 0, '{"x":-1390.76,"y":-479.22,"z":72.04}', 2700000),
	(68, 'MBWExecutiveCool', 'MBW Executive Cool', NULL, '{"x":-1392.74,"y":-480.18,"z":71.14}', '{"x":-1389.43,"y":-479.01,"z":71.14}', NULL, '["ex_sm_15_office_02c"]', 'MazeBankWest', 0, 1, 0, '{"x":-1390.76,"y":-479.22,"z":72.04}', 2700000),
	(69, 'MBWExecutive Contrast', 'MBW Executive Contrast', NULL, '{"x":-1392.74,"y":-480.18,"z":71.14}', '{"x":-1389.43,"y":-479.01,"z":71.14}', NULL, '["ex_sm_15_office_02a"]', 'MazeBankWest', 0, 1, 0, '{"x":-1390.76,"y":-479.22,"z":72.04}', 2700000),
	(70, 'MBWPowerBrokerIce', 'MBW Power Broker Ice', NULL, '{"x":-1392.74,"y":-480.18,"z":71.14}', '{"x":-1389.43,"y":-479.01,"z":71.14}', NULL, '["ex_sm_15_office_03a"]', 'MazeBankWest', 0, 1, 0, '{"x":-1390.76,"y":-479.22,"z":72.04}', 2700000),
	(71, 'MBWPowerBrokerConvservative', 'MBW Power Broker Convservative', NULL, '{"x":-1392.74,"y":-480.18,"z":71.14}', '{"x":-1389.43,"y":-479.01,"z":71.14}', NULL, '["ex_sm_15_office_03b"]', 'MazeBankWest', 0, 1, 0, '{"x":-1390.76,"y":-479.22,"z":72.04}', 2700000),
	(72, 'MBWPowerBrokerPolished', 'MBW Power Broker Polished', NULL, '{"x":-1392.74,"y":-480.18,"z":71.14}', '{"x":-1389.43,"y":-479.01,"z":71.14}', NULL, '["ex_sm_15_office_03c"]', 'MazeBankWest', 0, 1, 0, '{"x":-1390.76,"y":-479.22,"z":72.04}', 2700000);
/*!40000 ALTER TABLE `properties` ENABLE KEYS */;

-- Dumping structure for table essentialmode.rented_vehicles
CREATE TABLE IF NOT EXISTS `rented_vehicles` (
  `vehicle` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `plate` varchar(12) COLLATE utf8mb4_bin NOT NULL,
  `player_name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `base_price` int(11) NOT NULL,
  `rent_price` int(11) NOT NULL,
  `owner` varchar(22) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`plate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.rented_vehicles: ~0 rows (approximately)
/*!40000 ALTER TABLE `rented_vehicles` DISABLE KEYS */;
/*!40000 ALTER TABLE `rented_vehicles` ENABLE KEYS */;

-- Dumping structure for table essentialmode.shops
CREATE TABLE IF NOT EXISTS `shops` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `store` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `item` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `price` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=158 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.shops: ~115 rows (approximately)
/*!40000 ALTER TABLE `shops` DISABLE KEYS */;
INSERT INTO `shops` (`id`, `store`, `item`, `price`) VALUES
	(1, 'TwentyFourSeven', 'bread', 70),
	(2, 'TwentyFourSeven', 'water', 30),
	(3, 'RobsLiquor', 'bread', 70),
	(4, 'RobsLiquor', 'water', 30),
	(5, 'LTDgasoline', 'bread', 70),
	(6, 'LTDgasoline', 'water', 30),
	(7, 'TwentyFourSeven', 'chocolate', 60),
	(8, 'RobsLiquor', 'chocolate', 60),
	(9, 'LTDgasoline', 'chocolate', 60),
	(10, 'TwentyFourSeven', 'sandwich', 60),
	(11, 'RobsLiquor', 'sandwich', 60),
	(12, 'LTDgasoline', 'sandwich', 60),
	(13, 'TwentyFourSeven', 'hamburger', 70),
	(14, 'RobsLiquor', 'hamburger', 70),
	(15, 'LTDgasoline', 'hamburger', 70),
	(16, 'TwentyFourSeven', 'cupcake', 50),
	(17, 'RobsLiquor', 'cupcake', 50),
	(18, 'LTDgasoline', 'cupcake', 50),
	(19, 'TwentyFourSeven', 'cocacola', 30),
	(20, 'RobsLiquor', 'cocacola', 30),
	(21, 'LTDgasoline', 'cocacola', 30),
	(22, 'TwentyFourSeven', 'icetea', 30),
	(23, 'RobsLiquor', 'icetea', 30),
	(24, 'LTDgasoline', 'icetea', 30),
	(25, 'TwentyFourSeven', 'coffe', 30),
	(26, 'RobsLiquor', 'coffe', 30),
	(27, 'LTDgasoline', 'coffe', 30),
	(31, 'RobsLiquor', 'cigarett', 150),
	(32, 'RobsLiquor', 'lighter', 70),
	(33, 'LTDgasoline', 'cigarett', 150),
	(34, 'LTDgasoline', 'lighter', 70),
	(35, 'TwentyFourSeven', 'cigarett', 150),
	(36, 'TwentyFourSeven', 'lighter', 70),
	(37, 'Bar', 'beer', 80),
	(38, 'Bar', 'wine', 150),
	(39, 'Bar', 'vodka', 130),
	(40, 'Bar', 'tequila', 130),
	(41, 'Bar', 'whisky', 100),
	(42, 'Bar', 'cigarett', 150),
	(43, 'Bar', 'lighter', 75),
	(44, 'Disco', 'beer', 80),
	(45, 'Disco', 'wine', 150),
	(46, 'Disco', 'vodka', 130),
	(47, 'Disco', 'tequila', 130),
	(48, 'Disco', 'whisky', 100),
	(49, 'Disco', 'cigarett', 150),
	(50, 'Disco', 'lighter', 75),
	(51, 'fishing_shop', 'fishingrod', 2500),
	(52, 'fishing_shop', 'fishbait', 50),
	(53, 'fishing_shop', 'turtlebait', 150),
	(55, 'TwentyFourSeven', 'fixkit', 3000),
	(56, 'RobsLiquor', 'fixkit', 3000),
	(59, 'weed_shop', 'marijuana_cigarette', 500),
	(60, 'weed_shop', 'bong', 1000),
	(61, 'weed_shop', 'lighter', 75),
	(63, 'pillbox', 'oxygen_mask', 2500),
	(64, 'weaponeacc', 'clip', 10000),
	(65, 'weaponeacc', 'weaponflashlight', 10000),
	(66, 'weaponeacc', 'weapongrip', 10000),
	(67, 'weaponeacc', 'weaponskin', 10000),
	(70, 'LTDgasoline', 'fixkit', 3000),
	(71, 'LTDgasoline', 'croquettes', 150),
	(72, 'TwentyFourSeven', 'croquettes', 150),
	(73, 'RobsLiquor', 'croquettes', 150),
	(74, 'LTDgasoline', 'jumelles', 300),
	(75, 'TwentyFourSeven', 'jumelles', 300),
	(76, 'TwentyFourSeven', 'sickle', 1000),
	(77, 'RobsLiquor', 'sickle', 1000),
	(78, 'LTDgasoline', 'sickle', 1000),
	(85, 'LTDgasoline', 'phone', 3500),
	(86, 'TwentyFourSeven', 'phone', 3500),
	(87, 'RobsLiquor', 'phone', 3500),
	(88, 'LTDgasoline', 'milk_engine', 2000),
	(89, 'TwentyFourSeven', 'milk_engine', 2000),
	(90, 'RobsLiquor', 'milk_engine', 2000),
	(91, 'LTDgasoline', 'drill', 2500),
	(92, 'TwentyFourSeven', 'drill', 2500),
	(93, 'RobsLiquor', 'drill', 2500),
	(94, 'RobsLiquor', 'knife_chicken', 1500),
	(95, 'TwentyFourSeven', 'knife_chicken', 1500),
	(96, 'LTDgasoline', 'knife_chicken', 1500),
	(97, 'pillbox', 'anti', 70),
	(98, 'RobsLiquor', 'boxmilk', 30),
	(99, 'TwentyFourSeven', 'boxmilk', 30),
	(100, 'LTDgasoline', 'boxmilk', 30),
	(101, 'TwentyFourSeven', 'hatchet_lj', 1500),
	(102, 'LTDgasoline', 'hatchet_lj', 1500),
	(103, 'RobsLiquor', 'hatchet_lj', 1500),
	(119, 'pillbox', 'water', 30),
	(120, 'pillbox', 'bread', 70),
	(133, 'RobsLiquor', 'shell_engine', 2000),
	(134, 'LTDgasoline', 'shell_engine', 2000),
	(135, 'TwentyFourSeven', 'shell_engine', 2000),
	(136, 'TwentyFourSeven', 'shovel', 2000),
	(137, 'LTDgasoline', 'shovel', 2000),
	(138, 'RobsLiquor', 'shovel', 2000),
	(139, 'Bar', 'zippybag', 20),
	(140, 'Disco', 'zippybag', 20),
	(141, 'Bar', 'doubleaction_blueprint', 5000),
	(142, 'Bar', 'switchblade_blueprint', 5000),
	(143, 'Disco', 'switchblade_blueprint', 5000),
	(144, 'Disco', 'doubleaction_blueprint', 5000),
	(145, 'Bar', 'wrench_blueprint', 5000),
	(146, 'Disco', 'wrench_blueprint', 5000),
	(147, 'Bar', 'snspistol_blueprint', 5000),
	(148, 'Disco', 'snspistol_blueprint', 5000),
	(149, 'TwentyFourSeven', 'craftkit', 1000),
	(150, 'RobsLiquor', 'craftkit', 1000),
	(151, 'LTDgasoline', 'craftkit', 1000),
	(152, 'TwentyFourSeven', 'beer', 80),
	(153, 'LTDgasoline', 'beer', 80),
	(154, 'RobsLiquor', 'beer', 80),
	(155, 'TwentyFourSeven', 'jumelles', 1000),
	(156, 'RobsLiquor', 'jumelles', 1000),
	(157, 'LTDgasoline', 'jumelles', 1000);
/*!40000 ALTER TABLE `shops` ENABLE KEYS */;

-- Dumping structure for table essentialmode.society_moneywash
CREATE TABLE IF NOT EXISTS `society_moneywash` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(60) NOT NULL,
  `society` varchar(60) NOT NULL,
  `amount` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.society_moneywash: ~0 rows (approximately)
/*!40000 ALTER TABLE `society_moneywash` DISABLE KEYS */;
/*!40000 ALTER TABLE `society_moneywash` ENABLE KEYS */;

-- Dumping structure for table essentialmode.trunk_inventory
CREATE TABLE IF NOT EXISTS `trunk_inventory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `plate` varchar(8) COLLATE utf8mb4_bin NOT NULL,
  `data` text COLLATE utf8mb4_bin NOT NULL,
  `owned` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `plate` (`plate`)
) ENGINE=InnoDB AUTO_INCREMENT=195 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.trunk_inventory: ~1 rows (approximately)
/*!40000 ALTER TABLE `trunk_inventory` DISABLE KEYS */;
INSERT INTO `trunk_inventory` (`id`, `plate`, `data`, `owned`) VALUES
	(193, 'YYR 747 ', '{"coffre":[{"count":1,"name":"water"},{"count":1,"name":"bread"}]}', 1);
/*!40000 ALTER TABLE `trunk_inventory` ENABLE KEYS */;

-- Dumping structure for table essentialmode.twitter_accounts
CREATE TABLE IF NOT EXISTS `twitter_accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT '0',
  `password` varchar(50) COLLATE utf8mb4_bin NOT NULL DEFAULT '0',
  `avatar_url` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=422 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.twitter_accounts: ~0 rows (approximately)
/*!40000 ALTER TABLE `twitter_accounts` DISABLE KEYS */;
INSERT INTO `twitter_accounts` (`id`, `username`, `password`, `avatar_url`) VALUES
	(420, 'ทดสอบ', '123456', NULL),
	(421, 'theone_06', '123456', NULL);
/*!40000 ALTER TABLE `twitter_accounts` ENABLE KEYS */;

-- Dumping structure for table essentialmode.twitter_likes
CREATE TABLE IF NOT EXISTS `twitter_likes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `authorId` int(11) DEFAULT NULL,
  `tweetId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_twitter_likes_twitter_accounts` (`authorId`),
  KEY `FK_twitter_likes_twitter_tweets` (`tweetId`),
  CONSTRAINT `FK_twitter_likes_twitter_accounts` FOREIGN KEY (`authorId`) REFERENCES `twitter_accounts` (`id`),
  CONSTRAINT `FK_twitter_likes_twitter_tweets` FOREIGN KEY (`tweetId`) REFERENCES `twitter_tweets` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=277 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.twitter_likes: ~0 rows (approximately)
/*!40000 ALTER TABLE `twitter_likes` DISABLE KEYS */;
/*!40000 ALTER TABLE `twitter_likes` ENABLE KEYS */;

-- Dumping structure for table essentialmode.twitter_tweets
CREATE TABLE IF NOT EXISTS `twitter_tweets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `authorId` int(11) NOT NULL,
  `realUser` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp(),
  `likes` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `FK_twitter_tweets_twitter_accounts` (`authorId`),
  CONSTRAINT `FK_twitter_tweets_twitter_accounts` FOREIGN KEY (`authorId`) REFERENCES `twitter_accounts` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1391 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table essentialmode.twitter_tweets: ~6 rows (approximately)
/*!40000 ALTER TABLE `twitter_tweets` DISABLE KEYS */;
INSERT INTO `twitter_tweets` (`id`, `authorId`, `realUser`, `message`, `time`, `likes`) VALUES
	(1385, 420, 'steam:110000104a1abc7', 'หวัดดี', '2019-11-23 20:29:38', 0),
	(1386, 420, 'steam:110000104a1abc7', 'กหฟกฟหก', '2019-11-23 20:29:41', 0),
	(1387, 420, 'steam:110000104a1abc7', 'ฟหกหฟก', '2019-11-23 20:29:43', 0),
	(1388, 420, 'steam:110000104a1abc7', 'ฟหกหฟกหกฟหฟก', '2019-11-23 20:29:46', 0),
	(1389, 421, 'steam:11000013ac37e2b', 'อยู่ไหน', '2019-11-24 18:08:44', 0),
	(1390, 421, 'steam:11000013ac37e2b', 'อยู่ไหน', '2019-11-24 18:09:21', 0);
/*!40000 ALTER TABLE `twitter_tweets` ENABLE KEYS */;

-- Dumping structure for table essentialmode.users
CREATE TABLE IF NOT EXISTS `users` (
  `identifier` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `license` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `money` int(11) DEFAULT NULL,
  `name` varchar(50) COLLATE utf8mb4_bin DEFAULT '',
  `skin` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `job` varchar(50) COLLATE utf8mb4_bin DEFAULT 'unemployed',
  `job_grade` int(11) DEFAULT 0,
  `loadout` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `position` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `bank` int(11) DEFAULT NULL,
  `permission_level` int(11) DEFAULT NULL,
  `group` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `status` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `firstname` varchar(50) COLLATE utf8mb4_bin DEFAULT '',
  `lastname` varchar(50) COLLATE utf8mb4_bin DEFAULT '',
  `dateofbirth` varchar(25) COLLATE utf8mb4_bin DEFAULT '',
  `sex` varchar(10) COLLATE utf8mb4_bin DEFAULT '',
  `height` varchar(5) COLLATE utf8mb4_bin DEFAULT '',
  `lastdigits` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `tattoos` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `phone_number` varchar(10) COLLATE utf8mb4_bin DEFAULT NULL,
  `is_dead` tinyint(1) DEFAULT 0,
  `jail` int(11) NOT NULL DEFAULT 0,
  `last_property` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `pet` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `skills` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `gang` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `gang_grade` int(11) DEFAULT NULL,
  `isFirstConnection` int(11) DEFAULT 1,
  PRIMARY KEY (`identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.users: ~5 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`identifier`, `license`, `money`, `name`, `skin`, `job`, `job_grade`, `loadout`, `position`, `bank`, `permission_level`, `group`, `status`, `firstname`, `lastname`, `dateofbirth`, `sex`, `height`, `lastdigits`, `tattoos`, `phone_number`, `is_dead`, `jail`, `last_property`, `pet`, `skills`, `gang`, `gang_grade`, `isFirstConnection`) VALUES
	('steam:110000104a1abc7', 'license:1695737ba9de1c01bf92473f152b1df5ccb54e35', 0, 'THE-ONE', '{"mask_1":0,"hair_2":0,"bags_1":0,"chest_1":0,"complexion_1":0,"lipstick_4":0,"torso_1":7,"watches_1":0,"eyebrows_3":0,"decals_2":0,"chain_1":0,"torso_2":0,"eye_color":0,"glasses_2":0,"age_2":0,"watches_2":0,"bracelets_2":0,"beard_3":0,"sun_1":0,"sex":1,"blemishes_1":0,"mask_2":0,"beard_2":0,"hair_color_1":55,"arms":1,"blush_2":0,"bproof_2":0,"bags_2":0,"skin":0,"lipstick_1":0,"tshirt_1":41,"blush_1":0,"bproof_1":0,"makeup_1":0,"bracelets_1":0,"decals_1":0,"lipstick_3":0,"blemishes_2":0,"shoes_2":0,"eyebrows_2":0,"hair_color_2":0,"chain_2":0,"bodyb_1":0,"arms_2":0,"tshirt_2":0,"helmet_2":0,"makeup_4":0,"age_1":0,"helmet_1":-1,"glasses_1":5,"makeup_3":0,"sun_2":0,"complexion_2":0,"moles_1":0,"lipstick_2":0,"chest_2":0,"chest_3":0,"ears_1":-1,"moles_2":0,"pants_1":8,"bodyb_2":0,"eyebrows_1":0,"makeup_2":0,"ears_2":0,"pants_2":0,"eyebrows_4":0,"hair_1":4,"beard_1":0,"shoes_1":27,"face":29,"beard_4":0,"blush_3":0}', 'unemployed', 0, '[]', '{"x":377.4,"y":-610.8,"z":29.0}', -32500, 0, 'user', '[{"percent":61.56,"name":"hunger","val":615600},{"percent":71.17,"name":"thirst","val":711700},{"percent":0.0,"name":"drunk","val":0},{"percent":0.0,"name":"drunk","val":0}]', 'TTTTT', 'TTTTT', '1999-09-09', 'f', '200', NULL, NULL, '0555108642', 0, 0, NULL, '', '{"Wheelie":{"Stat":"MP0_WHEELIE_ABILITY","Current":0,"RemoveAmount":-0.2},"Driving":{"Stat":"MP0_DRIVING_ABILITY","Current":0,"RemoveAmount":-0.5},"Strength":{"Stat":"MP0_STRENGTH","Current":9.1,"RemoveAmount":-0.3},"Lung Capacity":{"Stat":"MP0_LUNG_CAPACITY","Current":0,"RemoveAmount":-0.1},"Shooting":{"Stat":"MP0_SHOOTING_ABILITY","Current":0,"RemoveAmount":-0.1},"Stamina":{"Stat":"MP0_STAMINA","Current":19.1,"RemoveAmount":-0.3}}', NULL, NULL, 1),
	('steam:110000115e0024c', 'license:66dedbcf0bb7b20fb19134c0a312d5108a608c41', 2147483647, 'Tiffy ❤', '{"hair_1":4,"complexion_1":0,"bracelets_2":0,"blush_3":0,"hair_2":0,"lipstick_3":0,"watches_1":0,"glasses_2":0,"blush_1":0,"glasses_1":5,"ears_1":-1,"makeup_3":0,"shoes_2":0,"bproof_1":0,"makeup_2":0,"watches_2":0,"chest_1":0,"beard_2":0,"mask_2":0,"helmet_1":-1,"beard_4":0,"hair_color_1":55,"sex":1,"blemishes_2":0,"chain_1":0,"eyebrows_4":0,"lipstick_1":0,"eyebrows_1":0,"chest_2":0,"bodyb_2":0,"sun_2":0,"beard_3":0,"face":29,"mask_1":0,"eye_color":0,"beard_1":0,"torso_2":0,"complexion_2":0,"eyebrows_3":0,"moles_1":0,"chest_3":0,"skin":0,"eyebrows_2":0,"makeup_4":0,"makeup_1":0,"blemishes_1":0,"age_2":0,"tshirt_1":41,"helmet_2":0,"sun_1":0,"lipstick_2":0,"decals_1":0,"bags_2":0,"torso_1":7,"ears_2":0,"decals_2":0,"tshirt_2":0,"moles_2":0,"shoes_1":27,"bracelets_1":0,"bags_1":0,"arms":1,"hair_color_2":0,"pants_1":8,"chain_2":0,"blush_2":0,"bodyb_1":0,"lipstick_4":0,"arms_2":0,"age_1":0,"pants_2":0,"bproof_2":0}', 'police', 4, '[{"label":"Knife","components":[],"ammo":0,"name":"WEAPON_KNIFE"},{"label":"Hammer","components":[],"ammo":0,"name":"WEAPON_HAMMER"},{"label":"Pistol","components":["clip_default"],"ammo":54,"name":"WEAPON_PISTOL"},{"label":"Combat pistol","components":["clip_default"],"ammo":54,"name":"WEAPON_COMBATPISTOL"},{"label":"AP pistol","components":["clip_default"],"ammo":54,"name":"WEAPON_APPISTOL"},{"label":"Pistol .50","components":["clip_default"],"ammo":54,"name":"WEAPON_PISTOL50"},{"label":"Heavy revolver","components":[],"ammo":54,"name":"WEAPON_REVOLVER"},{"label":"Micro SMG","components":["clip_default"],"ammo":30,"name":"WEAPON_MICROSMG"},{"label":"Sawed off shotgun","components":[],"ammo":14,"name":"WEAPON_SAWNOFFSHOTGUN"},{"label":"Assault shotgun","components":["clip_default"],"ammo":14,"name":"WEAPON_ASSAULTSHOTGUN"},{"label":"Carbine rifle","components":["clip_default"],"ammo":30,"name":"WEAPON_CARBINERIFLE"},{"label":"Special carbine","components":["clip_default"],"ammo":30,"name":"WEAPON_SPECIALCARBINE"},{"label":"Bullpup rifle","components":["clip_default"],"ammo":30,"name":"WEAPON_BULLPUPRIFLE"},{"label":"Combat MG","components":["clip_default"],"ammo":100,"name":"WEAPON_COMBATMG"},{"label":"Taser","components":[],"ammo":250,"name":"WEAPON_STUNGUN"},{"label":"Knuckledusters","components":[],"ammo":0,"name":"WEAPON_KNUCKLE"}]', '{"y":-1325.8,"z":30.2,"x":-210.7}', 2147483647, 0, 'superadmin', '[{"percent":3.26,"name":"hunger","val":32600},{"percent":14.945,"name":"thirst","val":149450},{"percent":0.0,"name":"drunk","val":0},{"percent":0.0,"name":"drunk","val":0}]', 'Tiffy', 'Miffy', '1994-16-02', 'f', '160', NULL, NULL, '0636776733', 0, 0, NULL, '', '{"Lung Capacity":{"RemoveAmount":-0.1,"Current":0,"Stat":"MP0_LUNG_CAPACITY"},"Strength":{"RemoveAmount":-0.3,"Current":7.6,"Stat":"MP0_STRENGTH"},"Driving":{"RemoveAmount":-0.5,"Current":0,"Stat":"MP0_DRIVING_ABILITY"},"Shooting":{"RemoveAmount":-0.1,"Current":0,"Stat":"MP0_SHOOTING_ABILITY"},"Stamina":{"RemoveAmount":-0.3,"Current":17.6,"Stat":"MP0_STAMINA"},"Wheelie":{"RemoveAmount":-0.2,"Current":0,"Stat":"MP0_WHEELIE_ABILITY"}}', NULL, NULL, 1),
	('steam:110000136cd1665', 'license:1695737ba9de1c01bf92473f152b1df5ccb54e35', 0, 'giftstickerline.3', '{"decals_2":0,"beard_2":0,"eyebrows_2":0,"lipstick_3":0,"lipstick_1":0,"shoes_2":0,"torso_1":10,"hair_color_1":0,"complexion_1":0,"makeup_2":0,"beard_4":0,"tshirt_2":0,"bags_1":0,"chain_2":0,"chain_1":0,"bracelets_2":0,"sun_2":0,"chest_2":0,"chest_1":0,"hair_color_2":0,"eyebrows_3":0,"bodyb_1":0,"bproof_1":0,"sex":0,"chest_3":0,"age_1":0,"hair_1":2,"moles_1":0,"decals_1":0,"hair_2":0,"pants_2":0,"bracelets_1":-1,"watches_1":-1,"face":0,"blush_1":0,"ears_2":0,"beard_3":0,"blemishes_1":0,"tshirt_1":4,"mask_1":0,"torso_2":0,"bodyb_2":0,"bags_2":0,"ears_1":-1,"sun_1":0,"blemishes_2":0,"moles_2":0,"lipstick_2":1,"blush_3":0,"glasses_2":0,"arms_2":0,"lipstick_4":0,"eyebrows_4":0,"bproof_2":0,"glasses_1":0,"shoes_1":27,"makeup_1":0,"eyebrows_1":0,"helmet_2":0,"makeup_4":0,"mask_2":0,"blush_2":0,"age_2":0,"arms":1,"helmet_1":-1,"skin":0,"pants_1":10,"complexion_2":0,"eye_color":0,"makeup_3":0,"watches_2":0,"beard_1":0}', 'unemployed', 0, '[]', '{"y":-980.1,"x":-244.1,"z":29.9}', 0, 0, 'user', '[{"name":"hunger","percent":91.68,"val":916800},{"name":"thirst","percent":93.76,"val":937600},{"name":"drunk","percent":0.0,"val":0},{"name":"drunk","percent":0.0,"val":0}]', 'หฟกหฟกหฟ', 'ดเกเดกเกดเกดเ', '10/09/1999', 'm', '200', NULL, NULL, '0694497680', 0, 0, NULL, '', '{"Wheelie":{"RemoveAmount":-0.2,"Stat":"MP0_WHEELIE_ABILITY","Current":0},"Driving":{"RemoveAmount":-0.5,"Stat":"MP0_DRIVING_ABILITY","Current":0},"Strength":{"RemoveAmount":-0.3,"Stat":"MP0_STRENGTH","Current":9.7},"Shooting":{"RemoveAmount":-0.1,"Stat":"MP0_SHOOTING_ABILITY","Current":0},"Lung Capacity":{"RemoveAmount":-0.1,"Stat":"MP0_LUNG_CAPACITY","Current":0},"Stamina":{"RemoveAmount":-0.3,"Stat":"MP0_STAMINA","Current":19.7}}', NULL, NULL, 1),
	('steam:11000013ac515f5', 'license:1695737ba9de1c01bf92473f152b1df5ccb54e35', 0, 'OMG', NULL, 'unemployed', 0, '[]', '{"y":-973.4,"z":31.2,"x":-258.2}', 0, 0, 'user', '[{"name":"hunger","percent":94.02,"val":940200},{"name":"thirst","percent":95.515,"val":955150},{"name":"drunk","percent":0.0,"val":0},{"name":"drunk","percent":0.0,"val":0}]', '', '', '', '', '', NULL, NULL, '0300875854', 0, 0, NULL, '', NULL, NULL, NULL, 1),
	('steam:11000013dce3ca4', 'license:1f76e3edb1e90af068b64bf939364578b4b3395a', 3500, 'Paworaprat', '{"blush_3":0,"makeup_1":0,"chest_1":0,"decals_1":0,"complexion_2":0,"arms":1,"chest_2":0,"blemishes_2":0,"skin":0,"eyebrows_4":0,"ears_1":-1,"ears_2":0,"tshirt_1":41,"sex":1,"beard_2":0,"makeup_4":0,"bracelets_2":0,"glasses_1":5,"shoes_1":27,"arms_2":0,"lipstick_1":0,"sun_1":0,"chain_1":0,"beard_4":0,"decals_2":0,"sun_2":0,"complexion_1":0,"face":29,"bracelets_1":0,"bodyb_1":0,"pants_1":8,"makeup_3":0,"watches_1":0,"helmet_2":0,"chain_2":0,"torso_2":0,"age_2":0,"mask_1":0,"tshirt_2":0,"blush_2":0,"age_1":0,"bags_2":0,"chest_3":0,"hair_1":4,"beard_3":0,"blush_1":0,"helmet_1":-1,"blemishes_1":0,"hair_color_2":0,"mask_2":0,"beard_1":0,"lipstick_4":0,"bproof_1":0,"torso_1":7,"eyebrows_2":0,"eyebrows_3":0,"watches_2":0,"hair_color_1":55,"hair_2":0,"lipstick_3":0,"moles_1":0,"moles_2":0,"bproof_2":0,"eye_color":0,"shoes_2":0,"eyebrows_1":0,"makeup_2":0,"pants_2":0,"glasses_2":0,"bodyb_2":0,"bags_1":0,"lipstick_2":0}', 'ambulance', 3, '[]', '{"x":-450.9,"y":-881.4,"z":50.8}', 999999999, 0, 'superadmin', '[{"val":457400,"percent":45.74,"name":"hunger"},{"val":593050,"percent":59.305,"name":"thirst"},{"val":0,"percent":0.0,"name":"drunk"},{"val":0,"percent":0.0,"name":"drunk"}]', 'NaTasha', 'Romanov', '1999-01-17', 'f', '168', NULL, NULL, '0700030517', 0, 0, NULL, '', '{"Driving":{"Current":0,"Stat":"MP0_DRIVING_ABILITY","RemoveAmount":-0.5},"Shooting":{"Current":0,"Stat":"MP0_SHOOTING_ABILITY","RemoveAmount":-0.1},"Stamina":{"Current":0,"Stat":"MP0_STAMINA","RemoveAmount":-0.3},"Wheelie":{"Current":0,"Stat":"MP0_WHEELIE_ABILITY","RemoveAmount":-0.2},"Lung Capacity":{"Current":0,"Stat":"MP0_LUNG_CAPACITY","RemoveAmount":-0.1},"Strength":{"Current":0,"Stat":"MP0_STRENGTH","RemoveAmount":-0.3}}', NULL, NULL, 1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table essentialmode.user_accounts
CREATE TABLE IF NOT EXISTS `user_accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(22) COLLATE utf8mb4_bin NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `money` double NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.user_accounts: ~6 rows (approximately)
/*!40000 ALTER TABLE `user_accounts` DISABLE KEYS */;
INSERT INTO `user_accounts` (`id`, `identifier`, `name`, `money`) VALUES
	(1, 'steam:11000013ac515f5', 'black_money', 0),
	(2, 'steam:110000104a1abc7', 'black_money', 0),
	(3, 'steam:110000115e0024c', 'black_money', 0),
	(4, 'steam:11000013dce3ca4', 'black_money', 0),
	(5, 'steam:11000013ac37e2b', 'black_money', 0),
	(6, 'steam:110000136cd1665', 'black_money', 0);
/*!40000 ALTER TABLE `user_accounts` ENABLE KEYS */;

-- Dumping structure for table essentialmode.user_inventory
CREATE TABLE IF NOT EXISTS `user_inventory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(22) COLLATE utf8mb4_bin NOT NULL,
  `item` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `count` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1321 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.user_inventory: ~1,320 rows (approximately)
/*!40000 ALTER TABLE `user_inventory` DISABLE KEYS */;
INSERT INTO `user_inventory` (`id`, `identifier`, `item`, `count`) VALUES
	(1, 'steam:11000013ac515f5', 'chest_a', 0),
	(2, 'steam:11000013ac515f5', 'carotool', 0),
	(3, 'steam:11000013ac515f5', 'meat', 0),
	(4, 'steam:11000013ac515f5', 'WEAPON_VINTAGEPISTOL', 0),
	(5, 'steam:11000013ac515f5', 'coke', 0),
	(6, 'steam:11000013ac515f5', 'tequila', 0),
	(7, 'steam:11000013ac515f5', 'blowtorch', 0),
	(8, 'steam:11000013ac515f5', 'WEAPON_HEAVYSHOTGUN', 0),
	(9, 'steam:11000013ac515f5', 'alive_chicken', 0),
	(10, 'steam:11000013ac515f5', 'WEAPON_BULLPUPSHOTGUN', 0),
	(11, 'steam:11000013ac515f5', 'bong', 0),
	(12, 'steam:11000013ac515f5', 'WEAPON_HAMMER', 0),
	(13, 'steam:11000013ac515f5', 'mushroom_p', 0),
	(14, 'steam:11000013ac515f5', 'rope', 0),
	(15, 'steam:11000013ac515f5', 'jumelles', 0),
	(16, 'steam:11000013ac515f5', 'squid', 0),
	(17, 'steam:11000013ac515f5', 'WEAPON_ASSAULTRIFLE', 0),
	(18, 'steam:11000013ac515f5', 'washed_stone', 0),
	(19, 'steam:11000013ac515f5', 'packaged_chicken', 0),
	(20, 'steam:11000013ac515f5', 'copper', 0),
	(21, 'steam:11000013ac515f5', 'WEAPON_POOLCUE', 0),
	(22, 'steam:11000013ac515f5', 'chickenwithoutfeather', 0),
	(23, 'steam:11000013ac515f5', 'petrol_raffin', 0),
	(24, 'steam:11000013ac515f5', 'lockpick', 0),
	(25, 'steam:11000013ac515f5', 'WEAPON_BULLPUPRIFLE', 0),
	(26, 'steam:11000013ac515f5', 'WEAPON_BATTLEAXE', 0),
	(27, 'steam:11000013ac515f5', 'bottle', 0),
	(28, 'steam:11000013ac515f5', 'snspistol_blueprint', 0),
	(29, 'steam:11000013ac515f5', 'wrench_blueprint', 0),
	(30, 'steam:11000013ac515f5', 'WEAPON_HEAVYSNIPER', 0),
	(31, 'steam:11000013ac515f5', 'chicken_package', 0),
	(32, 'steam:11000013ac515f5', 'WEAPON_GRENADE', 0),
	(33, 'steam:11000013ac515f5', 'WEAPON_FIREEXTINGUISHER', 0),
	(34, 'steam:11000013ac515f5', 'WEAPON_GRENADELAUNCHER', 0),
	(35, 'steam:11000013ac515f5', 'WEAPON_SAWNOFFSHOTGUN', 0),
	(36, 'steam:11000013ac515f5', 'WEAPON_KNUCKLE', 0),
	(37, 'steam:11000013ac515f5', 'rice_pro', 0),
	(38, 'steam:11000013ac515f5', 'WEAPON_GARBAGEBAG', 0),
	(39, 'steam:11000013ac515f5', 'WEAPON_SMOKEGRENADE', 0),
	(40, 'steam:11000013ac515f5', 'shell', 0),
	(41, 'steam:11000013ac515f5', 'iron', 0),
	(42, 'steam:11000013ac515f5', 'WEAPON_DIGISCANNER', 0),
	(43, 'steam:11000013ac515f5', 'gold', 0),
	(44, 'steam:11000013ac515f5', 'sickle', 0),
	(45, 'steam:11000013ac515f5', 'WEAPON_SWITCHBLADE', 0),
	(46, 'steam:11000013ac515f5', 'chocolate', 0),
	(47, 'steam:11000013ac515f5', 'clip', 0),
	(48, 'steam:11000013ac515f5', 'weaponskin', 0),
	(49, 'steam:11000013ac515f5', 'WEAPON_BALL', 0),
	(50, 'steam:11000013ac515f5', 'britadeira', 0),
	(51, 'steam:11000013ac515f5', 'WEAPON_REVOLVER', 0),
	(52, 'steam:11000013ac515f5', 'WEAPON_ADVANCEDRIFLE', 0),
	(53, 'steam:11000013ac515f5', 'WEAPON_BAT', 0),
	(54, 'steam:11000013ac515f5', 'slaughtered_chicken', 0),
	(55, 'steam:11000013ac515f5', 'WEAPON_COMPACTRIFLE', 0),
	(56, 'steam:11000013ac515f5', 'gym_membership', 0),
	(57, 'steam:11000013ac515f5', 'WEAPON_HEAVYPISTOL', 0),
	(58, 'steam:11000013ac515f5', 'essence', 0),
	(59, 'steam:11000013ac515f5', 'whisky', 0),
	(60, 'steam:11000013ac515f5', 'WEAPON_MINIGUN', 0),
	(61, 'steam:11000013ac515f5', 'WEAPON_SPECIALCARBINE', 0),
	(62, 'steam:11000013ac515f5', 'WEAPON_BZGAS', 0),
	(63, 'steam:11000013ac515f5', 'rice', 0),
	(64, 'steam:11000013ac515f5', 'water', 0),
	(65, 'steam:11000013ac515f5', 'zippybag', 0),
	(66, 'steam:11000013ac515f5', 'hydrocodone', 0),
	(67, 'steam:11000013ac515f5', 'worm', 0),
	(68, 'steam:11000013ac515f5', 'pork', 0),
	(69, 'steam:11000013ac515f5', 'WEAPON_SNSPISTOL', 0),
	(70, 'steam:11000013ac515f5', 'WEAPON_DOUBLEACTION', 0),
	(71, 'steam:11000013ac515f5', 'wood', 0),
	(72, 'steam:11000013ac515f5', 'WEAPON_PUMPSHOTGUN', 0),
	(73, 'steam:11000013ac515f5', 'wine', 0),
	(74, 'steam:11000013ac515f5', 'weed_pooch', 0),
	(75, 'steam:11000013ac515f5', 'cow_card', 0),
	(76, 'steam:11000013ac515f5', 'carokit', 0),
	(77, 'steam:11000013ac515f5', 'weapongrip', 0),
	(78, 'steam:11000013ac515f5', 'beer', 0),
	(79, 'steam:11000013ac515f5', 'honey_a', 0),
	(80, 'steam:11000013ac515f5', 'glasses', 0),
	(81, 'steam:11000013ac515f5', 'vicodin', 0),
	(82, 'steam:11000013ac515f5', 'chicken_meat', 0),
	(83, 'steam:11000013ac515f5', 'turtlebait', 0),
	(84, 'steam:11000013ac515f5', 'WEAPON_DAGGER', 0),
	(85, 'steam:11000013ac515f5', 'WEAPON_FIREWORK', 0),
	(86, 'steam:11000013ac515f5', 'WEAPON_NIGHTSTICK', 0),
	(87, 'steam:11000013ac515f5', 'turtle', 0),
	(88, 'steam:11000013ac515f5', 'WEAPON_HATCHET', 0),
	(89, 'steam:11000013ac515f5', 'switchblade_blueprint', 0),
	(90, 'steam:11000013ac515f5', 'shell_c', 0),
	(91, 'steam:11000013ac515f5', 'WEAPON_COMBATPISTOL', 0),
	(92, 'steam:11000013ac515f5', 'squidbait', 0),
	(93, 'steam:11000013ac515f5', 'sandwich', 0),
	(94, 'steam:11000013ac515f5', 'skate', 0),
	(95, 'steam:11000013ac515f5', 'shovel', 0),
	(96, 'steam:11000013ac515f5', 'drill', 0),
	(97, 'steam:11000013ac515f5', 'shell_package', 0),
	(98, 'steam:11000013ac515f5', 'shell_engine', 0),
	(99, 'steam:11000013ac515f5', 'stone', 0),
	(100, 'steam:11000013ac515f5', 'fishingrod', 0),
	(101, 'steam:11000013ac515f5', 'shell_b', 0),
	(102, 'steam:11000013ac515f5', 'shell_a', 0),
	(103, 'steam:11000013ac515f5', 'crazypersoncard', 0),
	(104, 'steam:11000013ac515f5', 'fixkit', 0),
	(105, 'steam:11000013ac515f5', 'bread', 0),
	(106, 'steam:11000013ac515f5', 'cannabis', 0),
	(107, 'steam:11000013ac515f5', 'sharkfin', 0),
	(108, 'steam:11000013ac515f5', 'WEAPON_STUNGUN', 0),
	(109, 'steam:11000013ac515f5', 'WEAPON_PROXMINE', 0),
	(110, 'steam:11000013ac515f5', 'shark', 0),
	(111, 'steam:11000013ac515f5', 'sportlunch', 0),
	(112, 'steam:11000013ac515f5', 'sand', 0),
	(113, 'steam:11000013ac515f5', 'sand_b', 0),
	(114, 'steam:11000013ac515f5', 'sand_a', 0),
	(115, 'steam:11000013ac515f5', 'radio', 0),
	(116, 'steam:11000013ac515f5', 'fishbait', 0),
	(117, 'steam:11000013ac515f5', 'protein_shake', 0),
	(118, 'steam:11000013ac515f5', 'bobbypin', 0),
	(119, 'steam:11000013ac515f5', 'clothe', 0),
	(120, 'steam:11000013ac515f5', 'WEAPON_COMPACTLAUNCHER', 0),
	(121, 'steam:11000013ac515f5', 'prawnbait', 0),
	(122, 'steam:11000013ac515f5', 'prawn', 0),
	(123, 'steam:11000013ac515f5', 'milk_package', 0),
	(124, 'steam:11000013ac515f5', 'powerade', 0),
	(125, 'steam:11000013ac515f5', 'wool', 0),
	(126, 'steam:11000013ac515f5', 'porkpackage', 0),
	(127, 'steam:11000013ac515f5', 'cupcake', 0),
	(128, 'steam:11000013ac515f5', 'phone', 0),
	(129, 'steam:11000013ac515f5', 'milk', 0),
	(130, 'steam:11000013ac515f5', 'petrol', 0),
	(131, 'steam:11000013ac515f5', 'pro_wood', 0),
	(132, 'steam:11000013ac515f5', 'WEAPON_KNIFE', 0),
	(133, 'steam:11000013ac515f5', 'oxygen_mask', 0),
	(134, 'steam:11000013ac515f5', 'mushroom_d', 0),
	(135, 'steam:11000013ac515f5', 'WEAPON_ASSAULTSMG', 0),
	(136, 'steam:11000013ac515f5', 'WEAPON_PISTOL', 0),
	(137, 'steam:11000013ac515f5', 'WEAPON_GUSENBERG', 0),
	(138, 'steam:11000013ac515f5', 'mushroom', 0),
	(139, 'steam:11000013ac515f5', 'morphine', 0),
	(140, 'steam:11000013ac515f5', 'meth', 0),
	(141, 'steam:11000013ac515f5', 'cutted_wood', 0),
	(142, 'steam:11000013ac515f5', 'milk_engine', 0),
	(143, 'steam:11000013ac515f5', 'gazbottle', 0),
	(144, 'steam:11000013ac515f5', 'WEAPON_MUSKET', 0),
	(145, 'steam:11000013ac515f5', 'lighter', 0),
	(146, 'steam:11000013ac515f5', 'cigarett', 0),
	(147, 'steam:11000013ac515f5', 'meatshark', 0),
	(148, 'steam:11000013ac515f5', 'bandage', 0),
	(149, 'steam:11000013ac515f5', 'medikit', 0),
	(150, 'steam:11000013ac515f5', 'chicken', 0),
	(151, 'steam:11000013ac515f5', 'marijuana', 0),
	(152, 'steam:11000013ac515f5', 'WEAPON_APPISTOL', 0),
	(153, 'steam:11000013ac515f5', 'marijuana_cigarette', 0),
	(154, 'steam:11000013ac515f5', 'WEAPON_PETROLCAN', 0),
	(155, 'steam:11000013ac515f5', 'fuel_b', 0),
	(156, 'steam:11000013ac515f5', 'WEAPON_RAILGUN', 0),
	(157, 'steam:11000013ac515f5', 'cocacola', 0),
	(158, 'steam:11000013ac515f5', 'WEAPON_MACHETE', 0),
	(159, 'steam:11000013ac515f5', 'coke_pooch', 0),
	(160, 'steam:11000013ac515f5', 'weaponflashlight', 0),
	(161, 'steam:11000013ac515f5', 'WEAPON_RPG', 0),
	(162, 'steam:11000013ac515f5', 'icetea', 0),
	(163, 'steam:11000013ac515f5', 'honey_b', 0),
	(164, 'steam:11000013ac515f5', 'WEAPON_AUTOSHOTGUN', 0),
	(165, 'steam:11000013ac515f5', 'WEAPON_HOMINGLAUNCHER', 0),
	(166, 'steam:11000013ac515f5', 'hatchet_lj', 0),
	(167, 'steam:11000013ac515f5', 'WEAPON_WRENCH', 0),
	(168, 'steam:11000013ac515f5', 'fabric', 0),
	(169, 'steam:11000013ac515f5', 'WEAPON_GOLFCLUB', 0),
	(170, 'steam:11000013ac515f5', 'handcuffs', 0),
	(171, 'steam:11000013ac515f5', 'WEAPON_STINGER', 0),
	(172, 'steam:11000013ac515f5', 'WEAPON_MACHINEPISTOL', 0),
	(173, 'steam:11000013ac515f5', 'hamburger', 0),
	(174, 'steam:11000013ac515f5', 'hamajifish', 0),
	(175, 'steam:11000013ac515f5', 'WEAPON_CROWBAR', 0),
	(176, 'steam:11000013ac515f5', 'Packaged_plank', 0),
	(177, 'steam:11000013ac515f5', 'WEAPON_COMBATPDW', 0),
	(178, 'steam:11000013ac515f5', 'anti', 0),
	(179, 'steam:11000013ac515f5', 'vodka', 0),
	(180, 'steam:11000013ac515f5', 'boxmilk', 0),
	(181, 'steam:11000013ac515f5', 'WEAPON_PIPEBOMB', 0),
	(182, 'steam:11000013ac515f5', 'meth_pooch', 0),
	(183, 'steam:11000013ac515f5', 'leather', 0),
	(184, 'steam:11000013ac515f5', 'fuel_a', 0),
	(185, 'steam:11000013ac515f5', 'blowpipe', 0),
	(186, 'steam:11000013ac515f5', 'fish', 0),
	(187, 'steam:11000013ac515f5', 'doubleaction_blueprint', 0),
	(188, 'steam:11000013ac515f5', 'AED', 0),
	(189, 'steam:11000013ac515f5', 'WEAPON_MARKSMANPISTOL', 0),
	(190, 'steam:11000013ac515f5', 'WEAPON_CARBINERIFLE', 0),
	(191, 'steam:11000013ac515f5', 'WEAPON_SNIPERRIFLE', 0),
	(192, 'steam:11000013ac515f5', 'WEAPON_FLASHLIGHT', 0),
	(193, 'steam:11000013ac515f5', 'WEAPON_SNOWBALL', 0),
	(194, 'steam:11000013ac515f5', 'WEAPON_PISTOL50', 0),
	(195, 'steam:11000013ac515f5', 'WEAPON_MG', 0),
	(196, 'steam:11000013ac515f5', 'WEAPON_HANDCUFFS', 0),
	(197, 'steam:11000013ac515f5', 'SteelScrap', 0),
	(198, 'steam:11000013ac515f5', 'WEAPON_STICKYBOMB', 0),
	(199, 'steam:11000013ac515f5', 'fixtool', 0),
	(200, 'steam:11000013ac515f5', 'diamond', 0),
	(201, 'steam:11000013ac515f5', 'WEAPON_MARKSMANRIFLE', 0),
	(202, 'steam:11000013ac515f5', 'diesel', 0),
	(203, 'steam:11000013ac515f5', 'WEAPON_ASSAULTSHOTGUN', 0),
	(204, 'steam:11000013ac515f5', 'croquettes', 0),
	(205, 'steam:11000013ac515f5', 'craftkit', 0),
	(206, 'steam:11000013ac515f5', 'WEAPON_DBSHOTGUN', 0),
	(207, 'steam:11000013ac515f5', 'knife_chicken', 0),
	(208, 'steam:11000013ac515f5', 'packaged_plank', 0),
	(209, 'steam:11000013ac515f5', 'WEAPON_MICROSMG', 0),
	(210, 'steam:11000013ac515f5', 'WEAPON_MINISMG', 0),
	(211, 'steam:11000013ac515f5', 'coffe', 0),
	(212, 'steam:11000013ac515f5', 'gunpowder', 0),
	(213, 'steam:11000013ac515f5', 'WEAPON_BOTTLE', 0),
	(214, 'steam:11000013ac515f5', 'bcabbage', 0),
	(215, 'steam:11000013ac515f5', 'WEAPON_FLAREGUN', 0),
	(216, 'steam:11000013ac515f5', 'WEAPON_FLARE', 0),
	(217, 'steam:11000013ac515f5', 'acabbage', 0),
	(218, 'steam:11000013ac515f5', 'WEAPON_SMG', 0),
	(219, 'steam:11000013ac515f5', 'WEAPON_MOLOTOV', 0),
	(220, 'steam:11000013ac515f5', 'WEAPON_COMBATMG', 0),
	(221, 'steam:110000104a1abc7', 'chest_a', 0),
	(222, 'steam:110000104a1abc7', 'carotool', 0),
	(223, 'steam:110000104a1abc7', 'coke', 0),
	(224, 'steam:110000104a1abc7', 'WEAPON_VINTAGEPISTOL', 0),
	(225, 'steam:110000104a1abc7', 'meat', 0),
	(226, 'steam:110000104a1abc7', 'alive_chicken', 0),
	(227, 'steam:110000104a1abc7', 'tequila', 0),
	(228, 'steam:110000104a1abc7', 'blowtorch', 0),
	(229, 'steam:110000104a1abc7', 'WEAPON_HEAVYSHOTGUN', 0),
	(230, 'steam:110000104a1abc7', 'WEAPON_BULLPUPSHOTGUN', 0),
	(231, 'steam:110000104a1abc7', 'bong', 0),
	(232, 'steam:110000104a1abc7', 'WEAPON_HAMMER', 0),
	(233, 'steam:110000104a1abc7', 'mushroom_p', 0),
	(234, 'steam:110000104a1abc7', 'rope', 0),
	(235, 'steam:110000104a1abc7', 'jumelles', 0),
	(236, 'steam:110000104a1abc7', 'WEAPON_ASSAULTRIFLE', 0),
	(237, 'steam:110000104a1abc7', 'squid', 0),
	(238, 'steam:110000104a1abc7', 'copper', 0),
	(239, 'steam:110000104a1abc7', 'washed_stone', 0),
	(240, 'steam:110000104a1abc7', 'packaged_chicken', 0),
	(241, 'steam:110000104a1abc7', 'WEAPON_POOLCUE', 0),
	(242, 'steam:110000104a1abc7', 'chickenwithoutfeather', 0),
	(243, 'steam:110000104a1abc7', 'petrol_raffin', 0),
	(244, 'steam:110000104a1abc7', 'WEAPON_BULLPUPRIFLE', 0),
	(245, 'steam:110000104a1abc7', 'lockpick', 0),
	(246, 'steam:110000104a1abc7', 'WEAPON_BATTLEAXE', 0),
	(247, 'steam:110000104a1abc7', 'bottle', 0),
	(248, 'steam:110000104a1abc7', 'snspistol_blueprint', 0),
	(249, 'steam:110000104a1abc7', 'wrench_blueprint', 0),
	(250, 'steam:110000104a1abc7', 'WEAPON_HEAVYSNIPER', 0),
	(251, 'steam:110000104a1abc7', 'chicken_package', 0),
	(252, 'steam:110000104a1abc7', 'WEAPON_GRENADE', 0),
	(253, 'steam:110000104a1abc7', 'WEAPON_FIREEXTINGUISHER', 0),
	(254, 'steam:110000104a1abc7', 'WEAPON_SAWNOFFSHOTGUN', 0),
	(255, 'steam:110000104a1abc7', 'WEAPON_GRENADELAUNCHER', 0),
	(256, 'steam:110000104a1abc7', 'rice_pro', 0),
	(257, 'steam:110000104a1abc7', 'WEAPON_KNUCKLE', 0),
	(258, 'steam:110000104a1abc7', 'WEAPON_GARBAGEBAG', 0),
	(259, 'steam:110000104a1abc7', 'shell', 0),
	(260, 'steam:110000104a1abc7', 'WEAPON_SMOKEGRENADE', 0),
	(261, 'steam:110000104a1abc7', 'iron', 0),
	(262, 'steam:110000104a1abc7', 'gold', 0),
	(263, 'steam:110000104a1abc7', 'WEAPON_SWITCHBLADE', 0),
	(264, 'steam:110000104a1abc7', 'sickle', 0),
	(265, 'steam:110000104a1abc7', 'WEAPON_DIGISCANNER', 0),
	(266, 'steam:110000104a1abc7', 'weaponskin', 0),
	(267, 'steam:110000104a1abc7', 'WEAPON_BALL', 0),
	(268, 'steam:110000104a1abc7', 'britadeira', 0),
	(269, 'steam:110000104a1abc7', 'chocolate', 0),
	(270, 'steam:110000104a1abc7', 'clip', 0),
	(271, 'steam:110000104a1abc7', 'WEAPON_REVOLVER', 0),
	(272, 'steam:110000104a1abc7', 'slaughtered_chicken', 0),
	(273, 'steam:110000104a1abc7', 'WEAPON_ADVANCEDRIFLE', 0),
	(274, 'steam:110000104a1abc7', 'WEAPON_BAT', 0),
	(275, 'steam:110000104a1abc7', 'WEAPON_COMPACTRIFLE', 0),
	(276, 'steam:110000104a1abc7', 'gym_membership', 0),
	(277, 'steam:110000104a1abc7', 'WEAPON_HEAVYPISTOL', 0),
	(278, 'steam:110000104a1abc7', 'essence', 0),
	(279, 'steam:110000104a1abc7', 'WEAPON_MINIGUN', 0),
	(280, 'steam:110000104a1abc7', 'whisky', 0),
	(281, 'steam:110000104a1abc7', 'WEAPON_SPECIALCARBINE', 0),
	(282, 'steam:110000104a1abc7', 'WEAPON_BZGAS', 0),
	(283, 'steam:110000104a1abc7', 'rice', 0),
	(284, 'steam:110000104a1abc7', 'water', 0),
	(285, 'steam:110000104a1abc7', 'zippybag', 0),
	(286, 'steam:110000104a1abc7', 'worm', 0),
	(287, 'steam:110000104a1abc7', 'WEAPON_SNSPISTOL', 0),
	(288, 'steam:110000104a1abc7', 'WEAPON_DOUBLEACTION', 0),
	(289, 'steam:110000104a1abc7', 'hydrocodone', 0),
	(290, 'steam:110000104a1abc7', 'pork', 0),
	(291, 'steam:110000104a1abc7', 'wood', 0),
	(292, 'steam:110000104a1abc7', 'WEAPON_PUMPSHOTGUN', 0),
	(293, 'steam:110000104a1abc7', 'weed_pooch', 0),
	(294, 'steam:110000104a1abc7', 'cow_card', 0),
	(295, 'steam:110000104a1abc7', 'wine', 0),
	(296, 'steam:110000104a1abc7', 'carokit', 0),
	(297, 'steam:110000104a1abc7', 'weapongrip', 0),
	(298, 'steam:110000104a1abc7', 'beer', 0),
	(299, 'steam:110000104a1abc7', 'honey_a', 0),
	(300, 'steam:110000104a1abc7', 'glasses', 0),
	(301, 'steam:110000104a1abc7', 'vicodin', 0),
	(302, 'steam:110000104a1abc7', 'chicken_meat', 0),
	(303, 'steam:110000104a1abc7', 'turtlebait', 0),
	(304, 'steam:110000104a1abc7', 'WEAPON_FIREWORK', 0),
	(305, 'steam:110000104a1abc7', 'WEAPON_DAGGER', 0),
	(306, 'steam:110000104a1abc7', 'WEAPON_NIGHTSTICK', 0),
	(307, 'steam:110000104a1abc7', 'turtle', 0),
	(308, 'steam:110000104a1abc7', 'switchblade_blueprint', 0),
	(309, 'steam:110000104a1abc7', 'shell_c', 0),
	(310, 'steam:110000104a1abc7', 'WEAPON_HATCHET', 0),
	(311, 'steam:110000104a1abc7', 'WEAPON_COMBATPISTOL', 0),
	(312, 'steam:110000104a1abc7', 'skate', 0),
	(313, 'steam:110000104a1abc7', 'squidbait', 0),
	(314, 'steam:110000104a1abc7', 'sandwich', 0),
	(315, 'steam:110000104a1abc7', 'shovel', 0),
	(316, 'steam:110000104a1abc7', 'drill', 0),
	(317, 'steam:110000104a1abc7', 'shell_package', 0),
	(318, 'steam:110000104a1abc7', 'shell_engine', 0),
	(319, 'steam:110000104a1abc7', 'stone', 0),
	(320, 'steam:110000104a1abc7', 'fishingrod', 0),
	(321, 'steam:110000104a1abc7', 'shell_b', 0),
	(322, 'steam:110000104a1abc7', 'shell_a', 0),
	(323, 'steam:110000104a1abc7', 'crazypersoncard', 0),
	(324, 'steam:110000104a1abc7', 'fixkit', 0),
	(325, 'steam:110000104a1abc7', 'bread', 0),
	(326, 'steam:110000104a1abc7', 'cannabis', 0),
	(327, 'steam:110000104a1abc7', 'sharkfin', 0),
	(328, 'steam:110000104a1abc7', 'WEAPON_STUNGUN', 0),
	(329, 'steam:110000104a1abc7', 'WEAPON_PROXMINE', 0),
	(330, 'steam:110000104a1abc7', 'shark', 0),
	(331, 'steam:110000104a1abc7', 'sportlunch', 0),
	(332, 'steam:110000104a1abc7', 'sand', 0),
	(333, 'steam:110000104a1abc7', 'sand_b', 0),
	(334, 'steam:110000104a1abc7', 'sand_a', 0),
	(335, 'steam:110000104a1abc7', 'radio', 0),
	(336, 'steam:110000104a1abc7', 'fishbait', 0),
	(337, 'steam:110000104a1abc7', 'protein_shake', 0),
	(338, 'steam:110000104a1abc7', 'bobbypin', 0),
	(339, 'steam:110000104a1abc7', 'clothe', 0),
	(340, 'steam:110000104a1abc7', 'WEAPON_COMPACTLAUNCHER', 0),
	(341, 'steam:110000104a1abc7', 'prawnbait', 0),
	(342, 'steam:110000104a1abc7', 'milk_package', 0),
	(343, 'steam:110000104a1abc7', 'powerade', 0),
	(344, 'steam:110000104a1abc7', 'prawn', 0),
	(345, 'steam:110000104a1abc7', 'porkpackage', 0),
	(346, 'steam:110000104a1abc7', 'cupcake', 0),
	(347, 'steam:110000104a1abc7', 'wool', 0),
	(348, 'steam:110000104a1abc7', 'phone', 0),
	(349, 'steam:110000104a1abc7', 'milk', 0),
	(350, 'steam:110000104a1abc7', 'petrol', 0),
	(351, 'steam:110000104a1abc7', 'pro_wood', 0),
	(352, 'steam:110000104a1abc7', 'WEAPON_KNIFE', 0),
	(353, 'steam:110000104a1abc7', 'oxygen_mask', 0),
	(354, 'steam:110000104a1abc7', 'mushroom_d', 0),
	(355, 'steam:110000104a1abc7', 'WEAPON_ASSAULTSMG', 0),
	(356, 'steam:110000104a1abc7', 'WEAPON_PISTOL', 0),
	(357, 'steam:110000104a1abc7', 'WEAPON_GUSENBERG', 0),
	(358, 'steam:110000104a1abc7', 'mushroom', 0),
	(359, 'steam:110000104a1abc7', 'morphine', 0),
	(360, 'steam:110000104a1abc7', 'meth', 0),
	(361, 'steam:110000104a1abc7', 'cutted_wood', 0),
	(362, 'steam:110000104a1abc7', 'milk_engine', 0),
	(363, 'steam:110000104a1abc7', 'gazbottle', 0),
	(364, 'steam:110000104a1abc7', 'WEAPON_MUSKET', 0),
	(365, 'steam:110000104a1abc7', 'lighter', 0),
	(366, 'steam:110000104a1abc7', 'cigarett', 0),
	(367, 'steam:110000104a1abc7', 'meatshark', 0),
	(368, 'steam:110000104a1abc7', 'bandage', 0),
	(369, 'steam:110000104a1abc7', 'chicken', 0),
	(370, 'steam:110000104a1abc7', 'medikit', -1),
	(371, 'steam:110000104a1abc7', 'marijuana_cigarette', 0),
	(372, 'steam:110000104a1abc7', 'marijuana', 0),
	(373, 'steam:110000104a1abc7', 'WEAPON_APPISTOL', 0),
	(374, 'steam:110000104a1abc7', 'WEAPON_PETROLCAN', 0),
	(375, 'steam:110000104a1abc7', 'fuel_b', 0),
	(376, 'steam:110000104a1abc7', 'WEAPON_RAILGUN', 0),
	(377, 'steam:110000104a1abc7', 'cocacola', 0),
	(378, 'steam:110000104a1abc7', 'WEAPON_MACHETE', 0),
	(379, 'steam:110000104a1abc7', 'icetea', 0),
	(380, 'steam:110000104a1abc7', 'honey_b', 0),
	(381, 'steam:110000104a1abc7', 'coke_pooch', 0),
	(382, 'steam:110000104a1abc7', 'weaponflashlight', 0),
	(383, 'steam:110000104a1abc7', 'WEAPON_RPG', 0),
	(384, 'steam:110000104a1abc7', 'WEAPON_HOMINGLAUNCHER', 0),
	(385, 'steam:110000104a1abc7', 'WEAPON_AUTOSHOTGUN', 0),
	(386, 'steam:110000104a1abc7', 'WEAPON_WRENCH', 0),
	(387, 'steam:110000104a1abc7', 'hatchet_lj', 0),
	(388, 'steam:110000104a1abc7', 'fabric', 0),
	(389, 'steam:110000104a1abc7', 'WEAPON_GOLFCLUB', 0),
	(390, 'steam:110000104a1abc7', 'handcuffs', 0),
	(391, 'steam:110000104a1abc7', 'WEAPON_STINGER', 0),
	(392, 'steam:110000104a1abc7', 'WEAPON_MACHINEPISTOL', 0),
	(393, 'steam:110000104a1abc7', 'hamburger', 0),
	(394, 'steam:110000104a1abc7', 'hamajifish', 0),
	(395, 'steam:110000104a1abc7', 'WEAPON_CROWBAR', 0),
	(396, 'steam:110000104a1abc7', 'Packaged_plank', 0),
	(397, 'steam:110000104a1abc7', 'WEAPON_COMBATPDW', 0),
	(398, 'steam:110000104a1abc7', 'anti', 0),
	(399, 'steam:110000104a1abc7', 'vodka', 0),
	(400, 'steam:110000104a1abc7', 'boxmilk', 0),
	(401, 'steam:110000104a1abc7', 'WEAPON_PIPEBOMB', 0),
	(402, 'steam:110000104a1abc7', 'meth_pooch', 0),
	(403, 'steam:110000104a1abc7', 'leather', 0),
	(404, 'steam:110000104a1abc7', 'fuel_a', 0),
	(405, 'steam:110000104a1abc7', 'blowpipe', 0),
	(406, 'steam:110000104a1abc7', 'fish', 0),
	(407, 'steam:110000104a1abc7', 'doubleaction_blueprint', 0),
	(408, 'steam:110000104a1abc7', 'AED', 0),
	(409, 'steam:110000104a1abc7', 'WEAPON_MARKSMANPISTOL', 0),
	(410, 'steam:110000104a1abc7', 'WEAPON_SNIPERRIFLE', 0),
	(411, 'steam:110000104a1abc7', 'WEAPON_FLASHLIGHT', 0),
	(412, 'steam:110000104a1abc7', 'WEAPON_CARBINERIFLE', 0),
	(413, 'steam:110000104a1abc7', 'WEAPON_SNOWBALL', 0),
	(414, 'steam:110000104a1abc7', 'WEAPON_PISTOL50', 0),
	(415, 'steam:110000104a1abc7', 'WEAPON_MG', 0),
	(416, 'steam:110000104a1abc7', 'WEAPON_HANDCUFFS', 0),
	(417, 'steam:110000104a1abc7', 'WEAPON_STICKYBOMB', 0),
	(418, 'steam:110000104a1abc7', 'SteelScrap', 0),
	(419, 'steam:110000104a1abc7', 'fixtool', 0),
	(420, 'steam:110000104a1abc7', 'diamond', 0),
	(421, 'steam:110000104a1abc7', 'WEAPON_ASSAULTSHOTGUN', 0),
	(422, 'steam:110000104a1abc7', 'WEAPON_MARKSMANRIFLE', 0),
	(423, 'steam:110000104a1abc7', 'diesel', 0),
	(424, 'steam:110000104a1abc7', 'croquettes', 0),
	(425, 'steam:110000104a1abc7', 'craftkit', 0),
	(426, 'steam:110000104a1abc7', 'WEAPON_DBSHOTGUN', 0),
	(427, 'steam:110000104a1abc7', 'knife_chicken', 0),
	(428, 'steam:110000104a1abc7', 'packaged_plank', 0),
	(429, 'steam:110000104a1abc7', 'WEAPON_MICROSMG', 0),
	(430, 'steam:110000104a1abc7', 'gunpowder', 0),
	(431, 'steam:110000104a1abc7', 'WEAPON_MINISMG', 0),
	(432, 'steam:110000104a1abc7', 'WEAPON_BOTTLE', 0),
	(433, 'steam:110000104a1abc7', 'coffe', 0),
	(434, 'steam:110000104a1abc7', 'bcabbage', 0),
	(435, 'steam:110000104a1abc7', 'WEAPON_FLAREGUN', 0),
	(436, 'steam:110000104a1abc7', 'WEAPON_FLARE', 0),
	(437, 'steam:110000104a1abc7', 'WEAPON_SMG', 0),
	(438, 'steam:110000104a1abc7', 'acabbage', 0),
	(439, 'steam:110000104a1abc7', 'WEAPON_MOLOTOV', 0),
	(440, 'steam:110000104a1abc7', 'WEAPON_COMBATMG', 0),
	(441, 'steam:110000115e0024c', 'drill', 0),
	(442, 'steam:110000115e0024c', 'WEAPON_HOMINGLAUNCHER', 0),
	(443, 'steam:110000115e0024c', 'hydrocodone', 0),
	(444, 'steam:110000115e0024c', 'weed_pooch', 0),
	(445, 'steam:110000115e0024c', 'sand_a', 0),
	(446, 'steam:110000115e0024c', 'WEAPON_GUSENBERG', 0),
	(447, 'steam:110000115e0024c', 'cupcake', 0),
	(448, 'steam:110000115e0024c', 'WEAPON_RPG', 0),
	(449, 'steam:110000115e0024c', 'bong', 0),
	(450, 'steam:110000115e0024c', 'WEAPON_SMG', 0),
	(451, 'steam:110000115e0024c', 'WEAPON_SMOKEGRENADE', 0),
	(452, 'steam:110000115e0024c', 'wrench_blueprint', 0),
	(453, 'steam:110000115e0024c', 'WEAPON_WRENCH', 0),
	(454, 'steam:110000115e0024c', 'WEAPON_MINISMG', 0),
	(455, 'steam:110000115e0024c', 'chicken_meat', 0),
	(456, 'steam:110000115e0024c', 'WEAPON_KNUCKLE', 0),
	(457, 'steam:110000115e0024c', 'hamajifish', 0),
	(458, 'steam:110000115e0024c', 'clip', 0),
	(459, 'steam:110000115e0024c', 'gym_membership', 0),
	(460, 'steam:110000115e0024c', 'weaponflashlight', 0),
	(461, 'steam:110000115e0024c', 'bread', 0),
	(462, 'steam:110000115e0024c', 'WEAPON_POOLCUE', 0),
	(463, 'steam:110000115e0024c', 'WEAPON_STUNGUN', 0),
	(464, 'steam:110000115e0024c', 'lockpick', 0),
	(465, 'steam:110000115e0024c', 'fuel_b', 0),
	(466, 'steam:110000115e0024c', 'WEAPON_APPISTOL', 0),
	(467, 'steam:110000115e0024c', 'rice_pro', 0),
	(468, 'steam:110000115e0024c', 'alive_chicken', 0),
	(469, 'steam:110000115e0024c', 'clothe', 0),
	(470, 'steam:110000115e0024c', 'cow_card', 0),
	(471, 'steam:110000115e0024c', 'protein_shake', 0),
	(472, 'steam:110000115e0024c', 'sand_b', 0),
	(473, 'steam:110000115e0024c', 'WEAPON_MINIGUN', 0),
	(474, 'steam:110000115e0024c', 'knife_chicken', 0),
	(475, 'steam:110000115e0024c', 'WEAPON_REVOLVER', 0),
	(476, 'steam:110000115e0024c', 'oxygen_mask', 0),
	(477, 'steam:110000115e0024c', 'WEAPON_CROWBAR', 0),
	(478, 'steam:110000115e0024c', 'WEAPON_HEAVYSNIPER', 0),
	(479, 'steam:110000115e0024c', 'WEAPON_MARKSMANPISTOL', 0),
	(480, 'steam:110000115e0024c', 'leather', 0),
	(481, 'steam:110000115e0024c', 'WEAPON_PIPEBOMB', 0),
	(482, 'steam:110000115e0024c', 'bobbypin', 0),
	(483, 'steam:110000115e0024c', 'zippybag', 0),
	(484, 'steam:110000115e0024c', 'worm', 0),
	(485, 'steam:110000115e0024c', 'wool', 0),
	(486, 'steam:110000115e0024c', 'cannabis', 0),
	(487, 'steam:110000115e0024c', 'copper', 0),
	(488, 'steam:110000115e0024c', 'WEAPON_STINGER', 0),
	(489, 'steam:110000115e0024c', 'WEAPON_GRENADE', 0),
	(490, 'steam:110000115e0024c', 'WEAPON_MARKSMANRIFLE', 0),
	(491, 'steam:110000115e0024c', 'WEAPON_DOUBLEACTION', 0),
	(492, 'steam:110000115e0024c', 'shell_a', 0),
	(493, 'steam:110000115e0024c', 'weaponskin', 0),
	(494, 'steam:110000115e0024c', 'WEAPON_MG', 0),
	(495, 'steam:110000115e0024c', 'WEAPON_HEAVYPISTOL', 0),
	(496, 'steam:110000115e0024c', 'WEAPON_MUSKET', 0),
	(497, 'steam:110000115e0024c', 'weapongrip', 0),
	(498, 'steam:110000115e0024c', 'water', 0),
	(499, 'steam:110000115e0024c', 'WEAPON_FIREEXTINGUISHER', 0),
	(500, 'steam:110000115e0024c', 'WEAPON_PISTOL', 0),
	(501, 'steam:110000115e0024c', 'handcuffs', 0),
	(502, 'steam:110000115e0024c', 'WEAPON_DIGISCANNER', 0),
	(503, 'steam:110000115e0024c', 'fishingrod', 0),
	(504, 'steam:110000115e0024c', 'rope', 0),
	(505, 'steam:110000115e0024c', 'marijuana_cigarette', 0),
	(506, 'steam:110000115e0024c', 'vicodin', 0),
	(507, 'steam:110000115e0024c', 'turtlebait', 0),
	(508, 'steam:110000115e0024c', 'WEAPON_BZGAS', 0),
	(509, 'steam:110000115e0024c', 'diamond', 0),
	(510, 'steam:110000115e0024c', 'WEAPON_STICKYBOMB', 0),
	(511, 'steam:110000115e0024c', 'tequila', 0),
	(512, 'steam:110000115e0024c', 'crazypersoncard', 0),
	(513, 'steam:110000115e0024c', 'switchblade_blueprint', 0),
	(514, 'steam:110000115e0024c', 'mushroom_d', 0),
	(515, 'steam:110000115e0024c', 'petrol_raffin', 0),
	(516, 'steam:110000115e0024c', 'hamburger', 0),
	(517, 'steam:110000115e0024c', 'stone', 0),
	(518, 'steam:110000115e0024c', 'WEAPON_BOTTLE', 0),
	(519, 'steam:110000115e0024c', 'squidbait', 0),
	(520, 'steam:110000115e0024c', 'squid', 0),
	(521, 'steam:110000115e0024c', 'sportlunch', 0),
	(522, 'steam:110000115e0024c', 'snspistol_blueprint', 0),
	(523, 'steam:110000115e0024c', 'WEAPON_HANDCUFFS', 0),
	(524, 'steam:110000115e0024c', 'WEAPON_ASSAULTRIFLE', 0),
	(525, 'steam:110000115e0024c', 'cigarett', 0),
	(526, 'steam:110000115e0024c', 'sickle', 0),
	(527, 'steam:110000115e0024c', 'WEAPON_SNIPERRIFLE', 0),
	(528, 'steam:110000115e0024c', 'skate', 0),
	(529, 'steam:110000115e0024c', 'porkpackage', 0),
	(530, 'steam:110000115e0024c', 'shovel', 0),
	(531, 'steam:110000115e0024c', 'WEAPON_SNSPISTOL', 0),
	(532, 'steam:110000115e0024c', 'bottle', 0),
	(533, 'steam:110000115e0024c', 'shell_package', 0),
	(534, 'steam:110000115e0024c', 'boxmilk', 0),
	(535, 'steam:110000115e0024c', 'doubleaction_blueprint', 0),
	(536, 'steam:110000115e0024c', 'anti', 0),
	(537, 'steam:110000115e0024c', 'bandage', 0),
	(538, 'steam:110000115e0024c', 'WEAPON_COMPACTRIFLE', 0),
	(539, 'steam:110000115e0024c', 'shell_engine', 0),
	(540, 'steam:110000115e0024c', 'WEAPON_PUMPSHOTGUN', 0),
	(541, 'steam:110000115e0024c', 'WEAPON_COMBATPISTOL', 0),
	(542, 'steam:110000115e0024c', 'hatchet_lj', 0),
	(543, 'steam:110000115e0024c', 'shell_c', 0),
	(544, 'steam:110000115e0024c', 'shell_b', 0),
	(545, 'steam:110000115e0024c', 'blowpipe', 0),
	(546, 'steam:110000115e0024c', 'cocacola', 0),
	(547, 'steam:110000115e0024c', 'WEAPON_BULLPUPSHOTGUN', 0),
	(548, 'steam:110000115e0024c', 'WEAPON_BAT', 0),
	(549, 'steam:110000115e0024c', 'WEAPON_ADVANCEDRIFLE', 0),
	(550, 'steam:110000115e0024c', 'WEAPON_ASSAULTSHOTGUN', 0),
	(551, 'steam:110000115e0024c', 'britadeira', 0),
	(552, 'steam:110000115e0024c', 'whisky', 0),
	(553, 'steam:110000115e0024c', 'SteelScrap', 0),
	(554, 'steam:110000115e0024c', 'bcabbage', 0),
	(555, 'steam:110000115e0024c', 'croquettes', 0),
	(556, 'steam:110000115e0024c', 'Packaged_plank', 0),
	(557, 'steam:110000115e0024c', 'WEAPON_SAWNOFFSHOTGUN', 0),
	(558, 'steam:110000115e0024c', 'WEAPON_ASSAULTSMG', 0),
	(559, 'steam:110000115e0024c', 'fabric', 0),
	(560, 'steam:110000115e0024c', 'acabbage', 0),
	(561, 'steam:110000115e0024c', 'WEAPON_HATCHET', 0),
	(562, 'steam:110000115e0024c', 'WEAPON_BALL', 0),
	(563, 'steam:110000115e0024c', 'sharkfin', 0),
	(564, 'steam:110000115e0024c', 'WEAPON_RAILGUN', 0),
	(565, 'steam:110000115e0024c', 'rice', 0),
	(566, 'steam:110000115e0024c', 'shark', 0),
	(567, 'steam:110000115e0024c', 'jumelles', 0),
	(568, 'steam:110000115e0024c', 'WEAPON_SPECIALCARBINE', 0),
	(569, 'steam:110000115e0024c', 'WEAPON_BULLPUPRIFLE', 0),
	(570, 'steam:110000115e0024c', 'sandwich', 0),
	(571, 'steam:110000115e0024c', 'iron', 0),
	(572, 'steam:110000115e0024c', 'WEAPON_MACHINEPISTOL', 0),
	(573, 'steam:110000115e0024c', 'radio', 0),
	(574, 'steam:110000115e0024c', 'meth_pooch', 0),
	(575, 'steam:110000115e0024c', 'WEAPON_FLAREGUN', 0),
	(576, 'steam:110000115e0024c', 'WEAPON_VINTAGEPISTOL', 0),
	(577, 'steam:110000115e0024c', 'WEAPON_PROXMINE', 0),
	(578, 'steam:110000115e0024c', 'blowtorch', 0),
	(579, 'steam:110000115e0024c', 'WEAPON_PISTOL50', 0),
	(580, 'steam:110000115e0024c', 'prawnbait', 0),
	(581, 'steam:110000115e0024c', 'chicken_package', 0),
	(582, 'steam:110000115e0024c', 'WEAPON_HAMMER', 0),
	(583, 'steam:110000115e0024c', 'prawn', 0),
	(584, 'steam:110000115e0024c', 'WEAPON_MOLOTOV', 0),
	(585, 'steam:110000115e0024c', 'chocolate', 0),
	(586, 'steam:110000115e0024c', 'pork', 0),
	(587, 'steam:110000115e0024c', 'chest_a', 0),
	(588, 'steam:110000115e0024c', 'powerade', 0),
	(589, 'steam:110000115e0024c', 'phone', 0),
	(590, 'steam:110000115e0024c', 'fixtool', 0),
	(591, 'steam:110000115e0024c', 'packaged_plank', 0),
	(592, 'steam:110000115e0024c', 'icetea', 0),
	(593, 'steam:110000115e0024c', 'lighter', 0),
	(594, 'steam:110000115e0024c', 'wood', 0),
	(595, 'steam:110000115e0024c', 'mushroom_p', 0),
	(596, 'steam:110000115e0024c', 'WEAPON_AUTOSHOTGUN', 0),
	(597, 'steam:110000115e0024c', 'mushroom', 0),
	(598, 'steam:110000115e0024c', 'morphine', 0),
	(599, 'steam:110000115e0024c', 'chicken', 0),
	(600, 'steam:110000115e0024c', 'chickenwithoutfeather', 0),
	(601, 'steam:110000115e0024c', 'milk_package', 0),
	(602, 'steam:110000115e0024c', 'pro_wood', 0),
	(603, 'steam:110000115e0024c', 'WEAPON_SNOWBALL', 0),
	(604, 'steam:110000115e0024c', 'WEAPON_DBSHOTGUN', 0),
	(605, 'steam:110000115e0024c', 'meth', 0),
	(606, 'steam:110000115e0024c', 'medikit', 0),
	(607, 'steam:110000115e0024c', 'marijuana', 0),
	(608, 'steam:110000115e0024c', 'meat', 0),
	(609, 'steam:110000115e0024c', 'WEAPON_GARBAGEBAG', 0),
	(610, 'steam:110000115e0024c', 'vodka', 0),
	(611, 'steam:110000115e0024c', 'coke_pooch', 0),
	(612, 'steam:110000115e0024c', 'meatshark', 0),
	(613, 'steam:110000115e0024c', 'shell', 0),
	(614, 'steam:110000115e0024c', 'coke', 0),
	(615, 'steam:110000115e0024c', 'WEAPON_SWITCHBLADE', 0),
	(616, 'steam:110000115e0024c', 'WEAPON_COMBATMG', 0),
	(617, 'steam:110000115e0024c', 'honey_a', 0),
	(618, 'steam:110000115e0024c', 'diesel', 0),
	(619, 'steam:110000115e0024c', 'washed_stone', 0),
	(620, 'steam:110000115e0024c', 'gunpowder', 0),
	(621, 'steam:110000115e0024c', 'gold', 0),
	(622, 'steam:110000115e0024c', 'glasses', 0),
	(623, 'steam:110000115e0024c', 'gazbottle', 0),
	(624, 'steam:110000115e0024c', 'fuel_a', 0),
	(625, 'steam:110000115e0024c', 'WEAPON_BATTLEAXE', 0),
	(626, 'steam:110000115e0024c', 'petrol', 0),
	(627, 'steam:110000115e0024c', 'WEAPON_PETROLCAN', 0),
	(628, 'steam:110000115e0024c', 'WEAPON_CARBINERIFLE', 0),
	(629, 'steam:110000115e0024c', 'fixkit', 0),
	(630, 'steam:110000115e0024c', 'WEAPON_MACHETE', 0),
	(631, 'steam:110000115e0024c', 'fishbait', 0),
	(632, 'steam:110000115e0024c', 'WEAPON_FLASHLIGHT', 0),
	(633, 'steam:110000115e0024c', 'carokit', 0),
	(634, 'steam:110000115e0024c', 'WEAPON_DAGGER', 0),
	(635, 'steam:110000115e0024c', 'fish', 0),
	(636, 'steam:110000115e0024c', 'WEAPON_NIGHTSTICK', 0),
	(637, 'steam:110000115e0024c', 'WEAPON_HEAVYSHOTGUN', 0),
	(638, 'steam:110000115e0024c', 'milk_engine', 0),
	(639, 'steam:110000115e0024c', 'essence', 0),
	(640, 'steam:110000115e0024c', 'cutted_wood', 0),
	(641, 'steam:110000115e0024c', 'packaged_chicken', 0),
	(642, 'steam:110000115e0024c', 'milk', 0),
	(643, 'steam:110000115e0024c', 'WEAPON_KNIFE', 0),
	(644, 'steam:110000115e0024c', 'WEAPON_FLARE', 0),
	(645, 'steam:110000115e0024c', 'AED', 0),
	(646, 'steam:110000115e0024c', 'craftkit', 0),
	(647, 'steam:110000115e0024c', 'WEAPON_COMPACTLAUNCHER', 0),
	(648, 'steam:110000115e0024c', 'honey_b', 0),
	(649, 'steam:110000115e0024c', 'slaughtered_chicken', 0),
	(650, 'steam:110000115e0024c', 'carotool', 0),
	(651, 'steam:110000115e0024c', 'sand', 0),
	(652, 'steam:110000115e0024c', 'beer', 0),
	(653, 'steam:110000115e0024c', 'WEAPON_COMBATPDW', 0),
	(654, 'steam:110000115e0024c', 'coffe', 0),
	(655, 'steam:110000115e0024c', 'WEAPON_GRENADELAUNCHER', 0),
	(656, 'steam:110000115e0024c', 'turtle', 0),
	(657, 'steam:110000115e0024c', 'WEAPON_GOLFCLUB', 0),
	(658, 'steam:110000115e0024c', 'WEAPON_FIREWORK', 0),
	(659, 'steam:110000115e0024c', 'WEAPON_MICROSMG', 0),
	(660, 'steam:110000115e0024c', 'wine', 0),
	(661, 'steam:11000013dce3ca4', 'WEAPON_STINGER', 0),
	(662, 'steam:11000013dce3ca4', 'diamond', 0),
	(663, 'steam:11000013dce3ca4', 'WEAPON_HAMMER', 0),
	(664, 'steam:11000013dce3ca4', 'WEAPON_ADVANCEDRIFLE', 0),
	(665, 'steam:11000013dce3ca4', 'croquettes', 0),
	(666, 'steam:11000013dce3ca4', 'copper', 0),
	(667, 'steam:11000013dce3ca4', 'whisky', 0),
	(668, 'steam:11000013dce3ca4', 'cupcake', 0),
	(669, 'steam:11000013dce3ca4', 'WEAPON_FIREWORK', 0),
	(670, 'steam:11000013dce3ca4', 'WEAPON_CARBINERIFLE', 0),
	(671, 'steam:11000013dce3ca4', 'alive_chicken', 0),
	(672, 'steam:11000013dce3ca4', 'mushroom_d', 0),
	(673, 'steam:11000013dce3ca4', 'WEAPON_SWITCHBLADE', 0),
	(674, 'steam:11000013dce3ca4', 'WEAPON_COMBATPDW', 0),
	(675, 'steam:11000013dce3ca4', 'milk_engine', 0),
	(676, 'steam:11000013dce3ca4', 'shell', 0),
	(677, 'steam:11000013dce3ca4', 'marijuana_cigarette', 0),
	(678, 'steam:11000013dce3ca4', 'chicken_meat', 0),
	(679, 'steam:11000013dce3ca4', 'bottle', 0),
	(680, 'steam:11000013dce3ca4', 'honey_b', 0),
	(681, 'steam:11000013dce3ca4', 'knife_chicken', 0),
	(682, 'steam:11000013dce3ca4', 'WEAPON_HOMINGLAUNCHER', 0),
	(683, 'steam:11000013dce3ca4', 'coke', 0),
	(684, 'steam:11000013dce3ca4', 'AED', 0),
	(685, 'steam:11000013dce3ca4', 'WEAPON_BZGAS', 0),
	(686, 'steam:11000013dce3ca4', 'blowpipe', 0),
	(687, 'steam:11000013dce3ca4', 'WEAPON_GUSENBERG', 0),
	(688, 'steam:11000013dce3ca4', 'WEAPON_VINTAGEPISTOL', 0),
	(689, 'steam:11000013dce3ca4', 'WEAPON_GARBAGEBAG', 0),
	(690, 'steam:11000013dce3ca4', 'WEAPON_MG', 0),
	(691, 'steam:11000013dce3ca4', 'WEAPON_FIREEXTINGUISHER', 0),
	(692, 'steam:11000013dce3ca4', 'clip', 0),
	(693, 'steam:11000013dce3ca4', 'SteelScrap', 0),
	(694, 'steam:11000013dce3ca4', 'WEAPON_STUNGUN', 0),
	(695, 'steam:11000013dce3ca4', 'WEAPON_NIGHTSTICK', 0),
	(696, 'steam:11000013dce3ca4', 'porkpackage', 0),
	(697, 'steam:11000013dce3ca4', 'WEAPON_REVOLVER', 0),
	(698, 'steam:11000013dce3ca4', 'Packaged_plank', 0),
	(699, 'steam:11000013dce3ca4', 'craftkit', 0),
	(700, 'steam:11000013dce3ca4', 'WEAPON_BALL', 0),
	(701, 'steam:11000013dce3ca4', 'WEAPON_HEAVYPISTOL', 0),
	(702, 'steam:11000013dce3ca4', 'rice', 0),
	(703, 'steam:11000013dce3ca4', 'cannabis', 0),
	(704, 'steam:11000013dce3ca4', 'WEAPON_PETROLCAN', 0),
	(705, 'steam:11000013dce3ca4', 'WEAPON_BATTLEAXE', 0),
	(706, 'steam:11000013dce3ca4', 'blowtorch', 0),
	(707, 'steam:11000013dce3ca4', 'WEAPON_MINISMG', 0),
	(708, 'steam:11000013dce3ca4', 'shovel', 0),
	(709, 'steam:11000013dce3ca4', 'WEAPON_HEAVYSNIPER', 0),
	(710, 'steam:11000013dce3ca4', 'WEAPON_MICROSMG', 0),
	(711, 'steam:11000013dce3ca4', 'vodka', 0),
	(712, 'steam:11000013dce3ca4', 'phone', 0),
	(713, 'steam:11000013dce3ca4', 'WEAPON_PISTOL50', 0),
	(714, 'steam:11000013dce3ca4', 'WEAPON_GRENADE', 0),
	(715, 'steam:11000013dce3ca4', 'zippybag', 0),
	(716, 'steam:11000013dce3ca4', 'hydrocodone', 0),
	(717, 'steam:11000013dce3ca4', 'meatshark', 0),
	(718, 'steam:11000013dce3ca4', 'meth_pooch', 0),
	(719, 'steam:11000013dce3ca4', 'chest_a', 0),
	(720, 'steam:11000013dce3ca4', 'WEAPON_MOLOTOV', 0),
	(721, 'steam:11000013dce3ca4', 'wrench_blueprint', 0),
	(722, 'steam:11000013dce3ca4', 'WEAPON_SAWNOFFSHOTGUN', 0),
	(723, 'steam:11000013dce3ca4', 'boxmilk', 0),
	(724, 'steam:11000013dce3ca4', 'sandwich', 0),
	(725, 'steam:11000013dce3ca4', 'medikit', -4),
	(726, 'steam:11000013dce3ca4', 'worm', 0),
	(727, 'steam:11000013dce3ca4', 'gym_membership', 0),
	(728, 'steam:11000013dce3ca4', 'sand_a', 0),
	(729, 'steam:11000013dce3ca4', 'iron', 0),
	(730, 'steam:11000013dce3ca4', 'pork', 0),
	(731, 'steam:11000013dce3ca4', 'WEAPON_ASSAULTRIFLE', 0),
	(732, 'steam:11000013dce3ca4', 'bcabbage', 0),
	(733, 'steam:11000013dce3ca4', 'wine', 0),
	(734, 'steam:11000013dce3ca4', 'WEAPON_POOLCUE', 0),
	(735, 'steam:11000013dce3ca4', 'weed_pooch', 0),
	(736, 'steam:11000013dce3ca4', 'WEAPON_MARKSMANRIFLE', 0),
	(737, 'steam:11000013dce3ca4', 'chicken_package', 0),
	(738, 'steam:11000013dce3ca4', 'sand_b', 0),
	(739, 'steam:11000013dce3ca4', 'morphine', 0),
	(740, 'steam:11000013dce3ca4', 'leather', 0),
	(741, 'steam:11000013dce3ca4', 'weapongrip', 0),
	(742, 'steam:11000013dce3ca4', 'WEAPON_PROXMINE', 0),
	(743, 'steam:11000013dce3ca4', 'rope', 0),
	(744, 'steam:11000013dce3ca4', 'WEAPON_DIGISCANNER', 0),
	(745, 'steam:11000013dce3ca4', 'diesel', 0),
	(746, 'steam:11000013dce3ca4', 'bandage', -1),
	(747, 'steam:11000013dce3ca4', 'WEAPON_GOLFCLUB', 0),
	(748, 'steam:11000013dce3ca4', 'radio', 0),
	(749, 'steam:11000013dce3ca4', 'petrol_raffin', 0),
	(750, 'steam:11000013dce3ca4', 'milk_package', 0),
	(751, 'steam:11000013dce3ca4', 'vicodin', 0),
	(752, 'steam:11000013dce3ca4', 'turtlebait', 0),
	(753, 'steam:11000013dce3ca4', 'WEAPON_SMG', 0),
	(754, 'steam:11000013dce3ca4', 'bread', 0),
	(755, 'steam:11000013dce3ca4', 'washed_stone', 0),
	(756, 'steam:11000013dce3ca4', 'shell_package', 0),
	(757, 'steam:11000013dce3ca4', 'WEAPON_HANDCUFFS', 0),
	(758, 'steam:11000013dce3ca4', 'icetea', 0),
	(759, 'steam:11000013dce3ca4', 'WEAPON_DBSHOTGUN', 0),
	(760, 'steam:11000013dce3ca4', 'switchblade_blueprint', 0),
	(761, 'steam:11000013dce3ca4', 'hamburger', 0),
	(762, 'steam:11000013dce3ca4', 'clothe', 0),
	(763, 'steam:11000013dce3ca4', 'squid', 0),
	(764, 'steam:11000013dce3ca4', 'WEAPON_MARKSMANPISTOL', 0),
	(765, 'steam:11000013dce3ca4', 'sportlunch', 0),
	(766, 'steam:11000013dce3ca4', 'bobbypin', 0),
	(767, 'steam:11000013dce3ca4', 'snspistol_blueprint', 0),
	(768, 'steam:11000013dce3ca4', 'beer', 0),
	(769, 'steam:11000013dce3ca4', 'gunpowder', 0),
	(770, 'steam:11000013dce3ca4', 'WEAPON_SMOKEGRENADE', 0),
	(771, 'steam:11000013dce3ca4', 'slaughtered_chicken', 0),
	(772, 'steam:11000013dce3ca4', 'prawnbait', 0),
	(773, 'steam:11000013dce3ca4', 'skate', 0),
	(774, 'steam:11000013dce3ca4', 'WEAPON_WRENCH', 0),
	(775, 'steam:11000013dce3ca4', 'shell_engine', 0),
	(776, 'steam:11000013dce3ca4', 'sickle', 0),
	(777, 'steam:11000013dce3ca4', 'WEAPON_BOTTLE', 0),
	(778, 'steam:11000013dce3ca4', 'packaged_plank', 0),
	(779, 'steam:11000013dce3ca4', 'shell_b', 0),
	(780, 'steam:11000013dce3ca4', 'WEAPON_HEAVYSHOTGUN', 0),
	(781, 'steam:11000013dce3ca4', 'WEAPON_CROWBAR', 0),
	(782, 'steam:11000013dce3ca4', 'meth', 0),
	(783, 'steam:11000013dce3ca4', 'WEAPON_MACHETE', 0),
	(784, 'steam:11000013dce3ca4', 'shell_a', 0),
	(785, 'steam:11000013dce3ca4', 'shark', 0),
	(786, 'steam:11000013dce3ca4', 'WEAPON_KNUCKLE', 0),
	(787, 'steam:11000013dce3ca4', 'WEAPON_COMPACTLAUNCHER', 0),
	(788, 'steam:11000013dce3ca4', 'WEAPON_COMBATPISTOL', 0),
	(789, 'steam:11000013dce3ca4', 'bong', 0),
	(790, 'steam:11000013dce3ca4', 'fixtool', 0),
	(791, 'steam:11000013dce3ca4', 'sand', 0),
	(792, 'steam:11000013dce3ca4', 'drill', 0),
	(793, 'steam:11000013dce3ca4', 'WEAPON_DOUBLEACTION', 0),
	(794, 'steam:11000013dce3ca4', 'coffe', 0),
	(795, 'steam:11000013dce3ca4', 'WEAPON_KNIFE', 0),
	(796, 'steam:11000013dce3ca4', 'turtle', 0),
	(797, 'steam:11000013dce3ca4', 'protein_shake', 0),
	(798, 'steam:11000013dce3ca4', 'coke_pooch', 0),
	(799, 'steam:11000013dce3ca4', 'pro_wood', 0),
	(800, 'steam:11000013dce3ca4', 'WEAPON_AUTOSHOTGUN', 0),
	(801, 'steam:11000013dce3ca4', 'water', 0),
	(802, 'steam:11000013dce3ca4', 'prawn', 0),
	(803, 'steam:11000013dce3ca4', 'powerade', 0),
	(804, 'steam:11000013dce3ca4', 'crazypersoncard', 0),
	(805, 'steam:11000013dce3ca4', 'WEAPON_PISTOL', 0),
	(806, 'steam:11000013dce3ca4', 'fuel_b', 0),
	(807, 'steam:11000013dce3ca4', 'shell_c', 0),
	(808, 'steam:11000013dce3ca4', 'chicken', 0),
	(809, 'steam:11000013dce3ca4', 'packaged_chicken', 0),
	(810, 'steam:11000013dce3ca4', 'WEAPON_APPISTOL', 0),
	(811, 'steam:11000013dce3ca4', 'oxygen_mask', 0),
	(812, 'steam:11000013dce3ca4', 'hatchet_lj', 0),
	(813, 'steam:11000013dce3ca4', 'honey_a', 0),
	(814, 'steam:11000013dce3ca4', 'WEAPON_FLASHLIGHT', 0),
	(815, 'steam:11000013dce3ca4', 'WEAPON_SNIPERRIFLE', 0),
	(816, 'steam:11000013dce3ca4', 'mushroom_p', 0),
	(817, 'steam:11000013dce3ca4', 'mushroom', 0),
	(818, 'steam:11000013dce3ca4', 'milk', 0),
	(819, 'steam:11000013dce3ca4', 'gazbottle', 0),
	(820, 'steam:11000013dce3ca4', 'WEAPON_DAGGER', 0),
	(821, 'steam:11000013dce3ca4', 'WEAPON_SPECIALCARBINE', 0),
	(822, 'steam:11000013dce3ca4', 'WEAPON_FLAREGUN', 0),
	(823, 'steam:11000013dce3ca4', 'marijuana', 0),
	(824, 'steam:11000013dce3ca4', 'lockpick', 0),
	(825, 'steam:11000013dce3ca4', 'chocolate', 0),
	(826, 'steam:11000013dce3ca4', 'WEAPON_RAILGUN', 0),
	(827, 'steam:11000013dce3ca4', 'glasses', 0),
	(828, 'steam:11000013dce3ca4', 'lighter', 0),
	(829, 'steam:11000013dce3ca4', 'hamajifish', 0),
	(830, 'steam:11000013dce3ca4', 'weaponskin', 0),
	(831, 'steam:11000013dce3ca4', 'jumelles', 0),
	(832, 'steam:11000013dce3ca4', 'WEAPON_FLARE', 0),
	(833, 'steam:11000013dce3ca4', 'fishbait', 0),
	(834, 'steam:11000013dce3ca4', 'gold', 0),
	(835, 'steam:11000013dce3ca4', 'tequila', 0),
	(836, 'steam:11000013dce3ca4', 'WEAPON_COMPACTRIFLE', 0),
	(837, 'steam:11000013dce3ca4', 'WEAPON_MACHINEPISTOL', 0),
	(838, 'steam:11000013dce3ca4', 'wood', 0),
	(839, 'steam:11000013dce3ca4', 'anti', 0),
	(840, 'steam:11000013dce3ca4', 'WEAPON_BAT', 0),
	(841, 'steam:11000013dce3ca4', 'wool', 0),
	(842, 'steam:11000013dce3ca4', 'handcuffs', 0),
	(843, 'steam:11000013dce3ca4', 'stone', 0),
	(844, 'steam:11000013dce3ca4', 'WEAPON_ASSAULTSHOTGUN', 0),
	(845, 'steam:11000013dce3ca4', 'fixkit', 0),
	(846, 'steam:11000013dce3ca4', 'cow_card', 0),
	(847, 'steam:11000013dce3ca4', 'cigarett', 0),
	(848, 'steam:11000013dce3ca4', 'meat', 0),
	(849, 'steam:11000013dce3ca4', 'rice_pro', 0),
	(850, 'steam:11000013dce3ca4', 'fishingrod', 0),
	(851, 'steam:11000013dce3ca4', 'fish', 0),
	(852, 'steam:11000013dce3ca4', 'fabric', 0),
	(853, 'steam:11000013dce3ca4', 'WEAPON_HATCHET', 0),
	(854, 'steam:11000013dce3ca4', 'britadeira', 0),
	(855, 'steam:11000013dce3ca4', 'fuel_a', 0),
	(856, 'steam:11000013dce3ca4', 'cocacola', 0),
	(857, 'steam:11000013dce3ca4', 'WEAPON_SNOWBALL', 0),
	(858, 'steam:11000013dce3ca4', 'doubleaction_blueprint', 0),
	(859, 'steam:11000013dce3ca4', 'WEAPON_MINIGUN', 0),
	(860, 'steam:11000013dce3ca4', 'petrol', 0),
	(861, 'steam:11000013dce3ca4', 'cutted_wood', 0),
	(862, 'steam:11000013dce3ca4', 'weaponflashlight', 0),
	(863, 'steam:11000013dce3ca4', 'WEAPON_BULLPUPSHOTGUN', 0),
	(864, 'steam:11000013dce3ca4', 'WEAPON_GRENADELAUNCHER', 0),
	(865, 'steam:11000013dce3ca4', 'WEAPON_BULLPUPRIFLE', 0),
	(866, 'steam:11000013dce3ca4', 'WEAPON_ASSAULTSMG', 0),
	(867, 'steam:11000013dce3ca4', 'squidbait', 0),
	(868, 'steam:11000013dce3ca4', 'chickenwithoutfeather', 0),
	(869, 'steam:11000013dce3ca4', 'WEAPON_COMBATMG', 0),
	(870, 'steam:11000013dce3ca4', 'WEAPON_PUMPSHOTGUN', 0),
	(871, 'steam:11000013dce3ca4', 'carotool', 0),
	(872, 'steam:11000013dce3ca4', 'WEAPON_RPG', 0),
	(873, 'steam:11000013dce3ca4', 'sharkfin', 0),
	(874, 'steam:11000013dce3ca4', 'acabbage', 0),
	(875, 'steam:11000013dce3ca4', 'WEAPON_STICKYBOMB', 0),
	(876, 'steam:11000013dce3ca4', 'WEAPON_SNSPISTOL', 0),
	(877, 'steam:11000013dce3ca4', 'carokit', 0),
	(878, 'steam:11000013dce3ca4', 'essence', 0),
	(879, 'steam:11000013dce3ca4', 'WEAPON_MUSKET', 0),
	(880, 'steam:11000013dce3ca4', 'WEAPON_PIPEBOMB', 0),
	(881, 'steam:11000013ac37e2b', 'lighter', 0),
	(882, 'steam:11000013ac37e2b', 'worm', 0),
	(883, 'steam:11000013ac37e2b', 'meth_pooch', 0),
	(884, 'steam:11000013ac37e2b', 'WEAPON_SNSPISTOL', 0),
	(885, 'steam:11000013ac37e2b', 'weapongrip', 0),
	(886, 'steam:11000013ac37e2b', 'milk_package', 0),
	(887, 'steam:11000013ac37e2b', 'WEAPON_REVOLVER', 0),
	(888, 'steam:11000013ac37e2b', 'diesel', 0),
	(889, 'steam:11000013ac37e2b', 'bottle', 0),
	(890, 'steam:11000013ac37e2b', 'acabbage', 0),
	(891, 'steam:11000013ac37e2b', 'gunpowder', 0),
	(892, 'steam:11000013ac37e2b', 'WEAPON_MG', 0),
	(893, 'steam:11000013ac37e2b', 'bcabbage', 0),
	(894, 'steam:11000013ac37e2b', 'WEAPON_PETROLCAN', 0),
	(895, 'steam:11000013ac37e2b', 'carotool', 0),
	(896, 'steam:11000013ac37e2b', 'fishbait', 0),
	(897, 'steam:11000013ac37e2b', 'britadeira', 0),
	(898, 'steam:11000013ac37e2b', 'sand', 0),
	(899, 'steam:11000013ac37e2b', 'water', 0),
	(900, 'steam:11000013ac37e2b', 'chicken', 0),
	(901, 'steam:11000013ac37e2b', 'WEAPON_SAWNOFFSHOTGUN', 0),
	(902, 'steam:11000013ac37e2b', 'pro_wood', 0),
	(903, 'steam:11000013ac37e2b', 'wool', 0),
	(904, 'steam:11000013ac37e2b', 'WEAPON_GRENADELAUNCHER', 0),
	(905, 'steam:11000013ac37e2b', 'chocolate', 0),
	(906, 'steam:11000013ac37e2b', 'petrol_raffin', 0),
	(907, 'steam:11000013ac37e2b', 'rice_pro', 0),
	(908, 'steam:11000013ac37e2b', 'protein_shake', 0),
	(909, 'steam:11000013ac37e2b', 'WEAPON_SPECIALCARBINE', 0),
	(910, 'steam:11000013ac37e2b', 'fixkit', 0),
	(911, 'steam:11000013ac37e2b', 'blowpipe', 0),
	(912, 'steam:11000013ac37e2b', 'zippybag', 0),
	(913, 'steam:11000013ac37e2b', 'WEAPON_HEAVYSNIPER', 0),
	(914, 'steam:11000013ac37e2b', 'WEAPON_FIREWORK', 0),
	(915, 'steam:11000013ac37e2b', 'wrench_blueprint', 0),
	(916, 'steam:11000013ac37e2b', 'WEAPON_BAT', 0),
	(917, 'steam:11000013ac37e2b', 'WEAPON_COMPACTRIFLE', 0),
	(918, 'steam:11000013ac37e2b', 'WEAPON_FLAREGUN', 0),
	(919, 'steam:11000013ac37e2b', 'WEAPON_BALL', 0),
	(920, 'steam:11000013ac37e2b', 'WEAPON_COMBATPISTOL', 0),
	(921, 'steam:11000013ac37e2b', 'WEAPON_COMBATPDW', 0),
	(922, 'steam:11000013ac37e2b', 'WEAPON_MACHETE', 0),
	(923, 'steam:11000013ac37e2b', 'WEAPON_ADVANCEDRIFLE', 0),
	(924, 'steam:11000013ac37e2b', 'wood', 0),
	(925, 'steam:11000013ac37e2b', 'WEAPON_GARBAGEBAG', 0),
	(926, 'steam:11000013ac37e2b', 'clip', 0),
	(927, 'steam:11000013ac37e2b', 'boxmilk', 0),
	(928, 'steam:11000013ac37e2b', 'mushroom_p', 0),
	(929, 'steam:11000013ac37e2b', 'WEAPON_HAMMER', 0),
	(930, 'steam:11000013ac37e2b', 'WEAPON_RAILGUN', 0),
	(931, 'steam:11000013ac37e2b', 'hatchet_lj', 0),
	(932, 'steam:11000013ac37e2b', 'alive_chicken', 0),
	(933, 'steam:11000013ac37e2b', 'WEAPON_HATCHET', 0),
	(934, 'steam:11000013ac37e2b', 'WEAPON_PROXMINE', 0),
	(935, 'steam:11000013ac37e2b', 'whisky', 0),
	(936, 'steam:11000013ac37e2b', 'WEAPON_STINGER', 0),
	(937, 'steam:11000013ac37e2b', 'clothe', 0),
	(938, 'steam:11000013ac37e2b', 'WEAPON_HEAVYSHOTGUN', 0),
	(939, 'steam:11000013ac37e2b', 'doubleaction_blueprint', 0),
	(940, 'steam:11000013ac37e2b', 'radio', 0),
	(941, 'steam:11000013ac37e2b', 'craftkit', 0),
	(942, 'steam:11000013ac37e2b', 'WEAPON_MUSKET', 0),
	(943, 'steam:11000013ac37e2b', 'chicken_package', 0),
	(944, 'steam:11000013ac37e2b', 'packaged_chicken', 0),
	(945, 'steam:11000013ac37e2b', 'cannabis', 0),
	(946, 'steam:11000013ac37e2b', 'weaponflashlight', 0),
	(947, 'steam:11000013ac37e2b', 'washed_stone', 0),
	(948, 'steam:11000013ac37e2b', 'WEAPON_GUSENBERG', 0),
	(949, 'steam:11000013ac37e2b', 'vicodin', 0),
	(950, 'steam:11000013ac37e2b', 'WEAPON_ASSAULTRIFLE', 0),
	(951, 'steam:11000013ac37e2b', 'shell_package', 0),
	(952, 'steam:11000013ac37e2b', 'WEAPON_CROWBAR', 0),
	(953, 'steam:11000013ac37e2b', 'WEAPON_FLASHLIGHT', 0),
	(954, 'steam:11000013ac37e2b', 'crazypersoncard', 0),
	(955, 'steam:11000013ac37e2b', 'WEAPON_BULLPUPSHOTGUN', 0),
	(956, 'steam:11000013ac37e2b', 'chickenwithoutfeather', 0),
	(957, 'steam:11000013ac37e2b', 'gazbottle', 0),
	(958, 'steam:11000013ac37e2b', 'croquettes', 0),
	(959, 'steam:11000013ac37e2b', 'WEAPON_BULLPUPRIFLE', 0),
	(960, 'steam:11000013ac37e2b', 'honey_b', 0),
	(961, 'steam:11000013ac37e2b', 'turtlebait', 0),
	(962, 'steam:11000013ac37e2b', 'turtle', 0),
	(963, 'steam:11000013ac37e2b', 'WEAPON_WRENCH', 0),
	(964, 'steam:11000013ac37e2b', 'switchblade_blueprint', 0),
	(965, 'steam:11000013ac37e2b', 'stone', 0),
	(966, 'steam:11000013ac37e2b', 'WEAPON_AUTOSHOTGUN', 0),
	(967, 'steam:11000013ac37e2b', 'squidbait', 0),
	(968, 'steam:11000013ac37e2b', 'WEAPON_ASSAULTSHOTGUN', 0),
	(969, 'steam:11000013ac37e2b', 'squid', 0),
	(970, 'steam:11000013ac37e2b', 'sportlunch', 0),
	(971, 'steam:11000013ac37e2b', 'skate', 0),
	(972, 'steam:11000013ac37e2b', 'bong', 0),
	(973, 'steam:11000013ac37e2b', 'snspistol_blueprint', 0),
	(974, 'steam:11000013ac37e2b', 'WEAPON_DBSHOTGUN', 0),
	(975, 'steam:11000013ac37e2b', 'coffe', 0),
	(976, 'steam:11000013ac37e2b', 'WEAPON_FLARE', 0),
	(977, 'steam:11000013ac37e2b', 'WEAPON_DOUBLEACTION', 0),
	(978, 'steam:11000013ac37e2b', 'cow_card', 0),
	(979, 'steam:11000013ac37e2b', 'WEAPON_DAGGER', 0),
	(980, 'steam:11000013ac37e2b', 'slaughtered_chicken', 0),
	(981, 'steam:11000013ac37e2b', 'sickle', 0),
	(982, 'steam:11000013ac37e2b', 'rice', 0),
	(983, 'steam:11000013ac37e2b', 'cutted_wood', 0),
	(984, 'steam:11000013ac37e2b', 'WEAPON_RPG', 0),
	(985, 'steam:11000013ac37e2b', 'fuel_a', 0),
	(986, 'steam:11000013ac37e2b', 'shell_c', 0),
	(987, 'steam:11000013ac37e2b', 'petrol', 0),
	(988, 'steam:11000013ac37e2b', 'shell_b', 0),
	(989, 'steam:11000013ac37e2b', 'milk', 0),
	(990, 'steam:11000013ac37e2b', 'WEAPON_MICROSMG', 0),
	(991, 'steam:11000013ac37e2b', 'diamond', 0),
	(992, 'steam:11000013ac37e2b', 'shell_a', 0),
	(993, 'steam:11000013ac37e2b', 'WEAPON_ASSAULTSMG', 0),
	(994, 'steam:11000013ac37e2b', 'coke_pooch', 0),
	(995, 'steam:11000013ac37e2b', 'shell', 0),
	(996, 'steam:11000013ac37e2b', 'anti', 0),
	(997, 'steam:11000013ac37e2b', 'weaponskin', 0),
	(998, 'steam:11000013ac37e2b', 'bobbypin', 0),
	(999, 'steam:11000013ac37e2b', 'prawn', 0),
	(1000, 'steam:11000013ac37e2b', 'sandwich', 0),
	(1001, 'steam:11000013ac37e2b', 'sand_b', 0),
	(1002, 'steam:11000013ac37e2b', 'cocacola', 0),
	(1003, 'steam:11000013ac37e2b', 'sand_a', 0),
	(1004, 'steam:11000013ac37e2b', 'WEAPON_HEAVYPISTOL', 0),
	(1005, 'steam:11000013ac37e2b', 'shell_engine', 0),
	(1006, 'steam:11000013ac37e2b', 'shovel', 0),
	(1007, 'steam:11000013ac37e2b', 'sharkfin', 0),
	(1008, 'steam:11000013ac37e2b', 'tequila', 0),
	(1009, 'steam:11000013ac37e2b', 'shark', 0),
	(1010, 'steam:11000013ac37e2b', 'blowtorch', 0),
	(1011, 'steam:11000013ac37e2b', 'powerade', 0),
	(1012, 'steam:11000013ac37e2b', 'fabric', 0),
	(1013, 'steam:11000013ac37e2b', 'WEAPON_KNUCKLE', 0),
	(1014, 'steam:11000013ac37e2b', 'WEAPON_NIGHTSTICK', 0),
	(1015, 'steam:11000013ac37e2b', 'porkpackage', 0),
	(1016, 'steam:11000013ac37e2b', 'marijuana', 0),
	(1017, 'steam:11000013ac37e2b', 'gym_membership', 0),
	(1018, 'steam:11000013ac37e2b', 'weed_pooch', 0),
	(1019, 'steam:11000013ac37e2b', 'WEAPON_MARKSMANRIFLE', 0),
	(1020, 'steam:11000013ac37e2b', 'WEAPON_VINTAGEPISTOL', 0),
	(1021, 'steam:11000013ac37e2b', 'beer', 0),
	(1022, 'steam:11000013ac37e2b', 'WEAPON_BZGAS', 0),
	(1023, 'steam:11000013ac37e2b', 'bandage', 0),
	(1024, 'steam:11000013ac37e2b', 'WEAPON_STUNGUN', 0),
	(1025, 'steam:11000013ac37e2b', 'phone', 0),
	(1026, 'steam:11000013ac37e2b', 'packaged_plank', 0),
	(1027, 'steam:11000013ac37e2b', 'mushroom', 0),
	(1028, 'steam:11000013ac37e2b', 'mushroom_d', 0),
	(1029, 'steam:11000013ac37e2b', 'WEAPON_FIREEXTINGUISHER', 0),
	(1030, 'steam:11000013ac37e2b', 'oxygen_mask', 0),
	(1031, 'steam:11000013ac37e2b', 'WEAPON_STICKYBOMB', 0),
	(1032, 'steam:11000013ac37e2b', 'morphine', 0),
	(1033, 'steam:11000013ac37e2b', 'milk_engine', 0),
	(1034, 'steam:11000013ac37e2b', 'meth', 0),
	(1035, 'steam:11000013ac37e2b', 'medikit', 0),
	(1036, 'steam:11000013ac37e2b', 'chest_a', 0),
	(1037, 'steam:11000013ac37e2b', 'fish', 0),
	(1038, 'steam:11000013ac37e2b', 'fixtool', 0),
	(1039, 'steam:11000013ac37e2b', 'WEAPON_CARBINERIFLE', 0),
	(1040, 'steam:11000013ac37e2b', 'meatshark', 0),
	(1041, 'steam:11000013ac37e2b', 'meat', 0),
	(1042, 'steam:11000013ac37e2b', 'WEAPON_MARKSMANPISTOL', 0),
	(1043, 'steam:11000013ac37e2b', 'gold', 0),
	(1044, 'steam:11000013ac37e2b', 'leather', 0),
	(1045, 'steam:11000013ac37e2b', 'WEAPON_BATTLEAXE', 0),
	(1046, 'steam:11000013ac37e2b', 'marijuana_cigarette', 0),
	(1047, 'steam:11000013ac37e2b', 'knife_chicken', 0),
	(1048, 'steam:11000013ac37e2b', 'WEAPON_PISTOL50', 0),
	(1049, 'steam:11000013ac37e2b', 'WEAPON_COMPACTLAUNCHER', 0),
	(1050, 'steam:11000013ac37e2b', 'cupcake', 0),
	(1051, 'steam:11000013ac37e2b', 'WEAPON_SNIPERRIFLE', 0),
	(1052, 'steam:11000013ac37e2b', 'jumelles', 0),
	(1053, 'steam:11000013ac37e2b', 'iron', 0),
	(1054, 'steam:11000013ac37e2b', 'icetea', 0),
	(1055, 'steam:11000013ac37e2b', 'hydrocodone', 0),
	(1056, 'steam:11000013ac37e2b', 'WEAPON_MINISMG', 0),
	(1057, 'steam:11000013ac37e2b', 'honey_a', 0),
	(1058, 'steam:11000013ac37e2b', 'SteelScrap', 0),
	(1059, 'steam:11000013ac37e2b', 'AED', 0),
	(1060, 'steam:11000013ac37e2b', 'hamburger', 0),
	(1061, 'steam:11000013ac37e2b', 'coke', 0),
	(1062, 'steam:11000013ac37e2b', 'hamajifish', 0),
	(1063, 'steam:11000013ac37e2b', 'WEAPON_PISTOL', 0),
	(1064, 'steam:11000013ac37e2b', 'fishingrod', 0),
	(1065, 'steam:11000013ac37e2b', 'WEAPON_HOMINGLAUNCHER', 0),
	(1066, 'steam:11000013ac37e2b', 'glasses', 0),
	(1067, 'steam:11000013ac37e2b', 'fuel_b', 0),
	(1068, 'steam:11000013ac37e2b', 'rope', 0),
	(1069, 'steam:11000013ac37e2b', 'WEAPON_HANDCUFFS', 0),
	(1070, 'steam:11000013ac37e2b', 'WEAPON_PUMPSHOTGUN', 0),
	(1071, 'steam:11000013ac37e2b', 'handcuffs', 0),
	(1072, 'steam:11000013ac37e2b', 'Packaged_plank', 0),
	(1073, 'steam:11000013ac37e2b', 'cigarett', 0),
	(1074, 'steam:11000013ac37e2b', 'copper', 0),
	(1075, 'steam:11000013ac37e2b', 'WEAPON_MACHINEPISTOL', 0),
	(1076, 'steam:11000013ac37e2b', 'WEAPON_APPISTOL', 0),
	(1077, 'steam:11000013ac37e2b', 'WEAPON_SMOKEGRENADE', 0),
	(1078, 'steam:11000013ac37e2b', 'WEAPON_PIPEBOMB', 0),
	(1079, 'steam:11000013ac37e2b', 'WEAPON_COMBATMG', 0),
	(1080, 'steam:11000013ac37e2b', 'drill', 0),
	(1081, 'steam:11000013ac37e2b', 'pork', 0),
	(1082, 'steam:11000013ac37e2b', 'lockpick', 0),
	(1083, 'steam:11000013ac37e2b', 'WEAPON_SMG', 0),
	(1084, 'steam:11000013ac37e2b', 'wine', 0),
	(1085, 'steam:11000013ac37e2b', 'prawnbait', 0),
	(1086, 'steam:11000013ac37e2b', 'WEAPON_MINIGUN', 0),
	(1087, 'steam:11000013ac37e2b', 'WEAPON_BOTTLE', 0),
	(1088, 'steam:11000013ac37e2b', 'WEAPON_MOLOTOV', 0),
	(1089, 'steam:11000013ac37e2b', 'WEAPON_SNOWBALL', 0),
	(1090, 'steam:11000013ac37e2b', 'WEAPON_KNIFE', 0),
	(1091, 'steam:11000013ac37e2b', 'chicken_meat', 0),
	(1092, 'steam:11000013ac37e2b', 'carokit', 0),
	(1093, 'steam:11000013ac37e2b', 'WEAPON_SWITCHBLADE', 0),
	(1094, 'steam:11000013ac37e2b', 'WEAPON_GRENADE', 0),
	(1095, 'steam:11000013ac37e2b', 'essence', 0),
	(1096, 'steam:11000013ac37e2b', 'bread', 0),
	(1097, 'steam:11000013ac37e2b', 'vodka', 0),
	(1098, 'steam:11000013ac37e2b', 'WEAPON_DIGISCANNER', 0),
	(1099, 'steam:11000013ac37e2b', 'WEAPON_POOLCUE', 0),
	(1100, 'steam:11000013ac37e2b', 'WEAPON_GOLFCLUB', 0),
	(1101, 'steam:110000136cd1665', 'WEAPON_SPECIALCARBINE', 0),
	(1102, 'steam:110000136cd1665', 'turtlebait', 0),
	(1103, 'steam:110000136cd1665', 'shell_b', 0),
	(1104, 'steam:110000136cd1665', 'WEAPON_SAWNOFFSHOTGUN', 0),
	(1105, 'steam:110000136cd1665', 'knife_chicken', 0),
	(1106, 'steam:110000136cd1665', 'WEAPON_HANDCUFFS', 0),
	(1107, 'steam:110000136cd1665', 'WEAPON_BZGAS', 0),
	(1108, 'steam:110000136cd1665', 'coke', 0),
	(1109, 'steam:110000136cd1665', 'turtle', 0),
	(1110, 'steam:110000136cd1665', 'WEAPON_RPG', 0),
	(1111, 'steam:110000136cd1665', 'WEAPON_MUSKET', 0),
	(1112, 'steam:110000136cd1665', 'WEAPON_STICKYBOMB', 0),
	(1113, 'steam:110000136cd1665', 'beer', 0),
	(1114, 'steam:110000136cd1665', 'prawnbait', 0),
	(1115, 'steam:110000136cd1665', 'shell_c', 0),
	(1116, 'steam:110000136cd1665', 'WEAPON_GRENADELAUNCHER', 0),
	(1117, 'steam:110000136cd1665', 'cocacola', 0),
	(1118, 'steam:110000136cd1665', 'fish', 0),
	(1119, 'steam:110000136cd1665', 'clip', 0),
	(1120, 'steam:110000136cd1665', 'WEAPON_MOLOTOV', 0),
	(1121, 'steam:110000136cd1665', 'WEAPON_MICROSMG', 0),
	(1122, 'steam:110000136cd1665', 'chicken_package', 0),
	(1123, 'steam:110000136cd1665', 'WEAPON_GARBAGEBAG', 0),
	(1124, 'steam:110000136cd1665', 'craftkit', 0),
	(1125, 'steam:110000136cd1665', 'WEAPON_ADVANCEDRIFLE', 0),
	(1126, 'steam:110000136cd1665', 'essence', 0),
	(1127, 'steam:110000136cd1665', 'WEAPON_FIREWORK', 0),
	(1128, 'steam:110000136cd1665', 'wine', 0),
	(1129, 'steam:110000136cd1665', 'WEAPON_RAILGUN', 0),
	(1130, 'steam:110000136cd1665', 'WEAPON_AUTOSHOTGUN', 0),
	(1131, 'steam:110000136cd1665', 'WEAPON_SNSPISTOL', 0),
	(1132, 'steam:110000136cd1665', 'britadeira', 0),
	(1133, 'steam:110000136cd1665', 'WEAPON_STUNGUN', 0),
	(1134, 'steam:110000136cd1665', 'gold', 0),
	(1135, 'steam:110000136cd1665', 'rice_pro', 0),
	(1136, 'steam:110000136cd1665', 'sand', 0),
	(1137, 'steam:110000136cd1665', 'WEAPON_POOLCUE', 0),
	(1138, 'steam:110000136cd1665', 'wrench_blueprint', 0),
	(1139, 'steam:110000136cd1665', 'jumelles', 0),
	(1140, 'steam:110000136cd1665', 'hydrocodone', 0),
	(1141, 'steam:110000136cd1665', 'phone', 0),
	(1142, 'steam:110000136cd1665', 'WEAPON_SMG', 0),
	(1143, 'steam:110000136cd1665', 'bottle', 0),
	(1144, 'steam:110000136cd1665', 'WEAPON_MARKSMANPISTOL', 0),
	(1145, 'steam:110000136cd1665', 'WEAPON_BAT', 0),
	(1146, 'steam:110000136cd1665', 'alive_chicken', 0),
	(1147, 'steam:110000136cd1665', 'WEAPON_HEAVYPISTOL', 0),
	(1148, 'steam:110000136cd1665', 'fuel_a', 0),
	(1149, 'steam:110000136cd1665', 'WEAPON_COMPACTLAUNCHER', 0),
	(1150, 'steam:110000136cd1665', 'mushroom_d', 0),
	(1151, 'steam:110000136cd1665', 'WEAPON_STINGER', 0),
	(1152, 'steam:110000136cd1665', 'WEAPON_PETROLCAN', 0),
	(1153, 'steam:110000136cd1665', 'WEAPON_BATTLEAXE', 0),
	(1154, 'steam:110000136cd1665', 'zippybag', 0),
	(1155, 'steam:110000136cd1665', 'gym_membership', 0),
	(1156, 'steam:110000136cd1665', 'worm', 0),
	(1157, 'steam:110000136cd1665', 'shell_a', 0),
	(1158, 'steam:110000136cd1665', 'bong', 0),
	(1159, 'steam:110000136cd1665', 'wood', 0),
	(1160, 'steam:110000136cd1665', 'WEAPON_MACHETE', 0),
	(1161, 'steam:110000136cd1665', 'whisky', 0),
	(1162, 'steam:110000136cd1665', 'weed_pooch', 0),
	(1163, 'steam:110000136cd1665', 'weaponskin', 0),
	(1164, 'steam:110000136cd1665', 'WEAPON_FLAREGUN', 0),
	(1165, 'steam:110000136cd1665', 'weapongrip', 0),
	(1166, 'steam:110000136cd1665', 'WEAPON_PISTOL50', 0),
	(1167, 'steam:110000136cd1665', 'WEAPON_BULLPUPSHOTGUN', 0),
	(1168, 'steam:110000136cd1665', 'vicodin', 0),
	(1169, 'steam:110000136cd1665', 'crazypersoncard', 0),
	(1170, 'steam:110000136cd1665', 'fishingrod', 0),
	(1171, 'steam:110000136cd1665', 'carokit', 0),
	(1172, 'steam:110000136cd1665', 'WEAPON_FLARE', 0),
	(1173, 'steam:110000136cd1665', 'washed_stone', 0),
	(1174, 'steam:110000136cd1665', 'vodka', 0),
	(1175, 'steam:110000136cd1665', 'WEAPON_CROWBAR', 0),
	(1176, 'steam:110000136cd1665', 'WEAPON_NIGHTSTICK', 0),
	(1177, 'steam:110000136cd1665', 'WEAPON_KNUCKLE', 0),
	(1178, 'steam:110000136cd1665', 'chocolate', 0),
	(1179, 'steam:110000136cd1665', 'tequila', 0),
	(1180, 'steam:110000136cd1665', 'switchblade_blueprint', 0),
	(1181, 'steam:110000136cd1665', 'WEAPON_SNIPERRIFLE', 0),
	(1182, 'steam:110000136cd1665', 'carotool', 0),
	(1183, 'steam:110000136cd1665', 'WEAPON_ASSAULTSHOTGUN', 0),
	(1184, 'steam:110000136cd1665', 'WEAPON_MACHINEPISTOL', 0),
	(1185, 'steam:110000136cd1665', 'cannabis', 0),
	(1186, 'steam:110000136cd1665', 'WEAPON_HATCHET', 0),
	(1187, 'steam:110000136cd1665', 'squidbait', 0),
	(1188, 'steam:110000136cd1665', 'WEAPON_BALL', 0),
	(1189, 'steam:110000136cd1665', 'meth', 0),
	(1190, 'steam:110000136cd1665', 'cupcake', 0),
	(1191, 'steam:110000136cd1665', 'fuel_b', 0),
	(1192, 'steam:110000136cd1665', 'squid', 0),
	(1193, 'steam:110000136cd1665', 'sportlunch', 0),
	(1194, 'steam:110000136cd1665', 'snspistol_blueprint', 0),
	(1195, 'steam:110000136cd1665', 'slaughtered_chicken', 0),
	(1196, 'steam:110000136cd1665', 'shell', 0),
	(1197, 'steam:110000136cd1665', 'leather', 0),
	(1198, 'steam:110000136cd1665', 'blowpipe', 0),
	(1199, 'steam:110000136cd1665', 'sickle', 0),
	(1200, 'steam:110000136cd1665', 'pro_wood', 0),
	(1201, 'steam:110000136cd1665', 'fishbait', 0),
	(1202, 'steam:110000136cd1665', 'WEAPON_DAGGER', 0),
	(1203, 'steam:110000136cd1665', 'WEAPON_PUMPSHOTGUN', 0),
	(1204, 'steam:110000136cd1665', 'WEAPON_COMPACTRIFLE', 0),
	(1205, 'steam:110000136cd1665', 'shell_package', 0),
	(1206, 'steam:110000136cd1665', 'WEAPON_VINTAGEPISTOL', 0),
	(1207, 'steam:110000136cd1665', 'shell_engine', 0),
	(1208, 'steam:110000136cd1665', 'skate', 0),
	(1209, 'steam:110000136cd1665', 'porkpackage', 0),
	(1210, 'steam:110000136cd1665', 'wool', 0),
	(1211, 'steam:110000136cd1665', 'Packaged_plank', 0),
	(1212, 'steam:110000136cd1665', 'marijuana_cigarette', 0),
	(1213, 'steam:110000136cd1665', 'sharkfin', 0),
	(1214, 'steam:110000136cd1665', 'shark', 0),
	(1215, 'steam:110000136cd1665', 'sandwich', 0),
	(1216, 'steam:110000136cd1665', 'sand_b', 0),
	(1217, 'steam:110000136cd1665', 'sand_a', 0),
	(1218, 'steam:110000136cd1665', 'rope', 0),
	(1219, 'steam:110000136cd1665', 'rice', 0),
	(1220, 'steam:110000136cd1665', 'SteelScrap', 0),
	(1221, 'steam:110000136cd1665', 'WEAPON_MINISMG', 0),
	(1222, 'steam:110000136cd1665', 'WEAPON_GRENADE', 0),
	(1223, 'steam:110000136cd1665', 'radio', 0),
	(1224, 'steam:110000136cd1665', 'mushroom', 0),
	(1225, 'steam:110000136cd1665', 'blowtorch', 0),
	(1226, 'steam:110000136cd1665', 'WEAPON_GOLFCLUB', 0),
	(1227, 'steam:110000136cd1665', 'protein_shake', 0),
	(1228, 'steam:110000136cd1665', 'WEAPON_SMOKEGRENADE', 0),
	(1229, 'steam:110000136cd1665', 'WEAPON_HOMINGLAUNCHER', 0),
	(1230, 'steam:110000136cd1665', 'meth_pooch', 0),
	(1231, 'steam:110000136cd1665', 'WEAPON_FIREEXTINGUISHER', 0),
	(1232, 'steam:110000136cd1665', 'prawn', 0),
	(1233, 'steam:110000136cd1665', 'hatchet_lj', 0),
	(1234, 'steam:110000136cd1665', 'stone', 0),
	(1235, 'steam:110000136cd1665', 'pork', 0),
	(1236, 'steam:110000136cd1665', 'WEAPON_COMBATPISTOL', 0),
	(1237, 'steam:110000136cd1665', 'gunpowder', 0),
	(1238, 'steam:110000136cd1665', 'petrol', 0),
	(1239, 'steam:110000136cd1665', 'water', 0),
	(1240, 'steam:110000136cd1665', 'packaged_chicken', 0),
	(1241, 'steam:110000136cd1665', 'oxygen_mask', 0),
	(1242, 'steam:110000136cd1665', 'cutted_wood', 0),
	(1243, 'steam:110000136cd1665', 'packaged_plank', 0),
	(1244, 'steam:110000136cd1665', 'morphine', 0),
	(1245, 'steam:110000136cd1665', 'milk_package', 0),
	(1246, 'steam:110000136cd1665', 'milk_engine', 0),
	(1247, 'steam:110000136cd1665', 'diamond', 0),
	(1248, 'steam:110000136cd1665', 'WEAPON_HAMMER', 0),
	(1249, 'steam:110000136cd1665', 'chicken_meat', 0),
	(1250, 'steam:110000136cd1665', 'medikit', 0),
	(1251, 'steam:110000136cd1665', 'meatshark', 0),
	(1252, 'steam:110000136cd1665', 'meat', 0),
	(1253, 'steam:110000136cd1665', 'WEAPON_CARBINERIFLE', 0),
	(1254, 'steam:110000136cd1665', 'WEAPON_WRENCH', 0),
	(1255, 'steam:110000136cd1665', 'marijuana', 0),
	(1256, 'steam:110000136cd1665', 'lockpick', 0),
	(1257, 'steam:110000136cd1665', 'WEAPON_PISTOL', 0),
	(1258, 'steam:110000136cd1665', 'lighter', 0),
	(1259, 'steam:110000136cd1665', 'iron', 0),
	(1260, 'steam:110000136cd1665', 'icetea', 0),
	(1261, 'steam:110000136cd1665', 'WEAPON_MINIGUN', 0),
	(1262, 'steam:110000136cd1665', 'honey_a', 0),
	(1263, 'steam:110000136cd1665', 'powerade', 0),
	(1264, 'steam:110000136cd1665', 'WEAPON_PIPEBOMB', 0),
	(1265, 'steam:110000136cd1665', 'hamburger', 0),
	(1266, 'steam:110000136cd1665', 'handcuffs', 0),
	(1267, 'steam:110000136cd1665', 'WEAPON_COMBATMG', 0),
	(1268, 'steam:110000136cd1665', 'WEAPON_APPISTOL', 0),
	(1269, 'steam:110000136cd1665', 'WEAPON_FLASHLIGHT', 0),
	(1270, 'steam:110000136cd1665', 'hamajifish', 0),
	(1271, 'steam:110000136cd1665', 'glasses', 0),
	(1272, 'steam:110000136cd1665', 'gazbottle', 0),
	(1273, 'steam:110000136cd1665', 'WEAPON_ASSAULTSMG', 0),
	(1274, 'steam:110000136cd1665', 'WEAPON_HEAVYSNIPER', 0),
	(1275, 'steam:110000136cd1665', 'weaponflashlight', 0),
	(1276, 'steam:110000136cd1665', 'shovel', 0),
	(1277, 'steam:110000136cd1665', 'bcabbage', 0),
	(1278, 'steam:110000136cd1665', 'acabbage', 0),
	(1279, 'steam:110000136cd1665', 'WEAPON_SWITCHBLADE', 0),
	(1280, 'steam:110000136cd1665', 'coke_pooch', 0),
	(1281, 'steam:110000136cd1665', 'drill', 0),
	(1282, 'steam:110000136cd1665', 'fabric', 0),
	(1283, 'steam:110000136cd1665', 'WEAPON_DBSHOTGUN', 0),
	(1284, 'steam:110000136cd1665', 'clothe', 0),
	(1285, 'steam:110000136cd1665', 'honey_b', 0),
	(1286, 'steam:110000136cd1665', 'WEAPON_GUSENBERG', 0),
	(1287, 'steam:110000136cd1665', 'milk', 0),
	(1288, 'steam:110000136cd1665', 'copper', 0),
	(1289, 'steam:110000136cd1665', 'WEAPON_HEAVYSHOTGUN', 0),
	(1290, 'steam:110000136cd1665', 'boxmilk', 0),
	(1291, 'steam:110000136cd1665', 'diesel', 0),
	(1292, 'steam:110000136cd1665', 'mushroom_p', 0),
	(1293, 'steam:110000136cd1665', 'coffe', 0),
	(1294, 'steam:110000136cd1665', 'fixkit', 0),
	(1295, 'steam:110000136cd1665', 'cow_card', 0),
	(1296, 'steam:110000136cd1665', 'WEAPON_COMBATPDW', 0),
	(1297, 'steam:110000136cd1665', 'WEAPON_REVOLVER', 0),
	(1298, 'steam:110000136cd1665', 'fixtool', 0),
	(1299, 'steam:110000136cd1665', 'doubleaction_blueprint', 0),
	(1300, 'steam:110000136cd1665', 'WEAPON_BOTTLE', 0),
	(1301, 'steam:110000136cd1665', 'croquettes', 0),
	(1302, 'steam:110000136cd1665', 'WEAPON_ASSAULTRIFLE', 0),
	(1303, 'steam:110000136cd1665', 'anti', 0),
	(1304, 'steam:110000136cd1665', 'bandage', 0),
	(1305, 'steam:110000136cd1665', 'WEAPON_KNIFE', 0),
	(1306, 'steam:110000136cd1665', 'WEAPON_DOUBLEACTION', 0),
	(1307, 'steam:110000136cd1665', 'bread', 0),
	(1308, 'steam:110000136cd1665', 'WEAPON_BULLPUPRIFLE', 0),
	(1309, 'steam:110000136cd1665', 'WEAPON_PROXMINE', 0),
	(1310, 'steam:110000136cd1665', 'WEAPON_MARKSMANRIFLE', 0),
	(1311, 'steam:110000136cd1665', 'cigarett', 0),
	(1312, 'steam:110000136cd1665', 'bobbypin', 0),
	(1313, 'steam:110000136cd1665', 'chest_a', 0),
	(1314, 'steam:110000136cd1665', 'chickenwithoutfeather', 0),
	(1315, 'steam:110000136cd1665', 'chicken', 0),
	(1316, 'steam:110000136cd1665', 'petrol_raffin', 0),
	(1317, 'steam:110000136cd1665', 'WEAPON_DIGISCANNER', 0),
	(1318, 'steam:110000136cd1665', 'WEAPON_SNOWBALL', 0),
	(1319, 'steam:110000136cd1665', 'WEAPON_MG', 0),
	(1320, 'steam:110000136cd1665', 'AED', 0);
/*!40000 ALTER TABLE `user_inventory` ENABLE KEYS */;

-- Dumping structure for table essentialmode.user_licenses
CREATE TABLE IF NOT EXISTS `user_licenses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `owner` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=123 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.user_licenses: ~0 rows (approximately)
/*!40000 ALTER TABLE `user_licenses` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_licenses` ENABLE KEYS */;

-- Dumping structure for table essentialmode.user_parkings
CREATE TABLE IF NOT EXISTS `user_parkings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(60) DEFAULT NULL,
  `garage` varchar(60) DEFAULT NULL,
  `zone` int(11) NOT NULL,
  `vehicle` longtext DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.user_parkings: ~0 rows (approximately)
/*!40000 ALTER TABLE `user_parkings` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_parkings` ENABLE KEYS */;

-- Dumping structure for table essentialmode.user_whitelist
CREATE TABLE IF NOT EXISTS `user_whitelist` (
  `identifier` varchar(255) NOT NULL,
  PRIMARY KEY (`identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table essentialmode.user_whitelist: ~0 rows (approximately)
/*!40000 ALTER TABLE `user_whitelist` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_whitelist` ENABLE KEYS */;

-- Dumping structure for table essentialmode.vehicles
CREATE TABLE IF NOT EXISTS `vehicles` (
  `id` int(11) NOT NULL,
  `name` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `model` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `price` int(11) NOT NULL,
  `category` varchar(60) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.vehicles: ~84 rows (approximately)
/*!40000 ALTER TABLE `vehicles` DISABLE KEYS */;
INSERT INTO `vehicles` (`id`, `name`, `model`, `price`, `category`) VALUES
	(0, 'Adder   15kg.', 'adder ', 3200000, 'supercar'),
	(0, 'Akuma 5kg.', 'akuma', 16000, 'motorcycles'),
	(0, 'Asea  20kg.', 'asea', 48000, 'sedans'),
	(0, 'Asterope   20kg.', 'asterope ', 48000, 'sedans'),
	(0, 'Bagger 5kg.', 'bagger', 17000, 'motorcycles'),
	(0, 'Baller 40kg.', 'baller', 240000, 'suv'),
	(0, 'Baller2  40kg.', 'baller2', 320000, 'suv'),
	(0, 'Banshee  20kg.', 'banshee', 480000, 'sport'),
	(0, 'Bestiagts    20kg.', 'bestiagts', 480000, 'sport'),
	(0, 'Bf400  5kg.', 'bf400', 24000, 'motorcycles'),
	(0, 'Bison  40kg.', 'bison', 80000, 'offroad'),
	(0, 'BMX', 'bmx', 800, 'cycles'),
	(0, 'Bobcatxl  40kg.', 'bobcatxl', 80000, 'offroad'),
	(0, 'Bodhi2   40kg.', 'bodhi2', 80000, 'offroad'),
	(0, 'Brioso   20kg.', 'brioso ', 48000, 'sedans'),
	(0, 'Buffalo2  20kg.', 'buffalo2', 320000, 'muscle'),
	(0, 'Burrito2   40kg.', 'burrito2', 64000, 'vans'),
	(0, 'Burrito4  40kg.', 'burrito4', 64000, 'vans'),
	(0, 'Burrito5  40kg.', 'burrito5', 64000, 'vans'),
	(0, 'Cog55  20kg.', 'cog55 ', 56000, 'sedans'),
	(0, 'Comet3   20kg.', 'comet3', 290000, 'muscle'),
	(0, 'Coquette  20kg.', 'coquette', 320000, 'muscle'),
	(0, 'Cyclone 15kg.', 'cyclone', 4000000, 'supercar'),
	(0, 'Daemon  5kg.', 'daemon', 25000, 'motorcycles'),
	(0, 'Daemon2  5kg.', 'daemon2', 24000, 'motorcycles'),
	(0, 'Defiler   5kg.', 'defiler', 40000, 'motorcycles'),
	(0, 'Diablous  5kg.', 'diablous', 17000, 'motorcycles'),
	(0, 'Diablous2  5kg.', 'diablous2', 40000, 'motorcycles'),
	(0, 'Elegy   20kg.', 'elegy', 460000, 'muscle'),
	(0, 'Enduro 5kg.', 'enduro', 16000, 'motorcycles'),
	(0, 'Entityxf   15kg.', 'entityxf ', 3200000, 'supercar'),
	(0, 'Esskey 5kg.', 'esskey', 17000, 'motorcycles'),
	(0, 'Faggio  5kg.', 'faggio', 12000, 'motorcycles'),
	(0, 'Faggio3  5kg.', 'faggio3', 3000, 'รถเริ่มต้น'),
	(0, 'Fcr2   5kg.', 'fcr2', 17000, 'motorcycles'),
	(0, 'Fixter', 'fixter', 1200, 'cycles'),
	(0, 'Fq2   40kg.', 'fq2', 160000, 'suv'),
	(0, 'Fugitive  20kg.', 'fugitive ', 56000, 'sedans'),
	(0, 'Furoregt   20kg.', 'furoregt', 560000, 'sport'),
	(0, 'Gauntlet   20kg.', 'gauntlet', 240000, 'muscle'),
	(0, 'Gburrito2  40kg.', 'gburrito2', 64000, 'vans'),
	(0, 'Gresley  40kg.', 'gresley', 320000, 'suv'),
	(0, 'Intruder  20kg.', 'intruder ', 56000, 'sedans'),
	(0, 'Issi2   20kg.', 'issi2 ', 48000, 'sedans'),
	(0, 'Italigtb   15kg.', 'italigtb ', 4800000, 'supercar'),
	(0, 'Italigtb2   15kg.', 'italigtb2', 4800000, 'supercar'),
	(0, 'Jester   20kg.', 'jester', 720000, 'sport'),
	(0, 'Khamelion   20kg.', 'khamelion', 720000, 'sport'),
	(0, 'Massacro   20kg.', 'massacro', 560000, 'sport'),
	(0, 'Nemesis   5kg.', 'nemesis', 32000, 'motorcycles'),
	(0, 'Nero   15kg.', 'nero', 7200000, 'supercar'),
	(0, 'Nero2   15kg.', 'nero2', 7200000, 'supercar'),
	(0, 'Nightshade 20kg.', 'nightshade', 240000, 'muscle'),
	(0, 'Ninef   20kg.', 'ninef', 640000, 'sport'),
	(0, 'Panto   20kg.', 'panto ', 48000, 'sedans'),
	(0, 'Patriot  40kg.', 'patriot', 240000, 'suv'),
	(0, 'Pcj 5kg.', 'pcj', 16000, 'motorcycles'),
	(0, 'Pfister811  15kg.', 'pfister811', 4000000, 'supercar'),
	(0, 'Premier  20kg.', 'premier ', 48000, 'sedans'),
	(0, 'Primo2   20kg.', 'primo2 ', 56000, 'sedans'),
	(0, 'Raiden   20kg', 'raiden', 640000, 'sport'),
	(0, 'Ratloader   20kg.', 'ratloader', 16000, 'รถเริ่มต้น'),
	(0, 'Rebel  40kg.', 'rebel', 64000, 'offroad'),
	(0, 'Sabregt   20kg.', 'sabregt', 240000, 'muscle'),
	(0, 'Sanchez2  5kg.', 'sanchez2', 17000, 'motorcycles'),
	(0, 'Schlagen  20kg.', 'schlagen', 720000, 'sport'),
	(0, 'Scorcher', 'scorcher', 1200, 'cycles'),
	(0, 'sentinel3 20kg.', 'sentinel3', 240000, 'muscle'),
	(0, 'Seven70   20kg.', 'seven70', 640000, 'sport'),
	(0, 'Stanier   20kg.', 'stanier ', 40000, 'sedans'),
	(0, 'T20   15kg.', 't20', 6400000, 'supercar'),
	(0, 'Tornado3   20kg.', 'tornado3', 16000, 'รถเริ่มต้น'),
	(0, 'Tornado4   20kg.', 'tornado4', 16000, 'รถเริ่มต้น'),
	(0, 'Tribike2', 'tribike2', 1400, 'cycles'),
	(0, 'Turismor   15kg.', 'turismor', 7200000, 'supercar'),
	(0, 'Vacca     15kg.', 'vacca', 6400000, 'supercar'),
	(0, 'Vader 5kg.', 'vader', 32000, 'motorcycles'),
	(0, 'Vagner  15kg.', 'vagner', 5600000, 'supercar'),
	(0, 'Visione   15kg.', 'visione', 7200000, 'supercar'),
	(0, 'Voodoo2    20kg.', 'voodoo2', 16000, 'รถเริ่มต้น'),
	(0, 'Vortex   5kg.', 'vortex', 40000, 'motorcycles'),
	(0, 'Xa21  15kg.', 'xa21', 4000000, 'supercar'),
	(0, 'Xls2   40kg.', 'xls2 ', 400000, 'suv'),
	(0, 'Zentorno   15kg.', 'zentorno', 5600000, 'supercar');
/*!40000 ALTER TABLE `vehicles` ENABLE KEYS */;

-- Dumping structure for table essentialmode.vehicles_for_sale
CREATE TABLE IF NOT EXISTS `vehicles_for_sale` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seller` varchar(50) NOT NULL,
  `vehicleProps` longtext NOT NULL,
  `price` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=223 DEFAULT CHARSET=utf8;

-- Dumping data for table essentialmode.vehicles_for_sale: ~0 rows (approximately)
/*!40000 ALTER TABLE `vehicles_for_sale` DISABLE KEYS */;
/*!40000 ALTER TABLE `vehicles_for_sale` ENABLE KEYS */;

-- Dumping structure for table essentialmode.vehicle_categories
CREATE TABLE IF NOT EXISTS `vehicle_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.vehicle_categories: ~10 rows (approximately)
/*!40000 ALTER TABLE `vehicle_categories` DISABLE KEYS */;
INSERT INTO `vehicle_categories` (`id`, `name`, `label`) VALUES
	(0, 'cycles', 'จักรยาน'),
	(0, 'motorcycles', 'รถมอเตอร์ไซ'),
	(0, 'muscle', 'Muscle'),
	(0, 'offroad', 'รถกระบะ'),
	(0, 'sedans', 'รถเก๋ง'),
	(0, 'sport', 'Sports'),
	(0, 'supercar', 'Supercar'),
	(0, 'suv', 'SUV'),
	(0, 'vans', 'รถตู้'),
	(0, 'รถเริ่มต้น', 'รถเริ่มต้น');
/*!40000 ALTER TABLE `vehicle_categories` ENABLE KEYS */;

-- Dumping structure for table essentialmode.vehicle_sold
CREATE TABLE IF NOT EXISTS `vehicle_sold` (
  `client` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `model` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `plate` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `soldby` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `date` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`plate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table essentialmode.vehicle_sold: ~0 rows (approximately)
/*!40000 ALTER TABLE `vehicle_sold` DISABLE KEYS */;
/*!40000 ALTER TABLE `vehicle_sold` ENABLE KEYS */;

-- Dumping structure for table essentialmode.vrp_srv_data
CREATE TABLE IF NOT EXISTS `vrp_srv_data` (
  `dkey` varchar(255) NOT NULL,
  `dvalue` text DEFAULT NULL,
  PRIMARY KEY (`dkey`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.vrp_srv_data: ~0 rows (approximately)
/*!40000 ALTER TABLE `vrp_srv_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `vrp_srv_data` ENABLE KEYS */;

-- Dumping structure for table essentialmode.vrp_users
CREATE TABLE IF NOT EXISTS `vrp_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `last_login` varchar(255) DEFAULT NULL,
  `whitelisted` tinyint(1) DEFAULT NULL,
  `banned` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.vrp_users: ~0 rows (approximately)
/*!40000 ALTER TABLE `vrp_users` DISABLE KEYS */;
INSERT INTO `vrp_users` (`id`, `last_login`, `whitelisted`, `banned`) VALUES
	(1, '127.0.0.1 21:21:27 25/11/2019', 0, 0),
	(2, '127.0.0.1 00:46:05 26/11/2019', 0, 0);
/*!40000 ALTER TABLE `vrp_users` ENABLE KEYS */;

-- Dumping structure for table essentialmode.vrp_user_data
CREATE TABLE IF NOT EXISTS `vrp_user_data` (
  `user_id` int(11) NOT NULL,
  `dkey` varchar(255) NOT NULL,
  `dvalue` text DEFAULT NULL,
  PRIMARY KEY (`user_id`,`dkey`),
  CONSTRAINT `fk_user_data_users` FOREIGN KEY (`user_id`) REFERENCES `vrp_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.vrp_user_data: ~0 rows (approximately)
/*!40000 ALTER TABLE `vrp_user_data` DISABLE KEYS */;
INSERT INTO `vrp_user_data` (`user_id`, `dkey`, `dvalue`) VALUES
	(2, 'vRP:datatable', '[]');
/*!40000 ALTER TABLE `vrp_user_data` ENABLE KEYS */;

-- Dumping structure for table essentialmode.vrp_user_ids
CREATE TABLE IF NOT EXISTS `vrp_user_ids` (
  `identifier` varchar(255) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`identifier`),
  KEY `fk_user_ids_users` (`user_id`),
  CONSTRAINT `fk_user_ids_users` FOREIGN KEY (`user_id`) REFERENCES `vrp_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.vrp_user_ids: ~0 rows (approximately)
/*!40000 ALTER TABLE `vrp_user_ids` DISABLE KEYS */;
INSERT INTO `vrp_user_ids` (`identifier`, `user_id`) VALUES
	('discord:563373622840786944', 2),
	('ip:127.0.0.1', 2),
	('license:1695737ba9de1c01bf92473f152b1df5ccb54e35', 2),
	('steam:110000104a1abc7', 2);
/*!40000 ALTER TABLE `vrp_user_ids` ENABLE KEYS */;

-- Dumping structure for table essentialmode.weashops
CREATE TABLE IF NOT EXISTS `weashops` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zone` varchar(255) NOT NULL,
  `item` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;

-- Dumping data for table essentialmode.weashops: ~40 rows (approximately)
/*!40000 ALTER TABLE `weashops` DISABLE KEYS */;
INSERT INTO `weashops` (`id`, `zone`, `item`, `price`) VALUES
	(1, 'GunShop', 'WEAPON_PISTOL', 300),
	(2, 'BlackWeashop', 'WEAPON_PISTOL', 500),
	(3, 'GunShop', 'WEAPON_FLASHLIGHT', 60),
	(4, 'BlackWeashop', 'WEAPON_FLASHLIGHT', 70),
	(5, 'GunShop', 'WEAPON_MACHETE', 90),
	(6, 'BlackWeashop', 'WEAPON_MACHETE', 110),
	(7, 'GunShop', 'WEAPON_NIGHTSTICK', 150),
	(8, 'BlackWeashop', 'WEAPON_NIGHTSTICK', 150),
	(9, 'GunShop', 'WEAPON_BAT', 100),
	(10, 'BlackWeashop', 'WEAPON_BAT', 100),
	(11, 'GunShop', 'WEAPON_STUNGUN', 50),
	(12, 'BlackWeashop', 'WEAPON_STUNGUN', 50),
	(13, 'GunShop', 'WEAPON_MICROSMG', 1400),
	(14, 'BlackWeashop', 'WEAPON_MICROSMG', 1700),
	(15, 'GunShop', 'WEAPON_PUMPSHOTGUN', 3400),
	(16, 'BlackWeashop', 'WEAPON_PUMPSHOTGUN', 3500),
	(17, 'GunShop', 'WEAPON_ASSAULTRIFLE', 10000),
	(18, 'BlackWeashop', 'WEAPON_ASSAULTRIFLE', 11000),
	(19, 'GunShop', 'WEAPON_SPECIALCARBINE', 15000),
	(20, 'BlackWeashop', 'WEAPON_SPECIALCARBINE', 16500),
	(21, 'GunShop', 'WEAPON_SNIPERRIFLE', 22000),
	(22, 'BlackWeashop', 'WEAPON_SNIPERRIFLE', 24000),
	(23, 'GunShop', 'WEAPON_FIREWORK', 18000),
	(24, 'BlackWeashop', 'WEAPON_FIREWORK', 20000),
	(25, 'GunShop', 'WEAPON_GRENADE', 500),
	(26, 'BlackWeashop', 'WEAPON_GRENADE', 650),
	(27, 'GunShop', 'WEAPON_BZGAS', 200),
	(28, 'BlackWeashop', 'WEAPON_BZGAS', 350),
	(29, 'GunShop', 'WEAPON_FIREEXTINGUISHER', 100),
	(30, 'BlackWeashop', 'WEAPON_FIREEXTINGUISHER', 100),
	(31, 'GunShop', 'WEAPON_BALL', 50),
	(32, 'BlackWeashop', 'WEAPON_BALL', 50),
	(33, 'GunShop', 'WEAPON_SMOKEGRENADE', 100),
	(34, 'BlackWeashop', 'WEAPON_SMOKEGRENADE', 100),
	(35, 'BlackWeashop', 'WEAPON_APPISTOL', 1100),
	(36, 'BlackWeashop', 'WEAPON_CARBINERIFLE', 12000),
	(37, 'BlackWeashop', 'WEAPON_HEAVYSNIPER', 30000),
	(38, 'BlackWeashop', 'WEAPON_MINIGUN', 45000),
	(39, 'BlackWeashop', 'WEAPON_RAILGUN', 50000),
	(40, 'BlackWeashop', 'WEAPON_STICKYBOMB', 500);
/*!40000 ALTER TABLE `weashops` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
